package com.millionspaces.mobile.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.millionspaces.mobile.R;

/**
 * Created by kasunka on 11/28/17.
 */

public class ThumbTextSeekBar extends RelativeLayout {
    public ThumbTextView tvThumb;
    public TextView startDay, endDay;
    public SeekBar seekBar, backgroundSeekbar;
    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener;
    private View.OnTouchListener touchListener;

    public ThumbTextSeekBar(Context context) {
        super(context);
        init();
    }

    public ThumbTextSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_thumb_text_seekbar, this);
        tvThumb = (ThumbTextView) findViewById(R.id.tvThumb);
        startDay = (TextView) findViewById(R.id.day_start);
        endDay = (TextView) findViewById(R.id.day_end);
        seekBar = (SeekBar) findViewById(R.id.day_progress);
        backgroundSeekbar = (SeekBar) findViewById(R.id.sbProgress);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (onSeekBarChangeListener != null)
                    onSeekBarChangeListener.onStopTrackingTouch(seekBar);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (onSeekBarChangeListener != null)
                    onSeekBarChangeListener.onStartTrackingTouch(seekBar);
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (onSeekBarChangeListener != null)
                    onSeekBarChangeListener.onProgressChanged(seekBar, progress, fromUser);
                    tvThumb.attachToSeekBar(seekBar);
            }
        });

        touchListener = new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        };

        backgroundSeekbar.setOnTouchListener(touchListener);
        seekBar.setOnTouchListener(touchListener);

    }

    public void setOnSeekBarChangeListener(SeekBar.OnSeekBarChangeListener l) {
        this.onSeekBarChangeListener = l;
    }

    public void setThumbText(String text) {
        tvThumb.setText(text);
    }

    public void setBackgroundProgress(int max, int refundDate, int progress, String start, String end){

        backgroundSeekbar.setMax(max);
        backgroundSeekbar.setProgress(refundDate);
        seekBar.setMax(max);
        seekBar.setProgress(progress);
        startDay.setText(start);
        endDay.setText(end);

        ValueAnimator anim = ValueAnimator.ofInt(0, progress);
        anim.setDuration(500);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int animProgress = (Integer) animation.getAnimatedValue();
                seekBar.setProgress(animProgress);
            }
        });
        anim.start();
    }

    public void setProgress(int progress) {
        if (progress == seekBar.getProgress() && progress == 0) {
            seekBar.setProgress(1);
            seekBar.setProgress(0);
        } else {
            seekBar.setProgress(progress);
        }
    }
}
