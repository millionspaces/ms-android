package com.millionspaces.mobile.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.adapters.BookingListAdapter;
import com.millionspaces.mobile.entities.response.Booking;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.millionspaces.mobile.utils.Config.GUEST_SPACE_BOOKINGS_PAST;
import static com.millionspaces.mobile.utils.Config.UPDATE_LIST_PAST;
import static com.millionspaces.mobile.utils.Config.UPDATE_LIST_UPCOMING;

/**
 * Created by kasunka on 10/10/17.
 */

public class BookingHostFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.booking_null_msg)
    TextView nullMsg;
    @BindView(R.id.host_booking_recyclerview)
    RecyclerView bookingRecyclerView;

    private Unbinder unbinder;
    private ArrayList<Booking> bookings;
    private BookingListAdapter mAdapter;
    private int type;
    private OnItemClickListener onItemClickListener;

    public static Fragment newInstance(ArrayList<Booking> value, int type){
        BookingHostFragment fragment = new BookingHostFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("VALUE",value);
        args.putInt("TYPE",type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof OnItemClickListener) {
                onItemClickListener = (OnItemClickListener)context;
            }
        }catch (ClassCastException e) {
            throw new ClassCastException();
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_booking_host, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        bookings = args.getParcelableArrayList("VALUE");
        type = args.getInt("TYPE");
        Log.d("Position", bookings.size()+" Fragment" +type);
        if (type == GUEST_SPACE_BOOKINGS_PAST){
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver, new IntentFilter(UPDATE_LIST_PAST));
        }else {
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver, new IntentFilter(UPDATE_LIST_UPCOMING));
        }
        if (bookings != null && !bookings.isEmpty()) {
            bookingRecyclerView.setVisibility(View.VISIBLE);
            bookingRecyclerView.setHasFixedSize(true);
            bookingRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mAdapter = new BookingListAdapter(getContext(),bookings,type);
            bookingRecyclerView.setAdapter(mAdapter);
            bookingRecyclerView.setNestedScrollingEnabled(true);
            setActionEvents();
        }else {
            nullMsg.setVisibility(View.VISIBLE);
        }
    }

    private void setActionEvents() {
        mAdapter.setOnItemClickListener(onItemClickListener);
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get data from intent and update
            Log.d("Position", intent.getIntExtra("POSITION", 0)+" Fragment");
            if (intent.getIntExtra("TYPE",-1) == 0) {
                Log.d("Reviewd",bookings.get(intent.getIntExtra("POSITION", 0)).isReviewed()+"");
                mAdapter.notifyItemChanged(intent.getIntExtra("POSITION", 0));
            }else {
                Log.d("Canceled",bookings.get(intent.getIntExtra("POSITION", 0)).getReservationStatus().getName()+"");
                mAdapter.notifyItemChanged(intent.getIntExtra("POSITION", 0));
            }
        }
    };
}
