package com.millionspaces.mobile.actions;

import java.util.HashMap;

/**
 * Created by kasunka on 9/15/17.
 */

public interface OnTimeSelected extends OnNoticePeriodSelected{
    void onTimeSelected(HashMap<String, String> blockedHoursMap, float charge, int position, boolean isTimeAdded, boolean isNewPosition);
}
