package com.millionspaces.mobile;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.millionspaces.mobile.entities.response.UserLoginResponse;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessLogin;
import com.millionspaces.mobile.network.interfaces.OnUpdateUser;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.PreferenceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_IMAGE;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_NAME;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_GET_USER;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_UPDATE_USER;

public class ProfileActivity extends BaseActivity implements OnSuccessLogin, View.OnClickListener, OnUpdateUser {
    @BindView(R.id.profile_parent)
    CoordinatorLayout mCoordinatorlayout;
    @BindView(R.id.user_pro_image)
    ImageView profileImage;
    @BindView(R.id.user_pro_image_gallery)
    ImageView profileImageGallery;
    @BindView(R.id.edt_name)
    EditText nameView;
    @BindView(R.id.edt_email)
    EditText emailView;
    @BindView(R.id.edt_mobile)
    EditText mobileView;
    @BindView(R.id.edt_company_name)
    EditText companyNameView;
    @BindView(R.id.edt_address)
    EditText addressView;
    @BindView(R.id.edt_job_title)
    EditText jobTitleView;
    @BindView(R.id.edt_company_phone)
    EditText companyPhoneView;
    @BindView(R.id.update)
    Button updateButton;

    private UserLoginResponse user;
    private String name;
    private String mobile;
    private String companyName;
    private String companyAddress;
    private String jobTitle;
    private String companyPhone;
    private Dialog dialog;

    private boolean isImageClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        init_views();
        getUser();
    }

    private void getUser() {
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).getLoggedUser(this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorlayout,this,ENF_GET_USER);
        }
    }

    private void init_views() {
        init_toolbar();
        setToolbarTitle(getString(R.string.profile_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Glide.with(this).load(PreferenceUtils.showPreferenceString(KEY_LOGGED_USER_IMAGE,this))
                .apply(RequestOptions.circleCropTransform()).into(profileImage);

        updateButton.setOnClickListener(this);
        profileImage.setOnClickListener(this);
    }

    @Override
    public void onConnectionErrorCallBack(int error) {
        switch (error){
            case ENF_GET_USER:
                getUser();
                break;
            case ENF_UPDATE_USER:
                attemptToUpdate();
                break;
        }
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
    }

    @Override
    public void onSuccessLogin(UserLoginResponse user) {
        hideProgress();
        this.user = user;
        nameView.setText(user.getName());
        emailView.setText(user.getEmail());
        addressView.setText(user.getAddress());
        mobileView.setText(user.getMobileNumber());
        companyNameView.setText(user.getCompanyName());
        companyPhoneView.setText(user.getCompanyPhone());
        jobTitleView.setText(user.getJobTitle());
        PreferenceUtils.savePreferenceString(KEY_LOGGED_USER_NAME,user.getName(),this);
        PreferenceUtils.savePreferenceString(KEY_LOGGED_USER_IMAGE,user.getImageUrl(),this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.update:
                attemptToUpdate();
                break;
            case R.id.dialog_button_positive:
                dialog.dismiss();
                finish();
                break;
            case R.id.dialog_button_negative:
                dialog.dismiss();
                break;
            case R.id.user_pro_image:
                if (!isImageClicked) {
                    profileImage.animate().scaleY((float) 0.8).scaleX((float) 0.8).start();
                }else {
                    profileImage.animate().scaleY((float) 1.0).scaleX((float) 1.0).start();
                }
                isImageClicked = !isImageClicked;
                break;
        }
    }

    private void attemptToUpdate() {

        //TODO: add validation here
        if (TextUtils.isEmpty(name)){

        }

        if (isAddedUpdate()){
            updateUser(user);
        }
    }

    private void getFiledUpdates() {
        name = nameView.getText().toString();
        mobile = mobileView.getText().toString();
        companyName = companyNameView.getText().toString();
        companyAddress = addressView.getText().toString();
        jobTitle = jobTitleView.getText().toString();
        companyPhone = companyPhoneView.getText().toString();
    }

    private boolean isAddedUpdate() {
        getFiledUpdates();
        boolean isUpdate = true;
            user.setName(name);
            user.setMobileNumber(mobile);
            user.setCompanyName(companyName);
            user.setAddress(companyAddress);
            user.setJobTitle(jobTitle);
            user.setCompanyPhone(companyPhone);
        return isUpdate;
    }

    private void updateUser(UserLoginResponse user) {
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            showProgress();
            MillionSpaceService.getInstance(this).updateUser(user,this);
        }else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorlayout,this,ENF_UPDATE_USER);
        }
    }

    @Override
    public void onUpdateUser(String response) {
        hideProgress();
        getUser();
        AlertMessageUtils.showSnackBarMessage(getString(R.string.success_user_update), mCoordinatorlayout);
    }

    @Override
    public void onBackPressed() {
        if (isAddedUpdate()){
            dialog = AlertMessageUtils.showExitWaringMsg(this,this);
        }else {
            super.onBackPressed();
        }
    }
}
