package com.millionspaces.mobile.network.interfaces;

import com.millionspaces.mobile.entities.response.Amenity;

import java.util.ArrayList;

/**
 * Created by kasunka on 7/19/17.
 */

public interface OnSuccessAminity extends OnErrorResponse {
    void successAmenity(ArrayList<Amenity> amenities);
}
