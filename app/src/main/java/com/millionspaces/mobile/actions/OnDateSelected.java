package com.millionspaces.mobile.actions;

/**
 * Created by kasunka on 8/30/17.
 */

public interface OnDateSelected {
    void onDateSelected(String day, String month, int year, boolean isSelected);
}
