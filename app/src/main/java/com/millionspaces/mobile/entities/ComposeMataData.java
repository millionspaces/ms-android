package com.millionspaces.mobile.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.millionspaces.mobile.entities.response.Amenity;
import com.millionspaces.mobile.entities.response.CancellationPolicy;
import com.millionspaces.mobile.entities.response.EventType;
import com.millionspaces.mobile.entities.response.Rule;
import com.millionspaces.mobile.entities.response.SeatingArrangement;

import java.util.ArrayList;

/**
 * Created by kasunka on 7/31/17.
 */

public class ComposeMataData implements Parcelable{
    private ArrayList<Amenity> aminities;
    private ArrayList<EventType> eventTypes;
    private ArrayList<CancellationPolicy> cancellationPolicies;
    private ArrayList<Rule> rules;
    private ArrayList<SeatingArrangement> seatingArrangements;
    private ArrayList<AmenityUnits> extraUnits;

    public ComposeMataData(ArrayList<Amenity> amenities, ArrayList<EventType> eventTypes, ArrayList<CancellationPolicy> cancellationPolicies, ArrayList<Rule> rules, ArrayList<SeatingArrangement> seatingArrangements, ArrayList<AmenityUnits> extraUnits) {
        this.aminities = amenities;
        this.eventTypes = eventTypes;
        this.cancellationPolicies = cancellationPolicies;
        this.rules = rules;
        this.seatingArrangements = seatingArrangements;
        this.extraUnits = extraUnits;
    }

    protected ComposeMataData(Parcel in) {
        aminities = in.createTypedArrayList(Amenity.CREATOR);
        eventTypes = in.createTypedArrayList(EventType.CREATOR);
        cancellationPolicies = in.createTypedArrayList(CancellationPolicy.CREATOR);
        rules = in.createTypedArrayList(Rule.CREATOR);
        seatingArrangements = in.createTypedArrayList(SeatingArrangement.CREATOR);
        extraUnits = in.createTypedArrayList(AmenityUnits.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(aminities);
        dest.writeTypedList(eventTypes);
        dest.writeTypedList(cancellationPolicies);
        dest.writeTypedList(rules);
        dest.writeTypedList(seatingArrangements);
        dest.writeTypedList(extraUnits);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ComposeMataData> CREATOR = new Creator<ComposeMataData>() {
        @Override
        public ComposeMataData createFromParcel(Parcel in) {
            return new ComposeMataData(in);
        }

        @Override
        public ComposeMataData[] newArray(int size) {
            return new ComposeMataData[size];
        }
    };

    public ArrayList<Amenity> getAmenities() {
        return aminities;
    }

    public ArrayList<EventType> getEventTypes() {
        return eventTypes;
    }

    public ArrayList<CancellationPolicy> getCancellationPolicies() {
        return cancellationPolicies;
    }

    public ArrayList<Rule> getRules() {
        return rules;
    }

    public ArrayList<SeatingArrangement> getSeatingArrangements() {
        return seatingArrangements;
    }

    public ArrayList<AmenityUnits> getExtraUnits() {
        return extraUnits;
    }
}
