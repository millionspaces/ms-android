package com.millionspaces.mobile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.entities.response.EventType;
import com.millionspaces.mobile.entities.response.SeatingArrangement;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.FUNCTION_EVENT_TYPE;

/**
 * Created by Kasunka on 6/6/2017.
 * Adapter class for app bar drop downs
 */

public class DropDownAdapter extends RecyclerView.Adapter<DropDownAdapter.ViewHolder>{
    private ArrayList<EventType> eventTypes = new ArrayList<>();
    private ArrayList<SeatingArrangement> seatingArrangements = new ArrayList<>();
    private int adapterType;
    private Context mContext;
    private OnItemClickListener onItemClickListener;

    public DropDownAdapter(Context context, int type, ArrayList<EventType> nameList, ArrayList<SeatingArrangement> seatingArrangements) {
        this.mContext = context;
        this.eventTypes = nameList;
        this.adapterType = type;
        this.seatingArrangements = seatingArrangements;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_app_bar_dropdown,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (adapterType == FUNCTION_EVENT_TYPE) {
            EventType eventType = eventTypes.get(position);
            holder.item.setText(eventType.getName());
            Picasso.with(mContext).load(eventType.getIcon()).into(holder.icon);
        }else {
            SeatingArrangement seatingArrangement = seatingArrangements.get(position);
            holder.item.setText(seatingArrangement.getName());
            Picasso.with(mContext).load(seatingArrangement.getIcon()).into(holder.icon);
        }
    }

    @Override
    public int getItemCount() {
        return adapterType ==  FUNCTION_EVENT_TYPE?eventTypes.size():seatingArrangements.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.list_item_drop_down_text)
        TextView item;
        @BindView(R.id.list_item_drop_down_icon)
        ImageView icon;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.OnClick(getAdapterPosition(),v);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
