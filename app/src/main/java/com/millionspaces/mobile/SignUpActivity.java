package com.millionspaces.mobile;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.entities.request.UserSignUpRequest;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessSignUp;
import com.millionspaces.mobile.utils.AlertMessageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.millionspaces.mobile.utils.Config.isValidMail;
import static com.millionspaces.mobile.utils.Config.sha256;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_USER_SIGNUP;

public class SignUpActivity extends BaseActivity implements View.OnClickListener,OnSuccessSignUp, OnItemClickListener {

    @BindView(R.id.signup_parent)
    CoordinatorLayout mCoordinaterLayout;
    @BindView(R.id.signup)
    Button signup;
    @BindView(R.id.edt_name)
    EditText nameView;
    @BindView(R.id.edt_email)
    EditText emailView;
    @BindView(R.id.edt_psw)
    EditText passwordView;

    private String email,name, psw;
    private boolean isExistingMail = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        init_actions();
    }

    private void init_actions() {
        signup.setOnClickListener(this);
        emailView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    validateEmail(emailView.getText().toString());
                }
            }
        });
    }

    private void validateEmail(String s) {
        MillionSpaceService.getInstance(this).validateSignUpMail(s, this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signup:
                attemptSignUp();
                break;
        }
    }

    @OnClick(R.id.signup_login_container)
    void navigateToLogin(){
        navigateFinishActivity(LoginActivity.class);
    }

    private void attemptSignUp() {
        name = nameView.getText().toString();
        email = emailView.getText().toString();
        psw = passwordView.getText().toString();
        View focusedView = null;
        String error = null;
        boolean isValid = true;

        if (isExistingMail){
            focusedView = emailView;
            isValid = false;
            error = getString(R.string.error_existing_mail);
        }else if (name.isEmpty()){
            focusedView = nameView;
            isValid = false;
            error = getString(R.string.error_empty_name);
        }else if (email.isEmpty()){
            focusedView = emailView;
            isValid = false;
            error = getString(R.string.error_empty_email);
        }else if (!isValidMail(email)){
            focusedView = emailView;
            isValid = false;
            error = getString(R.string.error_wrong_email);
        }else if (psw.isEmpty()) {
            focusedView = passwordView;
            isValid = false;
            error = getString(R.string.error_empty_password);
        }else if (psw.length()<5){
            focusedView = passwordView;
            isValid = false;
            error = getString(R.string.error_short_password);
        }

        if (isValid) {
            try {
                psw = sha256(psw);
            } catch (Exception e) {
                e.printStackTrace();
            }
            signupRequest(name, email, psw);
        }else {
            focusedView.requestFocus();
            ((TextView)focusedView).setError(error);
        }
    }

    private void signupRequest(String name, String email, String psw) {
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            showProgress();
            MillionSpaceService.getInstance(this).signUp(new UserSignUpRequest(name,email,psw), this);
        }else {
            AlertMessageUtils.showSnackBarCallBack(getString(R.string.error_network_failure),getResources().getString(R.string.error_network_failure_action),
                    mCoordinaterLayout,this,ENF_USER_SIGNUP);
        }
    }

    @Override
    public void onSuccessSignup() {
        hideProgress();
        AlertMessageUtils.getInstance().ShowSignUpSuccessDialog(email,this,this);
    }

    @Override
    public void onValidateSignUpMail(Boolean isExist) {
        isExistingMail = isExist;
        if (isExist) {
            attemptSignUp();
        }
    }

    @Override
    public void OnClick(int position, View view) {
        navigateFinishActivity(LoginActivity.class);
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinaterLayout);
    }

    @Override
    public void onValidateSignUpMailError(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinaterLayout);
    }

    @Override
    public void onConnectionErrorCallBack(int requestCode) {
        switch (requestCode){
            case ENF_USER_SIGNUP:
                signupRequest(name, email, psw);
                break;
        }
    }
}
