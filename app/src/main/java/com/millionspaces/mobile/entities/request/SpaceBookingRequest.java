package com.millionspaces.mobile.entities.request;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by kasunka on 9/14/17.
 */

public class SpaceBookingRequest implements Parcelable{
    private int space;
    private int eventType;
    private int guestCount;
    private int seatingArrangement;
    private String promoCode;
    private ArrayList<SpaceBookingDateRequest> dates;
    private float bookingCharge;
    private ArrayList<SapceBookingExtraAmenityRequest> bookingWithExtraAmenityDtoSet;

    public SpaceBookingRequest(int space, int eventType, int guestCount, int seating) {
        this.space = space;
        this.eventType = eventType;
        this.guestCount = guestCount;
        this.seatingArrangement = seating;
    }

    protected SpaceBookingRequest(Parcel in) {
        space = in.readInt();
        eventType = in.readInt();
        guestCount = in.readInt();
        seatingArrangement = in.readInt();
        promoCode = in.readString();
        dates = in.createTypedArrayList(SpaceBookingDateRequest.CREATOR);
        bookingCharge = in.readFloat();
        bookingWithExtraAmenityDtoSet = in.createTypedArrayList(SapceBookingExtraAmenityRequest.CREATOR);
    }

    public static final Creator<SpaceBookingRequest> CREATOR = new Creator<SpaceBookingRequest>() {
        @Override
        public SpaceBookingRequest createFromParcel(Parcel in) {
            return new SpaceBookingRequest(in);
        }

        @Override
        public SpaceBookingRequest[] newArray(int size) {
            return new SpaceBookingRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(space);
        dest.writeInt(eventType);
        dest.writeInt(guestCount);
        dest.writeInt(seatingArrangement);
        dest.writeString(promoCode);
        dest.writeTypedList(dates);
        dest.writeFloat(bookingCharge);
        dest.writeTypedList(bookingWithExtraAmenityDtoSet);
    }

    public void setDates(ArrayList<SpaceBookingDateRequest> dates) {
        this.dates = dates;
    }

    public void setBookingCharge(float bookingCharge) {
        this.bookingCharge = bookingCharge;
    }

    public void setBookingWithExtraAmenityDtoSet(ArrayList<SapceBookingExtraAmenityRequest> bookingWithExtraAmenityDtoSet) {
        this.bookingWithExtraAmenityDtoSet = bookingWithExtraAmenityDtoSet;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public int getGuestCount() {
        return guestCount;
    }
}
