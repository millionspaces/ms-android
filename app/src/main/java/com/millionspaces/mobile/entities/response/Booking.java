package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.millionspaces.mobile.entities.BookingDateTime;
import com.millionspaces.mobile.entities.BookingSpace;
import com.millionspaces.mobile.entities.Menu;
import com.millionspaces.mobile.entities.ReservationStatus;
import com.millionspaces.mobile.entities.request.SpaceReviewRequest;

import java.util.ArrayList;

/**
 * Created by kasunka on 10/10/17.
 */

public class Booking implements Parcelable{
    private int id;
    private String orderId;
    private UserLoginResponse user;
    private String eventType;
    private int guestCount;
    private String bookedDate;
    private float refund;
    private ArrayList<Menu> menu;
    private ReservationStatus reservationStatus;
    private ArrayList<BookingDateTime> dates;
    private String bookingCharge;
    private boolean isManual;
    private boolean isBlock;
    private ArrayList<Amenity> extraAmenityList;
    private BookingSpace space;
    private boolean isReviewed;
    private SpaceReviewRequest review;
    private EventType eventTypedetail;
    private SeatingArrangement seatingArrangement;
    private String remarks;


    protected Booking(Parcel in) {
        id = in.readInt();
        orderId = in.readString();
        user = in.readParcelable(UserLoginResponse.class.getClassLoader());
        eventType = in.readString();
        guestCount = in.readInt();
        bookedDate = in.readString();
        refund = in.readFloat();
        menu = in.createTypedArrayList(Menu.CREATOR);
        reservationStatus = in.readParcelable(ReservationStatus.class.getClassLoader());
        dates = in.createTypedArrayList(BookingDateTime.CREATOR);
        bookingCharge = in.readString();
        isManual = in.readByte() != 0;
        isBlock = in.readByte() != 0;
        extraAmenityList = in.createTypedArrayList(Amenity.CREATOR);
        space = in.readParcelable(BookingSpace.class.getClassLoader());
        isReviewed = in.readByte() != 0;
        review = in.readParcelable(SpaceReviewRequest.class.getClassLoader());
        eventTypedetail = in.readParcelable(EventType.class.getClassLoader());
        seatingArrangement = in.readParcelable(SeatingArrangement.class.getClassLoader());
        remarks = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(orderId);
        dest.writeParcelable(user, flags);
        dest.writeString(eventType);
        dest.writeInt(guestCount);
        dest.writeString(bookedDate);
        dest.writeFloat(refund);
        dest.writeTypedList(menu);
        dest.writeParcelable(reservationStatus, flags);
        dest.writeTypedList(dates);
        dest.writeString(bookingCharge);
        dest.writeByte((byte) (isManual ? 1 : 0));
        dest.writeByte((byte) (isBlock ? 1 : 0));
        dest.writeTypedList(extraAmenityList);
        dest.writeParcelable(space, flags);
        dest.writeByte((byte) (isReviewed ? 1 : 0));
        dest.writeParcelable(review,flags);
        dest.writeParcelable(eventTypedetail, flags);
        dest.writeParcelable(seatingArrangement, flags);
        dest.writeString(remarks);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Booking> CREATOR = new Creator<Booking>() {
        @Override
        public Booking createFromParcel(Parcel in) {
            return new Booking(in);
        }

        @Override
        public Booking[] newArray(int size) {
            return new Booking[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getOrderId() {
        return orderId;
    }

    public UserLoginResponse getUser() {
        return user;
    }

    public String getEventType() {
        return eventType;
    }

    public int getGuestCount() {
        return guestCount;
    }

    public String getBookedDate() {
        return bookedDate;
    }

    public float getRefund() {
        return refund;
    }

    public ArrayList<Menu> getMenu() {
        return menu;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public ArrayList<BookingDateTime> getDates() {
        return dates;
    }

    public String getBookingCharge() {
        return bookingCharge;
    }

    public boolean isManual() {
        return isManual;
    }

    public boolean isBlock() {
        return isBlock;
    }

    public ArrayList<Amenity> getExtraAmenityList() {
        return extraAmenityList;
    }

    public BookingSpace getSpace() {
        return space;
    }

    public boolean isReviewed() {
        return isReviewed;
    }

    public void setReviewed(boolean reviewed) {
        isReviewed = reviewed;
    }

    public SpaceReviewRequest getReview() {
        return review;
    }

    public EventType getEventTypedetail() {
        return eventTypedetail;
    }

    public SeatingArrangement getSeatingArrangement() {
        return seatingArrangement;
    }

    public String getRemarks() {
        return remarks;
    }
}
