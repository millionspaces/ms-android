package com.millionspaces.mobile.utils;

/**
 * Created by kasunka on 12/20/17.
 */

public class ConnectionCallBackConfig {
    public static final int ENF_ADVANCE_SEARCH_SPACES = 1;
    public static final int ENF_GET_SPACE_DETAILS = 2;
    public static final int ENF_GET_GUEST_CALENDAR_DETAILS = 3;
    public static final int ENF_GET_GUEST_RESERVE_BOOKING = 4;
    public static final int ENF_PUT_GUEST_MOBILE_NUMBER = 41;
    public static final int ENF_GET_GUEST_PROMO_CODE = 23;
    public static final int ENF_INITIAL_SYNC = 5;
    public static final int ENF_NON_INITIAL_SYNC = 51;
    public static final int ENF_USER_SIGNUP = 6;
    public static final int ENF_USER_SIGNUP_MAIL_VALIDATION = 7;
    public static final int ENF_GET_USER = 8;
    public static final int ENF_UPDATE_USER = 9;
    public static final int ENF_HOSTED_SPACES = 10;
    public static final int ENF_TENTATIVE_RESERVATION = 11;
    public static final int ENF_VALIDATE_SIGNUP_MAIL = 12;
    public static final int ENF_LOGIN_REQUEST = 13;
    public static final int ENF_SPACE_BOOKING_LIST = 14;
    public static final int ENF_GUEST_BOOKING_LIST = 15;
    public static final int ENF_GUEST_BOOKING_CANCEL = 16;
    public static final int ENF_GUEST_BOOKING = 17;
    public static final int ENF_GUEST_UPDATE_REMARKS = 18;
    public static final int ENF_HOST_BLOCK_SPACE = 19;
    public static final int ENF_HOST_GET_BOOKING = 20;
    public static final int ENF_HOST_UPDATE_BOOKING = 21;
    public static final int ENF_HOST_DELETE_BOOKING = 22;
}
