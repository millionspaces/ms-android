package com.millionspaces.mobile.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.millionspaces.mobile.BuildConfig;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.entities.response.Space;
import com.millionspaces.mobile.utils.Config;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.APP_CURRENCY_NODECIMAL_FORMAT;
import static com.millionspaces.mobile.utils.Config.APP_CURRENCY_TYPE_PRIFIX;
import static com.millionspaces.mobile.utils.Config.IMAGE_PLACEHOLDER_FACTOR;
import static com.millionspaces.mobile.utils.Config.IMAGE_PLACEHOLDER_FACTOR_WINDOW;

/**
 * Created by kasunka on 7/24/17.
 */

public class SpacesListAdapter extends RecyclerView.Adapter<SpacesListAdapter.SpaceViewHolder>{

    ArrayList<Space> list;
    private Context mContext;
    private static OnItemClickListener onItemClickListener;

    public SpacesListAdapter(ArrayList<Space> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public SpaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_space_card, parent, false);
        return new SpaceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SpaceViewHolder holder, final int position) {
        final Space space = list.get(position);
        RequestBuilder<Drawable> thumbnailRequest = Glide.with(mContext)
                .load(BuildConfig.IMAGE_SERVER_URL+IMAGE_PLACEHOLDER_FACTOR+BuildConfig.IMAGE_SERVER_DOMAIN+space.getThumbnailImage());

        Glide.with(mContext)
                .load(BuildConfig.IMAGE_SERVER_URL+ Config.optimizedImage(mContext) +BuildConfig.IMAGE_SERVER_DOMAIN+space.getThumbnailImage()) // Live URL
                .thumbnail(thumbnailRequest)
                .into(holder.image);

        Log.d("KK",BuildConfig.IMAGE_SERVER_URL+ Config.optimizedImage(mContext) +BuildConfig.IMAGE_SERVER_DOMAIN+space.getThumbnailImage());
        holder.nosPax.setText(String.valueOf(space.getParticipantCount()));
        holder.amount.setText(APP_CURRENCY_TYPE_PRIFIX +new DecimalFormat(APP_CURRENCY_NODECIMAL_FORMAT).format(space.getRatePerHour()));
        holder.name.setText(space.getName());
        holder.description.setText(space.getAddressLine2());
//        ViewCompat.setTransitionName(holder.image, "ABC"); // Enable for Activity Transitions
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SpaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.space_card_img) ImageView image;
        @BindView(R.id.space_card_name) TextView name;
        @BindView(R.id.space_card_desc) TextView description;
        @BindView(R.id.space_card_people_count) TextView nosPax;
        @BindView(R.id.space_card_amount) TextView amount;

        public SpaceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.OnClick(getAdapterPosition(),image);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
