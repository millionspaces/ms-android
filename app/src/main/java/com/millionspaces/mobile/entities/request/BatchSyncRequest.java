package com.millionspaces.mobile.entities.request;

/**
 * Created by kasunka on 8/8/17.
 */

public class BatchSyncRequest {
    private String table;
    private String  timeStamp;

    public BatchSyncRequest(String name, String timeStamp) {
        this.table = name;
        this.timeStamp = timeStamp;
    }
}
