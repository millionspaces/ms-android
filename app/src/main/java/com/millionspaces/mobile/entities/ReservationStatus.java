package com.millionspaces.mobile.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 10/10/17.
 */

public class ReservationStatus implements Parcelable{
    private int id;
    private String name;
    private String label;


    protected ReservationStatus(Parcel in) {
        id = in.readInt();
        name = in.readString();
        label = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(label);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReservationStatus> CREATOR = new Creator<ReservationStatus>() {
        @Override
        public ReservationStatus createFromParcel(Parcel in) {
            return new ReservationStatus(in);
        }

        @Override
        public ReservationStatus[] newArray(int size) {
            return new ReservationStatus[size];
        }
    };

    public ReservationStatus(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
