package com.millionspaces.mobile.network.interfaces;

import com.millionspaces.mobile.entities.response.PromoCodeResponse;

/**
 * Created by kasunka on 1/10/18.
 */

public interface OnSuccessPromoCode extends OnErrorResponse{
    void onSuccessPromoCode(PromoCodeResponse promoCodeResponse);
}
