package com.millionspaces.mobile.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.millionspaces.mobile.BuildConfig;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.entities.response.Space;
import com.millionspaces.mobile.utils.Config;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.APP_CURRENCY_NODECIMAL_FORMAT;
import static com.millionspaces.mobile.utils.Config.APP_CURRENCY_UNITS;
import static com.millionspaces.mobile.utils.Config.IMAGE_PLACEHOLDER_FACTOR;

/**
 * Created by kasunka on 7/25/17.
 */

public class SimilarSpaceAdapter extends RecyclerView.Adapter<SimilarSpaceAdapter.ViewHolder>{
    private Context mContext;
    private ArrayList<Space> spaces;

    private static OnItemClickListener onItemClickListener;

    public SimilarSpaceAdapter(ArrayList<Space> similarSpaces, Context context) {
        this.mContext = context;
        this.spaces = similarSpaces;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_space_similarspace_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Space space = spaces.get(position);
        RequestBuilder<Drawable> thumbnailRequest = Glide.with(mContext)
                .load(BuildConfig.IMAGE_SERVER_URL+IMAGE_PLACEHOLDER_FACTOR+BuildConfig.IMAGE_SERVER_DOMAIN+space.getThumbnailImage());
        Glide.with(mContext)
                .load(BuildConfig.IMAGE_SERVER_URL+Config.optimizedImage(mContext)+BuildConfig.IMAGE_SERVER_DOMAIN+space.getThumbnailImage())
                .thumbnail(thumbnailRequest)
                .into(holder.image);
        holder.name.setText(space.getName().replaceAll("[\\t\\n\\r]+"," "));
        holder.address.setText(space.getAddressLine2());
        holder.address.setVisibility(space.getAddressLine2() != null?View.VISIBLE:View.GONE);
        holder.rate.setText(APP_CURRENCY_UNITS +new DecimalFormat(APP_CURRENCY_NODECIMAL_FORMAT).format(space.getRatePerHour()));
        ViewCompat.setTransitionName(holder.image, space.getImages().get(0));
    }

    @Override
    public int getItemCount() {
        return spaces.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.similar_card_image)
        ImageView image;
        @BindView(R.id.similar_card_spacename)
        TextView name;
        @BindView(R.id.similar_card_spacerate)
        TextView rate;
        @BindView(R.id.similar_card_spaceaddress)
        TextView address;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.OnClick(getAdapterPosition(),image);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
