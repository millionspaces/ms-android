package com.millionspaces.mobile.actions;

public interface OnSuccessGuestMailEntered {
    void onSuccessMail(String mail);
}
