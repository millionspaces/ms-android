package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 7/31/17.
 */

public class CancellationPolicy implements Parcelable{
    private int id;
    private String name;
    private String description;
    private float refundRate;
    private String timeStamp;


    protected CancellationPolicy(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        refundRate = in.readFloat();
        timeStamp = in.readString();
    }



    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeFloat(refundRate);
        dest.writeString(timeStamp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CancellationPolicy> CREATOR = new Creator<CancellationPolicy>() {
        @Override
        public CancellationPolicy createFromParcel(Parcel in) {
            return new CancellationPolicy(in);
        }

        @Override
        public CancellationPolicy[] newArray(int size) {
            return new CancellationPolicy[size];
        }
    };

    public CancellationPolicy(int id, String name, String description, float refundRate, String timeStamp) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.refundRate = refundRate;
        this.timeStamp = timeStamp;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public float getRefundRate() {
        return refundRate;
    }

    public String getTimeStamp() {
        return timeStamp;
    }
}
