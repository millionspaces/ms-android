package com.millionspaces.mobile.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.millionspaces.mobile.BuildConfig;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.entities.response.Space;
import com.millionspaces.mobile.utils.Config;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.IMAGE_PLACEHOLDER_FACTOR;

/**
 * Created by kasun on 2/6/17.
 */

public class SpaceViewAdapter extends RecyclerView.Adapter<SpaceViewAdapter.SpaceViewHolder> {

    ArrayList<Space> list = new ArrayList<>();
    private Context mContext;
    private static OnItemClickListener onItemClickListener;

    public SpaceViewAdapter(ArrayList<Space> data, Context context){
        this.list = data;
        this.mContext = context;
    }

    @Override
    public SpaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_space_card_host, parent, false);
        return new SpaceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SpaceViewHolder holder, int position) {
        Space space = list.get(position);

        RequestBuilder<Drawable> thumbnailRequest = Glide.with(mContext)
                .load(BuildConfig.IMAGE_SERVER_URL+IMAGE_PLACEHOLDER_FACTOR+BuildConfig.IMAGE_SERVER_DOMAIN+space.getThumbnailImage());
        Glide.with(mContext)
                .load(BuildConfig.IMAGE_SERVER_URL+Config.optimizedImage(mContext)+BuildConfig.IMAGE_SERVER_DOMAIN+space.getThumbnailImage()) // Live URL
                .thumbnail(thumbnailRequest)
                .into(holder.image);
        holder.name.setText(space.getName());
        holder.description.setText(space.getAddressLine2() != null ? space.getAddressLine2() + ", " + space.getAddressLine1() : space.getAddressLine1());
        ViewCompat.setTransitionName(holder.image, space.getImages().get(0));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SpaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.myspace_card_img) ImageView image;
        @BindView(R.id.myspace_card_name) TextView name;
        @BindView(R.id.myspace_card_desc) TextView description;
        @BindView(R.id.button_availability) LinearLayout availability;
        @BindView(R.id.button_booking) LinearLayout booking;
        @BindView(R.id.button_edit) LinearLayout edit;

        public SpaceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
            availability.setOnClickListener(this);
            booking.setOnClickListener(this);
            edit.setOnClickListener(this);
            image.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.OnClick(getAdapterPosition(),v);
        }
    }

    public void setOnItemClickListner(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
