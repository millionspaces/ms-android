package com.millionspaces.mobile.entities.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.millionspaces.mobile.entities.ReservationStatus;

/**
 * Created by kasunka on 11/14/17.
 */

public class ManualBookingRequest implements Parcelable{
    private int id;
    private int space;
    private String fromDate;
    private String toDate;
    private String dateBookingMade;
    private String cost;
    private String note;
    private String guestContactNumber;
    private String guestEmail;
    private String guestName;
    private String noOfGuests;
    private String eventTitle;
    private String eventTypeId;
    private String extrasRequested;
    private String seatingArrangementId;
    private ReservationStatus reservationStatus;

    public ManualBookingRequest() {

    }

    protected ManualBookingRequest(Parcel in) {
        id = in.readInt();
        space = in.readInt();
        fromDate = in.readString();
        toDate = in.readString();
        dateBookingMade = in.readString();
        cost = in.readString();
        note = in.readString();
        guestContactNumber = in.readString();
        guestEmail = in.readString();
        guestName = in.readString();
        noOfGuests = in.readString();
        eventTitle = in.readString();
        eventTypeId = in.readString();
        extrasRequested = in.readString();
        seatingArrangementId = in.readString();
        reservationStatus = in.readParcelable(ReservationStatus.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(space);
        dest.writeString(fromDate);
        dest.writeString(toDate);
        dest.writeString(dateBookingMade);
        dest.writeString(cost);
        dest.writeString(note);
        dest.writeString(guestContactNumber);
        dest.writeString(guestEmail);
        dest.writeString(guestName);
        dest.writeString(noOfGuests);
        dest.writeString(eventTitle);
        dest.writeString(eventTypeId);
        dest.writeString(extrasRequested);
        dest.writeString(seatingArrangementId);
        dest.writeParcelable(reservationStatus, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ManualBookingRequest> CREATOR = new Creator<ManualBookingRequest>() {
        @Override
        public ManualBookingRequest createFromParcel(Parcel in) {
            return new ManualBookingRequest(in);
        }

        @Override
        public ManualBookingRequest[] newArray(int size) {
            return new ManualBookingRequest[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getDateBookingMade() {
        return dateBookingMade;
    }

    public void setDateBookingMade(String dateBookingMade) {
        this.dateBookingMade = dateBookingMade;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getGuestContactNumber() {
        return guestContactNumber;
    }

    public void setGuestContactNumber(String guestContactNumber) {
        this.guestContactNumber = guestContactNumber;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(String guestEmail) {
        this.guestEmail = guestEmail;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getNoOfGuests() {
        return noOfGuests;
    }

    public void setNoOfGuests(String noOfGuests) {
        this.noOfGuests = noOfGuests;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(String eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public String getExtrasRequested() {
        return extrasRequested;
    }

    public void setExtrasRequested(String extrasRequested) {
        this.extrasRequested = extrasRequested;
    }

    public String getSeatingArrangementId() {
        return seatingArrangementId;
    }

    public void setSeatingArrangementId(String seatingArrangementId) {
        this.seatingArrangementId = seatingArrangementId;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
    }
}
