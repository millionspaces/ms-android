package com.millionspaces.mobile;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.millionspaces.mobile.entities.request.SpaceBookingDateRequest;
import com.millionspaces.mobile.entities.response.BookingReserveResponse;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.PaymentCountDownTimer;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.COUNTER_TIME;
import static com.millionspaces.mobile.utils.Config.get12HoursValue;

public class PaymentWebActivity extends BaseActivity implements PaymentCountDownTimer.CounterTimerListener, View.OnClickListener {

    @BindView(R.id.payment_webview)
    WebView webView;
//    @BindView(R.id.timer_view)
//    TextView timerView;
    @BindView(R.id.payment_back)
    Button paymentBack;


    private BookingReserveResponse reserveObject;
//    private PaymentCountDownTimer timer;
//    private final Handler mHandler = new Handler();
    private Dialog exitDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_web);
        ButterKnife.bind(this);
        init_view();
    }

    private void init_view() {
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.payment_title);

        reserveObject = getIntent().getParcelableExtra(BUNDLE_STRING_PARAM);

//        timer = new PaymentCountDownTimer(this);
//        timer.setTimerDuration(COUNTER_TIME,0,PaymentCountDownTimer.TICK_IN_SECONDS);
//        timer.start();

        webViewWithPost();
    }

    private void webViewWithPost() {

        WebChromeClient chromeClient = new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);

            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                return super.onJsConfirm(view, url, message, result);
            }


        };

        WebViewClient webViewClient = new WebViewClient(){

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.d("Web Start",url);
//                showProgress();

//                if (url.contains(BuildConfig.PAYMENT_BASE_URL)) {
//                    if (progressDialog !=null && progressDialog.isShowing()){
//                        progressDialog.dismiss();
//                    }
//                    String[] params = url.split(Pattern.quote("?"))[1].split("&");
//                    Map<String, String> map = new HashMap<>();
//                    for (String param : params) {
//                        String name = param.split("=")[0];
//                        String value = param.split("=")[1];
//                        map.put(name, value);
//                        Log.d("PAYMENT" + "Param", name + " : " + value);
//                    }
//                }
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                super.onPageFinished(view, url);
//                if (progressDialog != null && progressDialog.isShowing()){
//                    progressDialog.dismiss();
//                }
                Log.d("Web end",url);
//                hideProgress();

//                if (url.contains("https://qpayment.millionspaces.com/api/payment/process")){
//                if (url.contains("https://payment.millionspaces.com/api/payment/process")){
                    paymentBack.setVisibility(View.VISIBLE);
                    paymentBack.setOnClickListener(PaymentWebActivity.this);
//                }
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                Log.d("Web Load",url);
            }


        };

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);

        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setSaveFormData(true);

        webView.setWebChromeClient(chromeClient);
        webView.setWebViewClient(webViewClient);

        String urlParameters = "bookingId="+reserveObject.getId()+
                "&payLaterEnabled=" + reserveObject.isPayLaterEnabled() +
                "&manualPaymentAllowed=" + reserveObject.isUserEligibleForManualPayment() +
                "&referenceId=" + reserveObject.getReferenceId() +
                "&spaceName=" + reserveObject.getSpaceName() +
                "&organization=" + reserveObject.getOrganization() +
                "&address=" + reserveObject.getAddress() +
                "&reservationDate=" + reserveObject.getReservationDate() +
                "&reservationTime=" + getReservationTime(reserveObject.getReservationTime()) +
                "&total=" + reserveObject.getAmount() ;

        try {
            webView.postUrl(BuildConfig.PAYMENT_BASE_URL, urlParameters.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onTick(final String time, long remainingTimeInMilis) {
//        mHandler.post(new Runnable() {
//            @Override
//            public void run() {
//                timerView.setText(time);
//            }
//        });
    }

    @Override
    public void onFinish() {
//        timer.cancel();
        hideProgress();
        finish();
        navigateActivity(HomeActivity.class);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.payment_back){
            finishAffinity();
            navigateActivity(HomeActivity.class);
        }else {
            if (exitDialog != null && exitDialog.isShowing()) {
                exitDialog.dismiss();
            }
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        if (webView.canGoBack()){
//            webView.goBack();
//        }
    }

    private DateTime getTime(String dateTime){
        return DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm'Z'").parseDateTime(dateTime);
    }

    private String getReservationTime(ArrayList<SpaceBookingDateRequest> reservationTime) {
        StringBuilder stringBuilder = new StringBuilder();
        for (SpaceBookingDateRequest times : reserveObject.getReservationTime()) {
            stringBuilder.append(getTime(times.getFromDate()).toString("hh:mm a") +" - "+getTime(times.getToDate()).toString("hh:mm a")+",\n");
        }
       return stringBuilder.toString();
    }
}
