package com.millionspaces.mobile.network;

import com.millionspaces.mobile.BuildConfig;

/**
 * Created by kasunka on 7/6/17.
 */

public class PaymentConfig {
    public static String PASSWORD = "j9vA4Z05";
    public static String MERCHANT_ID = "14481900";
    public static String ACQUIRED_ID = "415738";
    public static String CURRENCY = "LKR";
    public static String CURRENCY_CODE = "144";
    public static String SENTRY_PREFIX = "SENTRYORD";
    public static String PURCHASE_CURRENCY_EXPONENT = "2";
    public static String CAPTURE_FLAG = BuildConfig.CAPTURE_FLAG;
    public static String SIGNATURE_METHOD = "SHA1";
    public static String VERSION = "1.0.0";

    public static String HEADER_KEY_CONTENT_TYPE = "Content-Type";
    public static String HEADER_KEY_CONTENT_TYPE_VALUE = "application/x-www-form-urlencoded";
    public static String HEADER_KEY_CONTENT_ENCODING = "content-encoding";
    public static String HEADER_KEY_CONTENT_ENCODING_VALUE = "UTF-8";

}
