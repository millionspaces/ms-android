package com.millionspaces.mobile.entities.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 8/16/17.
 */

public class UserLoginRequest implements Parcelable {
    private String username;
    private String password;
    private String token;
    private int type;


    public UserLoginRequest(String token, int type) {
        this.token = token;
        this.type = type;
    }

    public UserLoginRequest(String username, String password, int type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    protected UserLoginRequest(Parcel in) {
        username = in.readString();
        password = in.readString();
        token = in.readString();
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(password);
        dest.writeString(token);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserLoginRequest> CREATOR = new Creator<UserLoginRequest>() {
        @Override
        public UserLoginRequest createFromParcel(Parcel in) {
            return new UserLoginRequest(in);
        }

        @Override
        public UserLoginRequest[] newArray(int size) {
            return new UserLoginRequest[size];
        }
    };

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
