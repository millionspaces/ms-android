package com.millionspaces.mobile.entities;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 7/19/17.
 */

public class AmenityUnits implements Parcelable {
    private int id;
    private String name;


    protected AmenityUnits(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AmenityUnits> CREATOR = new Creator<AmenityUnits>() {
        @Override
        public AmenityUnits createFromParcel(Parcel in) {
            return new AmenityUnits(in);
        }

        @Override
        public AmenityUnits[] newArray(int size) {
            return new AmenityUnits[size];
        }
    };

    public AmenityUnits(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
