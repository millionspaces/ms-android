package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 7/21/17.
 */

public class MeasurementUnit implements Parcelable{
    private int id;
    private String name;

    protected MeasurementUnit(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MeasurementUnit> CREATOR = new Creator<MeasurementUnit>() {
        @Override
        public MeasurementUnit createFromParcel(Parcel in) {
            return new MeasurementUnit(in);
        }

        @Override
        public MeasurementUnit[] newArray(int size) {
            return new MeasurementUnit[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
