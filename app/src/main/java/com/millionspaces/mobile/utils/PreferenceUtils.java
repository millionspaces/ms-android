package com.millionspaces.mobile.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;

/**
 * Created by Kasunka on 19/7/2016.
 */

public class PreferenceUtils {

    public static void savePreferenceString(String key, String value, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void savePreferenceStringByContext(String key, String value, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String showPreferenceString(String key, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        String savedPref = sharedPreferences.getString(key, "");
        return savedPref;
    }

    public static String showPreferenceStringByContext(String key, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Activity.MODE_PRIVATE);
        String savedPref = sharedPreferences.getString(key, "");
        return savedPref;
    }

    public static void clearPreferenceString (String key, Activity activity){
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().commit();
    }

    public static void savePreferenceBoolen (String key, Boolean value, Activity activity){
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean showPreferenceBoolean(String key, Boolean state, Activity activity){
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        boolean savedPref = sharedPreferences.getBoolean(key,state);
        return savedPref;
    }

    public static void saveStringSet(String key, HashSet<String> set, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(key, set);
        editor.commit();
    }

    public static HashSet<String> getStringSet(String key, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Activity.MODE_PRIVATE);
        HashSet<String> preferences = (HashSet<String>) sharedPreferences.getStringSet(key,null);
        return preferences;
    }
}
