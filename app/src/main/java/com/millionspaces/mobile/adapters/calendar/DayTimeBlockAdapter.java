package com.millionspaces.mobile.adapters.calendar;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnBlockSelected;
import com.millionspaces.mobile.entities.response.Availability;
import com.millionspaces.mobile.utils.AlertMessageUtils;

import org.joda.time.LocalTime;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.CalenderUtils.NOTICED_HOUR;
import static com.millionspaces.mobile.utils.CalenderUtils.SELECTED_HOUR;
import static com.millionspaces.mobile.utils.CalenderUtils.TOTALLY_BOOKED;
import static com.millionspaces.mobile.utils.Config.BLOCK_CHARGE_GUEST_BASE;

/**
 * Created by kasunka on 9/12/17.
 */

public class DayTimeBlockAdapter extends RecyclerView.Adapter<DayTimeBlockAdapter.ViewHolderBlocks>{

    private Context mContext;
    private ArrayList<Availability> dayAvailabilityPerBlock;
    private int blockChargeType, numberOfMinHeadCount, numberOfGuests;
    private ArrayList<Availability> selectedAvalabilityBlocks = new ArrayList<>();
    private SparseBooleanArray blockedBlockPositionMap;
    private OnBlockSelected onBlockSelected;
    private ColorStateList colorStateList;

    public DayTimeBlockAdapter(Context context, ArrayList<Availability> dayAvailabilityPerBlock, int blockChargeType, int numberOfMinHeadCount, int numberOfGuests) {
        this.mContext = context;
        this.dayAvailabilityPerBlock = dayAvailabilityPerBlock;
        this.blockChargeType = blockChargeType;
        this.numberOfMinHeadCount = numberOfMinHeadCount;
        this.numberOfGuests = numberOfGuests;
        blockedBlockPositionMap = new SparseBooleanArray();
        colorStateList = new ColorStateList(
                new int[][] {
                        new int[] { -android.R.attr.state_checked }, // unchecked
                        new int[] {  android.R.attr.state_checked }  // checked
                },
                new int[] {
                        ContextCompat.getColor(mContext, R.color.colorPrimaryTextLight),
                        ContextCompat.getColor(mContext,R.color.white),
                }
        );
    }

    @Override
    public ViewHolderBlocks onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_calendar_blocks, parent, false);
        return new ViewHolderBlocks(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolderBlocks holder, final int position) {
        final Availability availability = dayAvailabilityPerBlock.get(position);
        holder.selectionBox.setText(LocalTime.parse(availability.getFrom()).toString("hh:mm a")+" - " +
                ""+LocalTime.parse(availability.getTo()).toString("hh:mm a"));
        holder.blockprice.setText("LKR " +new DecimalFormat("####,###,###").format(
                blockChargeType != BLOCK_CHARGE_GUEST_BASE?availability.getCharge():availability.getCharge()*numberOfGuests));
        View.OnClickListener clickListener =  new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (blockedBlockPositionMap.get(position)){
                    blockedBlockPositionMap.delete(position);
                    availability.setAvailabilityStatus(null);
                    onBlockSelected.onBlockSelected(availability,blockChargeType != BLOCK_CHARGE_GUEST_BASE?availability.getCharge():availability.getCharge()*numberOfGuests,false);
                    selectedAvalabilityBlocks.remove(availability);
                }else {
                    if (availability.getAvailabilityStatus() != NOTICED_HOUR) {
                        blockedBlockPositionMap.put(position, true);
                        availability.setAvailabilityStatus(SELECTED_HOUR);
                        onBlockSelected.onBlockSelected(availability, blockChargeType != BLOCK_CHARGE_GUEST_BASE ? availability.getCharge() : availability.getCharge() * numberOfGuests, true);
                        selectedAvalabilityBlocks.add(availability);
                    }else {
                        onBlockSelected.onNoticePeriodSelected();
                    }
                }
                blockOverlappingBlocks();
                notifyItemChanged(position);
            }
        };

        holder.priceBlockContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertMessageUtils.showImageEnlargeDialog(availability.getMenuFiles().get(0).getUrl(),mContext);
            }
        });


        if (availability.getAvailabilityStatus() == TOTALLY_BOOKED ) {
            holder.selectionBox.setChecked(true);
            holder.selectionBox.setEnabled(false);
            holder.timeContainer.setBackgroundColor(ContextCompat.getColor(mContext, R.color.calenderDisableColor));
            holder.blockprice.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryTextLightest));
            holder.priceBlockContainer.setVisibility(View.GONE);
        }else if (availability.getTempAvailabilityStatus() == TOTALLY_BOOKED){
            holder.selectionBox.setChecked(true);
            holder.selectionBox.setEnabled(false);
            holder.selectionBox.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimaryTextLight));
            holder.timeContainer.setBackgroundColor(ContextCompat.getColor(mContext, R.color.calenderDisableColor));
            holder.blockprice.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryTextLight));
            holder.priceBlockContainer.setVisibility(View.GONE);
            holder.timeContainer.setOnClickListener(null);
            return;
        } else if (availability.getAvailabilityStatus() == SELECTED_HOUR){
            holder.timeContainer.setBackgroundColor(ContextCompat.getColor(mContext,R.color.calenderPrimaryBlue));
            blockedBlockPositionMap.put(position,true);
            holder.selectionBox.setChecked(true);
            holder.selectionBox.setTextColor(ContextCompat.getColor(mContext,R.color.white));
            holder.blockprice.setTextColor(ContextCompat.getColor(mContext,R.color.white));
            holder.priceBlockContainer.setVisibility(blockChargeType == BLOCK_CHARGE_GUEST_BASE?View.VISIBLE:View.GONE);
            holder.timeContainer.setOnClickListener(clickListener);
        }else {
            holder.timeContainer.setBackgroundColor(Color.TRANSPARENT);
            holder.selectionBox.setChecked(false);
            holder.selectionBox.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimaryTextLight));
            holder.blockprice.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimaryTextLight));
            holder.priceBlockContainer.setVisibility(View.GONE);
            holder.timeContainer.setOnClickListener(clickListener);
        }

        if (blockChargeType == BLOCK_CHARGE_GUEST_BASE){
            holder.numberOfApplicableguests.setText(mContext.getString(R.string.calendar_per_block_guest_number_of_head,numberOfMinHeadCount));
            holder.costPerHead.setText(mContext.getString(R.string.calendar_per_block_guest_price_per_head,"LKR " +new DecimalFormat("####,###,###").format(availability.getCharge())));
        }
    }

    private void blockOverlappingBlocks() {
        for (int a = 0; a<dayAvailabilityPerBlock.size(); a++){
            dayAvailabilityPerBlock.get(a).setTempAvailabilityStatus(null);
            notifyItemChanged(a);
        }

        for (int i = 0; i<selectedAvalabilityBlocks.size();i++){
            Availability selectedBlock = selectedAvalabilityBlocks.get(i);
            LocalTime selctedBlockStart = LocalTime.parse(selectedBlock.getFrom());
            LocalTime selctedBlockEnd = LocalTime.parse(selectedBlock.getTo());
            int selectedBlockId = selectedBlock.getId();

            for (int j = 0;j<dayAvailabilityPerBlock.size(); j++){
                Availability block = dayAvailabilityPerBlock.get(j);
                LocalTime blockStart = LocalTime.parse(block.getFrom());
                LocalTime blockEnd = LocalTime.parse(block.getTo());
                int blockId = block.getId();

                if (selectedBlockId != blockId ){
                    if (selctedBlockStart.isAfter(blockStart)) {
                        if (selctedBlockStart.isBefore(blockEnd)) {
                            block.setTempAvailabilityStatus(TOTALLY_BOOKED);
                        }
                    }else if (blockStart.isBefore(selctedBlockEnd)) {
                        block.setTempAvailabilityStatus(TOTALLY_BOOKED);
                    }
                }
                notifyItemChanged(j);
            }
        }

    }

    @Override
    public int getItemCount() {
        return dayAvailabilityPerBlock.size();
    }


    public class ViewHolderBlocks extends RecyclerView.ViewHolder {
        @BindView(R.id.calendar_block_checkBox)
        CheckBox selectionBox;
        @BindView(R.id.calender_block_price)
        TextView blockprice;
        @BindView(R.id.calendar_block_time_container)
        RelativeLayout timeContainer;
        @BindView(R.id.calender_block_guest_base_head)
        TextView numberOfApplicableguests;
        @BindView(R.id.calender_block_guest_base_cost_per_head)
        TextView costPerHead;
        @BindView(R.id.calendar_block_price_container)
        LinearLayout priceBlockContainer;

        public ViewHolderBlocks(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                selectionBox.setButtonTintList(colorStateList);
            }
        }
    }


    public void setOnBlockClickListner(OnBlockSelected onBlockClickListner){
        this.onBlockSelected = onBlockClickListner;
    }
}
