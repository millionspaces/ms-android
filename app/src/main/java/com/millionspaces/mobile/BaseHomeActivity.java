package com.millionspaces.mobile;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.auth.api.Auth;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSucessLogOut;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.PreferenceUtils;

import butterknife.BindView;

import static com.millionspaces.mobile.utils.Config.KEY_IS_LOGGED_IN;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_IMAGE;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_NAME;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_ROLE;

/**
 * Created by kasunka on 6/7/17.
 * base Activity class for Common navigation Drawer, Action bar and menu
 */

public abstract class BaseHomeActivity extends BaseActivity implements OnSucessLogOut {

    protected DrawerLayout drawer;
    protected ActionBarDrawerToggle toggle;
    protected NavigationView navigationView;

    @Nullable @BindView(R.id.nav_my_spaces) LinearLayout mySpaces;
    @Nullable @BindView(R.id.nav_list_my_space) LinearLayout listSpace;
    @Nullable @BindView(R.id.nav_my_activity) LinearLayout activity;
    @Nullable @BindView(R.id.nav_logout) LinearLayout login;

    @Nullable @BindView(R.id.nav_tile_login) TextView loginTile;
    @Nullable @BindView(R.id.nav_pro_name) TextView loggedUserName;
    @Nullable @BindView(R.id.nav_pro_version) TextView applicationVersion;
    @Nullable @BindView(R.id.nav_pro_image) ImageView loggedUserProfilePic;
    @Nullable @BindView(R.id.nav_profile_container) LinearLayout loggedUserContainer;

    private View.OnClickListener listener;

    public abstract Activity currentActivity();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    protected void init_drawer(Boolean isHomeEnable){
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                manageMenuTiles();
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (isHomeEnable){
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            toggle.setDrawerIndicatorEnabled(true);
            toggle.setToolbarNavigationClickListener(null);
        }else {
            toggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    protected void init_navigationview(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        manageMenuTiles();

        listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.nav_my_spaces:
                        navigateActivity(MySpaceActivity.class);
                        break;
                    case R.id.nav_list_my_space:
                        Toast.makeText(getApplicationContext(),getString(R.string.success_host_space),Toast.LENGTH_LONG).show();
                        break;
                    case R.id.nav_my_activity:
                        navigateActivity(BookingListActivity.class);
                        break;
                    case R.id.nav_profile_container:
                        if (PreferenceUtils.showPreferenceBoolean(KEY_IS_LOGGED_IN,false,currentActivity())) {
                            navigateActivity(ProfileActivity.class);
                        } else {
                            navigateActivity(LoginActivity.class); //TODO: Change this to Login activity onActivity Result
                        }
                        break;
                    case R.id.nav_logout:
                        if (PreferenceUtils.showPreferenceBoolean(KEY_IS_LOGGED_IN,false,currentActivity())) {
                            logout();
                            PreferenceUtils.savePreferenceBoolen(KEY_IS_LOGGED_IN,false,currentActivity());
                        }
                        manageMenuTiles();
                        break;
                }
                drawer.closeDrawer(GravityCompat.START);
            }
        };

        mySpaces.setOnClickListener(listener);
        listSpace.setOnClickListener(listener);
        activity.setOnClickListener(listener);
        login.setOnClickListener(listener);
        loggedUserContainer.setOnClickListener(listener);
    }

    private void logout() {
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            showProgress();
            MillionSpaceService.getInstance(this).logout(this);
        }else {
//            AlertMessageUtils.showSnackBarMessage(getString(R.string.error_network_failer), mCoordinatorlayout);
        }
    }

    //    Change log in menu tiles according to preference
//    Created 20/07/2017 By Kasunka
    private void manageMenuTiles() {
        if (PreferenceUtils.showPreferenceBoolean(KEY_IS_LOGGED_IN,false,this)){
            if (!PreferenceUtils.showPreferenceString(KEY_LOGGED_USER_ROLE,this).contains("GUEST")) {
                loggedUserName.setText(PreferenceUtils.showPreferenceString(KEY_LOGGED_USER_NAME, this));
                loginTile.setText(getString(R.string.menu_logout));
                Glide.with(this).load(PreferenceUtils.showPreferenceString(KEY_LOGGED_USER_IMAGE, this))
                        .apply(new RequestOptions().placeholder(R.drawable.default_profile).error(R.drawable.default_profile))
                        .apply(RequestOptions.circleCropTransform()).into(loggedUserProfilePic);
                mySpaces.setVisibility(View.VISIBLE);
                activity.setVisibility(View.VISIBLE);
                login.setVisibility(View.VISIBLE);
            } else {
                loggedUserName.setText("Guest");
                loginTile.setText(getString(R.string.menu_logout));
                loggedUserContainer.setOnClickListener(null);
            }
        }else {
            Glide.with(this).clear(loggedUserProfilePic);
            loggedUserName.setText(getString(R.string.menu_signup_signin));
            login.setVisibility(View.GONE);
            mySpaces.setVisibility(View.GONE);
            activity.setVisibility(View.GONE);
            loggedUserContainer.setOnClickListener(listener);
        }
        applicationVersion.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSuccessLogout() {
        hideProgress();
    }

    @Override
    public void onErrorResponse(String error) {

    }
}
