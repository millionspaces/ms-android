package com.millionspaces.mobile.entities.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 11/8/17.
 */

public class BookingDetailsRequest implements Parcelable{
    private int bookingId;
    private boolean isManual;

    public BookingDetailsRequest(int bookingId, boolean isManual) {
        this.bookingId = bookingId;
        this.isManual = isManual;
    }

    protected BookingDetailsRequest(Parcel in) {
        bookingId = in.readInt();
        isManual = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(bookingId);
        dest.writeByte((byte) (isManual ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingDetailsRequest> CREATOR = new Creator<BookingDetailsRequest>() {
        @Override
        public BookingDetailsRequest createFromParcel(Parcel in) {
            return new BookingDetailsRequest(in);
        }

        @Override
        public BookingDetailsRequest[] newArray(int size) {
            return new BookingDetailsRequest[size];
        }
    };
}
