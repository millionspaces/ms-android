package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 11/17/17.
 */

public class BookingRemarksResponse implements Parcelable{
    private boolean response;

    protected BookingRemarksResponse(Parcel in) {
        response = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (response ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingRemarksResponse> CREATOR = new Creator<BookingRemarksResponse>() {
        @Override
        public BookingRemarksResponse createFromParcel(Parcel in) {
            return new BookingRemarksResponse(in);
        }

        @Override
        public BookingRemarksResponse[] newArray(int size) {
            return new BookingRemarksResponse[size];
        }
    };

    public boolean isResponse() {
        return response;
    }

}
