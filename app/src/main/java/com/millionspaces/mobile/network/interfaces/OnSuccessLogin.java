package com.millionspaces.mobile.network.interfaces;


import com.millionspaces.mobile.entities.response.UserLoginResponse;

/**
 * Created by kasunka on 8/16/17.
 */

public interface OnSuccessLogin extends OnErrorResponse {
    void onSuccessLogin(UserLoginResponse user);
}
