package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.millionspaces.mobile.entities.request.SpaceBookingDateRequest;

import java.util.ArrayList;


/**
 * Created by kasunka on 9/19/17.
 */

public class BookingReserveResponse implements Parcelable{

    private String reservationDate;
    private int id;
    private String orderId;
    private String referenceId;
    private String spaceName;
    private String organization;
    private String address;
    private ArrayList<SpaceBookingDateRequest> reservationTime;
    private int bookingCharge;
    private String discount;
    private boolean isPayLaterEnabled;
    private boolean isUserEligibleForManualPayment;
//    private Promotion promotion
    private boolean advanceOnly;
    private boolean isBookingTimeEligibleForManualPayment;
    private String amount;
    private String error;


    protected BookingReserveResponse(Parcel in) {
        reservationDate = in.readString();
        id = in.readInt();
        orderId = in.readString();
        referenceId = in.readString();
        spaceName = in.readString();
        organization = in.readString();
        address = in.readString();
        reservationTime = in.createTypedArrayList(SpaceBookingDateRequest.CREATOR);
        bookingCharge = in.readInt();
        discount = in.readString();
        isPayLaterEnabled = in.readByte() != 0;
        isUserEligibleForManualPayment = in.readByte() != 0;
        advanceOnly = in.readByte() != 0;
        isBookingTimeEligibleForManualPayment = in.readByte() != 0;
        amount = in.readString();
        error = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(reservationDate);
        dest.writeInt(id);
        dest.writeString(orderId);
        dest.writeString(referenceId);
        dest.writeString(spaceName);
        dest.writeString(organization);
        dest.writeString(address);
        dest.writeTypedList(reservationTime);
        dest.writeInt(bookingCharge);
        dest.writeString(discount);
        dest.writeByte((byte) (isPayLaterEnabled ? 1 : 0));
        dest.writeByte((byte) (isUserEligibleForManualPayment ? 1 : 0));
        dest.writeByte((byte) (advanceOnly ? 1 : 0));
        dest.writeByte((byte) (isBookingTimeEligibleForManualPayment ? 1 : 0));
        dest.writeString(amount);
        dest.writeString(error);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingReserveResponse> CREATOR = new Creator<BookingReserveResponse>() {
        @Override
        public BookingReserveResponse createFromParcel(Parcel in) {
            return new BookingReserveResponse(in);
        }

        @Override
        public BookingReserveResponse[] newArray(int size) {
            return new BookingReserveResponse[size];
        }
    };

    public String getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(String reservationDate) {
        this.reservationDate = reservationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<SpaceBookingDateRequest> getReservationTime() {
        return reservationTime;
    }

    public void setReservationTime(ArrayList<SpaceBookingDateRequest> reservationTime) {
        this.reservationTime = reservationTime;
    }

    public int getBookingCharge() {
        return bookingCharge;
    }

    public void setBookingCharge(int bookingCharge) {
        this.bookingCharge = bookingCharge;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public boolean isPayLaterEnabled() {
        return isPayLaterEnabled;
    }

    public void setPayLaterEnabled(boolean payLaterEnabled) {
        isPayLaterEnabled = payLaterEnabled;
    }

    public boolean isUserEligibleForManualPayment() {
        return isUserEligibleForManualPayment;
    }

    public void setUserEligibleForManualPayment(boolean userEligibleForManualPayment) {
        isUserEligibleForManualPayment = userEligibleForManualPayment;
    }

    public boolean isAdvanceOnly() {
        return advanceOnly;
    }

    public void setAdvanceOnly(boolean advanceOnly) {
        this.advanceOnly = advanceOnly;
    }

    public boolean isBookingTimeEligibleForManualPayment() {
        return isBookingTimeEligibleForManualPayment;
    }

    public void setBookingTimeEligibleForManualPayment(boolean bookingTimeEligibleForManualPayment) {
        isBookingTimeEligibleForManualPayment = bookingTimeEligibleForManualPayment;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
