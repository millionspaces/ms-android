package com.millionspaces.mobile.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.millionspaces.mobile.BuildConfig;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.utils.Config;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.millionspaces.mobile.utils.Config.IMAGE_PLACEHOLDER_FACTOR;

/**
 * Created by kasunka on 2/16/18.
 */

public class GalleryImageViewFragment extends Fragment {

    private Unbinder unbinder;

    @BindView(R.id.img_pager_item)
    ImageView imageView;

    public GalleryImageViewFragment() {
    }

    public static GalleryImageViewFragment newInstance(Context mContext, String mImage, String transitionName) {
        GalleryImageViewFragment imageViewFragment = new GalleryImageViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("IMAGE_URL", mImage);
        bundle.putString("EXTRA_TRANSITION_NAME", transitionName);
        imageViewFragment.setArguments(bundle);
        return imageViewFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postponeEnterTransition();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gallery_pager_item_landscape, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setTransitionName(getArguments().getString("EXTRA_TRANSITION_NAME"));
        }

        RequestBuilder<Drawable> thumbnailRequest = Glide.with(this)
                .load(BuildConfig.IMAGE_SERVER_URL+IMAGE_PLACEHOLDER_FACTOR+ BuildConfig.IMAGE_SERVER_DOMAIN+getArguments().getString("IMAGE_URL"));
        Glide.with(this)
                .load(BuildConfig.IMAGE_SERVER_URL+Config.optimizedImage(getContext())+BuildConfig.IMAGE_SERVER_DOMAIN+getArguments().getString("IMAGE_URL")) // LIVE URL
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        startPostponedEnterTransition();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        startPostponedEnterTransition();
                        return false;
                    }
                })
                .thumbnail(thumbnailRequest)
                .into(imageView);
    }
}
