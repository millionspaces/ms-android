package com.millionspaces.mobile;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.millionspaces.mobile.actions.OnAddedReview;
import com.millionspaces.mobile.actions.OnCancelBooking;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.adapters.BookingPagerAdapter;
import com.millionspaces.mobile.entities.request.CancelBookingRequest;
import com.millionspaces.mobile.entities.request.SpaceReviewRequest;
import com.millionspaces.mobile.entities.response.Booking;
import com.millionspaces.mobile.entities.response.CancelBookingResponse;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessBookingList;
import com.millionspaces.mobile.network.interfaces.OnSuccessCancelBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessReview;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.views.BookingHostFragment;

import org.joda.time.DateTimeComparator;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.CalenderUtils.DATE_FORMAT;
import static com.millionspaces.mobile.utils.Config.BOOKING_STATUS_PENDING_PAYMENT_ID;
import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.CANCEL_GUEST_BOOKING;
import static com.millionspaces.mobile.utils.Config.ERROR_NETWORK_FAILURE;
import static com.millionspaces.mobile.utils.Config.GUEST_SPACE_BOOKINGS_PAST;
import static com.millionspaces.mobile.utils.Config.HOST_SPACE_BOOKINGS_PAST;
import static com.millionspaces.mobile.utils.Config.UPDATE_LIST_PAST;
import static com.millionspaces.mobile.utils.Config.UPDATE_LIST_UPCOMING;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_GUEST_BOOKING_CANCEL;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_GUEST_BOOKING_LIST;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_SPACE_BOOKING_LIST;

public class BookingListActivity extends BaseActivity implements View.OnClickListener, OnSuccessBookingList, TabLayout.OnTabSelectedListener, OnItemClickListener,OnAddedReview, OnSuccessReview ,OnCancelBooking, OnSuccessCancelBooking {
    @BindView(R.id.booking_list_parent)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager viewPager;

    private int spaceId;
    private ArrayList<Booking> upComingBooking = new ArrayList<>();
    private ArrayList<Booking> pastBooking = new ArrayList<>();
    private Booking selectedBooking = null;
    private CancelBookingRequest cancelRequest;
    private ArrayList<Fragment> fragList;
    private BookingPagerAdapter mPagerAdapter;
    private DateTimeComparator dateTimeComparator;
    private DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT);
    private int bookingOwnerTag = 0, selectedPosition = -1;

    private Dialog reviewDialog, cancelDialog, alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_list);
        ButterKnife.bind(this);
        init_views();
    }

    private void init_views() {
        init_toolbar();
        setToolbarTitle(getString(R.string.booking_list_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        getBookings();
        init_actions();
    }

    private void getBookings() {
        if (getIntent().hasExtra(BUNDLE_INTEGER_PARAM)) {
            spaceId = getIntent().getIntExtra(BUNDLE_INTEGER_PARAM, 0);
            getSpaceBookingList();
            bookingOwnerTag = HOST_SPACE_BOOKINGS_PAST;
        }else {
            getBookingList();
            bookingOwnerTag = GUEST_SPACE_BOOKINGS_PAST;
        }
    }

    private void init_actions() {

    }

    private void getSpaceBookingList() {
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            showProgress();
            MillionSpaceService.getInstance(this).getBookingList(spaceId,this);
        }else {
            AlertMessageUtils.showSnackBarMessage(getResources().getString(R.string.error_network_failure), mCoordinatorLayout);
        }
    }

    private void getBookingList() {
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            showProgress();
            MillionSpaceService.getInstance(this).getMyBookingList(this);
        }else {
            AlertMessageUtils.showSnackBarMessage(getResources().getString(R.string.error_network_failure), mCoordinatorLayout);
        }
    }

    @Override
    public void OnClick(int position, View view) {
        Log.d("Position", position+" Activity");
        selectedPosition = position;
        if (viewPager.getCurrentItem() == 0) {
            navigateBookingAction(view,pastBooking.get(position).getId(),pastBooking.get(position).isManual(),viewPager.getCurrentItem());
        }else {
            selectedBooking =  upComingBooking.get(position);
            navigateBookingAction(view,upComingBooking.get(position).getId(),upComingBooking.get(position).isManual(),viewPager.getCurrentItem());
        }
    }

    private void navigateBookingAction(View view, int id,boolean isManual ,int type){
        switch (view.getId()){
            case R.id.guest_booking_action:
                switch (type) {
                    case 0:
                        reviewDialog = AlertMessageUtils.showReviewScreen(this, id, this);
                        break;
                    default:
                        if (selectedBooking.getReservationStatus().getId() != BOOKING_STATUS_PENDING_PAYMENT_ID) {
                            cancelDialog = AlertMessageUtils.showCancelScreen(this, selectedBooking, BookingListActivity.this, this);
                        }else {
                            this.cancelRequest = new CancelBookingRequest(selectedBooking.getId(),CANCEL_GUEST_BOOKING,0);
                            alertDialog = AlertMessageUtils.showConfirmDialog(this,getString(R.string.cancel_booking_confirm_title),getString(R.string.cancel_booking_confirm_des),
                                    getString(R.string.cancel_booking_confirm_negative), getString(R.string.cancel_booking_confirm_positive),View.VISIBLE,this);
                        }
                        break;
                }
                break;
            default:
                Intent bookingPreview = new Intent(BookingListActivity.this,BookingPreviewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(BUNDLE_INTEGER_PARAM,id);
                bundle.putBoolean(BUNDLE_BOOLEAN_PARAM, isManual);
                bundle.putInt(BUNDLE_INTEGER_PARAM2,bookingOwnerTag+type);
                bookingPreview.putExtras(bundle);
                startActivityForResult(bookingPreview,bookingOwnerTag+type);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cancel_booking_card_img:
                Bundle bundle = new Bundle();
                bundle.putInt(BUNDLE_INTEGER_PARAM,selectedBooking.getSpace().getId());
                bundle.putString(BUNDLE_STRING_PARAM,"[1]");
                bundle.putBoolean(BUNDLE_BOOLEAN_PARAM,true);
                navigateActivityWithExtra(SpaceProfileActivity.class,bundle);
                break;
            case R.id.dialog_button_negative:
                alertDialog.dismiss();
                break;
            case R.id.dialog_button_positive:
                alertDialog.dismiss();
                cancelBooking();
                break;
        }
    }

    private void cancelBooking() {
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).cancelGuestBooking(cancelRequest, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ENF_GUEST_BOOKING_CANCEL);
        }
    }

    @Override
    public void onAddedReview(SpaceReviewRequest reviewRequest) {
        if (reviewDialog.isShowing()){
            reviewDialog.dismiss();
        }
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).setReview(reviewRequest, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ERROR_NETWORK_FAILURE);
        }
    }

    @Override
    public void onSuccessReview(String value) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(value, mCoordinatorLayout);
        pastBooking.get(selectedPosition).setReviewed(true);
        Intent intent = new Intent(UPDATE_LIST_PAST);
        intent.putExtra("TYPE", 0);
        intent.putExtra("POSITION", selectedPosition);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onCancelBooking(CancelBookingRequest request) {
        if (cancelDialog.isShowing()){
            cancelDialog.dismiss();
        }
        this.cancelRequest = request;
        alertDialog = AlertMessageUtils.showConfirmDialog(this,getString(R.string.cancel_booking_confirm_title),getString(R.string.cancel_booking_confirm_des),
                getString(R.string.cancel_booking_confirm_negative), getString(R.string.cancel_booking_confirm_positive),View.VISIBLE,this);
    }

    @Override
    public void onSuccessCancelBooking(CancelBookingResponse response) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(getString(R.string.success_cancel_booking), mCoordinatorLayout);
        upComingBooking.get(selectedPosition).getReservationStatus().setId(4);
        upComingBooking.get(selectedPosition).getReservationStatus().setName("CANCELLED");
        upComingBooking.get(selectedPosition).getReservationStatus().setLabel("Cancelled");
        Intent intent = new Intent(UPDATE_LIST_UPCOMING);
        intent.putExtra("TYPE", 1);
        intent.putExtra("POSITION", selectedPosition);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinatorLayout);
    }

    @Override
    public void onSuccessBookingList(ArrayList<Booking> value) {
        hideProgress();
        dateTimeComparator.getInstance();
        pastBooking.clear();
        upComingBooking.clear();
        LocalDateTime localDateTime = new LocalDateTime();
        ArrayList<Booking> inValidateBookings = new ArrayList<>();

        for (final Booking booking: value){
            //TODO: Remove if condition after proper backend validation
            if (booking.getDates().size() != 0 && booking.getBookedDate() != null){
//                Collections.sort(booking.getDates(), new Comparator<BookingDateTime>() {
//                    @Override
//                    public int compare(BookingDateTime o1, BookingDateTime o2) {
//                        Log.d("Date", formatter.parseDateTime(o1.getFromDate()).toString() + " " + formatter.parseDateTime(o2.getFromDate()).toString());
//                        if (o1.getFromDate() != null || o1.getFromDate().isEmpty()){
//                            booking.getDates().remove(o1);
//                        }
//                        return dateTimeComparator.compare(formatter.parseDateTime(o1.getFromDate())
//                                , formatter.parseDateTime(o2.getFromDate()));
//                    }
//                });


                if (localDateTime.isBefore(formatter.parseDateTime(booking.getDates().get(0).getToDate()).toLocalDateTime())){
                    upComingBooking.add(booking);
                }else {
                    pastBooking.add(booking);
                }
            }else {
                inValidateBookings.add(booking);
            }
        }
        value.removeAll(inValidateBookings);

//        sortDateLists(upComingBooking);
//        sortDateLists(pastBooking);

        fragList =  new ArrayList<>();
        fragList.add(BookingHostFragment.newInstance(pastBooking,bookingOwnerTag));
        fragList.add(BookingHostFragment.newInstance(upComingBooking,bookingOwnerTag+1));

        mPagerAdapter = new BookingPagerAdapter(getSupportFragmentManager(),this,fragList);
        viewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(mPagerAdapter.getTabView(i));
        }

        tabLayout.addOnTabSelectedListener(this);
        viewPager.setCurrentItem(1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("Result",requestCode+"");
        if (resultCode == RESULT_OK){
            if (requestCode == GUEST_SPACE_BOOKINGS_PAST){
                pastBooking.get(selectedPosition).setReviewed(true);
                Intent intent = new Intent(UPDATE_LIST_PAST);
                intent.putExtra("TYPE", 0);
                intent.putExtra("POSITION", selectedPosition);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            }else {
                upComingBooking.get(selectedPosition).getReservationStatus().setId(4);
                upComingBooking.get(selectedPosition).getReservationStatus().setName("CANCELLED");
                upComingBooking.get(selectedPosition).getReservationStatus().setLabel("Cancelled");
                Intent intent = new Intent(UPDATE_LIST_UPCOMING);
                intent.putExtra("TYPE", 1);
                intent.putExtra("POSITION", selectedPosition);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            }
        }
    }

    @Override
    public void onConnectionErrorCallBack(int requestCode) {
        switch (requestCode){
            case ENF_SPACE_BOOKING_LIST:
                getSpaceBookingList();
                break;
            case ENF_GUEST_BOOKING_LIST:
                getBookingList();
                break;
            case ENF_GUEST_BOOKING_CANCEL:
                cancelBooking();
                break;
        }
    }

    private void sortDateLists(final ArrayList<Booking> list) {
        //TODO Add sort after proper validation
        Collections.sort(list, new Comparator<Booking>() {
            @Override
            public int compare(Booking o1, Booking o2) {
                if (o1.getBookedDate() != null || o1.getBookedDate().isEmpty()){
                    list.remove(o1);
                    return 0;
                }
                return dateTimeComparator.compare(formatter.parseDateTime(o1.getBookedDate()),
                        formatter.parseDateTime(o2.getBookedDate()));
            }
        });

        for (Booking b: list){
            Log.d("Date",b.getBookedDate()!= null?b.getBookedDate():"Null" + " "+b.getId()+" "+b.isManual());
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        View view = tab.getCustomView();
        TextView title = ButterKnife.findById(view,R.id.tab_title);
        title.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryTextWhite));
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        View view = tab.getCustomView();
        TextView title = ButterKnife.findById(view,R.id.tab_title);
        title.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryTextLight));
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
