package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

public class ServerResponse implements Parcelable {
    private int status;
    private String message;

    protected ServerResponse(Parcel in) {
        status = in.readInt();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServerResponse> CREATOR = new Creator<ServerResponse>() {
        @Override
        public ServerResponse createFromParcel(Parcel in) {
            return new ServerResponse(in);
        }

        @Override
        public ServerResponse[] newArray(int size) {
            return new ServerResponse[size];
        }
    };

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
