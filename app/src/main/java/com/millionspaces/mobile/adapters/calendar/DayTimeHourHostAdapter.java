package com.millionspaces.mobile.adapters.calendar;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.IMethodUpdater;
import com.millionspaces.mobile.actions.OnHostTimeSelected;

import org.joda.time.LocalTime;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.CalenderUtils.SELECTED_HOUR;
import static com.millionspaces.mobile.utils.CalenderUtils.TOTALLY_BOOKED;
import static com.millionspaces.mobile.utils.CalenderUtils.TOTALLY_BOOKED_MS;
import static com.millionspaces.mobile.utils.CalenderUtils.TOTALLY_BOOKED_MS_FIRST;

/**
 * Created by kasunka on 11/20/17.
 */

public class DayTimeHourHostAdapter extends RecyclerView.Adapter<DayTimeHourHostAdapter.ViewHolderHours>{

    private Context mContext;
    private LocalTime openHour;
    private int noOfTotalHours;
    private HashMap<String ,String> blockedHoursMap;
    private SparseBooleanArray blockedHoursPositionMap;

    private OnHostTimeSelected onTimeSelected;
    private IMethodUpdater iMethodUpdater;

    public DayTimeHourHostAdapter(Context context, LocalTime openTime, int noOfWorkingHours, HashMap<String, String> blockedHours, IMethodUpdater iMethodUpdater) {
        this.mContext = context;
        this.openHour = openTime;
        this.noOfTotalHours = noOfWorkingHours;
        this.blockedHoursMap = blockedHours;
        this.iMethodUpdater = iMethodUpdater;
        blockedHoursPositionMap = new SparseBooleanArray();
    }

    @Override
    public ViewHolderHours onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_calender_hours, parent, false);
        return new ViewHolderHours(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderHours holder, int position) {
        final String  key  = openHour.plusHours(position).hourOfDay().getLocalTime().toString();
        holder.time.setText(openHour.plusHours(position).toString("hh a"));
        if (position < noOfTotalHours) {
            if (blockedHoursMap.containsKey(key)){
                switch (blockedHoursMap.get(key).split("-")[0]){
                    case TOTALLY_BOOKED:
                        holder.selectedIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.hostcalendarReservedColor));
                        holder.firstMsBookingIndicater.setVisibility(View.INVISIBLE);
                        holder.msIndicatorIco.setVisibility(View.INVISIBLE);
                        holder.msIndicator.setVisibility(View.INVISIBLE);
                        break;
                    case TOTALLY_BOOKED_MS_FIRST:
                        holder.selectedIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.hostcalendarReservedColor));
                        holder.firstMsBookingIndicater.setVisibility(View.VISIBLE);
                        holder.msIndicatorIco.setVisibility(View.VISIBLE);
                        holder.msIndicator.setVisibility(View.VISIBLE);
                        break;
                    case TOTALLY_BOOKED_MS:
                        holder.selectedIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.hostcalendarReservedColor));
                        holder.firstMsBookingIndicater.setVisibility(View.INVISIBLE);
                        holder.msIndicatorIco.setVisibility(View.INVISIBLE);
                        holder.msIndicator.setVisibility(View.VISIBLE);
                        break;
                    case SELECTED_HOUR:
                        blockedHoursPositionMap.put(position, true);
                        holder.selectedIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.calenderPrimaryBlue));
                        holder.firstMsBookingIndicater.setVisibility(View.INVISIBLE);
                        holder.msIndicatorIco.setVisibility(View.INVISIBLE);
                        holder.msIndicator.setVisibility(View.INVISIBLE);
                        break;
                }
            }else {
                holder.selectedIndicator.setBackgroundColor(Color.TRANSPARENT);
                holder.firstMsBookingIndicater.setVisibility(View.INVISIBLE);
                holder.msIndicatorIco.setVisibility(View.INVISIBLE);
                holder.msIndicator.setVisibility(View.INVISIBLE);
            }
//            if (blockedHoursMap.get(key) == TOTALLY_BOOKED) {
//                holder.selectedIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccentDark));
//            }else if (blockedHoursMap.get(key) == TOTALLY_BOOKED_MS_FIRST) {
//                holder.selectedIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccentDark));
//                holder.firstMsBookingIndicater.setVisibility(View.VISIBLE);
//                holder.msIndicatorIco.setVisibility(View.VISIBLE);
//                holder.msIndicator.setVisibility(View.VISIBLE);
//            }else if (blockedHoursMap.get(key) == TOTALLY_BOOKED_MS){
//                holder.selectedIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccentDark));
//                holder.msIndicator.setVisibility(View.VISIBLE);
//            } else if (blockedHoursMap.get(key) == SELECTED_HOUR) {
//                blockedHoursPositionMap.put(position, true);
//                holder.selectedIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.calenderPrimaryBlue));
//            } else {
//                holder.selectedIndicator.setBackgroundColor(Color.TRANSPARENT);
//            }
        }
    }

    public void clearSelections() {

        for(int i = 0; i < blockedHoursPositionMap.size(); i++) {
            int boolenKey = blockedHoursPositionMap.keyAt(i);
            String key = openHour.plusHours(boolenKey).hourOfDay().getLocalTime().toString();
            blockedHoursMap.remove(key);
        }
        blockedHoursPositionMap.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return noOfTotalHours+1;
    }


    public class ViewHolderHours extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.calender_perhour_hour)
        TextView time;
        @BindView(R.id.calender_perhour_hourholder)
        RelativeLayout timeHolder;
        @BindView(R.id.calender_perHour_seperator)
        View seperator;
        @BindView(R.id.calender_perhour_indicator)
        RelativeLayout selectedIndicator;
        @BindView(R.id.calender_perhour_first_indicator)
        LinearLayout firstMsBookingIndicater;
        @BindView(R.id.calender_perhour_firstms_indicator)
        ImageView msIndicatorIco;
        @BindView(R.id.calender_perhour_ms_indicator)
        LinearLayout msIndicator;

        public ViewHolderHours(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String key = openHour.plusHours(getAdapterPosition()).hourOfDay().getLocalTime().toString();
            int position = getAdapterPosition();

            Log.d("key", key);

            if (position < noOfTotalHours) {

                if (blockedHoursMap.containsKey(key)) {
                    String keyParams[] = blockedHoursMap.get(key).split("-");
                    switch (keyParams[0]) {
                        case TOTALLY_BOOKED:
                            onTimeSelected.onBookingSelected(true,Integer.valueOf(keyParams[1]));
                            break;
                        case TOTALLY_BOOKED_MS_FIRST:
                            onTimeSelected.onBookingSelected(false,Integer.valueOf(keyParams[1]));
                            break;
                        case TOTALLY_BOOKED_MS:
                            onTimeSelected.onBookingSelected(false,Integer.valueOf(keyParams[1]));
                            break;
                        case SELECTED_HOUR:
                            setSelectedView(key, position);
                            break;
                        default:
                            setSelectedView(key, position);
                            break;
                    }
                }else {
                    setSelectedView(key, position);
                }
            }
            notifyItemChanged(position);
        }

        private void setSelectedView(String key, int position) {
            if (blockedHoursMap.get(key) != TOTALLY_BOOKED && blockedHoursMap.get(key) != TOTALLY_BOOKED_MS_FIRST && blockedHoursMap.get(key) != TOTALLY_BOOKED_MS) {
                if (blockedHoursPositionMap.get(position)) {
                    if (!blockedHoursPositionMap.get(position + 1) || !blockedHoursPositionMap.get(position - 1)){
                        blockedHoursPositionMap.delete(position);
                        blockedHoursMap.remove(key);
                        iMethodUpdater.updateList(false,key,null);
                        onTimeSelected.onTimeSelected(blockedHoursMap,0, position,false,false);
                    }
                } else {
                    if (blockedHoursPositionMap.size() == 0 || blockedHoursPositionMap.get(position+1) || blockedHoursPositionMap.get(position-1)) {
                        blockedHoursPositionMap.put(position, true);
                        blockedHoursMap.put(key,SELECTED_HOUR);
                        iMethodUpdater.updateList(true,key,SELECTED_HOUR);
                        onTimeSelected.onTimeSelected(blockedHoursMap,0, position,true,false);
                    }else {
                        clearSelections();
                        blockedHoursPositionMap.put(position, true);
                        blockedHoursMap.put(key,SELECTED_HOUR);
                        iMethodUpdater.updateList(true,key,SELECTED_HOUR);
                        onTimeSelected.onTimeSelected(blockedHoursMap,0, position,true,true);
                    }
                }
            }
        }
    }

    public void setOnItemClick(OnHostTimeSelected onTimeSelected){
        this.onTimeSelected =  onTimeSelected;
    }
}
