package com.millionspaces.mobile.network.interfaces;

import com.millionspaces.mobile.entities.response.Space;

/**
 * Created by kasunka on 7/24/17.
 */

public interface OnSuccessSpace extends OnErrorResponse {
    void onSuccessSpace(Space space);
}
