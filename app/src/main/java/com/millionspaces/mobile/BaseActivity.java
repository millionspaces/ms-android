package com.millionspaces.mobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.millionspaces.mobile.actions.OnConnectionErrorCallBack;
import com.millionspaces.mobile.utils.Config;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM2;
import static com.millionspaces.mobile.utils.Config.PLACE_AUTOCOMPLETE_REQUEST_CODE;

/**
 * Created by kasunka on 6/7/17.
 * Application Base Activity class for handle common process
 */

public class BaseActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,OnConnectionErrorCallBack {
    protected ProgressDialog progressDialog; // TODO: remove with own progress

    protected CallbackManager callbackManager;
    protected GoogleApiClient mGoogleApiClient;
    protected FirebaseAnalytics mFirebaseAnalytics;

    protected Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu); //TODO: Make it with a base activity
        return true;
    }

    @Override
    public void onConnectionErrorCallBack(int error) {

    }

    protected void init_toolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    protected void setToolbarTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    //TODO: This method is only for development purpose.
    protected void keyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.millionspaces.mobile",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
    protected void navigateActivity(Class<?> activty){
        Intent intent = new Intent(this,activty);
        startActivity(intent);
    }

    protected void navigateFinishActivity(Class<?> activity){
        Intent intent = new Intent(this, activity);
        startActivity(intent);
        finish();
    }

    protected void navigateActivityWithExtraInt(Class<?> activity, int data){
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_INTEGER_PARAM, data);
        Intent intent = new Intent(this,activity);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    protected void navigateActivityWithExtra(Class<?> activity, Bundle bundle){
        Intent intent = new Intent(this,activity);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    protected void navigateActivityForResultWithExtra(Class<?> activity, Bundle bundle, int requestCode){
        Intent intent = new Intent(this,activity);
        intent.putExtras(bundle);
        startActivityForResult(intent,requestCode);
    }

    protected void navigateActivityWithExtras(Class<?> activity, int data, String list){
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_INTEGER_PARAM, data);
        bundle.putString(BUNDLE_STRING_PARAM, list);
        Intent intent = new Intent(this,activity);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    protected void navigateActivityWithExtraObj(Class<?> activty, Parcelable data){
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_STRING_PARAM, data);
        Intent intent = new Intent(this,activty);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    protected void navigateActivityForResult(Class<?> activity, int pathVariable){
        Intent intent = new Intent(this,activity);
        startActivityForResult(intent, pathVariable);
    }

    protected RequestListener getRequestListener(){
        return new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                supportStartPostponedEnterTransition();
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                supportStartPostponedEnterTransition();
                return false;
            }
        };
    }


    /**
     *   Location Place Service implementation
     *   Modified 06/06/2017 By Kasunka
     **/
    protected void init_search_place() {
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry(Config.MAP_PLACE_LOCATION)
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .build();
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(typeFilter)
                    .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Customize the error screen if required, Handled the error by placeholder.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Customize the error screen if required, Handled the error by placeholder.
        }
    }


    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
    }

    protected void GoogleSDKInitialize() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getApplicationContext().getResources().getString(R.string.server_client_id))
                .requestServerAuthCode(getApplicationContext().getResources().getString(R.string.server_client_id))
                .requestScopes(new Scope(Scopes.PLUS_ME), new Scope(Scopes.EMAIL), new Scope(Scopes.PROFILE))
                .requestEmail()
                .build();

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }else {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }
    }

    protected ProgressDialog showProgress(){
        progressDialog = ProgressDialog.show(this, null, "Please wait...",true, false);
//        progressDialog.getWindow().setBackgroundDrawable( new ColorDrawable(Color.TRANSPARENT));
//        progressDialog.setContentView( R.layout.dialog_progress_bar );
        return progressDialog;
    }

    protected void hideProgress(){
        if (null != progressDialog && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // TODO: Customize error Screen if Required.
    }

    private void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(new View(activity).getWindowToken(), 0);
    }

    protected void setupUI(final View view, final Activity activity) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView, activity);
            }
        }

    }
}
