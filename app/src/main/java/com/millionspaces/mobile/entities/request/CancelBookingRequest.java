package com.millionspaces.mobile.entities.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 11/27/17.
 */

public class CancelBookingRequest implements Parcelable{
    private int booking_id;
    private int event;
    private float refund;
    private String method;
    private int status;

    public CancelBookingRequest(int booking_id, int event, float refund) {
        this.booking_id = booking_id;
        this.event = event;
        this.refund = refund;
    }

    public CancelBookingRequest(int booking_id, int event, String method, int status) {
        this.booking_id = booking_id;
        this.event = event;
        this.method = method;
        this.status = status;
    }

    protected CancelBookingRequest(Parcel in) {
        booking_id = in.readInt();
        event = in.readInt();
        refund = in.readFloat();
        method = in.readString();
        status = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(booking_id);
        dest.writeInt(event);
        dest.writeFloat(refund);
        dest.writeString(method);
        dest.writeInt(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CancelBookingRequest> CREATOR = new Creator<CancelBookingRequest>() {
        @Override
        public CancelBookingRequest createFromParcel(Parcel in) {
            return new CancelBookingRequest(in);
        }

        @Override
        public CancelBookingRequest[] newArray(int size) {
            return new CancelBookingRequest[size];
        }
    };
}
