package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.millionspaces.mobile.entities.Menu;

import org.joda.time.LocalTime;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kasunka on 7/21/17.
 */

public class Availability implements Parcelable, Serializable, Comparable<Availability>{
    private int id;
    private int space;
    private int day;
    private String  from;
    private String  to;
    private float charge;
    private int isReimbursable;
    private ArrayList<Integer> dayList;
    private String availabilityStatus;
    private String tempAvailabilityStatus;
    private ArrayList<Menu> menuFiles;


    protected Availability(Parcel in) {
        id = in.readInt();
        space = in.readInt();
        day = in.readInt();
        from = in.readString();
        to = in.readString();
        charge = in.readFloat();
        isReimbursable = in.readInt();
        availabilityStatus = in.readString();
        tempAvailabilityStatus = in.readString();
        menuFiles = in.createTypedArrayList(Menu.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(space);
        dest.writeInt(day);
        dest.writeString(from);
        dest.writeString(to);
        dest.writeFloat(charge);
        dest.writeInt(isReimbursable);
        dest.writeString(availabilityStatus);
        dest.writeString(tempAvailabilityStatus);
        dest.writeTypedList(menuFiles);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Availability> CREATOR = new Creator<Availability>() {
        @Override
        public Availability createFromParcel(Parcel in) {
            return new Availability(in);
        }

        @Override
        public Availability[] newArray(int size) {
            return new Availability[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public float getCharge() {
        return charge;
    }

    public void setCharge(float charge) {
        this.charge = charge;
    }

    public int getIsReimbursable() {
        return isReimbursable;
    }

    public ArrayList<Integer> getDayList() {
        return dayList;
    }

    public void setDayList(ArrayList<Integer> dayList) {
        this.dayList = dayList;
    }

    public String getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(String availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }

    public String getTempAvailabilityStatus() {
        return tempAvailabilityStatus;
    }

    public void setTempAvailabilityStatus(String tempAvailabilityStatus) {
        this.tempAvailabilityStatus = tempAvailabilityStatus;
    }

    public ArrayList<Menu> getMenuFiles() {
        return menuFiles;
    }

    @Override
    public int compareTo(@NonNull Availability o) {
        if (getFrom() == null || o.getFrom() == null)
            return 0;
        return LocalTime.parse(getFrom()).compareTo(LocalTime.parse(o.getFrom()));
    }

}
