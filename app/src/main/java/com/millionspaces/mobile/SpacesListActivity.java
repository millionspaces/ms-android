package com.millionspaces.mobile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.millionspaces.mobile.actions.OnConnectionErrorCallBack;
import com.millionspaces.mobile.actions.OnItemCheckedListener;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.adapters.SpaceListFilterAdapter;
import com.millionspaces.mobile.adapters.SpacesListAdapter;
import com.millionspaces.mobile.entities.Location;
import com.millionspaces.mobile.entities.request.AdvanceSearchRequest;
import com.millionspaces.mobile.entities.response.Space;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessSpacesList;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.AnimationsUtils;
import com.millionspaces.mobile.utils.PreferenceUtils;
import com.millionspaces.mobile.views.FilterSheet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_PARCELABLE_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.DATE_FORMAT;
import static com.millionspaces.mobile.utils.Config.FILTER_DATE_FORMAT_01;
import static com.millionspaces.mobile.utils.Config.FILTER_DATE_FORMAT_02;
import static com.millionspaces.mobile.utils.Config.HOME_KEY_EVENT_TYPE;
import static com.millionspaces.mobile.utils.Config.HOME_KEY_LOCATION;
import static com.millionspaces.mobile.utils.Config.KEY_IS_LOGGED_IN;
import static com.millionspaces.mobile.utils.Config.KEY_ONACTIVITY_RESULT_LOGIN;
import static com.millionspaces.mobile.utils.Config.PLACE_AUTOCOMPLETE_REQUEST_CODE;
import static com.millionspaces.mobile.utils.Config.SPACE_AMINITY_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_BUDGET_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_EVENT_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_PARTICIPATION_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_RULE_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_SEATING_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_SPACE_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.get12HoursValue;
import static com.millionspaces.mobile.utils.Config.getBudgetString;
import static com.millionspaces.mobile.utils.Config.getParticipationString;
import static com.millionspaces.mobile.utils.Config.intersection;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_ADVANCE_SEARCH_SPACES;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_EVENT_SPACE_LIST_SELECT;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_NAME;

public class SpacesListActivity extends BaseActivity implements View.OnClickListener, OnSuccessSpacesList, OnItemCheckedListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, OnConnectionErrorCallBack {

    @BindView(R.id.spcelist_filter_button)
    LinearLayout filterButton;
    @BindView(R.id.spcelist_sort_button)
    LinearLayout sortButton;
    @BindView(R.id.spcelist_reset_button)
    LinearLayout resetButton;
    @BindView(R.id.spcelist_search_button)
    LinearLayout searchButton;
    @BindView(R.id.spaces_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.filterButtonContainer)
    LinearLayout filterButtonContainer;
    @BindView(R.id.spacelist_parent)
    CoordinatorLayout mCoordinatorlayout;

    @BindView(R.id.bottom_sheet_filter)
    View bottomSheetFilter;
    @BindView(R.id.space_filter_evnttype_collapse)
    RelativeLayout eventTypeCollapse;
    @BindView(R.id.space_filter_evnttype_container)
    LinearLayout eventTypeContainer;
    @BindView(R.id.space_filter_everntypes)
    RecyclerView eventTypeRecyclerView;
    @BindView(R.id.space_filter_spacetype_collapse)
    RelativeLayout spaceTypeCollapse;
    @BindView(R.id.space_filter_spacetype_container)
    LinearLayout spaceTypeContainer;
    @BindView(R.id.space_filter_spacetypes)
    RecyclerView spaceTypeRecyclerView;
    @BindView(R.id.space_filter_location_collapse)
    RelativeLayout locationCollapse;
    @BindView(R.id.space_filter_location_box)
    RelativeLayout locationView;
    @BindView(R.id.space_filter_location_container)
    LinearLayout locationContainer;
    @BindView(R.id.space_filter_location_text)
    TextView locationHint;
    @BindView(R.id.space_filter_location_clear)
    ImageView clearLocation;
    @BindView(R.id.space_filter_datetime_collapse)
    RelativeLayout dateTimeCollapse;
    @BindView(R.id.space_filter_datetime_container)
    LinearLayout dateTimeContainer;
    @BindView(R.id.space_filter_date_box)
    RelativeLayout sapceDateContainer;
    @BindView(R.id.space_filter_date_text)
    TextView dateHint;
    @BindView(R.id.space_filter_date_clear)
    ImageView clearDate;
    @BindView(R.id.space_filter_participation_collapse)
    RelativeLayout participationCollapse;
    @BindView(R.id.space_filter_participation_container)
    LinearLayout participationContainer;
    @BindView(R.id.space_filter_participation)
    RecyclerView participationRecyclerView;
    @BindView(R.id.space_filter_budget_collapse)
    RelativeLayout budgetCollapse;
    @BindView(R.id.space_filter_budget_container)
    LinearLayout budgetContainer;
    @BindView(R.id.space_filter_budget)
    RecyclerView budgetRecyclerView;
    @BindView(R.id.space_filter_amenites_collapse)
    RelativeLayout amenityCollapse;
    @BindView(R.id.space_filter_amenites_container)
    LinearLayout amenityContainer;
    @BindView(R.id.space_filter_amenity)
    RecyclerView amenityRecyclerView;
    @BindView(R.id.space_filter_rules_collapse)
    RelativeLayout rulesCollapse;
    @BindView(R.id.space_filter_rule_container)
    LinearLayout ruleContainer;
    @BindView(R.id.space_filter_rule)
    RecyclerView ruleRecyclerView;
    @BindView(R.id.space_filter_seating_collapse)
    RelativeLayout seatingCollapse;
    @BindView(R.id.space_filter_seating_container)
    LinearLayout seatingContainer;
    @BindView(R.id.space_filter_seating)
    RecyclerView seatingRecyclerView;
    @BindView(R.id.bottom_sheet_button_container)
    RelativeLayout filterActionContainer;
    @BindView(R.id.overlay)
    LinearLayout overLay;
    @BindView(R.id.space_error_img)
    ImageView errorImage;

    private SpacesListAdapter mAdapter ;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<Space> spaces = new ArrayList<>();

    private int pastVisiblesItems, visibleItemCount, totalItemCount, listCount;
    private boolean isLoading = false;
    private int page = 0;

    private BottomSheetBehavior mBottomSheetBehaviorFilter;
    private BottomSheetDialogFragment bottomSheetDialogFragment;
    private boolean isEventTypeVisible = false, isSpaceTypeVisible = false ,isLocationVisible = false, isdateTimeVisible =  false,
    isParticipationVisible = false, isBudgetVisible =  false, isAmenityVisible = false, isRuleVisible = false,
    isSeatingVisible = false;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private Calendar dateCalender;
    private Dialog errorDialog;

    private ArrayList<Integer> participation = new ArrayList<>();
    private ArrayList<Integer> budget = new ArrayList<>();
    private ArrayList<Integer> eventTypes = new ArrayList<>();
    private ArrayList<Integer> spaceTypes = new ArrayList<>();
    private ArrayList<Integer> amenities = new ArrayList<>();
    private ArrayList<Integer> rules =  new ArrayList<>();
    private ArrayList<Integer> seatings =  new ArrayList<>();
    public Location loc = null;
    private Date date = null;
    private boolean isFilterAdded = false;
    private String sortParam = null;
    private String organizationName = null;

    private SearchView searchView;
    private MenuItem searchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spaces_list);
        ButterKnife.bind(this);
        init_views();
    }

    private void init_views() {
        init_toolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        init_Actions();
        init_listView();
        getIntentValues();
    }

    private void init_Actions() {
        filterButton.setOnClickListener(this);
        sortButton.setOnClickListener(this);
        resetButton.setOnClickListener(this);
        searchButton.setOnClickListener(this);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = mLayoutManager.getItemCount();
                if(dy > 0  && listCount != totalItemCount) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!isLoading && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        isLoading = true;
                        page ++;
                        hideSearchContainer();
                        advanceSearch();
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        AnimationsUtils.moveUp(filterButtonContainer);
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        AnimationsUtils.sendDown(filterButtonContainer);
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        break;
                }
            }
        });

        mBottomSheetBehaviorFilter = BottomSheetBehavior.from(bottomSheetFilter);
        mBottomSheetBehaviorFilter.setHideable(true);
        mBottomSheetBehaviorFilter.setState(BottomSheetBehavior.STATE_HIDDEN);
        mBottomSheetBehaviorFilter.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    filterActionContainer.setVisibility(View.GONE);
                    if (isFilterAdded) {
                        spaces.clear();
                        page = 0;
                        totalItemCount = 0;
                        advanceSearch();
                    }
                }

                if (newState == BottomSheetBehavior.STATE_DRAGGING){
                    if (isFilterAdded){
                        overLay.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
            }
        });

        eventTypeCollapse.setOnClickListener(this);
        spaceTypeCollapse.setOnClickListener(this);
        locationCollapse.setOnClickListener(this);
        locationView.setOnClickListener(this);
        clearLocation.setOnClickListener(this);
        dateTimeCollapse.setOnClickListener(this);
        sapceDateContainer.setOnClickListener(this);
        participationCollapse.setOnClickListener(this);
        budgetCollapse.setOnClickListener(this);
        amenityCollapse.setOnClickListener(this);
        rulesCollapse.setOnClickListener(this);
        seatingCollapse.setOnClickListener(this);
        clearDate.setOnClickListener(this);

    }

    /*Get Filtered values form Home Activity
    Created 26/06/2017 By Kasunka*/
    private void getIntentValues(){
        Intent intent = getIntent();
        int eventType = intent.hasExtra(HOME_KEY_EVENT_TYPE)?intent.getIntExtra(HOME_KEY_EVENT_TYPE,0):null;
        if (eventType != 0){eventTypes.add(eventType);}

        loc = intent.hasExtra(HOME_KEY_LOCATION)? (Location) intent.getParcelableExtra(HOME_KEY_LOCATION) :null;
        advanceSearch();

        clearLocation.setImageDrawable(loc!= null ? ContextCompat.getDrawable(this,R.drawable.ic_close_dark):ContextCompat.getDrawable(this,R.drawable.ic_search_dark));
        locationHint.setText(loc!= null ? loc.getAddress():null);
    }

    /*Filtered Space List web call
    Modified 03/07/2017 By Kasunka*/
    public void advanceSearch(){
        AdvanceSearchRequest request = new AdvanceSearchRequest(
                participation != null?getParticipationString(participation):null,
                budget != null?getBudgetString(budget):null,
                new ArrayList<Integer>(new LinkedHashSet<Integer>(amenities)),
                eventTypes != null?new ArrayList<Integer>(new LinkedHashSet<Integer>(eventTypes)):null,
                spaceTypes != null?new ArrayList<Integer>(new LinkedHashSet<Integer>(spaceTypes)):null,
                loc,
                date != null?new SimpleDateFormat(DATE_FORMAT).format(date):null,
                rules != null?new ArrayList<Integer>(new LinkedHashSet<Integer>(rules)):null,
                seatings != null?new ArrayList<Integer>(new LinkedHashSet<Integer>(seatings)):null,
                sortParam,
                organizationName,mFirebaseAnalytics);
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            showProgress();
            MillionSpaceService.getInstance(this).getSpaceByFilter(request,this,page,this);
        }else {
            mAdapter.notifyDataSetChanged();
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),getResources().getString(R.string.error_network_failure_action),
                    mCoordinatorlayout,this,ENF_ADVANCE_SEARCH_SPACES);
        }
    }


    private void init_listView(){
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager );
        mAdapter = new SpacesListAdapter(spaces, this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_space_list, menu);

        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getString(R.string.space_list_filter_search_hint));

        TextView searchText = (TextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!searchView.getQuery().toString().equalsIgnoreCase(organizationName)) {
                        searchByOrganizationName(searchView.getQuery().toString());
                    }else {
                        hideSearchContainer();
                    }
                }
                return false;
            }
        });

        View closeButton = searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchByOrganizationName(null);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                searchView.post(new Runnable() {
                    @Override
                    public void run() {
                        searchView.setQuery(organizationName, false);
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void searchByOrganizationName(String s) {
        organizationName = s;
        isFilterAdded = true;
        spaces.clear();
        page = 0;
        totalItemCount = 0;
        hideSearchContainer();
        advanceSearch();
    }

    @Override
    public void onBackPressed() {
        hideSearchContainer();
        if (mBottomSheetBehaviorFilter.getState() == BottomSheetBehavior.STATE_EXPANDED){
            mBottomSheetBehaviorFilter.setState(BottomSheetBehavior.STATE_HIDDEN);
        }else {
            super.onBackPressed();
        }
    }

    private void hideSearchContainer() {
        if (searchView.isShown()) {
            searchMenuItem.collapseActionView();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.spcelist_filter_button:
                mBottomSheetBehaviorFilter.setState(BottomSheetBehavior.STATE_EXPANDED);
                filterActionContainer.setVisibility(View.VISIBLE);
                init_filterView();
                break;
            case R.id.spcelist_sort_button:
                bottomSheetDialogFragment = new FilterSheet();
                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                break;
            case R.id.spcelist_reset_button:
                participation.clear(); budget.clear();amenities.clear();eventTypes.clear(); spaceTypes.clear();loc = null; date = null;rules.clear();seatings.clear();
                isFilterAdded = true;
                mBottomSheetBehaviorFilter.setState(BottomSheetBehavior.STATE_HIDDEN);
                locationHint.setText("");
                dateHint.setText("");
                overLay.setVisibility(View.VISIBLE);
                break;
            case R.id.spcelist_search_button:
                mBottomSheetBehaviorFilter.setState(BottomSheetBehavior.STATE_HIDDEN);
                if (isFilterAdded){
                    overLay.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.space_filter_location_box:
                init_search_place();
                break;
            case R.id.space_filter_location_clear:
                if (null != loc){
                    loc = null;
                    isFilterAdded = true;
                    locationHint.setText("");
                    clearLocation.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_search_dark));
                }else {
                    init_search_place();
                    clearLocation.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_close_dark));
                }

                break;
            case R.id.space_filter_evnttype_collapse:
                collapseLayout(eventTypeContainer,isEventTypeVisible);
                isEventTypeVisible = !isEventTypeVisible;
                break;
            case R.id.space_filter_spacetype_collapse:
                collapseLayout(spaceTypeContainer,isSpaceTypeVisible);
                isSpaceTypeVisible = !isSpaceTypeVisible;
                break;
            case R.id.space_filter_location_collapse:
                collapseLayout(locationContainer,isLocationVisible);
                isLocationVisible = !isLocationVisible;
                break;
            case R.id.space_filter_datetime_collapse:
                collapseLayout(dateTimeContainer, isdateTimeVisible);
                isdateTimeVisible = !isdateTimeVisible;
                break;
            case R.id.space_filter_participation_collapse:
                collapseLayout(participationContainer, isParticipationVisible);
                isParticipationVisible = !isParticipationVisible;
                break;
            case R.id.space_filter_budget_collapse:
                collapseLayout(budgetContainer, isBudgetVisible);
                isBudgetVisible = !isBudgetVisible;
                break;
            case R.id.space_filter_amenites_collapse:
                collapseLayout(amenityContainer, isAmenityVisible);
                isAmenityVisible = !isAmenityVisible;
                break;
            case R.id.space_filter_rules_collapse:
                collapseLayout(ruleContainer, isRuleVisible);
                isRuleVisible = !isRuleVisible;
                break;
            case R.id.space_filter_seating_collapse:
                collapseLayout(seatingContainer, isSeatingVisible);
                isSeatingVisible = !isSeatingVisible;
                break;
            case R.id.space_filter_date_box:
                datePickerDialog.show();
                break;
            case R.id.space_filter_date_clear:
                dateHint.setText("");
                if (null != date){
                    date = null;
                    dateHint.setText("");
                    clearDate.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_calander_dark));
                }else {
                    datePickerDialog.show();
                }
                break;
            case R.id.dialog_action:
                errorDialog.dismiss();
                break;
            case R.id.overlay:
                // DO nothing, Override Action Event by a overlay
                break;
        }
    }

    private void collapseLayout(View v, boolean isVisible) {
        if (isVisible){
            v.setVisibility(View.GONE);
        }else {
            v.setVisibility(View.VISIBLE);
        }
    }

    /*Initialize bottom floating view items
    Created 31/06/2017 By Kasunka*/
    private void init_filterView() {
        eventTypeRecyclerView.setHasFixedSize(true);
        eventTypeRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
        SpaceListFilterAdapter eventTypeAdapter = new SpaceListFilterAdapter(this,SPACE_EVENT_TYPE_LIST,eventTypes);
        eventTypeRecyclerView.setAdapter(eventTypeAdapter);
        eventTypeRecyclerView.setNestedScrollingEnabled(true);
        eventTypeAdapter.setOnItemCheck(this);

        spaceTypeRecyclerView.setHasFixedSize(true);
        spaceTypeRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
        SpaceListFilterAdapter spaceTypeAdapter = new SpaceListFilterAdapter(this,SPACE_SPACE_TYPE_LIST,spaceTypes);
        spaceTypeRecyclerView.setAdapter(spaceTypeAdapter);
        spaceTypeRecyclerView.setNestedScrollingEnabled(true);
        spaceTypeAdapter.setOnItemCheck(this);

        participationRecyclerView.setHasFixedSize(true);
        participationRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
        SpaceListFilterAdapter participationAdapter = new SpaceListFilterAdapter(this,SPACE_PARTICIPATION_TYPE_LIST,participation);
        participationRecyclerView.setAdapter(participationAdapter);
        participationRecyclerView.setNestedScrollingEnabled(true);
        participationAdapter.setOnItemCheck(this);

        budgetRecyclerView.setHasFixedSize(true);
        budgetRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        SpaceListFilterAdapter budgetAdapter = new SpaceListFilterAdapter(this,SPACE_BUDGET_TYPE_LIST,budget);
        budgetRecyclerView.setAdapter(budgetAdapter);
        budgetRecyclerView.setNestedScrollingEnabled(true);
        budgetAdapter.setOnItemCheck(this);

        amenityRecyclerView.setHasFixedSize(true);
        amenityRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
        SpaceListFilterAdapter amenityAdapter = new SpaceListFilterAdapter(this,SPACE_AMINITY_TYPE_LIST,amenities);
        amenityRecyclerView.setAdapter(amenityAdapter);
        amenityRecyclerView.setNestedScrollingEnabled(true);
        amenityAdapter.setOnItemCheck(this);

        ruleRecyclerView.setHasFixedSize(true);
        ruleRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        SpaceListFilterAdapter ruleAdapter = new SpaceListFilterAdapter(this,SPACE_RULE_TYPE_LIST,rules);
        ruleRecyclerView.setAdapter(ruleAdapter);
        ruleRecyclerView.setNestedScrollingEnabled(true);
        ruleAdapter.setOnItemCheck(this);

        seatingRecyclerView.setHasFixedSize(true);
        seatingRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        SpaceListFilterAdapter seatingAdapter = new SpaceListFilterAdapter(this,SPACE_SEATING_TYPE_LIST,seatings);
        seatingRecyclerView.setAdapter(seatingAdapter);
        seatingRecyclerView.setNestedScrollingEnabled(true);
        seatingAdapter.setOnItemCheck(this);

        Calendar c = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        timePickerDialog = new TimePickerDialog(this,this,c.get(Calendar.HOUR),c.get(Calendar.MINUTE),false);
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinatorlayout);
    }


    @Override
    public void onSuccessSpacesList(final ArrayList<Space> spaceList) {
        hideProgress();
        isLoading = false;
        spaces.addAll(spaceList);

        overLay.setVisibility(View.GONE);
        mAdapter.notifyDataSetChanged();
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnClick(int position, View sharedImageView) {
                hideSearchContainer();

                if (PreferenceUtils.showPreferenceBoolean(KEY_IS_LOGGED_IN, false, SpacesListActivity.this)) {
                    Bundle mBundle = new Bundle();
                    mBundle.putString(FIREBASE_PARAM_SPACE_LIST_NAME, spaces.get(position).getName());
                    mFirebaseAnalytics.logEvent(FIREBASE_EVENT_SPACE_LIST_SELECT, mBundle);

                    //TODO : format code with removing event Types
                    Intent intent = new Intent(SpacesListActivity.this, SpaceProfileActivity.class);
                    intent.putExtra(BUNDLE_INTEGER_PARAM, spaces.get(position).getId());
//                intent.putExtra(BUNDLE_PARCELABLE_PARAM,spaces.get(position).getImages());
//                intent.putExtra(BUNDLE_STRING_PARAM, eventTypes.size() != 0 ? intersection(eventTypes, spaces.get(position).getEventType()).toString() : spaces.get(position).getEventType().toString()); // TODO : Remove This live with new Release,
// Event Type is null with current Object
//                intent.putExtra(BUNDLE_STRING_PARAM, eventTypes.size() != 0 ? intersection(eventTypes, spaces.get(position).getEventType()).toString() : spaces.get(position).getEventType().toString()); // TODO Get ids for similar spaces
//                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
//                        SpacesListActivity.this,
//                        sharedImageView,
//                        ViewCompat.getTransitionName(sharedImageView));
//                startActivity(intent,options.toBundle());
                    startActivity(intent);
                } else {
                    navigateActivityForResult(LoginActivity.class, KEY_ONACTIVITY_RESULT_LOGIN);
                }
            }
        });
    }

    @Override
    public void onSuccessSpacesListCount(int count) {
        if (isFilterAdded){
            mRecyclerView.smoothScrollToPosition(0);
        }
        isFilterAdded = false;
        this.listCount =  count;
        if (listCount == 0){
            errorImage.setVisibility(View.VISIBLE);
            AlertMessageUtils.showSnackBarMessage(getString(R.string.error_empty_listed_spaces), mCoordinatorlayout);
            setToolbarTitle(getResources().getQuantityString(R.plurals.space_list_titlebar_name, 0,0));
        }else {
            errorImage.setVisibility(View.GONE);
            setToolbarTitle(getResources().getQuantityString(R.plurals.space_list_titlebar_name, listCount,listCount));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                clearLocation.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_close_dark));
                Place place = PlaceAutocomplete.getPlace(this, data);
                locationHint.setText(place.getName());
                locationHint.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryTextLight));
                isFilterAdded =  true;
                loc = new Location(place.getLatLng().latitude, place.getLatLng().longitude,place.getName().toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                // TODO: Handle the error if required.

            } else if (resultCode == RESULT_CANCELED) {
                //TODO: Handle Location onBack press and Cancel Events
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onItemChecked(boolean checked, int id, int type) {
        switch (type){
            case SPACE_EVENT_TYPE_LIST:
                isFilterAdded = checked?eventTypes.add(id):eventTypes.remove(eventTypes.contains(id)?id:null);
                break;
            case SPACE_SPACE_TYPE_LIST:
                isFilterAdded = checked?spaceTypes.add(id):spaceTypes.remove(spaceTypes.contains(id)?id:null);
                break;
            case SPACE_PARTICIPATION_TYPE_LIST:
                isFilterAdded = checked?participation.add(id):participation.remove(participation.contains(id)?id:null);
                break;
            case SPACE_BUDGET_TYPE_LIST:
                isFilterAdded = checked?budget.add(id):budget.remove(budget.contains(id)?id:null);
                break;
            case SPACE_AMINITY_TYPE_LIST:
                isFilterAdded = checked?amenities.add(id):amenities.remove(amenities.contains(id)?id:null);
                break;
            case SPACE_RULE_TYPE_LIST:
                isFilterAdded = checked?rules.add(id):rules.remove(rules.contains(id)?id:null);
                break;
            case SPACE_SEATING_TYPE_LIST:
                isFilterAdded = checked?seatings.add(id):seatings.remove(seatings.contains(id)?id:null);
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        isFilterAdded = true;
        dateCalender = Calendar.getInstance();
        dateCalender.set(Calendar.YEAR, year);
        dateCalender.set(Calendar.MONTH, month);
        dateCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        timePickerDialog.show();
        date = dateCalender.getTime();
        clearDate.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_close_dark));
        dateHint.setText(new SimpleDateFormat(FILTER_DATE_FORMAT_01).format(date)+new SimpleDateFormat(FILTER_DATE_FORMAT_02).format(date));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        dateCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
        dateCalender.set(Calendar.MINUTE, minute);
        date = dateCalender.getTime();
        dateHint.setText(dateHint.getText()+", "+get12HoursValue(hourOfDay,minute));
    }

    @Override
    public void onConnectionErrorCallBack(int requestCode) {
        advanceSearch();
    }

    public void onSortAdded(String sortParam, boolean isAdded) {
        if (isAdded) {
            this.sortParam = sortParam;
            spaces.clear();
            page = 0;
            totalItemCount = 0;
            bottomSheetDialogFragment.dismiss();
            advanceSearch();
        }
    }

    public void showErrorAlert(){
        errorDialog = AlertMessageUtils.showAlertDialog(this,this);
    }

    public String getSortParam(){
        return sortParam;
    }
}
