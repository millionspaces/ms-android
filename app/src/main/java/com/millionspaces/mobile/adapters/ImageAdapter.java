package com.millionspaces.mobile.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.millionspaces.mobile.BuildConfig;
import com.millionspaces.mobile.GalleryFullViewActivity;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.SpaceProfileActivity;
import com.millionspaces.mobile.utils.Config;
import com.millionspaces.mobile.views.GalleryImageViewFragment;

import java.util.ArrayList;

import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.IMAGE_PLACEHOLDER_FACTOR;


/**
 * Created by Aux-106 on 5/2/2017.
 */

public class ImageAdapter extends PagerAdapter implements View.OnClickListener {
    private Context mContext;
    private boolean doubleTapToFullView = false;
    private String transitionName;
    private ArrayList<String> mImages = new ArrayList<>();
    private Activity activity;


    public ImageAdapter(Context context, ArrayList<String> imageArray, Activity activity) {
        this.mContext = context;
        this.mImages = imageArray;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return mImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        ActivityCompat.postponeEnterTransition(activity);
        return view == ((RelativeLayout) object);
    }

    /**
     * Changed method body and add Glide image loader
     * Modified 23/06/2017 By Kasunka
     **/
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView;

        if (mContext.getResources().getConfiguration().orientation == 1){
            itemView = LayoutInflater.from(mContext).inflate(R.layout.gallery_pager_item_portrait, container, false);
        }else {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.gallery_pager_item_landscape, container, false);
        }
        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);

        // Enable Below code to Activate Activity Transitions.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && position == 0) {
            imageView.setTransitionName("ABC");
        }

        RequestBuilder<Drawable> thumbnailRequest = Glide.with(mContext)
                .load(BuildConfig.IMAGE_SERVER_URL+IMAGE_PLACEHOLDER_FACTOR+ BuildConfig.IMAGE_SERVER_DOMAIN+mImages.get(position));
        Glide.with(mContext)
                .load(BuildConfig.IMAGE_SERVER_URL+Config.optimizedImage(mContext)+BuildConfig.IMAGE_SERVER_DOMAIN+mImages.get(position)) // LIVE URL
//                .listener(new RequestListener<Drawable>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                        ActivityCompat.startPostponedEnterTransition(activity);
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                        ActivityCompat.startPostponedEnterTransition(activity);
//                        return false;
//                    }
//                })
                .thumbnail(thumbnailRequest)
                .thumbnail(0.1f)
                .into(imageView);

        container.addView(itemView);
        itemView.setOnClickListener(this);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    /**
     * Implementation of click event on view pager item
     * Created 26/06/2017 By Kasunka
     **/
    // TODO : Enable code for switch perform action for double tap
    @Override
    public void onClick(View v) {
        if (mContext instanceof SpaceProfileActivity) {

//            if (doubleTapToFullView) {
                ((SpaceProfileActivity)mContext).showGalleryFullView();
//                return;
//            }

//            this.doubleTapToFullView = true;
//
//            new Handler().postDelayed(new Runnable() {
//
//                @Override
//                public void run() {
//                    doubleTapToFullView=false;
//                }
//            }, 2000);

        }else {
            ((GalleryFullViewActivity)mContext).finish();
        }
    }
}