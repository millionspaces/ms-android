package com.millionspaces.mobile.network.interfaces;

/**
 * Created by kasunka on 10/9/17.
 */

public interface OnUpdateUser extends OnErrorResponse {
    void onUpdateUser(String responce);
}
