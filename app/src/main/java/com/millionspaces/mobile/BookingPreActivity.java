package com.millionspaces.mobile;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.adapters.ItemSelectionAdapter;
import com.millionspaces.mobile.entities.response.SeatingArrangement;
import com.millionspaces.mobile.entities.response.Space;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.PreferenceUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.KEY_CURRENT_CHECKING_EVENT_TYPE;
import static com.millionspaces.mobile.utils.Config.KEY_CURRENT_CHECKING_GUEST_COUNT;
import static com.millionspaces.mobile.utils.Config.KEY_CURRENT_CHECKING_SEATING_ID;
import static com.millionspaces.mobile.utils.Config.SPACE_EVENT_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_SEATING_TYPE_LIST;

public class BookingPreActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    @BindView(R.id.parent_pre_calendar)
    CoordinatorLayout parent;
    @BindView(R.id.booking_pre_event_type)
    RecyclerView eventTypeRecyclerView;
    @BindView(R.id.booking_pre_seating)
    RecyclerView seatingRecyclerView;
    @BindView(R.id.calender_exceptedguest_text)
    EditText guestCountView;
    @BindView(R.id.booking_next_btn)
    Button nextButton;
    @BindView(R.id.calender_exceptedguest_error_alert)
    TextView errorAlertView;

    private Space space;
    private ArrayList<Integer> seating = new ArrayList<>();
    private int eventTypeId = -1;
    private int seatingId = -1;
    private String guestCount;
    private int minCount = 0, maxCount = 0;

    private ItemSelectionAdapter eventTypeAdapter;
    private ItemSelectionAdapter seatingAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_pre);
        ButterKnife.bind(this);
        init_views();
    }

    private void init_views() {
        init_toolbar();
        setToolbarTitle(getString(R.string.calender_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        space = getIntent().getParcelableExtra(BUNDLE_STRING_PARAM);
        for (SeatingArrangement seatingArrangement :space.getSeatingArrangements()) {
            seating.add(seatingArrangement.getId());
        }

        eventTypeRecyclerView.setHasFixedSize(true);
        eventTypeRecyclerView.setLayoutManager(new GridLayoutManager(this,3));
        eventTypeAdapter = new ItemSelectionAdapter(SPACE_EVENT_TYPE_LIST,space.getEventType(),-1,this);
        eventTypeRecyclerView.setAdapter(eventTypeAdapter);
        eventTypeRecyclerView.setNestedScrollingEnabled(true);

        seatingRecyclerView.setHasFixedSize(true);
        seatingRecyclerView.setLayoutManager(new GridLayoutManager(this,3));
        seatingAdapter = new ItemSelectionAdapter(SPACE_SEATING_TYPE_LIST,seating,-1,this);
        seatingRecyclerView.setAdapter(seatingAdapter);
        seatingRecyclerView.setNestedScrollingEnabled(true);

        minCount = space.getMinParticipantCount();
        maxCount = space.getParticipantCount();
        updatePaxCountHint(minCount,maxCount);

        init_actions();
    }

    private void updatePaxCountHint(int minCount, int maxCount) {
        guestCountView.setHint(minCount != 0? getString(R.string.calender_selection_hint_min)+" "+minCount+" - "+
                getString(R.string.calender_selection_hint_max)+" "+maxCount:
                getString(R.string.calender_selection_hint_max)+" "+maxCount);
    }

    private void init_actions() {
        setupUI(parent,this);
        parent.requestFocus();
        nextButton.setOnClickListener(this);

        eventTypeAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnClick(int position, View view) {
                eventTypeId = space.getEventType().get(position);
            }
        });

        seatingAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnClick(int position, View view) {
                seatingId = seating.get(position);
                maxCount = space.getSeatingArrangements().get(position).getParticipantCount();
                updatePaxCountHint(minCount,maxCount);
                validatePaxCount(guestCountView.getText().toString());
            }
        });

        guestCountView.addTextChangedListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.booking_next_btn:
                attemptToProceed();
                break;
        }
    }


    private void attemptToProceed() {
        guestCount = guestCountView.getText().toString();
        View focusedView = null;
        String error = null;
        boolean isValid = true;

        if (eventTypeId == -1){
            focusedView =  eventTypeRecyclerView;
            error = getString(R.string.error_empty_eventtype);
            isValid = false;
        }else if (seatingId == -1){
            focusedView = seatingRecyclerView;
            error = getString(R.string.error_empty_seatingstyle);
            isValid = false;
        }else if (guestCount.isEmpty() || Integer.parseInt(guestCount)<1){
            focusedView = guestCountView;
            error = getString(R.string.error_empty_guest_count);
            isValid = false;
        }

        if (isValid){
            proceed();
        }else {
            AlertMessageUtils.showSnackBarMessage(error,parent);
        }
    }

    private void proceed() {
        PreferenceUtils.savePreferenceString(KEY_CURRENT_CHECKING_EVENT_TYPE,String.valueOf(eventTypeId),this);
        PreferenceUtils.savePreferenceString(KEY_CURRENT_CHECKING_GUEST_COUNT,guestCount,this);
        PreferenceUtils.savePreferenceString(KEY_CURRENT_CHECKING_SEATING_ID,String.valueOf(seatingId),this);
        navigateActivity(BookingEngineActivity.class);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        validatePaxCount(s);
    }

    private void validatePaxCount(CharSequence s) {
        if (!TextUtils.isEmpty(s)) {
            if (minCount > Integer.parseInt(String.valueOf(s))) {
                errorAlertView.setVisibility(View.VISIBLE);
                errorAlertView.setText(getString(R.string.error_lower_guest_count));
            } else if (maxCount < Integer.parseInt(String.valueOf(s))) {
                errorAlertView.setVisibility(View.VISIBLE);
                errorAlertView.setText(getString(R.string.error_higher_guest_count));
            }else {
                errorAlertView.setVisibility(View.GONE);
            }
        }else {
            errorAlertView.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onBackPressed() {
        PreferenceUtils.clearPreferenceString(KEY_CURRENT_CHECKING_EVENT_TYPE,this);
        PreferenceUtils.clearPreferenceString(KEY_CURRENT_CHECKING_GUEST_COUNT,this);
        PreferenceUtils.clearPreferenceString(KEY_CURRENT_CHECKING_SEATING_ID,this);
        super.onBackPressed();
    }
}
