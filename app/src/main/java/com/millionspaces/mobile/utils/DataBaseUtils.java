package com.millionspaces.mobile.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.millionspaces.mobile.entities.AmenityUnits;
import com.millionspaces.mobile.entities.response.Amenity;
import com.millionspaces.mobile.entities.response.CancellationPolicy;
import com.millionspaces.mobile.entities.response.EventType;
import com.millionspaces.mobile.entities.response.Rule;
import com.millionspaces.mobile.entities.response.SeatingArrangement;

import java.util.ArrayList;

/**
 * Created by kasunka on 7/19/17.
 */

public class DataBaseUtils extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "MetaData";

    // Table name
    private static final String TABLE_AMENITIES = "aminities";
    private static final String TABLE_EVENT_TYPES = "eventtypes";
    private static final String TABLE_SPACE_RULES = "spacerules";
    private static final String TABLE_SEATING_ARRANGEMENT = "seating";
    private static final String TABLE_CANCELLATION_POLICY = "cancellation";
    private static final String TABLE_AMENITY_UNITS = "amenityunits";

    // Amenity Table Columns names
    private static final String AMENITY_KEY_ID = "id";
    private static final String AMENITY_KEY_NAME = "name";
    private static final String AMENITY_KEY_ICON = "icon";
    private static final String AMENITY_KEY_TIMESTAMP = "timeStamp";
    private static final String AMENITY_KEY_AMENITY_ID = "amenityId";
    private static final String AMENITY_KEY_AMENITY_NAME = "amenityName";

    // Event Types Table Columns names
    private static final String EVENT_KEY_ID = "id";
    private static final String EVENT_KEY_NAME = "name";
    private static final String EVENT_KEY_ICON = "icon";
    private static final String EVENT_KEY_TIMESTAMP = "timeStamp";

    // Space Rules Table Colums names
    private static final String RULE_KEY_ID = "id";
    private static final String RULE_KEY_NAME = "name";
    private static final String RULE_KEY_DISPLAY_NAME = "disname";
    private static final String RULE_KEY_ICON = "icon";
    private static final String RULE_KEY_TIMESTAMP = "timeStamp";

    // Seating Arrangement Table Columns names
    private static final String SEATING_KEY_ID = "id";
    private static final String SEATING_KEY_NAME = "name";
    private static final String SEATING_KEY_ICON = "icon";
    private static final String SEATING_KEY_TIMESTAMP = "timeStamp";

    // Space Cancellation Policy Table Colums names
    private static final String CANCEL_KEY_ID = "id";
    private static final String CANCEL_KEY_NAME = "name";
    private static final String CANCEL_KEY_DISCRIPTION = "discription";
    private static final String CANCEL_REFUND_RATE = "refund";
    private static final String CANCEL_KEY_TIMESTAMP = "timeStamp";

    // Space Cancellation Policy Table Columns names
    private static final String AMENITY_UNITS_KEY_ID = "id";
    private static final String AMENITY_UNITS_KEY_NAME = "name";

    // Table Create Statements
    // Amenity table
    private static final String CREATE_TABLE_AMINITY = "CREATE TABLE "
            + TABLE_AMENITIES + "(" + AMENITY_KEY_ID + " INTEGER," + AMENITY_KEY_NAME
            + " TEXT," + AMENITY_KEY_ICON + " TEXT," + AMENITY_KEY_TIMESTAMP
            + " TEXT," + AMENITY_KEY_AMENITY_ID + " INTEGER," + AMENITY_KEY_AMENITY_NAME + " TEXT" +")";

    // Event Type Table
    private static final String CREATE_TABLE_EVETTYPES = "CREATE TABLE "
            + TABLE_EVENT_TYPES + "(" + EVENT_KEY_ID + " INTEGER," + EVENT_KEY_NAME
            + " TEXT," + EVENT_KEY_ICON + " TEXT," + EVENT_KEY_TIMESTAMP + " TEXT" +")";

    // Rules Table
    private static final String CREATE_TABLE_RULES = "CREATE TABLE "
            + TABLE_SPACE_RULES + "(" + RULE_KEY_ID + " INTEGER," + RULE_KEY_NAME
            + " TEXT," +RULE_KEY_DISPLAY_NAME+ " TEXT," + RULE_KEY_ICON + " TEXT," + RULE_KEY_TIMESTAMP + " TEXT" +")";

    // Seating Arrangement Table
    private static final String CREATE_TABLE_SEATING = "CREATE TABLE "
            + TABLE_SEATING_ARRANGEMENT + "(" + SEATING_KEY_ID + " INTEGER," + SEATING_KEY_NAME
            + " TEXT," + SEATING_KEY_ICON + " TEXT," + SEATING_KEY_TIMESTAMP + " TEXT" +")";

    // Cancellation Policy Table
    private static final String CREATE_TEABLE_CANCELLATION = "CREATE TABLE "
            + TABLE_CANCELLATION_POLICY + "(" + CANCEL_KEY_ID + " INTEGER," + CANCEL_KEY_NAME
            + " TEXT," + CANCEL_KEY_DISCRIPTION + " TEXT," +  CANCEL_REFUND_RATE
            + " REAL," + CANCEL_KEY_TIMESTAMP + " TEXT" +")";

    // Amenity Units Table
    private static final String CREATE_TEABLE_AMENITY_UNITS = "CREATE TABLE "
            + TABLE_AMENITY_UNITS + "(" + AMENITY_UNITS_KEY_ID + " INTEGER," + AMENITY_UNITS_KEY_NAME
            + " TEXT" +")";


    private static final String DROP_TABLE = "DROP TABLE IF EXISTS ";
    private static final String SELECT_ALL = "SELECT  * FROM ";
    private static final String SELECT = "SELECT  ";
    private static final String FROM = " FROM ";
    private static final String WHERE = "=?";


    public DataBaseUtils(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_TABLE_AMINITY);
        db.execSQL(CREATE_TABLE_EVETTYPES);
        db.execSQL(CREATE_TABLE_RULES);
        db.execSQL(CREATE_TABLE_SEATING);
        db.execSQL(CREATE_TEABLE_CANCELLATION);
        db.execSQL(CREATE_TEABLE_AMENITY_UNITS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL(DROP_TABLE + CREATE_TABLE_AMINITY);
        db.execSQL(DROP_TABLE + CREATE_TABLE_EVETTYPES);
        db.execSQL(DROP_TABLE + CREATE_TABLE_RULES);
        db.execSQL(DROP_TABLE + CREATE_TABLE_SEATING);
        db.execSQL(DROP_TABLE + CREATE_TEABLE_CANCELLATION);
        db.execSQL(DROP_TABLE + CREATE_TEABLE_AMENITY_UNITS);

        // create new tables
        onCreate(db);
    }

    public void dropScheme(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(DROP_TABLE + TABLE_AMENITIES);
        db.execSQL(DROP_TABLE + TABLE_EVENT_TYPES);
        db.execSQL(DROP_TABLE + TABLE_SPACE_RULES);
        db.execSQL(DROP_TABLE + TABLE_SEATING_ARRANGEMENT);
        db.execSQL(DROP_TABLE + TABLE_CANCELLATION_POLICY);
        db.execSQL(DROP_TABLE + TABLE_AMENITY_UNITS);

        // create new tables
        onCreate(db);
    }

    //////////////////////////    Amenity      ////////////////////////

    //    Add Amenity value
    public void addAmenity(Amenity amenity){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(AMENITY_KEY_ID, amenity.getId());
        values.put(AMENITY_KEY_NAME, amenity.getName());
        values.put(AMENITY_KEY_ICON, amenity.getIcon());
        values.put(AMENITY_KEY_TIMESTAMP, amenity.getTimeStamp());
        values.put(AMENITY_KEY_AMENITY_ID, amenity.getAmenityUnitId());
        values.put(AMENITY_KEY_AMENITY_NAME, amenity.getAmenityUnitsDto().getName());
        db.insert(TABLE_AMENITIES,null,values);
        db.close();
    }

    //    Add Amenity List
    public void addAmenities(ArrayList<Amenity> list){
        for (Amenity amenity : list){
            addAmenity(amenity);
        }
    }

    //    get Amenity by Id
    public Amenity getAmenity(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_AMENITIES, new String[] { AMENITY_KEY_ID, AMENITY_KEY_NAME, AMENITY_KEY_ICON,
                        AMENITY_KEY_TIMESTAMP, AMENITY_KEY_AMENITY_ID, AMENITY_KEY_AMENITY_NAME}, AMENITY_KEY_ID + WHERE,
                new String[] { String.valueOf(id) }, null, null, null, null);
        Amenity amenity = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            amenity = new Amenity(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3));
        }
        closeDBnCursor(db,cursor);
        return amenity; // return Amenity
    }

    //    get All amenities in the db
    public ArrayList<Amenity> getAmenities(){
        ArrayList<Amenity> amenityList = new ArrayList<>();
        String selectQuery = SELECT_ALL + TABLE_AMENITIES; // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) { // looping through all rows and adding to list
            do {
                Amenity amenity = new Amenity(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3));
                amenityList.add(amenity); // Adding Amenity to list
            } while (cursor.moveToNext());
        }
        closeDBnCursor(db,cursor);
        return amenityList; // return contact list
    }

    //    Update existing Amenity
    public int updateAmenity(Amenity amenity){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AMENITY_KEY_ID, amenity.getId());
        values.put(AMENITY_KEY_NAME, amenity.getName());
        values.put(AMENITY_KEY_ICON, amenity.getIcon());
        values.put(AMENITY_KEY_TIMESTAMP, amenity.getTimeStamp());
        values.put(AMENITY_KEY_AMENITY_ID, amenity.getAmenityUnitId());
        values.put(AMENITY_KEY_AMENITY_NAME, amenity.getAmenityUnitsDto().getName());

        // updating row
        return db.update(TABLE_AMENITIES, values, AMENITY_KEY_ID + WHERE, new String[] { String.valueOf(amenity.getId()) });
    }

    //    Synchronize Amenity data
    public String  syncAmenities(ArrayList<Amenity> serverList){
        String timeStamp = null;
        for (Amenity amenity : serverList){
            Amenity localAmenity = getAmenity(amenity.getId());
            if (localAmenity != null) {
                if (!amenity.getTimeStamp().equals(localAmenity.getTimeStamp())) {
                    updateAmenity(amenity);
                }
            }else {
                addAmenity(amenity);

            }
            timeStamp = amenity.getTimeStamp();
        }
        closeDB();
        return timeStamp;
    }

    ///////////////////////////   Event Type    ////////////////////////////////

    //    Add Event Type value
    public void addEventType(EventType eventType){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(EVENT_KEY_ID, eventType.getId());
        values.put(EVENT_KEY_NAME, eventType.getName());
        values.put(EVENT_KEY_ICON, eventType.getIcon());
        values.put(EVENT_KEY_TIMESTAMP, eventType.getTimeStamp());
        db.insert(TABLE_EVENT_TYPES,null,values);
        db.close();
    }

    //    Add  Event List
    public void addEventTypes(ArrayList<EventType> list){
        for (EventType event: list){
            addEventType(event);
        }
    }

    //    get Event Type by Id
    public EventType getEventTypeById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_EVENT_TYPES, new String[]{EVENT_KEY_ID, EVENT_KEY_NAME, EVENT_KEY_ICON,
                        EVENT_KEY_TIMESTAMP}, EVENT_KEY_ID + WHERE,
                new String[]{String.valueOf(id)}, null, null, null, null);
        EventType eventType = null;
         if (cursor != null && cursor.getCount()>0) {
            cursor.moveToFirst();
            eventType = new EventType(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3));
        }
        closeDBnCursor(db,cursor);
        return eventType; // return contact
    }

    //    get All Event Types in the db
    public ArrayList<EventType> getEventTypes(){
        ArrayList<EventType> eventList = new ArrayList<>();
        String selectQuery = SELECT_ALL + TABLE_EVENT_TYPES; // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EventType eventType = new EventType(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3));
                eventList.add(eventType); // Adding Amenity to list
            } while (cursor.moveToNext());
        }
        closeDBnCursor(db,cursor);
        return eventList; // return contact list
    }

    //    Get Event Type by Id
    public ArrayList<Integer> getEventTypeIds(){
        ArrayList<Integer> eventIdList = new ArrayList<>();
        String selectQuery = SELECT+EVENT_KEY_ID+FROM + TABLE_EVENT_TYPES; // Select id Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                eventIdList.add(Integer.parseInt(cursor.getString(0))); // Adding Event Type Id to list
            } while (cursor.moveToNext());
        }
        closeDBnCursor(db,cursor);
        return eventIdList; // return Event Types Id
    }

    //    Update existing Event Type
    public int updateEventType(EventType eventType){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EVENT_KEY_ID, eventType.getId());
        values.put(EVENT_KEY_NAME, eventType.getName());
        values.put(EVENT_KEY_ICON, eventType.getIcon());
        values.put(EVENT_KEY_TIMESTAMP, eventType.getTimeStamp());

        // updating row
        return db.update(TABLE_EVENT_TYPES, values, EVENT_KEY_ID + WHERE, new String[] { String.valueOf(eventType.getId()) });
    }

    //    Synchronize Event Types
    public String syncEventTypes(ArrayList<EventType> serverList){
        String timeStamp = null;
        for (EventType eventType : serverList){
            EventType localEventType = getEventTypeById(eventType.getId());
            if (localEventType != null) {
                if (!eventType.getTimeStamp().equals(localEventType.getTimeStamp())) {
                    updateEventType(eventType);
                }
            }else {
                addEventType(eventType);
            }
            timeStamp = eventType.getTimeStamp();
        }
        closeDB();
        return timeStamp;
    }

    //////////////////////////////    Rules     //////////////////////////////////////

    //    Add Rules value
    public void addRule(Rule rule){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(RULE_KEY_ID, rule.getId());
        values.put(RULE_KEY_NAME, rule.getName());
        values.put(RULE_KEY_DISPLAY_NAME, rule.getDisplayName());
        values.put(RULE_KEY_ICON, rule.getIcon());
        values.put(RULE_KEY_TIMESTAMP, rule.getTimeStamp());
        db.insert(TABLE_SPACE_RULES,null,values);
        db.close();
    }

    //    Add  Rules List
    public void addRules(ArrayList<Rule> list){
        for (Rule rule: list){
            addRule(rule);
        }
    }

    //    get All Rules in the db
    public ArrayList<Rule> getRules(){
        ArrayList<Rule> ruleArrayList = new ArrayList<>();
        String selectQuery = SELECT_ALL + TABLE_SPACE_RULES; // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Rule rule = new Rule(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4));
                ruleArrayList.add(rule); // Adding Rule to list
            } while (cursor.moveToNext());
        }
        closeDBnCursor(db,cursor);
        return ruleArrayList; // return rule list
    }

    //    get Rule by Id
    public Rule getRule(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SPACE_RULES, new String[] { RULE_KEY_ID, RULE_KEY_NAME, RULE_KEY_DISPLAY_NAME,
                        RULE_KEY_ICON,RULE_KEY_TIMESTAMP}, RULE_KEY_ID + WHERE,
                new String[] { String.valueOf(id) }, null, null, null, null);
        Rule rule = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            rule = new Rule(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
        }
        closeDBnCursor(db,cursor);
        return rule; // return rule
    }

    //    Update existing Space Rules
    public int updateSpaceRules(Rule spaceRule){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RULE_KEY_ID, spaceRule.getId());
        values.put(RULE_KEY_NAME, spaceRule.getName());
        values.put(RULE_KEY_DISPLAY_NAME, spaceRule.getDisplayName());
        values.put(RULE_KEY_ICON, spaceRule.getIcon());
        values.put(RULE_KEY_TIMESTAMP, spaceRule.getTimeStamp());

        // updating row
        return db.update(TABLE_SPACE_RULES, values, RULE_KEY_ID + WHERE, new String[] { String.valueOf(spaceRule.getId()) });
    }

    //    Synchronize Space Rules
    public String syncSpaceRules(ArrayList<Rule> serverList){
        String timeStamp = null;
        for (Rule rule : serverList){
            Rule localRule = getRule(rule.getId());
            if (localRule != null) {
                if (!rule.getTimeStamp().equals(localRule.getTimeStamp())) {
                    updateSpaceRules(rule);
                }
            }else {
                addRule(rule);
            }
            timeStamp = rule.getTimeStamp();
        }
        closeDB();
        return timeStamp;
    }

    ///////////////////////////     Seating Arrangements    /////////////////////////////////

    //    Add Seating Arrangement value
    public void addSeatingArrangement(SeatingArrangement seatingArrangement){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SEATING_KEY_ID, seatingArrangement.getId());
        values.put(SEATING_KEY_NAME, seatingArrangement.getName());
        values.put(SEATING_KEY_ICON, seatingArrangement.getIcon());
        values.put(SEATING_KEY_TIMESTAMP, seatingArrangement.getTimeStamp());
        db.insert(TABLE_SEATING_ARRANGEMENT,null,values);
        db.close();
    }

    //    Add  Seating Arrangement List
    public void addSeatingList(ArrayList<SeatingArrangement> list){
        for (SeatingArrangement seatingArrangement: list){
            addSeatingArrangement(seatingArrangement);
        }
    }

    //    get Seating Arrangement  by Id
    public SeatingArrangement getSeatingArranmnet(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SEATING_ARRANGEMENT, new String[] { SEATING_KEY_ID, SEATING_KEY_NAME, SEATING_KEY_ICON,
                        SEATING_KEY_TIMESTAMP}, SEATING_KEY_ID + WHERE,
                new String[] { String.valueOf(id) }, null, null, null, null);
        SeatingArrangement seatingArrangement = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            seatingArrangement = new SeatingArrangement(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1), cursor.getString(2), cursor.getString(3));
        }
        closeDBnCursor(db,cursor);
        return seatingArrangement; // return Seating Arrangements
    }

    //    get All Seating Arrangement in the db
    public ArrayList<SeatingArrangement> getSeating(){
        ArrayList<SeatingArrangement> seatingArrangements = new ArrayList<>();
        String selectQuery = SELECT_ALL + TABLE_SEATING_ARRANGEMENT; // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SeatingArrangement seatingArrangement = new SeatingArrangement(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3));
                seatingArrangements.add(seatingArrangement);   // Adding Seating Arrangement to list
            } while (cursor.moveToNext());
        }
        closeDBnCursor(db,cursor);
        return seatingArrangements; // return Seating Arrangement list
    }

    //    Get All seating Arrangements
    public ArrayList<Integer> getSeatingIds(){
        ArrayList<Integer> seatingArrangementIds = new ArrayList<>();
        String selectQuery = SELECT+SEATING_KEY_ID+FROM + TABLE_SEATING_ARRANGEMENT; // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                seatingArrangementIds.add(Integer.parseInt(cursor.getString(0)));
            } while (cursor.moveToNext());
        }
        closeDBnCursor(db,cursor);
        return seatingArrangementIds; // return Seating Arrangement list
    }

    //    Update existing Seating Arrangement
    public int updateSeatingArrangement(SeatingArrangement seatingArrangement){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SEATING_KEY_ID, seatingArrangement.getId());
        values.put(SEATING_KEY_NAME, seatingArrangement.getName());
        values.put(SEATING_KEY_ICON, seatingArrangement.getIcon());
        values.put(SEATING_KEY_TIMESTAMP, seatingArrangement.getTimeStamp());

        // updating row
        return db.update(TABLE_SEATING_ARRANGEMENT, values, SEATING_KEY_ID + WHERE, new String[] { String.valueOf(seatingArrangement.getId()) });
    }

    //    Synchronize Seating Arrangements
    public String syncSeatingArrangement(ArrayList<SeatingArrangement> serverList){
        String timeStamp = null;
        for (SeatingArrangement seatingArrangement : serverList){
            SeatingArrangement localSeating = getSeatingArranmnet(seatingArrangement.getId());
            if (localSeating != null) {
                if (!seatingArrangement.getTimeStamp().equals(localSeating.getTimeStamp())) {
                    updateSeatingArrangement(seatingArrangement);
                }
            }else {
                addSeatingArrangement(seatingArrangement);
            }
            timeStamp = seatingArrangement.getTimeStamp();
        }
        closeDB();
        return timeStamp;
    }

    ///////////////////////////     Cancellation Policy       ////////////////////////////

    //    Add Cancellation Policy value
    public void addCancellationPolicy(CancellationPolicy cancellationPolicy){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CANCEL_KEY_ID, cancellationPolicy.getId());
        values.put(CANCEL_KEY_NAME, cancellationPolicy.getName());
        values.put(CANCEL_KEY_DISCRIPTION, cancellationPolicy.getDescription());
        values.put(CANCEL_REFUND_RATE, cancellationPolicy.getRefundRate());
        values.put(CANCEL_KEY_TIMESTAMP, cancellationPolicy.getTimeStamp());
        db.insert(TABLE_CANCELLATION_POLICY,null,values);
        db.close();
    }

    //    Add  Cancellation Policy List
    public void addCancellationPolicyList(ArrayList<CancellationPolicy> list){
        for (CancellationPolicy cancellationPolicy: list){
            addCancellationPolicy(cancellationPolicy);
        }
    }

    //    get All Cancellation Policies in the db
    public ArrayList<CancellationPolicy> getCancellationPolicy(){
        ArrayList<CancellationPolicy> cancellationPolicies = new ArrayList<>();
        String selectQuery = SELECT_ALL + TABLE_CANCELLATION_POLICY; // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CancellationPolicy cancellationPolicy = new CancellationPolicy(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getFloat(3),cursor.getString(3));
                cancellationPolicies.add(cancellationPolicy); // Adding Cancellation Policy to list
            } while (cursor.moveToNext());
        }
        closeDBnCursor(db,cursor);
        return cancellationPolicies; // return Cancellation policy list
    }

    //    get All Cancellation Policy by id the db
    public CancellationPolicy getCancellationPolicy(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CANCELLATION_POLICY, new String[] { CANCEL_KEY_ID, CANCEL_KEY_NAME,CANCEL_KEY_DISCRIPTION, CANCEL_REFUND_RATE, CANCEL_KEY_TIMESTAMP}, CANCEL_KEY_ID + WHERE,
                new String[] { String.valueOf(id) }, null, null, null, null);
        CancellationPolicy cancellationPolicy = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            cancellationPolicy = new CancellationPolicy(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), Float.valueOf(cursor.getString(3)), cursor.getString(4));
        }
        closeDBnCursor(db,cursor);
        return cancellationPolicy; // return cancellation Policy
    }

    //    Update existing Cancellation Policy
    public int updateCancellationPolicy(CancellationPolicy cancellationPolicy){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CANCEL_KEY_ID, cancellationPolicy.getId());
        values.put(CANCEL_KEY_NAME, cancellationPolicy.getName());
        values.put(CANCEL_KEY_DISCRIPTION, cancellationPolicy.getDescription());
        values.put(CANCEL_REFUND_RATE, cancellationPolicy.getRefundRate());
        values.put(CANCEL_KEY_TIMESTAMP, cancellationPolicy.getTimeStamp());

        // updating row
        return db.update(TABLE_CANCELLATION_POLICY, values, CANCEL_KEY_ID + WHERE, new String[] { String.valueOf(cancellationPolicy.getId()) });
    }

    //    Synchronize Cancellation Policy
    public String syncCancellationPolicy(ArrayList<CancellationPolicy> serverList){
        String timeStamp = null;
        for (CancellationPolicy cancellationPolicy : serverList){
            CancellationPolicy localCanPolicy = getCancellationPolicy(cancellationPolicy.getId());
            if (localCanPolicy != null) {
                if (!cancellationPolicy.getTimeStamp().equals(localCanPolicy.getTimeStamp())) {
                    updateCancellationPolicy(cancellationPolicy);
                }
            }else {
                addCancellationPolicy(cancellationPolicy);
            }
            timeStamp = cancellationPolicy.getTimeStamp();
        }
        closeDB();
        return timeStamp;
    }

    ///////////////////////         Amenity Units      /////////////////////////


    //    Add Amenity Units value
    public void addAmenityUnit(AmenityUnits aminityUnits){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(AMENITY_UNITS_KEY_ID, aminityUnits.getId());
        values.put(AMENITY_UNITS_KEY_NAME, aminityUnits.getName());
        db.insert(TABLE_AMENITY_UNITS,null,values);
        db.close();
    }

    // Add Amenity Units List
    public void addAmenityUnites(ArrayList<AmenityUnits> amenityUnitses) {
        for (AmenityUnits units: amenityUnitses){
            addAmenityUnit(units);
        }
    }

    //    get All Amenity Units in the db
    public ArrayList<AmenityUnits> getAmenityUnits(){
        ArrayList<AmenityUnits> amenityUnitses = new ArrayList<>();
        String selectQuery = SELECT_ALL + TABLE_AMENITY_UNITS;  // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AmenityUnits amenityUnits = new AmenityUnits(Integer.parseInt(cursor.getString(0)),cursor.getString(1));
                amenityUnitses.add(amenityUnits); // Adding Amenity Units to list
            } while (cursor.moveToNext());
        }
        closeDBnCursor(db,cursor);
        return amenityUnitses; // return Amenity Units list
    }

    //    get Amenity Units by Id
    public AmenityUnits getAmenityUnit (int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_AMENITY_UNITS, new String[] { AMENITY_UNITS_KEY_ID, AMENITY_UNITS_KEY_NAME}, AMENITY_UNITS_KEY_ID + WHERE,
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        AmenityUnits amenityUnits = new AmenityUnits(Integer.parseInt(cursor.getString(0)), cursor.getString(1));
        closeDBnCursor(db,cursor);
        return amenityUnits; // return amenity Units
    }

    //////////////////////////////////////////////////////////////////

    public void closeDBnCursor(SQLiteDatabase db, Cursor cursor){
        cursor.close();
        db.close();
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public SQLiteDatabase getDataBase(){
        return this.getReadableDatabase();
    }

}
