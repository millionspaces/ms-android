package com.millionspaces.mobile.entities.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 9/14/17.
 */

public class SpaceBookingDateRequest implements Parcelable {
    private String fromDate;
    private String toDate;

    public SpaceBookingDateRequest(String fromDate, String toDate) {
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    protected SpaceBookingDateRequest(Parcel in) {
        fromDate = in.readString();
        toDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fromDate);
        dest.writeString(toDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SpaceBookingDateRequest> CREATOR = new Creator<SpaceBookingDateRequest>() {
        @Override
        public SpaceBookingDateRequest createFromParcel(Parcel in) {
            return new SpaceBookingDateRequest(in);
        }

        @Override
        public SpaceBookingDateRequest[] newArray(int size) {
            return new SpaceBookingDateRequest[size];
        }
    };

    public String getFromDate() {
        return fromDate;
    }

    public String getToDate() {
        return toDate;
    }
}
