package com.millionspaces.mobile.entities.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 8/21/17.
 */

public class UserSignUpRequest implements Parcelable {
    private String email;
    private String name;
    private String password;
    private String lastName;
    private String address;
    private String mobileNumber;
    private String jobTitle;
    private float commissionPercentage;
    private String imageUrl;
    private String companyName;
    private String companyPhone;
    private String about;
    private String accountHolderName;
    private String accountNumber;
    private String bank;
    private String bankBranch;

    public UserSignUpRequest(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    protected UserSignUpRequest(Parcel in) {
        email = in.readString();
        name = in.readString();
        password = in.readString();
        lastName = in.readString();
        address = in.readString();
        mobileNumber = in.readString();
        jobTitle = in.readString();
        commissionPercentage = in.readFloat();
        imageUrl = in.readString();
        companyName = in.readString();
        companyPhone = in.readString();
        about = in.readString();
        accountHolderName = in.readString();
        accountNumber = in.readString();
        bank = in.readString();
        bankBranch = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(name);
        dest.writeString(password);
        dest.writeString(lastName);
        dest.writeString(address);
        dest.writeString(mobileNumber);
        dest.writeString(jobTitle);
        dest.writeFloat(commissionPercentage);
        dest.writeString(imageUrl);
        dest.writeString(companyName);
        dest.writeString(companyPhone);
        dest.writeString(about);
        dest.writeString(accountHolderName);
        dest.writeString(accountNumber);
        dest.writeString(bank);
        dest.writeString(bankBranch);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserSignUpRequest> CREATOR = new Creator<UserSignUpRequest>() {
        @Override
        public UserSignUpRequest createFromParcel(Parcel in) {
            return new UserSignUpRequest(in);
        }

        @Override
        public UserSignUpRequest[] newArray(int size) {
            return new UserSignUpRequest[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public float getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(float commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }
}
