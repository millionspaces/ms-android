package com.millionspaces.mobile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.entities.request.CancelBookingRequest;
import com.millionspaces.mobile.entities.request.SpaceBookingDateRequest;
import com.millionspaces.mobile.entities.response.BookingReserveResponse;
import com.millionspaces.mobile.entities.response.CancelBookingResponse;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.PaymentConfig;
import com.millionspaces.mobile.network.interfaces.OnSuccessCancelBooking;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.PaymentCountDownTimer;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.CalenderUtils.DATE_FORMAT;
import static com.millionspaces.mobile.utils.Config.APP_CURRENCY_TYPE_PRIFIX;
import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM3;
import static com.millionspaces.mobile.utils.Config.COUNTER_TIME;
import static com.millionspaces.mobile.utils.Config.GUEST_SPACE_BOOKINGS_UPCOMIG;
import static com.millionspaces.mobile.utils.Config.PROMOTION_GUEST_BOOKING;
import static com.millionspaces.mobile.utils.Config.RESERVE_GUEST_BOOKING;
import static com.millionspaces.mobile.utils.Config.RESERVE_GUEST_BOOKING_STRING;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_TENTATIVE_RESERVATION;

public class PaymentActivity extends BaseActivity implements View.OnClickListener, OnItemClickListener, PaymentCountDownTimer.CounterTimerListener, OnSuccessCancelBooking {
    @BindView(R.id.payment_parent)
    CoordinatorLayout mCoordinateLayout;
    @BindView(R.id.radioCardOption)
    RadioButton radioCard;
    @BindView(R.id.radioManualOption)
    RadioButton radioManualOnline;
    @BindView(R.id.radioManualOfflineOption)
    RadioButton radioManualOffline;
    @BindView(R.id.payment_manual_online_container)
    LinearLayout manualOptOnlineContainer;
    @BindView(R.id.payment_card_opt_container)
    LinearLayout cardOptContainer;
    @BindView(R.id.payment_manual_offline_container)
    LinearLayout manualOptOfflineContainer;
    @BindView(R.id.payment_btn_next)
    Button nextButton;
    @BindView(R.id.payment_card_visa)
    ImageView visaButton;
    @BindView(R.id.payment_card_master)
    ImageView masterButton;
//    @BindView(R.id.payment_card_amex)
//    ImageView amexButton;
//    @BindView(R.id.payment_card_disc)
//    ImageView discButton;
    @BindView(R.id.payment_manual_online_ins)
    TextView manualOnTxtIns;
    @BindView(R.id.payment_manual_offline_ins)
    TextView manualOffTxtIns;
    @BindView(R.id.payment_webview)
    WebView webView;
    @BindView(R.id.timer_view)
    TextView timerView;
    @BindView(R.id.scroll_parent)
    NestedScrollView scrollParent;

    @BindView(R.id.payment_reference_id)
    TextView peymentId;
    @BindView(R.id.payment_reference_total_amount)
    TextView totalAmount;
    @BindView(R.id.payment_reference_sapce_name)
    TextView spaceName;
    @BindView(R.id.payment_reference_space_address)
    TextView spaceAddress;
    @BindView(R.id.payment_reference_space_date)
    TextView reservedDate;
    @BindView(R.id.payment_reference_space_time_container)
    LinearLayout reservedTimeContainer;
    @BindView(R.id.payment_infinity_war_promo)
    LinearLayout infinityPromoContainer;
    @BindView(R.id.radioInfinityPromo)
    RadioButton radioInfinityPromo;

    @BindView(R.id.onlineText)
    TextView onlineText;
    @BindView(R.id.offlineText)
    TextView offlineText;

    @BindViews({R.id.payment_card_visa, R.id.payment_card_master})
    List<ImageView> cardButtons;

    private RadioButton selectedButton;
    private String TAG = " Payament : ";
    private BookingReserveResponse reserveObject;
    private String orderId;
    private int orderIdInt;
    private String amount;
    private ProgressDialog progressDialog;
    private Dialog dialog, exitDialog;
    private PaymentCountDownTimer timer;
    private final Handler mHandler = new Handler();
    private int retryAttempt = 0;
    private Handler handler;
    private Runnable runnableCode;

    private static final String ADDITIONAL_CHARACTER = "0";
    private static final int MANUAL_PAYMENT_REQUEST = 232;
    private static final DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        init_view();
    }

    private void init_view() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.payment_title);

        reserveObject = getIntent().getParcelableExtra(BUNDLE_STRING_PARAM);
        orderId =  reserveObject.getOrderId();
        orderIdInt =  reserveObject.getId();
        String nominalAmount = reserveObject.getAmount();
        amount = reserveObject.getAmount().replace(".","");
        settingAmount();

        peymentId.setText(reserveObject.getOrderId());
        totalAmount.setText(APP_CURRENCY_TYPE_PRIFIX + new DecimalFormat("####,###,###.00").format(Float.valueOf(nominalAmount)));
        spaceName.setText(reserveObject.getSpaceName());
        spaceAddress.setText(reserveObject.getAddress());

        LayoutInflater layoutInflater = getLayoutInflater();
        for (SpaceBookingDateRequest date : reserveObject.getReservationTime()){
            View priceView = layoutInflater.inflate(R.layout.list_item_payment_dates, null, false);
            TextView priceTextView = ButterKnife.findById(priceView, R.id.payment_reference_space_time);
            Log.d("Date", date.getFromDate()+" "+date.getToDate());
            String [] dateTimeStart = date.getFromDate().split("T");
            String [] dateTimeEnd = date.getToDate().split("T");
            LocalDate localDate = LocalDate.parse(dateTimeStart[0]);

            reservedDate.setText(localDate.toString("dd MMMM yyyy"));
            priceTextView.setText(LocalTime.parse(dateTimeStart[1].replace("Z","")).toString("hh:mm a")+" - "+
                    LocalTime.parse(dateTimeEnd[1].replace("Z","")).toString("hh:mm a"));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(0, 5, 0, 5);

            reservedTimeContainer.addView(priceView,params);
        }

        if (Float.parseFloat(nominalAmount) != 0) {
            visaButton.setOnClickListener(this);
            masterButton.setOnClickListener(this);
//        amexButton.setOnClickListener(this);
//        discButton.setOnClickListener(this);
            radioCard.setOnClickListener(this);
            radioManualOnline.setOnClickListener(this);
            radioManualOffline.setOnClickListener(this);
            cardOptContainer.setOnClickListener(this);
            manualOptOnlineContainer.setOnClickListener(this);
            manualOptOfflineContainer.setOnClickListener(this);
        }else {
            radioCard.setEnabled(false);
            radioManualOnline.setEnabled(false);
            radioManualOffline.setEnabled(false);
            scrollParent.fullScroll(2);

            infinityPromoContainer.setVisibility(View.VISIBLE);
            radioInfinityPromo.setOnClickListener(this);
            infinityPromoContainer.setOnClickListener(this);
            radioInfinityPromo.setChecked(true);
            visaButton.setColorFilter(ContextCompat.getColor(this,R.color.seperatorColor));
            masterButton.setColorFilter(ContextCompat.getColor(this,R.color.seperatorColor));
            onlineText.setTextColor(ContextCompat.getColor(this,R.color.seperatorColor));
            offlineText.setTextColor(ContextCompat.getColor(this,R.color.seperatorColor));
        }

        nextButton.setOnClickListener(this);

        if (formatter.parseDateTime(reserveObject.getReservationTime().get(0).getFromDate().replace("Z","")).isBefore(new DateTime().plusDays(1))){
            radioManualOffline.setClickable(false);
            radioManualOffline.setEnabled(false);
            radioManualOnline.setClickable(false);
            radioManualOnline.setEnabled(false);
            radioManualOnline.setOnClickListener(null);
            radioManualOffline.setOnClickListener(null);
            manualOptOnlineContainer.setOnClickListener(null);
            manualOptOfflineContainer.setOnClickListener(null);
        }

        timer = new PaymentCountDownTimer(this);
        timer.setTimerDuration(COUNTER_TIME,0,PaymentCountDownTimer.TICK_IN_SECONDS);
        timer.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.radioCardOption:
                toggleOptionCards(visaButton);
                break;
            case R.id.radioManualOption:
                selectManualOnlineOption();
                break;
            case R.id.radioManualOfflineOption:
                selectManualOfflineOption();
                break;
            case R.id.payment_manual_online_container:
                selectManualOnlineOption();
                break;
            case R.id.payment_manual_offline_container:
                selectManualOfflineOption();
                break;
            case R.id.payment_btn_next:
                preceedToPayment();
                break;
            case R.id.payment_card_visa:
                toggleOptionCards(visaButton);
                break;
            case R.id.payment_card_master:
                toggleOptionCards(masterButton);
                break;
//            case R.id.payment_card_amex:
//                toggleOptionCards(amexButton);
//                break;
//            case R.id.payment_card_disc:
//                toggleOptionCards(discButton);
//                break;
            case R.id.dialog_button_positive:
                timer.cancel();
                exitDialog.dismiss();
                navigateActivity(HomeActivity.class);
                finishAffinity();
                break;
            case R.id.payment_infinity_war_promo:
            case R.id.radioInfinityPromo:
                radioInfinityPromo.setChecked(true);
                break;
        }
    }

    @Override
    public void OnClick(int position, View view) {
        switch (view.getId()){
            case R.id.payment_action_button_01:
                switch (position){
                    case 0:  //Success Payment, Navigate to home
                        timer.cancel();
                        finishAffinity();
                        navigateActivity(HomeActivity.class);
                        break;
                    case 1: // Manual cancellation IPG or browser error interruption over 3 times
                        webViewWithPost();
                        break;
                    case 2: // IPG transaction failed and attempting to retry
                        timer.cancel();
                        webViewWithPost();
                        break;
                }
                break;
            case R.id.payment_action_button_02:
                switch (position){
                    case 0:  //Success Payment, Navigate to Booking
                        timer.cancel();
                        finishAffinity();
                        Bundle bundle = new Bundle();
                        bundle.putInt(BUNDLE_INTEGER_PARAM,orderIdInt);
                        bundle.putBoolean(BUNDLE_BOOLEAN_PARAM, false);
                        bundle.putInt(BUNDLE_INTEGER_PARAM2,GUEST_SPACE_BOOKINGS_UPCOMIG);
                        bundle.putBoolean(BUNDLE_BOOLEAN_PARAM2,true);
                        navigateActivityWithExtra(BookingPreviewActivity.class,bundle);
                        break;
                    case 1: // Manual cancellation IPG or browser error interruption over 3 times
                        timer.cancel();
                        finish();
                        break;
                    case 2: // IPG transaction failed and back
                        dialog.dismiss();
                        break;
                }
                break;
        }
        hideProgress();
        if (dialog.isShowing()) dialog.dismiss();
    }

    private void selectManualOfflineOption() {
        radioManualOffline.setChecked(true);
        toggle24PaymentWindow(manualOffTxtIns,manualOnTxtIns,false);
        toggleOptionCards(null);
        deSelectPreviousRadio(radioManualOffline);
    }

    private void selectManualOnlineOption() {
        radioManualOnline.setChecked(true);
        toggle24PaymentWindow(manualOnTxtIns,manualOffTxtIns,false);
        toggleOptionCards(null);
        deSelectPreviousRadio(radioManualOnline);
    }

    private void selectCardOption() {
        radioCard.setChecked(true);
        toggle24PaymentWindow(manualOnTxtIns,manualOffTxtIns,true);
        deSelectPreviousRadio(radioCard);
    }

    private void settingAmount(){
        int characterCount = 12- amount.length();
        for (int i = 0 ; i<characterCount;i++){
            amount = ADDITIONAL_CHARACTER+amount;
        }
    }

    private void toggle24PaymentWindow(TextView textView1, TextView textView2, boolean isVisible){
        if (isVisible){
            textView1.setVisibility(View.GONE);
            textView2.setVisibility(View.GONE);
        }else {
            textView1.setVisibility(View.VISIBLE);
            textView2.setVisibility(View.GONE);
        }
    }

    private void toggleOptionCards(ImageView selectedView) {
        for (ImageView view : cardButtons) {
            view.setBackground(null);
        }
        if (null != selectedView) {
            selectCardOption();
            selectedView.setBackground(ContextCompat.getDrawable(this, R.drawable.background_selected_payment_card));
        }
    }

    private void deSelectPreviousRadio(RadioButton radioButton) {
        if (null != selectedButton && selectedButton != radioButton) {
            selectedButton.setChecked(false);
        }
        selectedButton = radioButton;
    }

    private void preceedToPayment() {
        if (radioCard.isChecked()) {
            setToolbarTitle(getString(R.string.ipg_title));
            webViewWithPost();
        }else if (radioManualOffline.isChecked() || radioManualOnline.isChecked()) {
            timer.cancel();
            Bundle bundle = new Bundle();
            bundle.putString(BUNDLE_STRING_PARAM, totalAmount.getText().toString());
            bundle.putString(BUNDLE_STRING_PARAM2, timerView.getText().toString());
            bundle.putString(BUNDLE_STRING_PARAM3, orderId);
            bundle.putInt(BUNDLE_INTEGER_PARAM, orderIdInt);
            navigateActivityForResultWithExtra(ManualPaymentActivity.class, bundle, MANUAL_PAYMENT_REQUEST);
        }else if (radioInfinityPromo.isChecked()){
            attemptPromotion();
        }else {
            AlertMessageUtils.showSnackBarMessage(getString(R.string.error_select_payment_option), mCoordinateLayout);
        }
    }

    String key = null;

    private String generateHashKey(String orderID) {

        String base64 = null;
        try {
            key = PaymentConfig.PASSWORD
                    + PaymentConfig.MERCHANT_ID
                    + PaymentConfig.ACQUIRED_ID
                    + PaymentConfig.SENTRY_PREFIX + orderID
                    + amount
                    + PaymentConfig.CURRENCY_CODE;
            Log.d(TAG + "Key", key);

            String sha1Key = sha1(key);
            Log.d(TAG + "Sha1", sha1Key);

            String hashKey = hexToString(sha1Key);
            Log.d(TAG + "Hash", hashKey);

            byte[] data = hashKey.getBytes("iso-8859-1");
            base64 = Base64.encodeToString(data, Base64.DEFAULT);
            Log.d(TAG + "Base64", base64);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return base64;
    }


    private String sha1(String s) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(s.getBytes("iso-8859-1"), 0, s.length());
        byte[] bytes = md.digest();
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String tmp = Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1);
            buffer.append(tmp);
        }
        return buffer.toString();
    }

    public static String hexToString(String hex) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hex.length(); i += 2) {
            String str = hex.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }
        return output.toString();
    }


    private void webViewWithPost() {

        if (progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        final android.webkit.CookieManager cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(webView, true);
        }

        WebChromeClient chromeClient = new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                return super.onJsConfirm(view, url, message, result);
            }
        };

        WebViewClient webViewClient = new WebViewClient(){

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.d("Web Start",url);

                if (progressDialog !=null && !progressDialog.isShowing()){
                    progressDialog.show();
                }

                if (url.contains(BuildConfig.PAYMENT_BASE_URL)) {
                    if (progressDialog !=null && progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    String[] params = url.split(Pattern.quote("?"))[1].split("&");
                    Map<String, String> map = new HashMap<>();
                    for (String param : params) {
                        String name = param.split("=")[0];
                        String value = param.split("=")[1];
                        map.put(name, value);
                        Log.d(TAG + "Param", name + " : " + value);
                    }
                    showAlertMessage(map);
                }
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                super.onPageFinished(view, url);
                if (progressDialog != null && progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                Log.d("Web end",url);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                Log.d("Web Load",url);
            }
        };

        String urlParameters = "AcqID=" + PaymentConfig.ACQUIRED_ID +
                "&CaptureFlag=" + PaymentConfig.CAPTURE_FLAG +
                "&MerID=" + PaymentConfig.MERCHANT_ID +
                "&MerRespURL=" + BuildConfig.PAYMENT_CALLBACK_URL +
                "&OrderID=SENTRYORD" + orderId +
                "&PurchaseAmt=" + amount+
                "&PurchaseCurrency=" + PaymentConfig.CURRENCY_CODE +
                "&PurchaseCurrencyExponent=" + PaymentConfig.PURCHASE_CURRENCY_EXPONENT +
                "&Signature=" + generateHashKey(orderId) +
                "&SignatureMethod=" + PaymentConfig.SIGNATURE_METHOD +
                "&Version=" + PaymentConfig.VERSION;

        webView.setVisibility(View.VISIBLE);
        handler = new Handler();
        handler.post(runnableCode);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);

        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setSaveFormData(true);

        webView.setWebViewClient(webViewClient);
        webView.setWebChromeClient(chromeClient);

        try {
            webView.postUrl(BuildConfig.PAYMENT_REQUEST_HNB, urlParameters.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void showAlertMessage(Map<String, String> map) {
        webView.setVisibility(View.GONE);
        if (map.get("status").contains(getString(R.string.error_payment_gateway_browser_interruption)) && retryAttempt<3){
            webViewWithPost();
            retryAttempt++;
        }else {
            dialog = AlertMessageUtils.ShowPaymentStatusDialog(
                    map.get("code").contains("1") ? ContextCompat.getDrawable(this, R.drawable.ic_payment_success) : ContextCompat.getDrawable(this, R.drawable.ic_payment_fail),
                    map.get("code"), map.get("status").replace("+", " "), this, this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){

        }else {
            String[] time = data.getStringExtra("TIME").split(":");
            timer.setTimerDuration(Integer.parseInt(time[0]),Integer.parseInt(time[1]),PaymentCountDownTimer.TICK_IN_SECONDS);
            timer.start();
        }
    }

    @Override
    public void onTick(final String time, long remainingTimeInMilis) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                timerView.setText(time);
            }
        });
    }

    @Override
    public void onFinish() {
        timer.cancel();
        if (dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
        exitDialog = AlertMessageUtils.showConfirmDialog(this,getString(R.string.exceed_timer_alert),getString(R.string.exceed_timer_des),null,getString(R.string.exceed_timer_negative),View.GONE,this);
    }

    @Override
    public void onBackPressed() {
        timer.cancel();
        if (webView.isShown()){
            webView.setVisibility(View.GONE);
        }else {
            navigateActivity(BookingEngineActivity.class);
            finish();
        }
    }


    private void attemptPromotion(){
        CancelBookingRequest bookingRequest = new CancelBookingRequest(orderIdInt,RESERVE_GUEST_BOOKING,RESERVE_GUEST_BOOKING_STRING,PROMOTION_GUEST_BOOKING);
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).cancelGuestBooking(bookingRequest, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure), getResources().getString(R.string.error_network_failure_action), mCoordinateLayout,this,ENF_TENTATIVE_RESERVATION);
        }
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinateLayout);
    }

    @Override
    public void onSuccessCancelBooking(CancelBookingResponse response) {
        hideProgress();
        timer.cancel();
        navigateActivity(PromotionWindowActivity.class);
        finishAffinity();
    }
}
