package com.millionspaces.mobile.utils;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.actions.OnItemSelected;
import com.millionspaces.mobile.adapters.DropDownAdapter;
import com.millionspaces.mobile.adapters.StatusDropDownAdapter;
import com.millionspaces.mobile.entities.response.EventType;
import com.millionspaces.mobile.entities.response.SeatingArrangement;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.millionspaces.mobile.utils.Config.FUNCTION_EVENT_TYPE;
import static com.millionspaces.mobile.utils.Config.FUNCTION_SEATING_TYPE;
import static com.millionspaces.mobile.utils.Config.STATUS_LIST;

/**
 * Created by Kasunka on 6/6/2017.
 * Provide custom popup views activity class
 */

public class PopUpViewUtils {

    // Static method to show pop up drop down in Home page Event Types
    public static PopupWindow showDropDownEventType(final Context context, final OnItemSelected callBack, final View viewParent, final ArrayList<EventType> list, int Type){
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.pop_up_dropdownview, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        popupWindow.setOutsideTouchable(true);

        RecyclerView mRecylerView = (RecyclerView)popupView.findViewById(R.id.list_appbar_item);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        mRecylerView.setNestedScrollingEnabled(true);
        mRecylerView.setLayoutManager(layoutManager);
        mRecylerView.hasFixedSize();

        DropDownAdapter mAdapter = new DropDownAdapter(context, FUNCTION_EVENT_TYPE, list, null);
        mAdapter.notifyDataSetChanged();
        mRecylerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnClick(int position, View view) {
                popupWindow.dismiss();
                callBack.onItemSelected(position,list.get(position).getId(),list.get(position).getName(),2);
            }
        });
        popupWindow.showAtLocation(viewParent, Gravity.CENTER,0,0);
        return popupWindow;
    }


    public static PopupWindow showDropDownsForCalendar (final Context context, final OnItemSelected callBack, final View viewParent, final ArrayList<Integer> list, final int type){
        LayoutInflater layoutInflater = (LayoutInflater)context
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.pop_up_dropdownview_calendar, null);
        final PopupWindow popupWindow = new PopupWindow(popupView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        popupWindow.setOutsideTouchable(true);

        final ArrayList<EventType> eventTypes = new ArrayList<>();
        final ArrayList<SeatingArrangement> seatingArrangements = new ArrayList<>();

        TextView title = (TextView)popupView.findViewById(R.id.list_calender_selection_title);
        RecyclerView mRecylerView = (RecyclerView)popupView.findViewById(R.id.list_appbar_item);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        mRecylerView.setNestedScrollingEnabled(true);
        mRecylerView.setLayoutManager(layoutManager);
        mRecylerView.hasFixedSize();

        DataBaseUtils dataBaseUtils = new DataBaseUtils(context);

        switch (type){
            case FUNCTION_EVENT_TYPE:
                title.setText(context.getString(R.string.calendar_selection_title_eventtype));
                for (Integer i :list){
                    eventTypes.add(dataBaseUtils.getEventTypeById(i));
                }
                break;
            case FUNCTION_SEATING_TYPE:
                title.setText(context.getString(R.string.calendar_selection_title_seating));
                for (Integer i :list){
                    seatingArrangements.add(dataBaseUtils.getSeatingArranmnet(i));
                }
                break;
        }

        DropDownAdapter mAdapter = new DropDownAdapter(context,type,eventTypes,seatingArrangements);
        mAdapter.notifyDataSetChanged();
        mRecylerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnClick(int position, View view) {
                popupWindow.dismiss();
                if (type == FUNCTION_EVENT_TYPE ) {
                    callBack.onItemSelected(position, eventTypes.get(position).getId(), eventTypes.get(position).getName().toUpperCase(), FUNCTION_EVENT_TYPE);
                }else {
                    callBack.onItemSelected(position, seatingArrangements.get(position).getId(), seatingArrangements.get(position).getName().toUpperCase(), FUNCTION_SEATING_TYPE);
                }
            }
        });

        popupWindow.showAsDropDown(viewParent,10,0);
        return popupWindow;
    }

    public static PopupWindow showStatusDialog(Context context, final View viewParent){
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.pop_up_dropdownview, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        popupWindow.setOutsideTouchable(true);
        RecyclerView mRecylerView = (RecyclerView)popupView.findViewById(R.id.list_appbar_item);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        mRecylerView.setNestedScrollingEnabled(true);
        mRecylerView.setLayoutManager(layoutManager);
        mRecylerView.hasFixedSize();

        StatusDropDownAdapter mAdapter = new StatusDropDownAdapter(context);
        mAdapter.notifyDataSetChanged();
        mRecylerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnClick(int position, View view) {
                popupWindow.dismiss();
                Log.d("STatus", STATUS_LIST.get(position));
            }
        });
        popupWindow.showAtLocation(viewParent, Gravity.CENTER,0,0);
        return popupWindow;
    }


    public static PopupWindow showNumberPicker(final Context context, final View viewParent){
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.pop_up_dropdown_number_picker, null);
        final PopupWindow popupWindow = new PopupWindow(popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        EditText editText = (EditText)popupView.findViewById(R.id.calendar_extra_amenity_amount);

        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.showAsDropDown(viewParent,0,-120);

        editText.requestLayout();
        return popupWindow;
    }
}
