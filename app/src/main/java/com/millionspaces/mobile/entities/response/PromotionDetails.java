package com.millionspaces.mobile.entities.response;

import java.util.ArrayList;

/**
 * Created by kasunka on 1/10/18.
 */

public class PromotionDetails {
    private int id;
    private ArrayList<Integer> spaces;
    private int toAllSpace;
    private String promoCode;
    private int discount;
    private int isFlatDiscount;
    private long startDate;
    private long endDate;
    private long createDate;

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getIsFlatDiscount() {
        return isFlatDiscount;
    }

    public void setIsFlatDiscount(int isFlatDiscount) {
        this.isFlatDiscount = isFlatDiscount;
    }
}
