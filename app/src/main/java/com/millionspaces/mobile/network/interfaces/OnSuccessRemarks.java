package com.millionspaces.mobile.network.interfaces;

/**
 * Created by kasunka on 11/17/17.
 */

public interface OnSuccessRemarks extends OnErrorResponse {
    void onSuccessRemarks(boolean isAdded);
}
