package com.millionspaces.mobile.entities.response;

/**
 * Created by kasunka on 1/10/18.
 */

public class PromoCodeResponse {
    private String message;
    private PromotionDetails promoDetails;
    private int status;

    public String getMessage() {
        return message;
    }

    public PromotionDetails getPromoDetails() {
        return promoDetails;
    }

    public int getStatus() {
        return status;
    }
}
