package com.millionspaces.mobile.network.interfaces;

/**
 * Created by kasunka on 6/11/17.
 * common Error Handling method
 */

public interface OnErrorResponse {
    void onErrorResponse(String error);
}
