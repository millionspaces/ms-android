package com.millionspaces.mobile.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.millionspaces.mobile.BookingEngineActivity;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnAmenitySelected;
import com.millionspaces.mobile.actions.OnTimeSelected;
import com.millionspaces.mobile.adapters.calendar.ExtraAmenityAdapter;
import com.millionspaces.mobile.entities.response.ExtraAmenity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by kasunka on 8/30/17.
 */

public class ExtraAmenityFragment extends Fragment{
    private ArrayList<ExtraAmenity> extraAmenity;
    private ExtraAmenityAdapter mAdapter;
    private Unbinder unbinder;

    private OnAmenitySelected onAmenitySelected;
    private OnTimeSelected onTimeSelected;

    @BindView(R.id.extaamenity_null_msg)
    TextView nullMsg;
    @BindView(R.id.extaamenity_recyclerview)
    RecyclerView extarRecyclerView;

    private static final String PARAMS = "amenityArray";


    public static Fragment newInstance(ArrayList<ExtraAmenity> extraAmenity) {
        ExtraAmenityFragment fragment = new ExtraAmenityFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(PARAMS,extraAmenity);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof OnAmenitySelected) {
                onAmenitySelected = (OnAmenitySelected) context;
            }
        } catch (ClassCastException e) {
            throw new ClassCastException();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_extra_amenity, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        extraAmenity = args.getParcelableArrayList(PARAMS);
        ((BookingEngineActivity) getActivity()).setUpUI(view, this);
        if (extraAmenity != null && !extraAmenity.isEmpty()){
            extarRecyclerView.setVisibility(View.VISIBLE);
            extarRecyclerView.setHasFixedSize(true);
            extarRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mAdapter = new ExtraAmenityAdapter(getContext(),extraAmenity);
            extarRecyclerView.setAdapter(mAdapter);
            extarRecyclerView.setNestedScrollingEnabled(true);
            updateSelectedHours();
            setActionEvents();
        }else {
            nullMsg.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setActionEvents() {
        mAdapter.setOnItemClickListner(onAmenitySelected);
    }

    public void updateSelectedHours(){
        mAdapter.setNumberOfHours(((BookingEngineActivity)getActivity()).spaceTotalHours);
    }
}
