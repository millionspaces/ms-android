package com.millionspaces.mobile.actions;

/**
 * Created by kasunka on 11/7/17.
 */

public interface OnConnectionErrorCallBack {
    void onConnectionErrorCallBack(int requestCode);
}
