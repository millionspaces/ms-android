package com.millionspaces.mobile.adapters.calendar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnAmenitySelected;
import com.millionspaces.mobile.entities.response.ExtraAmenity;
import com.millionspaces.mobile.utils.DataBaseUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kasunka on 9/13/17.
 */

public class ExtraAmenityAdapter extends RecyclerView.Adapter<ExtraAmenityAdapter.ViewHolderExtra> {

    private ArrayList<ExtraAmenity> extraAmenities;
    private DataBaseUtils dataBaseUtils;
    private Context mContext;
    private float totalAmenityAmount = 0;
    private int numberOfHours = 0;
    private boolean onBind;

    private OnAmenitySelected onAmenitySelected;

    public ExtraAmenityAdapter(Context context, ArrayList<ExtraAmenity> extraAmenity) {
        this.extraAmenities = extraAmenity;
        Collections.sort(extraAmenities, new Comparator<ExtraAmenity>() {
            @Override
            public int compare(ExtraAmenity o1, ExtraAmenity o2) {
                return Double.compare(o2.getAmenityUnit(), o1.getAmenityUnit());
            }
        });

        this.mContext = context;
        dataBaseUtils = new DataBaseUtils(context);
    }

    @Override
    public ViewHolderExtra onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_calendar_extras, parent, false);
        return new ViewHolderExtra(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderExtra holder, int position) {
        onBind = true;
        ExtraAmenity extraAmenity = extraAmenities.get(position);
        holder.amenityName.setText(extraAmenity.getName());
        Glide.with(mContext).load(dataBaseUtils.getAmenity(extraAmenity.getAmenityId()).getIcon()).into(holder.icon);
        holder.extraRate.setText("LKR " + new DecimalFormat("####,###,###").format(extraAmenity.getExtraRate()) + " "
                + dataBaseUtils.getAmenityUnit(extraAmenity.getAmenityUnit()).getName());
        holder.inputAmount.setText(extraAmenity.getNumberOfAmenityCount() != 0 ? String.valueOf(extraAmenity.getNumberOfAmenityCount()) : "0");
        holder.totalAmmount.setText(extraAmenity.getNumberOfAmenityCount() != 0 ? "LKR " + new DecimalFormat("####,###,###").
                format(extraAmenity.getNumberOfAmenityCount() * extraAmenity.getExtraRate()) : "LKR 0");
        totalAmenityAmount += extraAmenity.getNumberOfAmenityCount() * extraAmenity.getExtraRate();
        if (extraAmenity.getAmenityUnit() == 1) {
            holder.buttonContainer.setVisibility(View.GONE);
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setChecked(extraAmenity.isPerHourAmenityChecked());
            holder.totalAmmount.setText(numberOfHours != 0 ? "LKR " + new DecimalFormat("####,###,###").
                    format(numberOfHours * extraAmenity.getExtraRate()) : "LKR 0");
        }
        onBind = false;
    }

    @Override
    public int getItemCount() {
        return extraAmenities.size();
    }

    public void setOnItemClickListner(OnAmenitySelected onAmenitySelected) {
        this.onAmenitySelected = onAmenitySelected;
    }

    public void setNumberOfHours(int hours) {
        this.numberOfHours = hours;
    }

    public class ViewHolderExtra extends RecyclerView.ViewHolder implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, TextWatcher {
        @BindView(R.id.calender_Extra_amenity_icon)
        ImageView icon;
        @BindView(R.id.calender_Extra_amenity_title)
        TextView amenityName;
        @BindView(R.id.calender_extra_rate)
        TextView extraRate;
        @BindView(R.id.calender_extra_button_minus)
        ImageView minus;
        @BindView(R.id.calender_extra_button_plus)
        ImageView plus;
        @BindView(R.id.calender_extra_amount)
        EditText inputAmount;
        @BindView(R.id.calender_extra_addition_amount)
        TextView totalAmmount;
        @BindView(R.id.calender_extra_button_container)
        RelativeLayout buttonContainer;
        @BindView(R.id.calender_extra_checkbox_container)
        CheckBox checkBox;

        private int numberOfAmenity = 0;

        public ViewHolderExtra(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            minus.setOnClickListener(this);
            plus.setOnClickListener(this);
            checkBox.setOnCheckedChangeListener(this);
            inputAmount.addTextChangedListener(this);
        }

        @Override
        public void onClick(View v) {
            numberOfAmenity = Integer.valueOf(inputAmount.getText().toString());
            switch (v.getId()) {
                case R.id.calender_extra_button_minus:
                    inputAmount.setText(String.valueOf(numberOfAmenity == 0 ? 0 : numberOfAmenity - 1));
                    break;
                case R.id.calender_extra_button_plus:
                    inputAmount.setText(String.valueOf(numberOfAmenity + 1));
                    break;
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (numberOfHours != 0) {
                totalAmenityAmount += isChecked ? +extraAmenities.get(getAdapterPosition()).getExtraRate() * numberOfHours : -extraAmenities.get(getAdapterPosition()).getExtraRate() * numberOfHours;
                extraAmenities.get(getAdapterPosition()).setPerHourAmenityChecked(isChecked);
                onAmenitySelected.onPerHourAmenitySelected(extraAmenities.get(getAdapterPosition()).getAmenityId(), 1, isChecked, extraAmenities.get(getAdapterPosition()).getExtraRate());
            } else {
                buttonView.setChecked(false);
                onAmenitySelected.onNullPerHourAmenityChecked();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            Log.d("char start", s.toString());
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Log.d("char end", s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.d("OnChenged", s.toString());
            if (!onBind) {
                numberOfAmenity = Integer.valueOf(inputAmount.getText().toString().isEmpty() ? "0" : inputAmount.getText().toString());
                extraAmenities.get(getAdapterPosition()).setNumberOfAmenityCount(numberOfAmenity);
                totalAmmount.setText("LKR " + new DecimalFormat("####,###,###").format(numberOfAmenity * extraAmenities.get(getAdapterPosition()).getExtraRate()));
                setTotalAmenityAmount();
            }
        }

        private void setTotalAmenityAmount() {
            totalAmenityAmount = 0;
            for (int i = 0; i < extraAmenities.size(); i++) {
                ExtraAmenity extraAmenity = extraAmenities.get(i);
                if (extraAmenity.getAmenityUnit() != 1) {
                    totalAmenityAmount += extraAmenity.getExtraRate() * extraAmenity.getNumberOfAmenityCount();
                } else if (extraAmenity.isPerHourAmenityChecked()) {
                    totalAmenityAmount += extraAmenity.getExtraRate() * numberOfHours;
                }
                onAmenitySelected.onAmenitySelected(extraAmenities.get(getAdapterPosition()).getAmenityId(), numberOfAmenity, numberOfAmenity > 0 ? true : false, totalAmenityAmount);
            }
        }
    }
}
