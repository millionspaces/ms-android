package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 7/21/17.
 */

public class ExtraAmenity implements Parcelable{
    private int amenityId;
    private String name;
    private int amenityUnit;
    private float extraRate;
    private int numberOfAmenityCount;
    private boolean isPerHourAmenityChecked;
    private float totalAmenityRate;


    protected ExtraAmenity(Parcel in) {
        amenityId = in.readInt();
        name = in.readString();
        amenityUnit = in.readInt();
        extraRate = in.readFloat();
        numberOfAmenityCount = in.readInt();
        isPerHourAmenityChecked = in.readByte() != 0;
        totalAmenityRate = in.readFloat();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(amenityId);
        dest.writeString(name);
        dest.writeInt(amenityUnit);
        dest.writeFloat(extraRate);
        dest.writeInt(numberOfAmenityCount);
        dest.writeByte((byte) (isPerHourAmenityChecked ? 1 : 0));
        dest.writeFloat(totalAmenityRate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ExtraAmenity> CREATOR = new Creator<ExtraAmenity>() {
        @Override
        public ExtraAmenity createFromParcel(Parcel in) {
            return new ExtraAmenity(in);
        }

        @Override
        public ExtraAmenity[] newArray(int size) {
            return new ExtraAmenity[size];
        }
    };

    public int getAmenityId() {
        return amenityId;
    }

    public void setAmenityId(int amenityId) {
        this.amenityId = amenityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmenityUnit() {
        return amenityUnit;
    }

    public void setAmenityUnit(int amenityUnit) {
        this.amenityUnit = amenityUnit;
    }

    public float getExtraRate() {
        return extraRate;
    }

    public void setExtraRate(int extraRate) {
        this.extraRate = extraRate;
    }

    public int getNumberOfAmenityCount() {
        return numberOfAmenityCount;
    }

    public void setNumberOfAmenityCount(int numberOfAmenityCount) {
        this.numberOfAmenityCount = numberOfAmenityCount;
    }

    public float getTotalAmenityRate() {
        return totalAmenityRate;
    }

    public void setTotalAmenityRate(float totalAmenityRate) {
        this.totalAmenityRate = totalAmenityRate;
    }

    public boolean isPerHourAmenityChecked() {
        return isPerHourAmenityChecked;
    }

    public void setPerHourAmenityChecked(boolean perHourAmenityChecked) {
        isPerHourAmenityChecked = perHourAmenityChecked;
    }
}
