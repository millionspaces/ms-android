package com.millionspaces.mobile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.millionspaces.mobile.actions.OnAddedReview;
import com.millionspaces.mobile.actions.OnCancelBooking;
import com.millionspaces.mobile.adapters.ItemSelectionAdapter;
import com.millionspaces.mobile.entities.request.BookingDetailsRequest;
import com.millionspaces.mobile.entities.request.BookingRemarksRequest;
import com.millionspaces.mobile.entities.request.CancelBookingRequest;
import com.millionspaces.mobile.entities.request.SpaceReviewRequest;
import com.millionspaces.mobile.entities.response.Amenity;
import com.millionspaces.mobile.entities.response.Booking;
import com.millionspaces.mobile.entities.response.CancelBookingResponse;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessCancelBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessRemarks;
import com.millionspaces.mobile.network.interfaces.OnSuccessReview;
import com.millionspaces.mobile.utils.AlertMessageUtils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.CalenderUtils.DATE_FORMAT;
import static com.millionspaces.mobile.utils.Config.APP_CURRENCY_NODECIMAL_FORMAT;
import static com.millionspaces.mobile.utils.Config.APP_CURRENCY_TYPE_PRIFIX;
import static com.millionspaces.mobile.utils.Config.BOOKING_STATUS_CONFIRMED;
import static com.millionspaces.mobile.utils.Config.BOOKING_STATUS_PAYMENT_DONE;
import static com.millionspaces.mobile.utils.Config.BOOKING_STATUS_PENDING_PAYMENT;
import static com.millionspaces.mobile.utils.Config.BOOKING_STATUS_PENDING_PAYMENT_ID;
import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.CANCEL_GUEST_BOOKING;
import static com.millionspaces.mobile.utils.Config.ERROR_NETWORK_FAILURE;
import static com.millionspaces.mobile.utils.Config.GUEST_SPACE_BOOKINGS_PAST;
import static com.millionspaces.mobile.utils.Config.GUEST_SPACE_BOOKINGS_UPCOMIG;
import static com.millionspaces.mobile.utils.Config.HOST_SPACE_BOOKINGS_PAST;
import static com.millionspaces.mobile.utils.Config.HOST_SPACE_BOOKINGS_UPCOMIG;
import static com.millionspaces.mobile.utils.Config.SPACE_AMINITY_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.getStatusColor;
import static com.millionspaces.mobile.utils.Config.isNumeric;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_GUEST_BOOKING;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_GUEST_UPDATE_REMARKS;

public class BookingPreviewActivity extends BaseActivity implements OnSuccessBooking, View.OnClickListener,OnAddedReview, OnSuccessReview,OnSuccessRemarks ,OnCancelBooking, OnSuccessCancelBooking {
    @BindView(R.id.booking_preview_parent)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.bookingpre_space_title)
    TextView spaceTitle;
    @BindView(R.id.bookingpre_space_venue)
    TextView spaceAddress;
    @BindView(R.id.bookingpre_reserved_person)
    TextView reservedPerson;
    @BindView(R.id.bookingpre_reserved_person_cnt)
    TextView reservedPersonCnt;
    @BindView(R.id.bookingpre_reserved_person_cnt_container)
    LinearLayout reservedPersonCntContainer;
    @BindView(R.id.bookingpre_reserved_person_email)
    TextView reservedPersonEmail;
    @BindView(R.id.bookingpre_reserved_person_emal_container)
    LinearLayout reservedPersonEmailContainer;
    @BindView(R.id.bookingpre_booking_date)
    TextView reservedDate;
    @BindView(R.id.bookingpre_booking_time_text)
    TextView reservationTimeTitle;
    @BindView(R.id.bookingpre_booking_date_text)
    TextView reservationDateTitle;
    @BindView(R.id.bookingpre_booking_time_container)
    LinearLayout reservedTimeContainer;
    @BindView(R.id.bookingpre_booking_made)
    TextView reservedMade;
    @BindView(R.id.bookingpre_booking_amount)
    TextView reservedAmount;
    @BindView(R.id.bookingpre_booking_amount_container)
    LinearLayout reservedAmountContainer;
    @BindView(R.id.bookingpre_booking_event)
    TextView reservedEvent;
    @BindView(R.id.bookingpre_booking_seating)
    TextView reservedSeating;
    @BindView(R.id.bookingpre_booking_seating_container)
    LinearLayout reservedSeatingContainer;
    @BindView(R.id.bookingpre_booking_gstcount)
    TextView reservedGuestCount;
    @BindView(R.id.bookingpre_booking_menu_container)
    LinearLayout menuContainer;
    @BindView(R.id.bookingpre_booking_menu_menu)
    TextView menuName;
    @BindView(R.id.bookingpre_booking_review_container)
    LinearLayout reviewContainer;
    @BindView(R.id.bookingpre_booking_review_title)
    TextView reviewTitle;
    @BindView(R.id.bookingpre_booking_review)
    TextView review;
    @BindView(R.id.bookingpre_booking_review_service)
    AppCompatRatingBar serviceRating;
    @BindView(R.id.bookingpre_booking_review_clean)
    AppCompatRatingBar cleanRating;
    @BindView(R.id.bookingpre_booking_review_value)
    AppCompatRatingBar valueRating;
    @BindView(R.id.bookingpre_booking_amenity_container)
    LinearLayout amenityContainer;
    @BindView(R.id.bookingpre_booking_com_amenity_container)
    LinearLayout compAmenityContainer;
    @BindView(R.id.bookingpre_booking_cha_amenity_container)
    LinearLayout charAmenityContainer;
    @BindView(R.id.bookingpre_booking_com_amenity_list)
    RecyclerView compAmenityRecyclerView;
    @BindView(R.id.bookingpre_booking_cha_amenity_list)
    RecyclerView charAmenityRecyclerView;
    @BindView(R.id.bookingpre_booking_cancel_title)
    TextView cancelTitle;
    @BindView(R.id.bookingpre_booking_cancel_desription)
    TextView cancelDescription;
    @BindView(R.id.bookingpre_card_status)
    TextView bookingStatus;
    @BindView(R.id.booking_space_make)
    ImageView bookingMadeStatus;
    @BindView(R.id.bookingpre_booking_remarks)
    EditText bookingRemarksView;
    @BindView(R.id.bookingpre_booking_remarks_container)
    LinearLayout remarksContainer;
    @BindView(R.id.bookingpre_booking_action)
    Button actionButton;

    private int bookingId,bookingOwnerTag = 0;
    private Dialog reviewDialog, cancelDialog,alertDialog;
    private Booking booking;
    private CancelBookingRequest cancelRequest;
    private boolean isManual, isSuccessPayment;
    private DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT);
    Intent returnIntent = new Intent();

    private View.OnClickListener cancelDialogListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_preview);
        ButterKnife.bind(this);
        init_views();
        getBooking();
    }

    private void init_views() {
        init_toolbar();
        setToolbarTitle(getString(R.string.booking_preview_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        bookingId = getIntent().getExtras().getInt(BUNDLE_INTEGER_PARAM);
        isManual = getIntent().getExtras().getBoolean(BUNDLE_BOOLEAN_PARAM);
        bookingOwnerTag = getIntent().getExtras().getInt(BUNDLE_INTEGER_PARAM2);
        isSuccessPayment = getIntent().getExtras().getBoolean(BUNDLE_BOOLEAN_PARAM2);
        init_action();
    }

    private void init_action() {
        actionButton.setOnClickListener(this);

        cancelDialogListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.cancel_booking_card_img:
                        Bundle bundle = new Bundle();
                        bundle.putInt(BUNDLE_INTEGER_PARAM,booking.getSpace().getId());
                        bundle.putString(BUNDLE_STRING_PARAM,"[1]");
                        bundle.putBoolean(BUNDLE_BOOLEAN_PARAM,true);
                        navigateActivityWithExtra(SpaceProfileActivity.class,bundle);
                        break;
                    case R.id.dialog_button_negative:
                        alertDialog.dismiss();
                        break;
                    case R.id.dialog_button_positive:
                        alertDialog.dismiss();
                        cancelBooking();
                        break;
                }
            }
        };
    }

    private void cancelBooking() {
        if (NetworkManager.getInstance().checkInternetConenction(BookingPreviewActivity.this)) {
            showProgress();
            MillionSpaceService.getInstance(BookingPreviewActivity.this).cancelGuestBooking(cancelRequest, BookingPreviewActivity.this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,BookingPreviewActivity.this,ERROR_NETWORK_FAILURE);
        }
    }

    private void getBooking() {
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            showProgress();
            MillionSpaceService.getInstance(this).getUserBooking(new BookingDetailsRequest(bookingId,isManual),this);
        }else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ERROR_NETWORK_FAILURE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (bookingOwnerTag){
            case GUEST_SPACE_BOOKINGS_PAST:
                reviewDialog = AlertMessageUtils.showReviewScreen(this,bookingId,this);
                break;
            case GUEST_SPACE_BOOKINGS_UPCOMIG:
                if (booking.getReservationStatus().getId() != BOOKING_STATUS_PENDING_PAYMENT_ID) {
                    cancelDialog = AlertMessageUtils.showCancelScreen(this, booking, cancelDialogListener, this);
                }else {
                    this.cancelRequest = new CancelBookingRequest(booking.getId(),CANCEL_GUEST_BOOKING,0);
                    alertDialog = AlertMessageUtils.showConfirmDialog(this,getString(R.string.cancel_booking_confirm_title),getString(R.string.cancel_booking_confirm_des),
                            getString(R.string.cancel_booking_confirm_negative), getString(R.string.cancel_booking_confirm_positive),View.VISIBLE,cancelDialogListener);
                }
                break;
            case HOST_SPACE_BOOKINGS_PAST:
                updateBookingRemarks();
                break;
            case HOST_SPACE_BOOKINGS_UPCOMIG:
                updateBookingRemarks();
                break;
        }
    }

    @Override
    public void onConnectionErrorCallBack(int requestCode) {
        switch (requestCode){
            case ENF_GUEST_BOOKING:
                getBooking();
                break;
            case ENF_GUEST_UPDATE_REMARKS:
                updateBookingRemarks();
                break;
        }
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinatorLayout);
    }

    @Override
    public void onSuccessBooking(Booking value) {
        hideProgress();
        mCoordinatorLayout.setVisibility(View.VISIBLE);
        init_values(value);
    }

    @Override
    public void onAddedReview(SpaceReviewRequest reviewRequest) {
        if (reviewDialog.isShowing()){
            reviewDialog.dismiss();
        }
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).setReview(reviewRequest, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ERROR_NETWORK_FAILURE);
        }
    }

    @Override
    public void onSuccessReview(String value) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(value, mCoordinatorLayout);
        returnIntent.putExtra("TYPE", 0);
        setResult(Activity.RESULT_OK,returnIntent);
        actionButton.setVisibility(View.GONE);
    }

    @Override
    public void onCancelBooking(CancelBookingRequest request) {
        if (cancelDialog.isShowing()){
            cancelDialog.dismiss();
        }
        this.cancelRequest = request;
        alertDialog = AlertMessageUtils.showConfirmDialog(this,getString(R.string.cancel_booking_confirm_title),getString(R.string.cancel_booking_confirm_des),
                getString(R.string.cancel_booking_confirm_negative), getString(R.string.cancel_booking_confirm_positive),View.VISIBLE,cancelDialogListener);
    }

    @Override
    public void onSuccessCancelBooking(CancelBookingResponse responce) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(getString(R.string.success_cancel_booking), mCoordinatorLayout);
        returnIntent.putExtra("TYPE", 1);
        setResult(Activity.RESULT_OK,returnIntent);
        actionButton.setVisibility(View.GONE);
    }

    private void init_values(Booking value) {
        this.booking = value;
        mCoordinatorLayout.setVisibility(View.VISIBLE);
        spaceTitle.setText(value.getSpace().getName());
        if (value.getReservationStatus()!= null){
            bookingStatus.setText(value.getReservationStatus().getLabel());
            bookingStatus.setBackgroundColor(getStatusColor(value.getReservationStatus().getName(),this));
        }else {
            bookingStatus.setVisibility(View.GONE);
        }

        if (!value.isManual() && bookingOwnerTag != GUEST_SPACE_BOOKINGS_PAST){
            bookingMadeStatus.setVisibility(View.VISIBLE);
        }
        spaceAddress.setText(value.getSpace().getAddressLine1()+", "+value.getSpace().getAddressLine2());
        reservedPerson.setText(value.getUser().getName());
        reservedPersonCntContainer.setVisibility(value.getUser().getMobile()!= null && !TextUtils.isEmpty(value.getUser().getMobile())? View.VISIBLE:View.GONE);
        reservedPersonCnt.setText(value.getUser().getMobile());
        reservedPersonEmail.setText(value.getUser().getEmail());
        reservedPersonEmailContainer.setVisibility(value.getUser().getEmail() != null? View.VISIBLE:View.GONE);
        if (value.getBookingCharge() != null && isNumeric(value.getBookingCharge())) {
            reservedAmount.setText(APP_CURRENCY_TYPE_PRIFIX + new DecimalFormat(APP_CURRENCY_NODECIMAL_FORMAT).format(Float.valueOf(value.getBookingCharge())));
        }else {
            reservedAmountContainer.setVisibility(View.GONE);
        }


        if (value.getDates() != null && !value.getDates().isEmpty()) {
            DateTime startDate = formatter.parseDateTime(value.getDates().get(0).getFromDate().replace("Z", ""));
            DateTime endDate = formatter.parseDateTime(value.getDates().get(0).getToDate().replace("Z", ""));

            if (value.getDates().size() > 1) {
                reservedDate.setText(startDate.toString("dd MMMM yyyy"));
                for (int i = 0; i<value.getDates().size(); i++){
                    startDate = formatter.parseDateTime(value.getDates().get(i).getFromDate().replace("Z", ""));
                    endDate = formatter.parseDateTime(value.getDates().get(i).getToDate().replace("Z", ""));
                    inflateView(startDate.toString("hh.mm a") + " - " + endDate.toString("hh.mm a"));
                }
            }else {
                if (startDate.compareTo(endDate) == 0) {
                    reservedDate.setText(startDate.toString("dd MMMM yyyy"));
                    inflateView(startDate.toString("hh.mm a") + " - " + endDate.toString("hh.mm a"));
                }else {
                    reservedDate.setText(startDate.toString("dd MMM yyyy, hh.mm a"));
                    reservationDateTitle.setText(getString(R.string.booking_preview_title_reservation_date_start));
                    reservationTimeTitle.setText(getString(R.string.booking_preview_title_reservation_date_end));
                    inflateView(endDate.toString("dd MMM yyyy, hh.mm a"));
                }
            }
//            reservedMade.setText(formatter.parseDateTime(value.getBookedDate().replace("Z","")).toString("dd MMMM yyy"));
            reservedMade.setText(value.getBookedDate().replace("T"," ").replace("Z"," ")); //TODO: uncomment above code after backend refactor
        }
        reservedEvent.setText(value.getEventTypedetail() != null?value.getEventTypedetail().getName():"N/A");
        if (value.getSeatingArrangement() != null) {
            reservedSeating.setText(value.getSeatingArrangement().getName());
        }else {
            reservedSeatingContainer.setVisibility(View.GONE);
        }
        reservedGuestCount.setText(String.valueOf(value.getGuestCount()));

        if (value.getMenu() != null && !value.getMenu().isEmpty()){
            menuContainer.setVisibility(View.VISIBLE);
            menuName.setText(getString(R.string.booking_preview_title_reservation_menu_name,value.getMenu().get(0).getMenuId()));
        }else {
            menuContainer.setVisibility(View.GONE);
        }

        if (value.isReviewed() && value.getReview() != null){
            String rating[] = value.getReview().getRate().split("\\|");
            reviewContainer.setVisibility(View.VISIBLE);
            reviewTitle.setText(value.getReview().getTitle());
            serviceRating.setRating(Float.valueOf(rating[1]));
            cleanRating.setRating(Float.valueOf(rating[2]));
            valueRating.setRating(Float.valueOf(rating[3]));
            review.setText(value.getReview().getDescription());
        }else {
            reviewContainer.setVisibility(View.GONE);
        }

        if (value.getSpace().getAmenities() != null && value.getExtraAmenityList() != null){
            amenityContainer.setVisibility(View.VISIBLE);
            if (!value.getSpace().getAmenities().isEmpty()){
                compAmenityContainer.setVisibility(View.VISIBLE);
                compAmenityRecyclerView.setHasFixedSize(true);
                compAmenityRecyclerView.setNestedScrollingEnabled(true);
                compAmenityRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
                ArrayList<Integer> intAmenityList = new ArrayList<>();
                for(Amenity amenity:value.getSpace().getAmenities()){
                    intAmenityList.add(amenity.getId());
                }
                compAmenityRecyclerView.setAdapter(new ItemSelectionAdapter(SPACE_AMINITY_TYPE_LIST,intAmenityList,-1,this));
            }else {
                compAmenityContainer.setVisibility(View.GONE);
            }

            if (value.getExtraAmenityList() != null && !value.getExtraAmenityList().isEmpty()){
                charAmenityRecyclerView.setHasFixedSize(true);
                charAmenityRecyclerView.setNestedScrollingEnabled(true);
                charAmenityRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
                ArrayList<Integer> intExAmenityList = new ArrayList<>();
                for(Amenity amenity:value.getExtraAmenityList()){
                    intExAmenityList.add(amenity.getId());
                }
                charAmenityRecyclerView.setAdapter(new ItemSelectionAdapter(SPACE_AMINITY_TYPE_LIST,intExAmenityList,-1,this));
            }else {
                charAmenityContainer.setVisibility(View.GONE);
            }
        }else {
            amenityContainer.setVisibility(View.GONE);
        }

        cancelTitle.setText(value.getSpace().getCancellationPolicy().getName());
        cancelDescription.setText(value.getSpace().getCancellationPolicy().getDescription());

        if (bookingOwnerTag == GUEST_SPACE_BOOKINGS_PAST){
            if ((value.getReservationStatus().getName().contains(BOOKING_STATUS_CONFIRMED) || value.getReservationStatus().getName().contains(BOOKING_STATUS_PAYMENT_DONE))
                    && !value.isReviewed()) {
                actionButton.setVisibility(View.VISIBLE);
                actionButton.setText(getString(R.string.booking_list_action_review));
                actionButton.setBackgroundColor(ContextCompat.getColor(BookingPreviewActivity.this,R.color.status_pending_payment));
            }else {
                actionButton.setVisibility(View.GONE);
            }
        }else if (bookingOwnerTag == GUEST_SPACE_BOOKINGS_UPCOMIG){
            if (value.getReservationStatus().getName().contains(BOOKING_STATUS_CONFIRMED) || value.getReservationStatus().getName().contains(BOOKING_STATUS_PENDING_PAYMENT)
                    || value.getReservationStatus().getName().contains(BOOKING_STATUS_PAYMENT_DONE)) {
                actionButton.setVisibility(View.VISIBLE);
                actionButton.setText(getString(R.string.booking_list_action_cancel));
                actionButton.setBackgroundColor(ContextCompat.getColor(BookingPreviewActivity.this,R.color.status_cancelled));
            }else {
                actionButton.setVisibility(View.GONE);
            }
        }else if (bookingOwnerTag == HOST_SPACE_BOOKINGS_UPCOMIG || bookingOwnerTag == HOST_SPACE_BOOKINGS_PAST){
            actionButton.setVisibility(View.VISIBLE);
            remarksContainer.setVisibility(View.VISIBLE);
            bookingRemarksView.setText(value.getRemarks() != null? value.getRemarks():null);
            actionButton.setText(getString(R.string.booking_list_action_save));
            actionButton.setBackgroundColor(ContextCompat.getColor(BookingPreviewActivity.this,R.color.status_pending_payment));
        }
    }

    private void inflateView(String s) {
        LayoutInflater layoutInflater = getLayoutInflater();
        View timeView = layoutInflater.inflate(R.layout.list_item_payment_times, null, false);
        TextView timeTextView = ButterKnife.findById(timeView, R.id.bookingpre_booking_time);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 5, 0, 5);
        timeTextView.setText(s);
        reservedTimeContainer.addView(timeView, params);
    }


    private void updateBookingRemarks() {
        String remarks = bookingRemarksView.getText().toString();
        BookingRemarksRequest remarksRequest = new BookingRemarksRequest(booking.getId(),booking.isManual(),remarks);

        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).setRemarks(remarksRequest, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ENF_GUEST_UPDATE_REMARKS);
        }
    }

    @Override
    public void onSuccessRemarks(boolean isAdded) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(getString(R.string.success_booking_remarks_updated),mCoordinatorLayout);
    }

    @Override
    public void onBackPressed() {
        if (isSuccessPayment){
            navigateActivity(HomeActivity.class);
        }else {
            super.onBackPressed();
        }
    }
}
