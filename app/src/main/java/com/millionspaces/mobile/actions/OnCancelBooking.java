package com.millionspaces.mobile.actions;

import com.millionspaces.mobile.entities.request.CancelBookingRequest;

/**
 * Created by kasunka on 11/27/17.
 */

public interface OnCancelBooking {
    void onCancelBooking(CancelBookingRequest request);
}
