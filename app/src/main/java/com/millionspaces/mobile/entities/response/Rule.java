package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 7/21/17.
 */

public class Rule implements Parcelable{
    private int id;
    private String name;
    private String displayName;
    private String mobileIcon;
    private String timeStamp;

    protected Rule(Parcel in) {
        id = in.readInt();
        name = in.readString();
        displayName = in.readString();
        mobileIcon = in.readString();
        timeStamp = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(displayName);
        dest.writeString(mobileIcon);
        dest.writeString(timeStamp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Rule> CREATOR = new Creator<Rule>() {
        @Override
        public Rule createFromParcel(Parcel in) {
            return new Rule(in);
        }

        @Override
        public Rule[] newArray(int size) {
            return new Rule[size];
        }
    };

    public Rule(int id, String name, String displayName, String mobileIcon, String timeStamp) {
        this.id = id;
        this.name = name;
        this.displayName = displayName;
        this.mobileIcon = mobileIcon;
        this.timeStamp = timeStamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIcon() {
        return mobileIcon;
    }

    public void setIcon(String icon) {
        this.mobileIcon = icon;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
