package com.millionspaces.mobile.actions;

import com.millionspaces.mobile.entities.request.SpaceReviewRequest;

/**
 * Created by kasunka on 11/16/17.
 */

public interface OnAddedReview {
    void onAddedReview(SpaceReviewRequest reviewRequest);
}
