package com.millionspaces.mobile.network.interfaces;

import com.millionspaces.mobile.entities.response.Space;

import java.util.ArrayList;


/**
 * Created by kasunka on 7/21/17.
 */

public interface OnSuccessSpacesList extends OnErrorResponse{
    void onSuccessSpacesList(ArrayList<Space> spaces);
    void onSuccessSpacesListCount(int count);
}
