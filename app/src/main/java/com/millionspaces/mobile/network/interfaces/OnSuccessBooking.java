package com.millionspaces.mobile.network.interfaces;


import com.millionspaces.mobile.entities.response.Booking;

/**
 * Created by kasunka on 10/14/17.
 */

public interface OnSuccessBooking extends OnErrorResponse {
    void onSuccessBooking(Booking value);
}
