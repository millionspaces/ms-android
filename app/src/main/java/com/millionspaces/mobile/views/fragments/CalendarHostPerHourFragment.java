package com.millionspaces.mobile.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.millionspaces.mobile.HostCalendarActivity;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.IMethodUpdater;
import com.millionspaces.mobile.actions.OnHostTimeSelected;
import com.millionspaces.mobile.adapters.calendar.DayTimeHourHostAdapter;
import com.millionspaces.mobile.utils.CalendarManager;

import org.joda.time.LocalTime;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.millionspaces.mobile.utils.CalenderUtils.SELECTED_HOUR;

/**
 * Created by kasunka on 11/13/17.
 */

public class CalendarHostPerHourFragment extends Fragment implements OnHostTimeSelected,IMethodUpdater {

    @BindView(R.id.calender_hours_hours)
    RecyclerView calenderHoursRecyclerView;

    private OnHostTimeSelected onTimeSelected;

    private HashMap<String ,String> blockedHoursMap;
    private int noOfWorkingHours = 0;
    private DayTimeHourHostAdapter timeHourAdapter;

    private CalendarManager mCalendarManager;
    private Unbinder unbinder;

    public static Fragment newInstance() {
        CalendarHostPerHourFragment fragment = new CalendarHostPerHourFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof OnHostTimeSelected) {
                onTimeSelected = (OnHostTimeSelected) context;
            }
        }catch (ClassCastException e) {
            throw new ClassCastException();
        }
        mCalendarManager = ((HostCalendarActivity)getActivity()).calendarManager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calendar_per_hour_host, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LocalTime openTime = LocalTime.parse("00:00:00");
        noOfWorkingHours = 24;
        blockedHoursMap = mCalendarManager.getUnavailableHoursPerHourHost(mCalendarManager.getSelectedDate());
        setUpHourList(openTime,noOfWorkingHours);
    }

    private void setUpHourList(LocalTime openTime, int noOfWorkingHours) {
        calenderHoursRecyclerView.setHasFixedSize(true);
        calenderHoursRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        timeHourAdapter = new DayTimeHourHostAdapter(getContext(),openTime,noOfWorkingHours,blockedHoursMap,this);
        calenderHoursRecyclerView.setAdapter(timeHourAdapter);
        setUpActions();
    }

    private void setUpActions() {
        timeHourAdapter.setOnItemClick(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCalendarManager.getBlockedHoursMap().clear();
    }

    @Override
    public void onTimeSelected(HashMap<String, String> blockedHoursMap, float charge, int position, boolean isTimeAdded, boolean isNewPosition) {
        HashMap<String, String> selectedHoursMap = new LinkedHashMap<>();
        if (isNewPosition){
            selectedHoursMap.clear();
        }
        for (Map.Entry<String, String> entry : blockedHoursMap.entrySet()){
            if (entry.getValue().contains(SELECTED_HOUR)){
                selectedHoursMap.put(entry.getKey(),entry.getValue());
            }
        }
        onTimeSelected.onTimeSelected(selectedHoursMap,0,position,isTimeAdded,true);
    }

    @Override
    public void onBookingSelected(boolean isManual, int bookingId) {
        onTimeSelected.onBookingSelected(isManual,bookingId);
    }

    @Override
    public void updateList(boolean isAdded, String key, String value) {
        if (isAdded){
            mCalendarManager.getBlockedHoursMap().put(key,value);
        }else {
            mCalendarManager.getBlockedHoursMap().remove(key);
        }
    }
}
