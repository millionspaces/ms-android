package com.millionspaces.mobile.actions;

/**
 * Created by kasunka on 7/31/17.
 */

public interface OnItemCheckedListener {
    void onItemChecked(boolean checked, int id, int type);
}
