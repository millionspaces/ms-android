package com.millionspaces.mobile.actions;

public interface OnNumberSelected {
    void onNumberAdded(String number);
}
