package com.millionspaces.mobile.adapters.calendar;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnDateSelected;
import com.millionspaces.mobile.utils.CalendarManager;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.RESPONSE_PRICE_GUEST;

/**
 * Created by kasunka on 8/30/17.
 */

public class MonthPagerAdapter extends PagerAdapter implements OnDateSelected {
    private Context mContext;
    private String [] startDate;
    private HashMap<String,String> bookingHashMap;
    private int numberOfMonths;
    private int width;
    private CalendarManager mCalendarManager;

    @BindView(R.id.calender_days)
    RecyclerView calenderDaysRecyclerView;
    private DayAdapter dayAdapter;
    private HostDayAdapter hostDayAdapter;

    private OnDateSelected onDateSelected;

    public MonthPagerAdapter(Context context, int width, CalendarManager calendarManager){
        this.mContext = context;
        this.width = width;
        this.mCalendarManager = calendarManager;

        this.numberOfMonths = mCalendarManager.getNumberOfMonths();
        this.startDate = mCalendarManager.getStartDate().split("-");
        this.bookingHashMap = mCalendarManager.getBlockDaysMap();
    }

    @Override
    public int getCount() {
        return numberOfMonths;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.list_calender_month, container, false);
        ButterKnife.bind(this,itemView);

        Calendar dayCalender = new GregorianCalendar(Integer.parseInt(startDate[0]), Integer.parseInt(startDate[1])-1+position, 1);

        setUpCalender(dayCalender.get(Calendar.YEAR), dayCalender.get(Calendar.MONTH),dayCalender.getActualMaximum(Calendar.DAY_OF_MONTH),
                dayCalender.get(Calendar.DAY_OF_WEEK),mCalendarManager);

        container.addView(itemView);
        return itemView;
    }

    private void setUpCalender(int year, int month, int noOfDaysOfCurrentMonth, int startDayOfCurrentMonth, CalendarManager calendarManager) {
        calenderDaysRecyclerView.setHasFixedSize(true);
        calenderDaysRecyclerView.setLayoutManager(new GridLayoutManager(mContext,7));
        if (calendarManager.getAvailabilityMethod() != RESPONSE_PRICE_GUEST) {
            dayAdapter = new DayAdapter(mContext, width, year, month, noOfDaysOfCurrentMonth, startDayOfCurrentMonth, calendarManager);
            calenderDaysRecyclerView.setAdapter(dayAdapter);
            dayAdapter.setOnItemClickListner(this);
        }else {
            hostDayAdapter = new HostDayAdapter(mContext, width, year, month, noOfDaysOfCurrentMonth, startDayOfCurrentMonth, calendarManager);
            calenderDaysRecyclerView.setAdapter(hostDayAdapter);
            hostDayAdapter.setOnItemClickListner(this);
        }

        calenderDaysRecyclerView.setNestedScrollingEnabled(true);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public void onDateSelected(String day, String month, int year, boolean isSelected) {
        onDateSelected.onDateSelected(day,month,year,isSelected);
    }

    public void setOnItemClickListner(OnDateSelected onDateSelected) {
        this.onDateSelected = onDateSelected;
    }
}
