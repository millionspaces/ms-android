package com.millionspaces.mobile.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.millionspaces.mobile.BookingEngineActivity;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnBlockSelected;
import com.millionspaces.mobile.actions.OnTimeSelected;
import com.millionspaces.mobile.adapters.calendar.DayTimeBlockAdapter;
import com.millionspaces.mobile.entities.response.Availability;
import com.millionspaces.mobile.utils.CalendarManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by kasunka on 9/11/17.
 */

public class CalenderPerBlockFragment extends Fragment implements OnBlockSelected {

    @BindView(R.id.calender_block_hours)
    RecyclerView calenderBlockRecyclerView;
    @BindView(R.id.calender_block_backto_day)
    ImageView backToDay;

    private ArrayList<Availability> dayAvailabilityPerBlock;
    private int blockChargeType, numberOfMinHeadCount, numberOfGuests;
    private DayTimeBlockAdapter timeBlockAdapter;
    private OnBlockSelected onBlockSelected;

    CalendarManager mCalendarManager;
    private Unbinder unbinder;

    public static Fragment newInstance() {
        CalenderPerBlockFragment fragment = new CalenderPerBlockFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTimeSelected) {
            onBlockSelected = (OnBlockSelected) context;
        }
        mCalendarManager = ((BookingEngineActivity) getActivity()).calendarManager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calender_per_block, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dayAvailabilityPerBlock = mCalendarManager.getUnavailableHoursPerBlock();
        blockChargeType = mCalendarManager.getBlockChargeType();
        numberOfMinHeadCount = mCalendarManager.getNumberOfMinCount();
        numberOfGuests = mCalendarManager.getNumberOfGuests();
        setUpPerBlockList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        for (Availability availability : mCalendarManager.getBlockedBlockMap()) {
            availability.setAvailabilityStatus(null);
            availability.setTempAvailabilityStatus(null);
        }
        mCalendarManager.getBlockedBlockMap().clear();
    }

    private void setUpPerBlockList() {
        calenderBlockRecyclerView.setHasFixedSize(true);
        calenderBlockRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        timeBlockAdapter = new DayTimeBlockAdapter(getContext(), dayAvailabilityPerBlock, blockChargeType,numberOfMinHeadCount,numberOfGuests);
        calenderBlockRecyclerView.setAdapter(timeBlockAdapter);
        calenderBlockRecyclerView.setNestedScrollingEnabled(true);
        setUpActions();
    }

    private void setUpActions() {
        timeBlockAdapter.setOnBlockClickListner(this);
    }

    @OnClick(R.id.calender_block_backto_day)
    void backToDay() {
        ((CalenderFragment) getParentFragment()).showBackFragment();
    }

    @Override
    public void onBlockSelected(Availability availability, float charge, boolean isBlockAdded) {
        ViewGroup.LayoutParams params = calenderBlockRecyclerView.getLayoutParams();
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        calenderBlockRecyclerView.setLayoutParams(params);
        onBlockSelected.onBlockSelected(availability, charge, isBlockAdded);
    }

    @Override
    public void onNoticePeriodSelected() {
        onBlockSelected.onNoticePeriodSelected();
    }
}
