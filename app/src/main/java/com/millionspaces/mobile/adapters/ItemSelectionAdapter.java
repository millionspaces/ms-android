package com.millionspaces.mobile.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.entities.response.Amenity;
import com.millionspaces.mobile.entities.response.EventType;
import com.millionspaces.mobile.entities.response.SeatingArrangement;
import com.millionspaces.mobile.utils.DataBaseUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.SPACE_AMINITY_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_EVENT_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_SEATING_TYPE_LIST;

/**
 * Created by kasunka on 10/16/17.
 */

public class ItemSelectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private ArrayList<Integer> intList;
    private Context mContext;
    private int cardViewType;
    private DataBaseUtils db;

    private OnItemClickListener onItemClickListener;
    private int selectedItem = -1;

    public ItemSelectionAdapter(int type, ArrayList<Integer> intList,int id ,Context context) {
        this.cardViewType = type;
        this.intList = intList;
        this.mContext =  context;
        db = new DataBaseUtils(mContext);
        for (int i = 0; i< intList.size(); i++){
            if (intList.get(i) == id){
                selectedItem = i;
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        switch (viewType){
            case SPACE_AMINITY_TYPE_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_booking_preview_amenity, parent, false);
                return new ViewHolderAmenity(v);
            default:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_selctor_item, parent, false);
                return new ViewHolderItem(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (cardViewType){
            case SPACE_EVENT_TYPE_LIST:
                EventType eventType = db.getEventTypeById(intList.get(position));
                Glide.with(mContext).load(eventType.getIcon()).into(((ViewHolderItem)holder).icon);
                ((ViewHolderItem)holder).name.setText(eventType.getName());
                setItemSelection(position,((ViewHolderItem)holder));
                break;
            case SPACE_SEATING_TYPE_LIST:
                SeatingArrangement seating =  db.getSeatingArranmnet(intList.get(position));
                Glide.with(mContext).load(seating.getIcon()).into(((ViewHolderItem)holder).icon);
                ((ViewHolderItem)holder).name.setText(seating.getName());
                setItemSelection(position,((ViewHolderItem)holder));
                break;
            case SPACE_AMINITY_TYPE_LIST:
                Amenity amenity = db.getAmenity(intList.get(position));
                Glide.with(mContext).load(amenity.getIcon()).into(((ViewHolderAmenity)holder).icon);
                ((ViewHolderAmenity)holder).name.setText(amenity.getName());
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (cardViewType){
            case SPACE_AMINITY_TYPE_LIST:
                return SPACE_AMINITY_TYPE_LIST;
            case SPACE_EVENT_TYPE_LIST:
                return SPACE_EVENT_TYPE_LIST;
            default:
                return SPACE_EVENT_TYPE_LIST;
        }
    }

    private void setItemSelection(int position, ViewHolderItem holder) {
        if (position== selectedItem){
            holder.parentView.setBackground(ContextCompat.getDrawable(mContext,R.drawable.background_selected_payment_card));
            holder.name.setTextColor(ContextCompat.getColor(mContext,R.color.mySpacePrimaryBlue));
            holder.icon.setColorFilter(ContextCompat.getColor(mContext,R.color.mySpacePrimaryBlue));
        }else {
            holder.parentView.setBackground(null);
            holder.name.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimaryTextLight));
            holder.icon.setColorFilter(ContextCompat.getColor(mContext,R.color.colorPrimaryTextLight));
        }
    }


    @Override
    public int getItemCount() {
        return intList.size();
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.eventtype_icon)
        ImageView icon;
        @BindView(R.id.eventtype_name)
        TextView name;
        @BindView(R.id.item_parent)
        RelativeLayout parentView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int previousSelection = selectedItem;
            selectedItem = getAdapterPosition();
            notifyItemChanged(previousSelection);
            notifyItemChanged(selectedItem);
            onItemClickListener.OnClick(getAdapterPosition(),v);
        }
    }

    public class ViewHolderAmenity extends RecyclerView.ViewHolder{
        @BindView(R.id.amenity_icon)
        ImageView icon;
        @BindView(R.id.amenity_name)
        TextView name;

        public ViewHolderAmenity(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListner){
        this.onItemClickListener = itemClickListner;
    }
}
