package com.millionspaces.mobile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.millionspaces.mobile.actions.OnDatePicked;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.adapters.ItemSelectionAdapter;
import com.millionspaces.mobile.adapters.StatusDropDownAdapter;
import com.millionspaces.mobile.entities.ReservationStatus;
import com.millionspaces.mobile.entities.request.ManualBookingRequest;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessDeleteManualBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessGetManualBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessManualBooking;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.DataBaseUtils;

import org.joda.time.DateTime;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.R.id.dialog_button_negative;
import static com.millionspaces.mobile.R.id.dialog_button_positive;
import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTENT_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.RESERVATION_STATUS_PAYMENT_DONE;
import static com.millionspaces.mobile.utils.Config.RESERVATION_STATUS_PENDING_PAYMENT;
import static com.millionspaces.mobile.utils.Config.SPACE_EVENT_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_SEATING_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.STATUS_LIST;
import static com.millionspaces.mobile.utils.Config.STATUS_LIST_MAP;
import static com.millionspaces.mobile.utils.Config.isValidMail;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_HOST_BLOCK_SPACE;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_HOST_DELETE_BOOKING;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_HOST_GET_BOOKING;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_HOST_UPDATE_BOOKING;

public class HostBookingActivity extends BaseActivity implements View.OnClickListener, OnSuccessManualBooking, OnSuccessGetManualBooking, OnSuccessDeleteManualBooking, OnDatePicked {

    @BindView(R.id.host_calander_event_type)
    RecyclerView eventTypeRecyclerView;
    @BindView(R.id.seating_arrangement)
    RecyclerView seatingRecyclerView;
    @BindView(R.id.host_calander_start_date)
    RelativeLayout startDateContainer;
    @BindView(R.id.host_calander_start_date_ico)
    ImageView startDateIco;
    @BindView(R.id.host_calander_start_date_txt)
    TextView startDateView;
    @BindView(R.id.host_calender_parent)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.host_calander_end_date)
    RelativeLayout endDateContainer;
    @BindView(R.id.host_calander_end_date_ico)
    ImageView endDateIco;
    @BindView(R.id.host_calander_end_date_txt)
    TextView endDateTextView;
    @BindView(R.id.host_calander_guest_name)
    EditText guestNameView;
    @BindView(R.id.host_calander_guest_booking_email)
    EditText guestEmailView;
    @BindView(R.id.host_calander_guest_cnt)
    EditText guestContactNoView;
    @BindView(R.id.host_calander_guest_booking_amount)
    EditText guestBookingAmountView;
    @BindView(R.id.host_calander_status)
    RelativeLayout statusContainer;
    @BindView(R.id.host_calander_status_ico)
    ImageView statusIco;
    @BindView(R.id.host_calander_status_list)
    RecyclerView statusRecyclerView;
    @BindView(R.id.host_calander_status_txt)
    TextView statusView;
    @BindView(R.id.host_calander_status_amenities)
    EditText amenitiesView;
    @BindView(R.id.host_calander_status_remarks)
    EditText remarksView;
    @BindView(R.id.host_calander_action_positive)
    Button positive;
    @BindView(R.id.host_calander_action_negative)
    Button negative;
    @BindView(R.id.host_calander_guest_count)
    EditText noOfGuestView;

    private ItemSelectionAdapter eventTypeAdapter;
    private ItemSelectionAdapter seatingAdapter;
    private DataBaseUtils dataBaseUtils;
    private DateTime startDate = null, endDate =  null, calendarDate = null;
    private Bundle mBundle;
    private int dateType = 0;
    private Dialog alertDialog;

    private static final int STARTING_DATE = 0;
    private static final int ENDING_DATE = 1;

    private String guestName, guestContactNo, bookingAmount, guestEmail;
    private int spaceId, bookingId, eventTypeId, seatingId, reservationStatusId;
    private String spaceDates;
    private boolean isExistingBooking = false;
    private ManualBookingRequest booking;
    private ArrayList<Integer> eventTypeIds, seatingIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host_booking);
        ButterKnife.bind(this);
        init_views();
    }

    private void init_views() {
        init_toolbar();
        setToolbarTitle(getString(R.string.host_calender_booking_block_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        dataBaseUtils = new DataBaseUtils(this);
        eventTypeIds = dataBaseUtils.getEventTypeIds();
        seatingIds = dataBaseUtils.getSeatingIds();
        mBundle = getIntent().getExtras();
        spaceId = mBundle.getInt(BUNDLE_INTEGER_PARAM);
        bookingId = mBundle.getInt(BUNDLE_INTEGER_PARAM2);
        spaceDates = mBundle.getString(BUNDLE_STRING_PARAM);
        isExistingBooking = mBundle.getBoolean(BUNDLE_BOOLEAN_PARAM);
        if (!TextUtils.isEmpty(spaceDates) && !isExistingBooking){
            updateTextViews(spaceDates);
        }else if (isExistingBooking){
            getBookingDetails(bookingId);
            positive.setText(getString(R.string.host_calender_booking_action_update));
            negative.setText(getString(R.string.host_calender_booking_action_remove));
            negative.setVisibility(View.VISIBLE);
        }

        init_lists();
        setSelectionLists(0, 0);
        init_action();
        setupUI(mCoordinatorLayout,this);
        hideSoftKeyboard();
    }

    private void init_lists() {
        eventTypeRecyclerView.setHasFixedSize(true);
        eventTypeRecyclerView.setLayoutManager(new GridLayoutManager(this,3));
        eventTypeRecyclerView.setNestedScrollingEnabled(true);

        seatingRecyclerView.setHasFixedSize(true);
        seatingRecyclerView.setLayoutManager(new GridLayoutManager(this,3));
        seatingRecyclerView.setNestedScrollingEnabled(true);
    }

    private void setSelectionLists(int seatId, int eventId) {
        eventTypeAdapter = new ItemSelectionAdapter(SPACE_EVENT_TYPE_LIST,eventTypeIds,eventId,this);
        eventTypeRecyclerView.setAdapter(eventTypeAdapter);

        seatingAdapter = new ItemSelectionAdapter(SPACE_SEATING_TYPE_LIST, seatingIds,seatId,this);
        seatingRecyclerView.setAdapter(seatingAdapter);

        eventTypeAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnClick(int position, View view) {
                eventTypeId = eventTypeIds.get(position);
            }
        });

        seatingAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnClick(int position, View view) {
                seatingId = seatingIds.get(position);
            }
        });
    }

    private void init_action() {
        startDateContainer.setOnClickListener(this);
        startDateIco.setOnClickListener(this);
        endDateContainer.setOnClickListener(this);
        endDateIco.setOnClickListener(this);
        statusContainer.setOnClickListener(this);
        statusIco.setOnClickListener(this);
        positive.setOnClickListener(this);
        negative.setOnClickListener(this);
        calendarDate = new DateTime();
    }

    private void updateTextViews(String string) {
        String[] dates = string.split(",");
        startDate = new DateTime(dates[0]);
        endDate = new DateTime(dates[1]);
        startDateView.setText(startDate.toString("dd MMM yyyy, hh.mm a"));
        endDateTextView.setText(endDate.toString("dd MMM yyyy, hh.mm a"));
    }

    @Override
    public void onDatePicked(DateTime dateTime) {
        switch (dateType){
            case STARTING_DATE:
                startDate = dateTime;
                startDateView.setText(dateTime.toString("dd MMM yyyy, hh.mm a"));
                break;
            case ENDING_DATE:
                endDate = dateTime;
                endDateTextView.setText(dateTime.toString("dd MMM yyyy, hh.mm a"));
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.host_calander_start_date:
                showStartDatePicker();
                break;
            case R.id.host_calander_start_date_ico:
                showStartDatePicker();
                break;
            case R.id.host_calander_end_date:
                showEndDatePicker();
                break;
            case R.id.host_calander_end_date_ico:
                showEndDatePicker();
                break;
            case R.id.host_calander_status:
                showStatusDropDown();
                break;
            case R.id.host_calander_status_ico:
                showStatusDropDown();
                break;
            case R.id.host_calander_action_positive:
                positiveAction();
                break;
            case R.id.host_calander_action_negative:
                negativeAction();
                break;
            case dialog_button_positive:
                alertDialog.dismiss();
                deleteBooking(bookingId);
                break;
            case dialog_button_negative:
                if (alertDialog.isShowing()){
                    alertDialog.dismiss();
                }
                break;
        }
    }

    private void showStartDatePicker() {
        dateType = STARTING_DATE;
        AlertMessageUtils.showDateTimePickerDialog(HostBookingActivity.this, getString(R.string.host_calender_booking_start_time),
                calendarDate.getYear(), calendarDate.getMonthOfYear(), calendarDate.getDayOfMonth(),false,this);
    }

    private void showEndDatePicker() {
        dateType = ENDING_DATE;
        if (startDate != null) {
            AlertMessageUtils.showDateTimePickerDialog(HostBookingActivity.this, getString(R.string.host_calender_booking_end_time),
                    calendarDate.getYear(), calendarDate.getMonthOfYear(), calendarDate.getDayOfMonth(),true,this);
        }else {
            AlertMessageUtils.showSnackBarMessage(getString(R.string.error_host_bookig_empty_starting_date),mCoordinatorLayout);
        }
    }

    private void positiveAction() {
        if (isExistingBooking){
            attemptToUpdate(bookingId);
        }else {
            attemptToSubmit();
        }
    }

    private void negativeAction() {
        if (isExistingBooking){
            attemptToDelete();
        }else {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(BUNDLE_INTENT_PARAM,getString(R.string.error_manual_booking));
            setResult(Activity.RESULT_CANCELED,returnIntent);
            finish();
        }
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!= null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void showStatusDropDown(){
        statusRecyclerView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        statusRecyclerView.setNestedScrollingEnabled(true);
        statusRecyclerView.setLayoutManager(layoutManager);
        statusRecyclerView.hasFixedSize();

        StatusDropDownAdapter mAdapter = new StatusDropDownAdapter(this);
        mAdapter.notifyDataSetChanged();
        statusRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnClick(int position, View view) {
                statusView.setText(STATUS_LIST.get(position));
                statusRecyclerView.setVisibility(View.GONE);
                switch (position){
                    case 0:
                        reservationStatusId = RESERVATION_STATUS_PAYMENT_DONE;
                        break;
                    default:
                        reservationStatusId = RESERVATION_STATUS_PENDING_PAYMENT;
                        break;
                }
            }
        });
    }


    private void attemptToSubmit(){
        String error=  null;
        boolean isValid = true;

        guestName = guestNameView.getText().toString();
        guestContactNo = guestContactNoView.getText().toString();
        bookingAmount =  guestBookingAmountView.getText().toString();
        guestEmail =  guestEmailView.getText().toString();

        if (startDate == null){
            error = getString(R.string.error_host_bookig_empty_starting_date);
            isValid = false;
        }else if (endDate == null){
            error = getString(R.string.error_host_bookig_empty_end_date);
            isValid = false;
        }else if (endDate.isBefore(startDate)){
            error = getString(R.string.error_host_bookig_wrong_date);
            isValid = false;
        }else if (!TextUtils.isEmpty(guestEmail) && !isValidMail(guestEmail)){
            error = getString(R.string.error_wrong_email);
            isValid = false;
        }else if (!TextUtils.isEmpty(guestContactNo) && guestContactNo.length()!=10){
            error = "Please enter valid mobile number";
            isValid = false;
        }


        if (isValid){
            blockSpace();
        }else {
            AlertMessageUtils.showSnackBarMessage(error,mCoordinatorLayout);
        }
    }

    private void blockSpace() {
        ManualBookingRequest request = new ManualBookingRequest();
        request.setSpace(spaceId);
        request.setFromDate(startDate.toString("yyyy-MM-dd'T'HH:mm")+"Z");
        request.setToDate(endDate.toString("yyyy-MM-dd'T'HH:mm")+"Z");
        request.setDateBookingMade(calendarDate.toString("yyyy-MM-dd'T'HH:mm:ss"));
        request.setCost(guestBookingAmountView.getText().toString());
        request.setNote(remarksView.getText().toString());
        request.setGuestContactNumber(guestContactNoView.getText().toString());
        request.setGuestEmail(guestEmailView.getText().toString());
        request.setGuestName(guestNameView.getText().toString());
        request.setNoOfGuests(noOfGuestView.getText().toString());
        request.setEventTypeId(eventTypeId == 0? null:String.valueOf(eventTypeId));
        request.setExtrasRequested(amenitiesView.getText().toString());
        if (seatingId !=0){request.setSeatingArrangementId(String.valueOf(seatingId));}
        request.setReservationStatus(reservationStatusId != 0?new ReservationStatus(reservationStatusId):null);

        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).putMaualBooking(request, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ENF_HOST_BLOCK_SPACE);
        }
    }

    private void getBookingDetails(int bookingID) {
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).getSpaceManualBooking(bookingID, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ENF_HOST_GET_BOOKING);
        }
    }

    //TODO : Change date format after backend correction
    private void attemptToUpdate(int bookingID) {
        ManualBookingRequest request = new ManualBookingRequest();
        request.setId(bookingID);
        request.setSpace(spaceId);
        request.setFromDate(startDate.toString("yyyy-MM-dd'T'HH:mm")+"Z");
        request.setToDate(endDate.toString("yyyy-MM-dd'T'HH:mm")+"Z");
        request.setDateBookingMade(booking.getDateBookingMade());
        request.setCost(guestBookingAmountView.getText().toString());
        request.setNote(remarksView.getText().toString());
        request.setGuestContactNumber(guestContactNoView.getText().toString());
        request.setGuestEmail(guestEmailView.getText().toString());
        request.setGuestName(guestNameView.getText().toString());
        request.setNoOfGuests(noOfGuestView.getText().toString());
        request.setEventTypeId(eventTypeId !=0?String.valueOf(eventTypeId):null);
        request.setExtrasRequested(amenitiesView.getText().toString());
        request.setSeatingArrangementId(seatingId !=0?String.valueOf(seatingId):null);
        request.setReservationStatus(reservationStatusId != 0?new ReservationStatus(reservationStatusId):booking.getReservationStatus());

        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).updateManualBooking(request, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ENF_HOST_UPDATE_BOOKING);
        }
    }

    private void attemptToDelete() {
        alertDialog = AlertMessageUtils.showConfirmDialog(this,getString(R.string.host_calendar_booking_dialog_title),getString(R.string.host_calendar_booking_dialog_description),
                getString(R.string.host_calendar_booking_dialog_negative),getString(R.string.host_calendar_booking_dialog_postitive),
                View.VISIBLE,this);
    }

    private void deleteBooking(int bookingID){
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).deleteManualBooking(bookingID, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ENF_HOST_DELETE_BOOKING);
        }
    }

    @Override
    public void onSuccessGetBooking(ManualBookingRequest booking) {
        hideProgress();
        this.booking = booking;
        startDateContainer.setOnClickListener(null);
        startDateIco.setOnClickListener(null);
        endDateContainer.setOnClickListener(null);
        endDateIco.setOnClickListener(null);

        startDate = new DateTime(booking.getFromDate().replace(" ","T"));
        startDateView.setText(startDate.toString("dd MMM yyyy, HH.mm a"));
        endDate =  new DateTime(booking.getToDate().replace(" ","T"));
        endDateTextView.setText(endDate.toString("dd MMM yyyy, HH.mm a"));
        guestNameView.setText(booking.getGuestName());
        guestEmailView.setText(booking.getGuestEmail());
        guestContactNoView.setText(booking.getGuestContactNumber());
        guestBookingAmountView.setText(booking.getCost());
        noOfGuestView.setText(booking.getNoOfGuests());
        amenitiesView.setText(booking.getExtrasRequested());
        remarksView.setText(booking.getNote());
        setSelectionLists(booking.getSeatingArrangementId() != null?Integer.parseInt(booking.getSeatingArrangementId()):0,
                booking.getEventTypeId() != null ?Integer.parseInt(booking.getEventTypeId()):0);

        statusView.setText(booking.getReservationStatus() != null?STATUS_LIST_MAP.get(booking.getReservationStatus().getId()):null);
    }

    @Override
    public void onSuccessManualBooking(String id) {
        hideProgress();
        Intent returnIntent = new Intent();
        if (!id.equals("0")) {
            returnIntent.putExtra(BUNDLE_INTENT_PARAM, getString(R.string.success_calendar_updated_success));
            setResult(Activity.RESULT_OK, returnIntent);
        }else {
            returnIntent.putExtra(BUNDLE_INTENT_PARAM, getString(R.string.error_manual_booking_overlaps));
            setResult(Activity.RESULT_CANCELED,returnIntent);
        }
        finish();
    }

    @Override
    public void onSuccessDeleteManualBooking(int isSuccess) {
        hideProgress();
        Intent returnIntent = new Intent();
        returnIntent.putExtra(BUNDLE_INTENT_PARAM,spaceId);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        Intent returnIntent = new Intent();
        returnIntent.putExtra(BUNDLE_INTENT_PARAM,error);
        setResult(Activity.RESULT_CANCELED,returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(BUNDLE_INTENT_PARAM,getString(R.string.error_manual_booking));
        setResult(Activity.RESULT_CANCELED,returnIntent);
        finish();
    }

    @Override
    public void onConnectionErrorCallBack(int requestCode) {
        switch (requestCode){
            case ENF_HOST_BLOCK_SPACE:
                blockSpace();
                break;
            case ENF_HOST_GET_BOOKING:
                getBookingDetails(bookingId);
                break;
            case ENF_HOST_UPDATE_BOOKING:
                attemptToUpdate(bookingId);
                break;
            case ENF_HOST_DELETE_BOOKING:
                deleteBooking(bookingId);
                break;
        }
    }
}
