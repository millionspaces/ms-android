package com.millionspaces.mobile.utils;

import android.text.TextUtils;
import android.util.Log;

import com.millionspaces.mobile.entities.response.Availability;
import com.millionspaces.mobile.entities.response.BookingDate;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static com.millionspaces.mobile.utils.CalenderUtils.DATE_FORMAT;
import static com.millionspaces.mobile.utils.CalenderUtils.NOTICED_HOUR;
import static com.millionspaces.mobile.utils.CalenderUtils.PARTIALLY_BOOKED;
import static com.millionspaces.mobile.utils.CalenderUtils.SELECTED_DAY;
import static com.millionspaces.mobile.utils.CalenderUtils.TOTALLY_BOOKED;
import static com.millionspaces.mobile.utils.CalenderUtils.TOTALLY_BOOKED_MS;
import static com.millionspaces.mobile.utils.CalenderUtils.TOTALLY_BOOKED_MS_FIRST;
import static com.millionspaces.mobile.utils.Config.RESPONSE_PRICE_BLOCK;
import static com.millionspaces.mobile.utils.Config.RESPONSE_PRICE_HOUR;


/**
 * Created by kasunka on 9/25/17.
 */

public class CalendarManager{

    private String startDate = null;
    private String endDate = null;
    private ArrayList<BookingDate> futureBookingDates;
    private ArrayList<Availability> availabilities;
    private String availabilityMethod = null;
    private int blockChargeType = -1;
    private int numberOfMinCount = 0;
    private int bufferTime = 0, numberOfGuests = 0, noticePeriod = 0;

    private int numberOfMonths = 0;
    private DateTime blockStartDay, blockEndDay;

    //    Block days Map with key format of yyyyMMdd for Hourbase
    private HashMap<String, String> blockDaysMap = new LinkedHashMap<>();

    //    Block days Map with key format yyyyMMdd for BlockBase
    private HashMap<String, ArrayList<Integer>> partiallyBlockDaysPerBlockMap = new LinkedHashMap<>();

    //    Notice block Map with key format yyyyMMdd for blockBase
    private HashMap<String, ArrayList<Integer>> noticeBlockDaysPerBlockMap = new LinkedHashMap<>();

    //    Block days Map with key format yyyyMMdd for HourBase
    private HashMap<String, ArrayList<String>> partiallyBlockDaysPerHourMap = new LinkedHashMap<>();

    //    Block Hours Map with key format yyyyMMdd for Hour Base
    private HashMap<String ,String> blockedHoursMap = new LinkedHashMap<>();

    //    Bolock Blocks Array List with Block List
    private ArrayList<Availability> blockedBlockMap = new ArrayList<>();

    private String selectedDate = null;
    private String selectedDateStatus = null;
    private DateTime selectedDateObject = null;

    DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT);

    public CalendarManager(String calendarStart, String calendarEnd, ArrayList<BookingDate> futureBookingDates,
                           ArrayList<Availability> availability, String availabilityMethod, int blockChargeType, int numberOfMinCount,
                           int numberOfGuests, int bufferTime, int noticePeriod) {
        this.startDate = calendarStart;
        this.endDate = calendarEnd;
        this.futureBookingDates = futureBookingDates;
        this.availabilities = availability;
        this.availabilityMethod = availabilityMethod;
        this.blockChargeType = blockChargeType;
        this.numberOfMinCount = numberOfMinCount;
        this.numberOfGuests = numberOfGuests;
        this.bufferTime = bufferTime;
        this.noticePeriod = noticePeriod;
        blockCalendarDates();
    }

    private void blockCalendarDates() {
        String[] startDateSeperate = startDate.split("-");
        String[] endDateSeperate = endDate.split("-");

        numberOfMonths = (Integer.parseInt(endDateSeperate[0]) - Integer.parseInt(startDateSeperate[0])) * 12 +
                (Integer.parseInt(endDateSeperate[1]) - Integer.parseInt(startDateSeperate[1]) + 1);


        DateTime today = new DateTime();
        String key = today.getYear() + String.format("%02d", today.getMonthOfYear() - 1) + String.format("%02d", today.getDayOfMonth());

        blockStartDays(startDateSeperate);
        if (availabilityMethod.contains(RESPONSE_PRICE_BLOCK)) {
            blockUnavailableBlockedDays();
            blockPerBlockCurrentDatePastHours(today,key);
            blockPerBlockNoticePeriod(today,noticePeriod);
            blockPerBlockBookingDays(futureBookingDates);
        } else if (availabilityMethod.contains(RESPONSE_PRICE_HOUR)){
            blockUnavailableBlockedDays();
            blockPerHourCurrentDatePastHours(today,key);
            blockPerHourNoticePeriod(today,noticePeriod);
            blockPerHourBookingDays(futureBookingDates);
        }else {
            blockPerHourHostBookingDays(futureBookingDates);
        }
        blockEndDays(endDateSeperate);
    }


    /*
    *  Block Previous days on Calender from the today/given day Full
    *  Created By kasunka on 8/04/17
    */
    private void blockStartDays(String[] startDate) {
        for (int i = 1; i < Integer.parseInt(startDate[2]); i++) {
            blockDaysMap.put(startDate[0] + String.format("%02d", Integer.parseInt(startDate[1]) - 1) + String.format("%02d", i), TOTALLY_BOOKED);
        }

        for (String key : blockDaysMap.keySet()) {
            Log.d("days", key + " " + blockDaysMap.get(key));
        }
    }

    /*
    *  Block BlockBase/HourBase unavailable days on Calender from the given day by host
    *  Created By kasunka on 9/12/17
    */
    private void blockUnavailableBlockedDays() {
        boolean isWeekdaysAvailable = false, isSatDayAvailable = false, isSunDayAvailable = false,
                isMondayAvailable = false, isTueDayAvailable = false, isWedDayAvailable = false,
                isThuDayAvailable = false, isFriDayAvailable = false;
        for (Availability availability : availabilities) {
            switch (availability.getDay()) {
                case 8:
                    isWeekdaysAvailable = true;
                    break;
                case 1:
                    isMondayAvailable = true;
                    break;
                case 2:
                    isTueDayAvailable = true;
                    break;
                case 3:
                    isWedDayAvailable = true;
                    break;
                case 4:
                    isThuDayAvailable = true;
                    break;
                case 5:
                    isFriDayAvailable = true;
                    break;
                case 6:
                    isSatDayAvailable = true;
                    break;
                case 7:
                    isSunDayAvailable = true;
                    break;
            }
        }

        if (!isWeekdaysAvailable && availabilityMethod.contains(RESPONSE_PRICE_BLOCK)) {
            Log.d("Days", "No Weekdays");
            blockAllWeekdays();
        }

        if (!isMondayAvailable && availabilityMethod.contains(RESPONSE_PRICE_HOUR)) {
            Log.d("Days", "No Mondays");
            blockDaysAllCalender(1);
        }

        if (!isTueDayAvailable && availabilityMethod.contains(RESPONSE_PRICE_HOUR)) {
            Log.d("Days", "No Tuesdays");
            blockDaysAllCalender(2);
        }

        if (!isWedDayAvailable && availabilityMethod.contains(RESPONSE_PRICE_HOUR)) {
            Log.d("Days", "No Wednesdays");
            blockDaysAllCalender(3);
        }

        if (!isThuDayAvailable && availabilityMethod.contains(RESPONSE_PRICE_HOUR)) {
            Log.d("Days", "No Thursday");
            blockDaysAllCalender(4);
        }

        if (!isFriDayAvailable && availabilityMethod.contains(RESPONSE_PRICE_HOUR)) {
            Log.d("Days", "No Fridays");
            blockDaysAllCalender(5);
        }

        if (!isSatDayAvailable) {
            Log.d("Days", "No Saturday");
            blockDaysAllCalender(6);
        }

        if (!isSunDayAvailable) {
            Log.d("Days", "No Sunday");
            blockDaysAllCalender(7);
        }
    }

    /*
    *  Block BlockBase unavailable Weekdays on Calender from the given day by host
    *  Created By kasunka on 9/12/17
    */
    private void blockAllWeekdays() {
        for (int i = 1; i <= 5; i++) {
            blockDaysAllCalender(i);
        }
    }

    /*
    *  Block BlockBase unavailable days on Calender from the given day by host
    *  Created By kasunka on 9/12/17
    */
    private void blockDaysAllCalender(int day) {
        DateTime calenderEndDate = formatter.parseDateTime(endDate + "T00:00");
        DateTime calenderDays = formatter.parseDateTime(startDate + "T00:00").toDateTime().withDayOfWeek(day);
        blockDaysMap.put(String.valueOf(calenderDays.getYear()) + String.format("%02d", calenderDays.getMonthOfYear() - 1) + String.format("%02d", calenderDays.getDayOfMonth()), TOTALLY_BOOKED);
        while (calenderDays.isBefore(calenderEndDate)) {
            calenderDays = calenderDays.plusWeeks(1);
            blockDaysMap.put(String.valueOf(calenderDays.getYear()) + String.format("%02d", calenderDays.getMonthOfYear() - 1) + String.format("%02d", calenderDays.getDayOfMonth()), TOTALLY_BOOKED);
        }
    }

    /*
    *  Block BlockBase Current Date past Hours on Calender partially/Full
    *  Created By kasunka on 8/04/17
    */
    private void blockPerBlockCurrentDatePastHours(DateTime today, String key) {
        int bookedBlockCount = 0;
        ArrayList<Integer> blockedIdArray = new ArrayList<>();
        ArrayList<Availability> availabilityArrayList = getDayAvailabilityPerBlock(today.getDayOfWeek());
        for (Availability availability : availabilityArrayList) {
            if (today.toLocalTime().isAfter(LocalTime.parse(availability.getFrom()))){
                blockedIdArray.add(availability.getId());
                bookedBlockCount++;
            }
        }

        if (blockDaysMap.get(key) != TOTALLY_BOOKED) {
            partiallyBlockDaysPerBlockMap.put(key, blockedIdArray);
        }

        if (availabilityArrayList.size() <= bookedBlockCount) {
            blockDaysMap.put(key, TOTALLY_BOOKED);
        }
    }

    /*
    *  Block BlockBase Notice Period on Calender partially/Full
    *  Created By kasunka on 8/04/17
    */
    private void blockPerBlockNoticePeriod(DateTime today, int noticePeriod) {
        Log.d("Time",today.getHourOfDay()+" "+today.withTimeAtStartOfDay().getHourOfDay());
        DateTime date = today.plusHours(1);
        int notice = noticePeriod, timeDuration = 23 - date.getHourOfDay();
        String key;
        do {
            ArrayList<Integer> blockedIdArray = new ArrayList<>();
            ArrayList<Availability> availabilityArrayList = getDayAvailabilityPerBlock(date.getDayOfWeek());

            key = date.getYear() + String.format("%02d", date.getMonthOfYear() - 1) + String.format("%02d", date.getDayOfMonth());
            Log.d("Time", date.getHourOfDay()+" "+notice+" "+timeDuration+" "+date.toString());
            if (timeDuration < notice){
                for (Availability availability : availabilityArrayList) {
                    if (date.withTimeAtStartOfDay().minusMinutes(1).toLocalTime().isAfter(LocalTime.parse(availability.getFrom()))){
                        blockedIdArray.add(availability.getId());
                    }
                }
            }else {
                for (Availability availability : availabilityArrayList) {
                    if (date.plusHours(noticePeriod).toLocalTime().isAfter(LocalTime.parse(availability.getFrom()))){
                        blockedIdArray.add(availability.getId());
                    }
                }
            }
            Log.d("Time notification block", key+" "+blockedIdArray.toString());

            if (blockDaysMap.get(key) != TOTALLY_BOOKED) {
                noticeBlockDaysPerBlockMap.put(key, blockedIdArray);
            }
            date = date.withTimeAtStartOfDay().plusDays(1);
            notice = notice-24;
            timeDuration = 23;
        }while (notice > 0);
    }


    /*
    *  Block BlockBase Booking days on Calender partially/Full
    *  Created By kasunka on 8/04/17
    */
    private void blockPerBlockBookingDays(ArrayList<BookingDate> futureBookingDates) {
        for (BookingDate booking : futureBookingDates) {
            blockStartDay = formatter.parseDateTime(booking.getFrom());
            blockEndDay = formatter.parseDateTime(booking.getTo());

            if (blockStartDay.toLocalDate().equals(blockEndDay.toLocalDate())) {
                blockPerBlockPartiallyBookingDates(blockStartDay, blockEndDay);
            } else {
                blockPerBlockPartiallyBookingDates(blockStartDay, blockStartDay.withTimeAtStartOfDay().plusHours(24).minusMinutes(1));
//                for (int i = blockStartDay.plusDays(1).getDayOfMonth(); i < blockEndDay.getDayOfMonth(); i++) {
//                    blockDaysMap.put(blockStartDay.getYear() + String.format("%02d", blockStartDay.getMonthOfYear() - 1) + String.format("%02d", i), TOTALLY_BOOKED);
//                }
                int numberOfdays = Days.daysBetween(blockStartDay,blockEndDay).getDays();
                for (int i = 1; i < numberOfdays; i++) {
                    DateTime loopingDate = blockStartDay.plusDays(i);
                    blockDaysMap.put(loopingDate.getYear() + String.format("%02d", loopingDate.getMonthOfYear() - 1) + String.format("%02d", loopingDate.getDayOfMonth()), TOTALLY_BOOKED);
                }
                blockPerBlockPartiallyBookingDates(blockEndDay.withTimeAtStartOfDay(), blockEndDay);
            }
        }
    }

    /*
    *  Check BlockBase Booking days on Calender partially/full
    *  Created By kasunka on 8/04/17
    */
    private void blockPerBlockPartiallyBookingDates(DateTime startTime, DateTime endTime) {
        String key = startTime.getYear() + String.format("%02d", startTime.getMonthOfYear() - 1) + String.format("%02d", startTime.getDayOfMonth());
        Log.d("Temp",key+" "+startTime.toString()+" "+endTime.toString());
        Log.d("weekday", startTime.getDayOfWeek() + "");
        ArrayList<Integer> blockedIdArray;
        if (blockDaysMap.containsKey(key) && !blockDaysMap.get(key).contains(TOTALLY_BOOKED)) {
            Log.d("PartiallyBlocked B", startTime.toLocalTime() + "-" + endTime.toLocalTime() + " " + key);
            blockedIdArray = partiallyBlockDaysPerBlockMap.get(key);
            blockedIdArray.addAll(getDayAvailabilityIdList(key, startTime, endTime));
            partiallyBlockDaysPerBlockMap.put(key, blockedIdArray);
        } else {
            Log.d("PartiallyBlocked A", startTime.toLocalTime() + "-" + endTime.toLocalTime() + " " + key);
            blockedIdArray = getDayAvailabilityIdList(key, startTime, endTime);
            partiallyBlockDaysPerBlockMap.put(key, blockedIdArray);
        }
    }


    /*
    *  Get Block Base Availability ID in Availability Array
    *  Created By kasunka on 8/04/17
    */
    private ArrayList<Integer> getDayAvailabilityIdList(String key, DateTime startTime, DateTime endTime) {
        ArrayList<Integer> blockedIdArray = new ArrayList<>();
        ArrayList<Availability> availabilityArrayList = getDayAvailabilityPerBlock(startTime.getDayOfWeek());
        int bookedBlockCount = 0;
        for (Availability availability : availabilityArrayList) {
            LocalTime blockStart = LocalTime.parse(availability.getFrom());
            LocalTime blockEnd = LocalTime.parse(availability.getTo());

            if (blockStart.isBefore(startTime.toLocalTime())) {
                if (blockEnd.isAfter(startTime.toLocalTime())) {
                    blockDaysMap.put(key, PARTIALLY_BOOKED);
                    blockedIdArray.add(availability.getId());
                    bookedBlockCount++;
                }
            } else if (blockStart.isBefore(endTime.toLocalTime())) {
                blockDaysMap.put(key, PARTIALLY_BOOKED);
                blockedIdArray.add(availability.getId());
                bookedBlockCount++;
            } else if (blockStart.toString().contains(startTime.toLocalTime().toString())){
                blockDaysMap.put(key, PARTIALLY_BOOKED);
                blockedIdArray.add(availability.getId());
                bookedBlockCount++;
            }
        }

        if (availabilityArrayList.size() <= bookedBlockCount) {
            blockDaysMap.put(key, TOTALLY_BOOKED);
        }
        return blockedIdArray;
    }

    /*
    *  Get Blocked Base Matched days on Server days And Calender Days on week (Monday ==1)
    *  Created By kasunka on 8/04/17
    */
    public ArrayList<Availability> getDayAvailabilityPerBlock(int dayOfWeek) {
        if (dayOfWeek <= 5) {
            dayOfWeek = 8;
        }

        ArrayList<Availability> avai = new ArrayList<>();
        for (Availability availability : availabilities) {
            Log.d("Block B", availability.getDay() + "");
            if (availability.getDay() == dayOfWeek) {
                avai.add(availability);
            }
        }
        return avai;
    }

    /*
    *  Block Block Base booking hours on Calendar Day view
    *  Created By kasunka on 9/27/17
    */
    public ArrayList<Availability> getUnavailableHoursPerBlock(){
        ArrayList<Integer> noticedBlockIds = noticeBlockDaysPerBlockMap.get(getSelectedDate())== null?
                new ArrayList<Integer>() :noticeBlockDaysPerBlockMap.get(getSelectedDate());
        ArrayList<Integer> blockIds = partiallyBlockDaysPerBlockMap.get(getSelectedDate())== null?
                new ArrayList<Integer>() :partiallyBlockDaysPerBlockMap.get(getSelectedDate());
        blockedBlockMap = getDayAvailabilityPerBlock(getSelectedDateObject().getDayOfWeek());

        for (int i = 0; i<blockedBlockMap.size();i++){
            for (int j = 0; j<noticedBlockIds.size();j++) {
                if (noticedBlockIds.get(j) == blockedBlockMap.get(i).getId()){
                    blockedBlockMap.get(i).setAvailabilityStatus(NOTICED_HOUR);
                }
            }
        }

        for (int i = 0; i<blockedBlockMap.size();i++){
            for (int j = 0; j<blockIds.size();j++) {
                if (blockIds.get(j) == blockedBlockMap.get(i).getId()){
                    blockedBlockMap.get(i).setAvailabilityStatus(TOTALLY_BOOKED);
                }
            }
        }
        Collections.sort(blockedBlockMap);
        return blockedBlockMap;
    }


    /*
    *  Block HourBase Current date Past Hours on Calender partially/Full
    *  Created By kasunka on 12/08/17
    */
    private void blockPerHourCurrentDatePastHours(DateTime today, String key) {
        if (blockDaysMap.get(key) != TOTALLY_BOOKED && today.toLocalTime().isBefore(LocalTime.parse(getDayAvailability(today.getDayOfWeek()).getTo()))) {
            partiallyBlockDaysPerHourMap.put(key, new ArrayList<>(Arrays.asList(TOTALLY_BOOKED+ "-" +today.withTimeAtStartOfDay().toLocalTime() + "-" + today.plusHours(1).toLocalTime())));
        }else {
            blockDaysMap.put(key, TOTALLY_BOOKED);
        }
    }

    /*
    *  Block HourBase Notice Period on Calender partially/Full
    *  Created By kasunka on 8/04/17
    */
    private void blockPerHourNoticePeriod(DateTime today, int noticePeriod) {
        Log.d("Time",today.getHourOfDay()+" "+today.withTimeAtStartOfDay().getHourOfDay());
        DateTime date = today.plusHours(1);
        int notice = noticePeriod, timeDuration = 23 - date.getHourOfDay();
        String key;
        do {
            ArrayList<String> notificationBlock = new ArrayList<>();
            key = date.getYear() + String.format("%02d", date.getMonthOfYear() - 1) + String.format("%02d", date.getDayOfMonth());
            Log.d("Time", date.getHourOfDay()+" "+notice+" "+timeDuration+" "+date.toString());
            if (timeDuration < notice){
                notificationBlock.add(NOTICED_HOUR + "-" + date.getHourOfDay()+":00:00.000" + "-" + date.withTimeAtStartOfDay().minusMinutes(1).toLocalTime());
            }else {
                notificationBlock.add(NOTICED_HOUR + "-" + date.getHourOfDay()+":00:00.000" + "-" + date.plusHours(noticePeriod).getHourOfDay()+":00:00.000");
            }
            Log.d("Time notification block", key+" "+notificationBlock.toString());

            if (partiallyBlockDaysPerHourMap.containsKey(key)){
                ArrayList<String> blockHours = partiallyBlockDaysPerHourMap.get(key);
                blockHours.addAll(notificationBlock);
            }else {
                partiallyBlockDaysPerHourMap.put(key,notificationBlock);
            }

            if (blockDaysMap.get(key) != TOTALLY_BOOKED) {
                blockDaysMap.put(key, NOTICED_HOUR);
            }
            date = date.withTimeAtStartOfDay().plusDays(1);
            notice = notice-24;
            timeDuration = 23;
        }while (notice > 0);
    }

    /*
    *  Block HourBase Booking days on Calender partially/Full
    *  Created By kasunka on 8/04/17
    */
    private void blockPerHourBookingDays(ArrayList<BookingDate> futureBookingDates) {
        for (BookingDate booking : futureBookingDates) {
            blockStartDay = formatter.parseDateTime(booking.getFrom()).minusHours(bufferTime);
            blockEndDay = formatter.parseDateTime(booking.getTo()).plusHours(bufferTime);

            if (blockStartDay.toLocalDate().equals(blockEndDay.toLocalDate())) {
                blockPerHourPartiallyBookingDates(blockStartDay, blockEndDay);
            } else {
                blockPerHourPartiallyBookingDates(blockStartDay, blockStartDay.withTimeAtStartOfDay().plusDays(1).minusMinutes(1));
                int numberOfdays = Days.daysBetween(blockStartDay.withTimeAtStartOfDay(),blockEndDay.withTimeAtStartOfDay()).getDays();
                for (int i = 1; i < numberOfdays; i++) {
                    DateTime loopingDate = blockStartDay.plusDays(i);
                    blockDaysMap.put(loopingDate.getYear() + String.format("%02d", loopingDate.getMonthOfYear() - 1) + String.format("%02d", loopingDate.getDayOfMonth()), TOTALLY_BOOKED);
                }
                blockPerHourPartiallyBookingDates(blockEndDay.withTimeAtStartOfDay(), blockEndDay);
            }
        }
    }

    /*
    *  Check Hour Base Booking days on Calender partially/full
    *  Created By kasunka on 8/04/17
    */
    private void blockPerHourPartiallyBookingDates(DateTime startTime, DateTime endTime) {
        String key = startTime.getYear() + String.format("%02d", startTime.getMonthOfYear() - 1) + String.format("%02d", startTime.getDayOfMonth());
        Log.d("Temp",key+" "+startTime.toString()+" "+endTime.toString());
        ArrayList<String> blockedHours;
//        startTime = startTime.minusHours(bufferTime).isBefore(startTime.withTimeAtStartOfDay())?startTime.withTimeAtStartOfDay():startTime.minusHours(bufferTime);
//        endTime = endTime.plusHours(bufferTime).isAfter(endTime.withTimeAtStartOfDay().plusDays(1).minusMillis(1))?endTime.withTimeAtStartOfDay().minusMillis(1):endTime.plusHours(bufferTime);
        Log.d("Temp",key+" "+startTime.toString()+" "+endTime.toString());
        if (blockDaysMap.containsKey(key) && blockDaysMap.get(key)!=TOTALLY_BOOKED) {
            Log.d("PartiallyBlocked B", startTime.toLocalTime() + "-" + endTime.toLocalTime() + " " + key+" "+blockDaysMap.get(key));
            blockedHours = partiallyBlockDaysPerHourMap.get(key);
            blockedHours.add(TOTALLY_BOOKED + "-" + startTime.toLocalTime() + "-" + endTime.toLocalTime()); // Done Plus or minus buffer time according to the Space definition
            Log.d("PartiallyBlocked B", blockedHours.toString());
            partiallyBlockDaysPerHourMap.put(key, blockedHours);

        } else {
            Log.d("PartiallyBlocked A", startTime.toLocalTime() + "-" + endTime.toLocalTime() + " " + key);
            blockedHours = new ArrayList<>(Arrays.asList(TOTALLY_BOOKED + "-" + startTime.toLocalTime() + "-" + endTime.toLocalTime())); // Done Plus or minus buffer time according to the Space definition
            partiallyBlockDaysPerHourMap.put(key, blockedHours);
        }
        isFullyBooked(blockedHours, startTime.getDayOfWeek(), key);
    }


    /*
    *  Check Hour Base Booking days on Calendar Full blocked days
    *  Created By kasunka on 8/04/17
    */
    private boolean isFullyBooked(ArrayList<String> blockedHours, int dayOfWeek, String key) {
        boolean isFullyBlokced = false;
        int totalBookingHours = 0;
        LocalTime openTime = null;
        LocalTime closeTime = null;
        for (String time : blockedHours) {
            String times[] = time.split("-");
            LocalTime startTime = LocalTime.parse(times[1]);
            LocalTime endTime = LocalTime.parse(times[2]);

            if (blockDaysMap.containsKey(key) && blockDaysMap.get(key).contains(TOTALLY_BOOKED)){
                isFullyBlokced = true;
                return isFullyBlokced;
            }

            Availability availability = getDayAvailability(dayOfWeek);
            openTime = LocalTime.parse(availability.getFrom());
            closeTime = LocalTime.parse(availability.getTo());

            if (startTime.isBefore(openTime)) {
                if (endTime.isAfter(openTime)) {
                    if (endTime.isAfter(closeTime)) {
                        isFullyBlokced = true;
                        blockDaysMap.put(key, TOTALLY_BOOKED);
                    } else {
                        totalBookingHours += Hours.hoursBetween(openTime, endTime).getHours();
                        blockDaysMap.put(key, PARTIALLY_BOOKED);
                    }
                }
            } else {
                if (startTime.isBefore(closeTime)) {
                    if (endTime.isBefore(closeTime)) {
                        totalBookingHours += Hours.hoursBetween(startTime, endTime).getHours();
                        blockDaysMap.put(key, PARTIALLY_BOOKED);
                    } else {
                        totalBookingHours += Hours.hoursBetween(startTime, closeTime).getHours();
                        blockDaysMap.put(key, PARTIALLY_BOOKED);
                    }
                }
            }
            Log.d("Hours", totalBookingHours + "");

        }

        if (!isFullyBlokced && totalBookingHours >= Hours.hoursBetween(openTime, closeTime).getHours()) {
            blockDaysMap.put(key, TOTALLY_BOOKED);
        }
        return isFullyBlokced;
    }

    /*
    *  Block Hour Base booking hours on Calendar Day view
    *  Created By kasunka on 9/24/17
    */
    public HashMap<String ,String> getUnavailableHoursPerHour(String selectedDay){
        ArrayList<String> blockedHours = partiallyBlockDaysPerHourMap.get(selectedDay);
        if (blockedHours != null) {
            for (String duration : blockedHours) {
                String[] limits = duration.split("-");
                String blockType = limits[0];
                LocalTime blockStartTime = LocalTime.parse(limits[1]);
                LocalTime blockEndTime = LocalTime.parse(limits[2]);

                Log.d("Map a", blockStartTime.getHourOfDay() + " " + blockEndTime.getHourOfDay() + " " + Hours.hoursBetween(blockStartTime, blockEndTime).getHours());
                for (int hourDuration = 0; hourDuration < Math.abs(Hours.hoursBetween(blockStartTime, blockEndTime).getHours()); hourDuration++) {
                    Log.d("Map b", blockStartTime.getHourOfDay() + " " + blockEndTime.getHourOfDay() + " " + hourDuration);
                    blockedHoursMap.put(blockStartTime.plusHours(hourDuration).hourOfDay().getLocalTime().toString(), blockType.contains(TOTALLY_BOOKED)?TOTALLY_BOOKED:NOTICED_HOUR);
                }
                Log.d("Map c", blockedHoursMap.toString());
            }
        }
        return blockedHoursMap;
    }


    /*
    *  Get Matched days on Server days And Calender Days on week (Monday ==1)
    *  Created By kasunka on 8/04/17
    */
    public Availability getDayAvailability(int dayOfWeek) {
        Availability avai = null;
        for (Availability availability : availabilities) {
            if (availability.getDay() == dayOfWeek) {
                avai = availability;
            }
        }
        return avai;
    }

    /*
    *  Get Starting and closing hours of the space according to the Selected date
    *  Created By kasunka on 8/04/17
    */
    public String[] hourLimitsForTheDay() {
        Log.d("Selected Day", getSelectedDateObject() + "");
        String[] limits = new String[2];
        for (Availability availability : availabilities) {
            if (availability.getDay() == getSelectedDateObject().getDayOfWeek()) {
                limits[0] = availability.getFrom()!="00:00:00"?availability.getFrom():"00:00:01";
                limits[1] = TextUtils.equals(availability.getTo(),"00:00:00")?"23:59:59":availability.getTo();
            }
        }
        return limits;
    }

    /*
    *  Block Later days on Calender from the given day by host
    *  Created By kasunka on 8/04/17
    */
    private void blockEndDays(String[] enddate) {
        DateTime calenderEndDate = formatter.parseDateTime(Integer.parseInt(enddate[0])+"-"+Integer.parseInt(enddate[1])+"-"+Integer.parseInt(enddate[2]) + "T00:00");
        for (int i = Integer.parseInt(enddate[2]) + 1; i <= calenderEndDate.dayOfMonth().getMaximumValue(); i++) {
            blockDaysMap.put(enddate[0] + String.format("%02d", Integer.parseInt(enddate[1]) - 1) + String.format("%02d", i), TOTALLY_BOOKED);
        }
        for (String key : blockDaysMap.keySet()) {
            Log.d("days", key + " " + blockDaysMap.get(key));

        }
    }


    /*
    *  HOST Block HourBase/BlockBase Booking days on Calender partially/Full
    *  Created By kasunka on 8/04/17
    */
    private void blockPerHourHostBookingDays(ArrayList<BookingDate> futureBookingDates) {
        for (BookingDate booking : futureBookingDates) {
            blockStartDay = formatter.parseDateTime(booking.getFrom());
            blockEndDay = booking.getTo().split("T")[1].contains("00:00")?formatter.parseDateTime(booking.getTo()).minusMinutes(1):formatter.parseDateTime(booking.getTo());

            if (blockStartDay.toLocalDate().equals(blockEndDay.toLocalDate())) {
                blockPerHourPartiallyBookingHostDates(blockStartDay, blockEndDay, booking.isManual(),booking.getId());
            } else {
                blockPerHourPartiallyBookingHostDates(blockStartDay, blockStartDay.withTimeAtStartOfDay().plusDays(1).minusMillis(1),booking.isManual(),booking.getId());
                int numberOfdays = Days.daysBetween(blockStartDay,blockEndDay).getDays();
                for (int i = 1; i < numberOfdays; i++) {
                    DateTime loopingDate = blockStartDay.plusDays(i);
                    String key = loopingDate.getYear() + String.format("%02d", loopingDate.getMonthOfYear() - 1) + String.format("%02d", loopingDate.getDayOfMonth());
                    blockDaysMap.put(key, TOTALLY_BOOKED);
                    partiallyBlockDaysPerHourMap.put(key, new ArrayList<>(Arrays.asList("00:00:00.000" + "-" + "23:59:59.999"+"-"+booking.isManual()+"-"+booking.getId())));
                }
                blockPerHourPartiallyBookingHostDates(blockEndDay.withTimeAtStartOfDay(), blockEndDay,booking.isManual(),booking.getId());
            }
        }
    }


    /*
    *  HOST Check Hour Base Booking days on Calender partially/full
    *  Created By kasunka on 8/04/17
    */
    private void blockPerHourPartiallyBookingHostDates(DateTime startTime, DateTime endTime, int isManual, int bookingId) {
        String key = startTime.getYear() + String.format("%02d", startTime.getMonthOfYear() - 1) + String.format("%02d", startTime.getDayOfMonth());
        Log.d("Temp",key+" "+startTime.toString()+" "+endTime.toString());
        ArrayList<String> blockedHours;
        Log.d("Temp",key+" "+startTime.toString()+" "+endTime.toString());
        if (blockDaysMap.containsKey(key) && blockDaysMap.get(key)!=TOTALLY_BOOKED) {
            Log.d("PartiallyBlocked B", startTime.toLocalTime() + "-" + endTime.toLocalTime() + " " + key+" "+blockDaysMap.get(key));
            blockedHours = partiallyBlockDaysPerHourMap.get(key);
            blockedHours.add(startTime.toLocalTime() + "-" + endTime.toLocalTime()+"-"+isManual+"-"+bookingId);
            Log.d("PartiallyBlocked B", blockedHours.toString());
            partiallyBlockDaysPerHourMap.put(key, blockedHours);

        } else {
            Log.d("PartiallyBlocked A", startTime.toLocalTime() + "-" + endTime.toLocalTime() + " " + key);
            blockedHours = new ArrayList<>(Arrays.asList(startTime.toLocalTime() + "-" + endTime.toLocalTime()+"-"+isManual+"-"+bookingId));
            partiallyBlockDaysPerHourMap.put(key, blockedHours);
        }
        isFullyBookedHost(blockedHours, key);
    }


    /*
    *  HOST Check HourBase/BlockBase Booking days on Calendar Full blocked days
    *  Created By kasunka on 8/04/17
    */
    private boolean isFullyBookedHost(ArrayList<String> blockedHours, String key) {
        boolean isFullyBlockced = false;
        int totalBookingHours = 0;
        LocalTime openTime = null;
        LocalTime closeTime = null;
        for (String time : blockedHours) {
            String times[] = time.split("-");
            LocalTime startTime = LocalTime.parse(times[0]);
            LocalTime endTime = LocalTime.parse(times[1]);

            openTime = LocalTime.parse("00:00:00.000");
            closeTime = LocalTime.parse("23:59:59.999");

            if (startTime.isBefore(openTime)) {
                if (endTime.isAfter(openTime)) {
                    if (endTime.isAfter(closeTime)) {
                        isFullyBlockced = true;
                        blockDaysMap.put(key, TOTALLY_BOOKED);
                    } else {
                        totalBookingHours += Hours.hoursBetween(openTime, endTime).getHours();
                        blockDaysMap.put(key, PARTIALLY_BOOKED);
                    }
                }
            } else {
                if (startTime.isBefore(closeTime)) {
                    if (endTime.isBefore(closeTime)) {
                        totalBookingHours += Hours.hoursBetween(startTime, endTime).getHours();
                        blockDaysMap.put(key, PARTIALLY_BOOKED);
                    } else {
                        totalBookingHours += Hours.hoursBetween(startTime, closeTime).getHours();
                        blockDaysMap.put(key, PARTIALLY_BOOKED);
                    }
                }
            }
            Log.d("Hours", totalBookingHours + "");
        }

        if (!isFullyBlockced && totalBookingHours >= Hours.hoursBetween(openTime, closeTime).getHours()) {
            blockDaysMap.put(key, TOTALLY_BOOKED);
        }
        return isFullyBlockced;
    }

    /*
    *  Block Hour Base booking hours on Calendar Day view
    *  Created By kasunka on 9/24/17
    */
    public HashMap<String ,String> getUnavailableHoursPerHourHost(String selectedDay){
        ArrayList<String> blockedHours = partiallyBlockDaysPerHourMap.get(selectedDay);
        if (blockedHours != null) {
            for (String duration : blockedHours) {
                String[] limits = duration.split("-");
                LocalTime blockStartTime = LocalTime.parse(limits[0]);
                LocalTime blockEndTime = LocalTime.parse(limits[1]);
                int isManual = Integer.valueOf(limits[2]);
                int bookingId = Integer.valueOf(limits[3]);
                int numberOfHours = blockEndTime.isBefore(LocalTime.parse("23:00:00"))?Math.abs(Hours.hoursBetween(blockStartTime, blockEndTime).getHours()):
                        Math.abs(Hours.hoursBetween(blockStartTime, blockEndTime).getHours())+1;

                Log.d("Map a", blockStartTime.getHourOfDay() + " " + blockEndTime.getHourOfDay() + " " + Hours.hoursBetween(blockStartTime, blockEndTime).getHours());
                for (int hourDuration = 0; hourDuration < numberOfHours; hourDuration++) {
                    Log.d("Map b", blockStartTime.getHourOfDay() + " " + blockEndTime.getHourOfDay() + " " + hourDuration);

                    if (isManual == 0){
                        if (hourDuration == 0){
                            blockedHoursMap.put(blockStartTime.plusHours(hourDuration).hourOfDay().getLocalTime().toString(), TOTALLY_BOOKED_MS_FIRST+"-"+bookingId);
                        }else {
                            blockedHoursMap.put(blockStartTime.plusHours(hourDuration).hourOfDay().getLocalTime().toString(), TOTALLY_BOOKED_MS+"-"+bookingId);
                        }
                    }else {
                        blockedHoursMap.put(blockStartTime.plusHours(hourDuration).hourOfDay().getLocalTime().toString(), TOTALLY_BOOKED+"-"+bookingId);
                    }
                }
                Log.d("Map c", blockedHoursMap.toString());
            }
        }
        return blockedHoursMap;
    }

    /*
    *  Clear Calendar Selected BookingDateTime on Click event
    *  Created By kasunka on 9/25/17
    */
    public void clearSelectedDate() {
        Log.d("Selected d", selectedDate != null? selectedDate.toString():"null" + " = "+selectedDateStatus);
        if (blockDaysMap.containsKey(selectedDate)) {
            if (selectedDateStatus != null){
                blockDaysMap.put(selectedDate,selectedDateStatus);
                setSelectedDateStatus(null);
            }else {
                blockDaysMap.remove(selectedDate);
            }
        }
    }

    public HashMap<String, String> getBlockDaysMap() {
        return blockDaysMap;
    }

    public HashMap<String, String> getBlockedHoursMap() {
        return blockedHoursMap;
    }

    public HashMap<String, ArrayList<Integer>> getPartiallyBlockDaysPerBlockMap() {
        return partiallyBlockDaysPerBlockMap;
    }

    public ArrayList<Availability> getBlockedBlockMap() {
        return blockedBlockMap;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public int getNumberOfMonths() {
        return numberOfMonths;
    }

    public String getAvailabilityMethod() {
        return availabilityMethod;
    }

    public void setSelectedDate(String selectedDate) {
        this.selectedDate = selectedDate;
        Log.d("Selected a", selectedDate+ " "+ blockDaysMap.get(selectedDate));
        setSelectedDateStatus(blockDaysMap.get(selectedDate));
        blockDaysMap.put(selectedDate, SELECTED_DAY);
        Log.d("Selected b", selectedDate+" "+blockDaysMap.get(selectedDate));
    }

    public String getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDateStatus(String selectedDateStatus) {
        this.selectedDateStatus = selectedDateStatus;
    }

    public DateTime getSelectedDateObject() {
        return selectedDateObject;
    }

    public void setSelectedDateObject(String selectedDateObject) {
        this.selectedDateObject = formatter.parseDateTime(selectedDateObject+ "T00:00");
        Log.d("Selected c", selectedDateObject.toString());
    }

    public int getBlockChargeType() {
        return blockChargeType;
    }

    public int getNumberOfMinCount() {
        return numberOfMinCount;
    }

    public int getNumberOfGuests() {
        return numberOfGuests;
    }

    public int getNoticePeriod() {
        return noticePeriod;
    }
}

