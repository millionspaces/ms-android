package com.millionspaces.mobile.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.millionspaces.mobile.R;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by kasunka on 10/10/17.
 */

public class BookingPagerAdapter extends FragmentPagerAdapter{
    private Context mContext;
    private ArrayList<Fragment> list;
    private int tabTitles[] =  new int[] { R.string.booking_list_tab_title_past, R.string.booking_list_tab_title_upcoming };
    private int[] imageResId = { R.drawable.background_tab_selector_icon_past, R.drawable.background_tab_selector_icon_upcoming};

    public BookingPagerAdapter(FragmentManager fm, Context context, ArrayList<Fragment> fragList) {
        super(fm);
        this.mContext = context;
        this.list = fragList;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public View getTabView(int position) {
        View v = null;
        if (position == 0) {
            v = LayoutInflater.from(mContext).inflate(R.layout.tab_view_booking_list_past, null);
        }else {
            v = LayoutInflater.from(mContext).inflate(R.layout.tab_view_booking_list_upcomming, null);
        }
        TextView title = ButterKnife.findById(v,R.id.tab_title);
        title.setText(tabTitles[position]);
        ImageView img = ButterKnife.findById(v,R.id.tab_icon);
        img.setImageResource(imageResId[position]);
        return v;
    }
}
