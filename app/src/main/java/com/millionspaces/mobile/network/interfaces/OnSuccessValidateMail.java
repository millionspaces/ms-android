package com.millionspaces.mobile.network.interfaces;

/**
 * Created by kasunka on 8/23/17.
 */

public interface OnSuccessValidateMail extends OnErrorResponse {
    void onSuccesValidate(boolean isValid);
}
