package com.millionspaces.mobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.millionspaces.mobile.actions.OnItemSelected;
import com.millionspaces.mobile.entities.Location;
import com.millionspaces.mobile.utils.AnimationsUtils;
import com.millionspaces.mobile.utils.DataBaseUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.FUNCTION_EVENT_TYPE;
import static com.millionspaces.mobile.utils.Config.HOME_KEY_EVENT_TYPE;
import static com.millionspaces.mobile.utils.Config.HOME_KEY_LOCATION;
import static com.millionspaces.mobile.utils.Config.PLACE_AUTOCOMPLETE_REQUEST_CODE;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_EVENT_HOME_SEARCH;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_HOME_EVENT_TYPE;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_HOME_EVENT_TYPE_EMPTY;
import static com.millionspaces.mobile.utils.PopUpViewUtils.showDropDownEventType;

public class HomeActivity extends BaseHomeActivity implements View.OnClickListener, OnItemSelected {
    @BindView(R.id.search_container_box)
    LinearLayout searchContainer;
    @BindView(R.id.home_search_box_container)
    LinearLayout boxContainer;
    @BindView(R.id.home_search_box)
    LinearLayout searchBox;
    @BindView(R.id.home_search_location_box)
    LinearLayout searchLocationBox;
    @BindView(R.id.home_search_action_box)
    LinearLayout searchActionBox;
    @BindView(R.id.home_search_icon)
    ImageView searchIcon;
    @BindView(R.id.home_search_text)
    TextView searchHint;
    @BindView(R.id.home_location_text)
    TextView locationHint;
    @BindView(R.id.home_search_button)
    Button search;
    @BindView(R.id.home_meeting_btn)
    RelativeLayout meeting;
    @BindView(R.id.home_weddig_btn)
    RelativeLayout weeding;
    @BindView(R.id.home_interview_btn)
    RelativeLayout interview;
    @BindView(R.id.home_photo_btn)
    RelativeLayout photo;
    @BindView(R.id.home_party_btn)
    RelativeLayout party;

    @BindView(R.id.home_meeting_img)
    ImageView meetingImg;
    @BindView(R.id.home_party_img)
    ImageView partyImg;
    @BindView(R.id.home_photo_img)
    ImageView photoImg;
    @BindView(R.id.home_wedding_img)
    ImageView weddingImg;
    @BindView(R.id.home_interview_img)
    ImageView interviewImg;

    private int eventId;
    private Location location = null;
    private PopupWindow popupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        init_views();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        eventId = 0;
        location = null;
        searchHint.setText(getResources().getString(R.string.home_event_type_action));
        searchHint.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryTextLightest));
        locationHint.setText(getResources().getString(R.string.home_location_action));
        locationHint.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryTextLightest));
    }

    private void init_views() {
        init_toolbar();
        init_drawer(true);
        init_navigationview();
        init_Actions();

        Glide.with(this).load(R.drawable.home_main_meeting).into(meetingImg);
        Glide.with(this).load(R.drawable.home_main_party).into(partyImg);
        Glide.with(this).load(R.drawable.home_main_photoshoot).into(photoImg);
        Glide.with(this).load(R.drawable.home_main_wedding).into(weddingImg);
        Glide.with(this).load(R.drawable.home_main_interviews).into(interviewImg);
    }

    private void init_Actions() {
        searchContainer.setOnClickListener(this);
        searchBox.setOnClickListener(this);
        searchIcon.setOnClickListener(this);
        searchLocationBox.setOnClickListener(this);
        search.setOnClickListener(this);
        meeting.setOnClickListener(this);
        weeding.setOnClickListener(this);
        interview.setOnClickListener(this);
        photo.setOnClickListener(this);
        party.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.search_container_box:
                hideActionLayouts();
                break;
            case R.id.home_search_box:
                if (searchLocationBox.isShown()){
                    showEventTypePopUp(v);
                }else {
                    showActionLayouts();
                }
                break;
            case R.id.home_search_icon:
                showActionLayouts();
                break;
            case R.id.home_search_location_box:
                init_search_place();
                break;
            case R.id.home_search_button:
                filterSpaces(eventId,location);
                break;
            case R.id.home_meeting_btn:
                eventId = 4;
                filterSpaces(eventId,null);
                break;
            case R.id.home_weddig_btn:
                eventId = 2;
                filterSpaces(eventId,null);
                break;
            case R.id.home_interview_btn:
                eventId = 5;
                filterSpaces(eventId,null);
                break;
            case R.id.home_photo_btn:
                eventId = 7;
                filterSpaces(eventId,null);
                break;
            case R.id.home_party_btn:
                eventId = 1;
                filterSpaces(eventId,null);
                break;
        }
    }

    /*Start Space List with/Without Filters
    Created 26/06/2017 By Kasunka*/
    private void filterSpaces(int events, Location location) {
        Bundle mBundle = new Bundle();
        mBundle.putInt(HOME_KEY_EVENT_TYPE,events);
        mBundle.putParcelable(HOME_KEY_LOCATION,location);
        mBundle.putString(FIREBASE_PARAM_HOME_EVENT_TYPE,location != null? location.getAddress():FIREBASE_PARAM_HOME_EVENT_TYPE_EMPTY);
        mFirebaseAnalytics.logEvent(FIREBASE_EVENT_HOME_SEARCH, mBundle);
        navigateActivityWithExtra(SpacesListActivity.class,mBundle);
    }

    /*Show Action Layout
    Created 26/06/2017 By Kasunka*/
    private void hideActionLayouts() {
        if (searchLocationBox.isShown()) {
            searchLocationBox.setVisibility(View.GONE);
            searchActionBox.setVisibility(View.GONE);
            searchIcon.setVisibility(View.VISIBLE);
            searchHint.setText(getResources().getString(R.string.home_search_hint));
            AnimationsUtils.collapse(boxContainer);
            searchHint.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryTextLightest));
            locationHint.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryTextLightest));
        }
    }

    /*Hide Action Layout
    Created 26/06/2017 By Kasunka*/
    private void showActionLayouts() {
        if (!searchLocationBox.isShown()) {
            searchLocationBox.setVisibility(View.VISIBLE);
            searchActionBox.setVisibility(View.VISIBLE);
            searchIcon.setVisibility(View.GONE);
            searchHint.setText(getResources().getString(R.string.home_event_type_action));
            locationHint.setText(getResources().getString(R.string.home_location_action));
            AnimationsUtils.expand(boxContainer);
        }
    }

    /*Show event type list
    Created 26/06/2017 By Kasunka*/
    private void showEventTypePopUp(View v){
        popupWindow = showDropDownEventType(this, this ,v,new DataBaseUtils(this).getEventTypes(),1);
    }

    @Override
    public Activity currentActivity() {
        return this;
    }

    @Override
    public void onItemSelected(int position, int id,String value, int functionCode) {
        switch (functionCode){
            case FUNCTION_EVENT_TYPE:
                searchHint.setText(value);
                searchHint.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryTextLight));
                this.eventId = id;
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                locationHint.setText(place.getName());
                locationHint.setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryTextLight));
                location = new Location(place.getLatLng().latitude, place.getLatLng().longitude,place.getName().toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                // TODO: Customize the error screen if required, Handled the error by placeholder.

            } else if (resultCode == RESULT_CANCELED) {
                // TODO: Customize the error screen if required, Handled the error by placeholder.
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (null != popupWindow && popupWindow.isShowing()){
            popupWindow.dismiss();
        }else {
            super.onBackPressed();
        }
    }
}
