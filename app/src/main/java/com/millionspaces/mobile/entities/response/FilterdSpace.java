package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by kasunka on 7/26/17.
 */

public class FilterdSpace implements Parcelable{
    private int count;
    private ArrayList<Space> spaces;

    protected FilterdSpace(Parcel in) {
        count = in.readInt();
        spaces = in.createTypedArrayList(Space.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(count);
        dest.writeTypedList(spaces);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FilterdSpace> CREATOR = new Creator<FilterdSpace>() {
        @Override
        public FilterdSpace createFromParcel(Parcel in) {
            return new FilterdSpace(in);
        }

        @Override
        public FilterdSpace[] newArray(int size) {
            return new FilterdSpace[size];
        }
    };

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Space> getSpaces() {
        return spaces;
    }

    public void setSpaces(ArrayList<Space> spaces) {
        this.spaces = spaces;
    }
}
