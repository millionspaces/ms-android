package com.millionspaces.mobile.utils;


import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.entities.SpaceType;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by kasun on 2/7/17.
 */

public class Config {

//    Map Location for filter place API service, preSet To Sri Lanaka
    public static final String MAP_PLACE_LOCATION = "LK";

//    SharedPreferences keys
    public static final String KEY_IS_INITIAL_LOUNCH = "is_initial_lounch";
    public static final String KEY_IS_UPDATED_META = "is_updated_meta";
    public static final String KEY_UPDATED_TIMESTAMP = "updated_timestamp";
    public static final String KEY_IS_LOGGED_IN = "is_logged_in";
    public static final String KEY_LOGGED_USER_NAME = "username";
    public static final String KEY_LOGGED_USER_EMAIL = "useremail";
    public static final String KEY_LOGGED_USER_ID = "userid";
    public static final String KEY_LOGGED_USER_IMAGE = "userimage";
    public static final String KEY_LOGGED_USER_ROLE = "userrole";
    public static final String KEY_CURRENT_CHECKING_SPACE = "currentspace";
    public static final String KEY_CURRENT_CHECKING_EVENT_TYPE = "eventypeid";
    public static final String KEY_CURRENT_CHECKING_GUEST_COUNT = "cuestcount";
    public static final String KEY_CURRENT_CHECKING_SEATING_ID = "seatingid";

//    Intent Keys
    public static final String HOME_KEY_LOCATION = "location";
    public static final String HOME_KEY_EVENT_TYPE = "eventtype";
    public static final String BUNDLE_INTEGER_PARAM = "paramint";
    public static final String BUNDLE_INTEGER_PARAM2 = "paramintwe";
    public static final String BUNDLE_INTEGER_PARAM3 = "paraminthree";
    public static final String BUNDLE_BOOLEAN_PARAM = "parambol";
    public static final String BUNDLE_BOOLEAN_PARAM2 = "parambol2";
    public static final String BUNDLE_STRING_PARAM = "param";
    public static final String BUNDLE_STRING_PARAM2 = "param2";
    public static final String BUNDLE_STRING_PARAM3 = "param3";
    public static final int KEY_ONACTIVITY_RESULT_LOGIN = 1212;
    public static final int KEY_ONACTIVITY_RESULT_GUEST_BOOKING = 1313;
    public static final String BUNDLE_INTENT_PARAM = "paramintent";
    public static final String BUNDLE_PARCELABLE_PARAM = "parampracebaleintent";

//    ServerResponse Parameters
    public static final String RESPONSE_PRICE_BLOCK = "BLOCK_BASE";
    public static final String RESPONSE_PRICE_HOUR = "HOUR_BASE";
    public static final String RESPONSE_PRICE_GUEST = "GUEST_BASE";

    public static final int BLOCK_CHARGE_SPACE_ONLY = 1;
    public static final int BLOCK_CHARGE_GUEST_BASE = 2;
    public static final int BLOCK_CHARGE_HOST_NULL = -1;

    public static final String PAYMENT_GATEWAY_HNB_URL = "https://www.hnbpg.hnb.lk/SENTRY/PaymentGateway/Application/WFrmHNBCheckout.aspx";
    public static final String PAYMENT_PROVIDER_ERROR = "https://cap.attempts.securecode.com/acspage/cap";

    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    public static final String APP_CURRENCY_UNITS = "LKR ";
    public static final String APP_CURRENCY_NODECIMAL_FORMAT = "####,###,###";
    public static final String APP_CURRENCY_TYPE_PRIFIX = "LKR ";

//    Space List Parameters
    public static final int SPACE_EVENT_TYPE_LIST = 1;
    public static final int SPACE_SPACE_TYPE_LIST = 9;
    public static final int SPACE_PRICE_TYPE_LIST = 2;
    public static final int SPACE_PRICE_TYPE_LIST_HOURLYBASED = 21;
    public static final int SPACE_PRICE_TYPE_LIST_BLOCKBASED = 22;
    public static final int SPACE_PRICE_TYPE_LIST_BLOCKGUESTBASED = 23;
    public static final int SPACE_AMINITY_TYPE_LIST = 3;
    public static final int SPACE_AMINITY_TYPE_LIST_CHARGBLE = 31;
    public static final int SPACE_MENU_TYPE_LIST = 4;
    public static final int SPACE_SEATING_TYPE_LIST = 5;
    public static final int SPACE_PARTICIPATION_TYPE_LIST = 6;
    public static final int SPACE_BUDGET_TYPE_LIST = 7;
    public static final int SPACE_RULE_TYPE_LIST = 8;

//    Space Availability Type
    public static final int SPACE_PRICE_BOLCK_MON_ID = 1;
    public static final int SPACE_PRICE_BOLCK_TUE_ID = 2;
    public static final int SPACE_PRICE_BOLCK_WED_ID = 3;
    public static final int SPACE_PRICE_BOLCK_THE_ID = 4;
    public static final int SPACE_PRICE_BOLCK_FRI_ID = 5;
    public static final int SPACE_PRICE_BOLCK_SAT_ID = 6;
    public static final int SPACE_PRICE_BOLCK_SUN_ID = 7;
    public static final int SPACE_PRICE_BOLCK_WEE_ID = 8;

    public static final String SPACE_PRICE_BOLCK_MON = "Monday";
    public static final String SPACE_PRICE_BOLCK_TUE = "Tuesday";
    public static final String SPACE_PRICE_BOLCK_WED = "Wednesday";
    public static final String SPACE_PRICE_BOLCK_THE = "Thursday";
    public static final String SPACE_PRICE_BOLCK_FRI = "Friday";
    public static final String SPACE_PRICE_BOLCK_SAT = "Saturday";
    public static final String SPACE_PRICE_BOLCK_SUN = "Sunday";
    public static final String SPACE_PRICE_BOLCK_WEE = "Weekday";

    public static final Map<Integer, String > DAY_MAP = Collections.unmodifiableMap(
            new HashMap<Integer, String>() {{
                put(SPACE_PRICE_BOLCK_MON_ID, SPACE_PRICE_BOLCK_MON);
                put(SPACE_PRICE_BOLCK_TUE_ID, SPACE_PRICE_BOLCK_TUE);
                put(SPACE_PRICE_BOLCK_WED_ID, SPACE_PRICE_BOLCK_WED);
                put(SPACE_PRICE_BOLCK_THE_ID, SPACE_PRICE_BOLCK_THE);
                put(SPACE_PRICE_BOLCK_FRI_ID, SPACE_PRICE_BOLCK_FRI);
                put(SPACE_PRICE_BOLCK_SAT_ID, SPACE_PRICE_BOLCK_SAT);
                put(SPACE_PRICE_BOLCK_SUN_ID, SPACE_PRICE_BOLCK_SUN);
                put(SPACE_PRICE_BOLCK_WEE_ID, SPACE_PRICE_BOLCK_WEE);
            }});

//    Image Quality Factor
//    public static final String IMAGE_PLACEHOLDER_FACTOR = "e_blur:1500,q_10/";
    public static final String IMAGE_PLACEHOLDER_FACTOR = "c_scale,e_blur:1500,q_10,w_200/";
    public static final String IMAGE_PLACEHOLDER_FACTOR_WINDOW = "c_scale,w_";
//    public static final String IMAGE_PLACEHOLDER_FACTOR = "w_400,g_south_west,y_400,l_msk_mskalx/q_50,f_auto/";

    public static String optimizedImage(Context context){
        return IMAGE_PLACEHOLDER_FACTOR_WINDOW + context.getResources().getInteger(R.integer.screen_width)+"/";
    }

//    Space Types
    public static final ArrayList<SpaceType> SPACE_TYPES_LISTS = new ArrayList<>(Arrays.asList(new SpaceType(1,"Indoor"),new SpaceType(2,"Outdoor"), new SpaceType(3,"Indoor & Outdoor")));

//    Participation Count
    public static final ArrayList<String> PARTICIPATION_COUNT = new ArrayList<>(Arrays.asList("1 - 10","10 - 50","50 - 100", "100 - 250", "250 - 500", "Above 500"));

//    Budget indicative price per hour
    public static final ArrayList<String> BUDGET_COUNT = new ArrayList<>(Arrays.asList("Below LKR 5,000", "LKR 5,000 - LKR 10,000","LKR 10,000 - LKR 15,000","Above LKR 15,000"));

    public static final int HOST_SPACE_BOOKINGS_PAST = 1;
    public static final int HOST_SPACE_BOOKINGS_UPCOMIG = 2;
    public static final int GUEST_SPACE_BOOKINGS_PAST = 3;
    public static final int GUEST_SPACE_BOOKINGS_UPCOMIG = 4;

//    BookingDateTime Formats
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String DATE_FORMAT_02 = "yyyy-MM-dd'T'HH:mmZ";
    public static final String FILTER_DATE_FORMAT_01 = "dd MMMM";
    public static final String FILTER_DATE_FORMAT_02 = " yyyy";

//    Error CallBack values
    public static final int ERROR_NETWORK_FAILURE = 1;

//    Get Participation Count String
    public static String getParticipationString(ArrayList<Integer> list){
        StringBuilder sb = new StringBuilder();
        if (!list.isEmpty()) {
            Collections.sort(list);
            sb.append(list.get(0) != 5 ? PARTICIPATION_COUNT.get(list.get(0)).split(" - ")[0] : "500");
            sb.append("-");
            sb.append(list.get(list.size() - 1) != 5 ? PARTICIPATION_COUNT.get(list.get(list.size() - 1)).split(" - ")[1] : "5000");
        }
        return list.isEmpty()?null:sb.toString();
    }

    public static String getBudgetString(ArrayList<Integer> list){
        StringBuilder sb = new StringBuilder();
        if (!list.isEmpty()){
            Collections.sort(list);
            sb.append(list.get(0) == 0? "1":"");
            sb.append(list.get(0) != 0 && list.get(0) != 3 ? BUDGET_COUNT.get(list.get(0)).split(" - ")[0]:"");
            sb.append(list.get(0) == 3? "15000":"");
            sb.append("-");
            int last = list.get(list.size()-1);
            sb.append(last == 0?"5000":"");
            sb.append(last != 0 && last != 3 ?BUDGET_COUNT.get(last).split(" - ")[1]:"");
            sb.append(last == 3?"100000":"");
        }
        return sb.toString().replace("LKR ","").isEmpty()?null:sb.toString().replace("LKR ","").replace(",","");
    }

    public static String getDayOfMonthSuffix(final int n) {
        if (n >= 1 && n <= 31) {
            if (n >= 11 && n <= 13) {
                return "th";
            }
            switch (n % 10) {
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    return "th";
            }
        }
        return null;
    }

    public static String get12HoursValue(int hourOfDay, int minute) {
        String meridianNess = (Integer.valueOf(hourOfDay)/12==0?"AM":"PM");
        int hour = Integer.valueOf(hourOfDay)%12;
        if (hour == 0) hour = 12;
        return hour+ ":"+minute+" "+ meridianNess;
    }


//    Sort Dates integer
    public static class DateSorter implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            return o1.compareTo(o2);
        }
    }

    public static boolean isValidMail(String mail){
        return android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches();
    }

    public static boolean isValidMobile(String contactNumber){
        return contactNumber.length() == 10;
    }

    public static final int FUNCTION_EVENT_TYPE = 2;
    public static final int FUNCTION_SEATING_TYPE = 4;


    public static String sha256(String s) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(s.getBytes());
        byte[] bytes = md.digest();
        return bytesToHexString(bytes);
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static boolean isNumeric(String s) {
        return s != null && s.matches("\\d+(?:\\.\\d+)?");
    }


    public static final String BOOKING_STATUS_INITIATED = "INITIATED";
    public static final String BOOKING_STATUS_PENDING_PAYMENT = "PENDING_PAYMENT";
    public static final String BOOKING_STATUS_PAYMENT_DONE = "PAYMENT_DONE";
    public static final String BOOKING_STATUS_CANCELLED = "CANCELLED";
    public static final String BOOKING_STATUS_CONFIRMED = "CONFIRMED";
    public static final String BOOKING_STATUS_EXPIRED = "EXPIRED";
    public static final String BOOKING_STATUS_DISCARDED = "DISCARDED";


    public static final int BOOKING_STATUS_PENDING_PAYMENT_ID = 2;

    public static int getStatusColor(String name ,Context mContext) {
        switch (name){
            case BOOKING_STATUS_INITIATED:
                return ContextCompat.getColor(mContext,R.color.status_pending_payment);
            case BOOKING_STATUS_PENDING_PAYMENT:
                return ContextCompat.getColor(mContext,R.color.status_pending_payment);
            case BOOKING_STATUS_PAYMENT_DONE:
                return ContextCompat.getColor(mContext,R.color.status_confirmed);
            case BOOKING_STATUS_CANCELLED:
                return ContextCompat.getColor(mContext, R.color.status_cancelled);
            case BOOKING_STATUS_CONFIRMED:
                return ContextCompat.getColor(mContext,R.color.status_confirmed);
            case BOOKING_STATUS_EXPIRED:
                return ContextCompat.getColor(mContext, R.color.status_expired);
            case BOOKING_STATUS_DISCARDED:
                return ContextCompat.getColor(mContext, R.color.status_expired);
            default:
                return ContextCompat.getColor(mContext,R.color.status_confirmed);
        }
    }

    public static int RESERVATION_STATUS_PENDING_PAYMENT = 2;
    public static int RESERVATION_STATUS_PAYMENT_DONE = 3;
    public static final Map<Integer, String > STATUS_LIST_MAP = Collections.unmodifiableMap(
            new HashMap<Integer, String>() {{
                put(RESERVATION_STATUS_PENDING_PAYMENT, "Pending Payment");
                put(RESERVATION_STATUS_PAYMENT_DONE, "Confirmed");
            }});
    public static ArrayList<String> STATUS_LIST = new ArrayList<String >(STATUS_LIST_MAP.values());



    public static int getCancellationColors(int id, Context mContext){
        switch (id){
            case 1:
                return ContextCompat.getColor(mContext,R.color.cancel_status_flexible);
            case 2:
                return ContextCompat.getColor(mContext,R.color.cancel_status_moderate);
            case 3:
                return ContextCompat.getColor(mContext,R.color.cancel_status_strict);
            default:
                return ContextCompat.getColor(mContext,R.color.cancel_status_flexible);
        }
    }

    public static final int CANCEL_GUEST_BOOKING = 3;
    public static final int RESERVE_GUEST_BOOKING = 1;
    public static final int PROMOTION_GUEST_BOOKING = 4;
    public static final String  RESERVE_GUEST_BOOKING_STRING = "MANUAL";

    public static ArrayList<Integer> intersection(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        ArrayList<Integer> result = new ArrayList<Integer>(list1);
        result.retainAll(list2);
        return result;
    }

    public static final String MILLIONSPACE_MOBILE_NUMBER = "tel:0117 811 811";

    public static final String[] TIME_LIST = new String []{"00.00 AM","01.00 AM","02.00 AM","03.00 AM","04.00 AM","05.00 AM","06.00 AM",
            "07.00 AM","08.00 AM","09.00 AM","10.00 AM","11.00 AM","12.00 PM","01.00 PM","02.00 PM","03.00 PM","04.00 PM","05.00 PM","06.00 PM",
            "07.00 PM","08.00 PM","09.00 PM","10.00 PM","11.00 PM"};


    public final static String UPDATE_LIST_UPCOMING = "UPDATELISTUPCOMING";
    public final static String UPDATE_LIST_PAST = "UPDATELISTPAST";

    public final static int COUNTER_TIME = 5;

    public final static int REFUND_FLEXIBLE = 1;
    public final static int REFUND_MODERATE = 2;
    public final static int REFUND_STRICT = 3;

    public final static int SUCCESS_PROMO_CODE = 1;
    public final static int INVALID_PROMO_CODE = 2;
    public final static int EXPIRED_PROMO_CODE = 3;
}
