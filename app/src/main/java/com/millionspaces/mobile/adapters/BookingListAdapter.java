package com.millionspaces.mobile.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.millionspaces.mobile.BuildConfig;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.entities.response.Booking;
import com.millionspaces.mobile.utils.Config;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.CalenderUtils.DATE_FORMAT;
import static com.millionspaces.mobile.utils.Config.BOOKING_STATUS_CONFIRMED;
import static com.millionspaces.mobile.utils.Config.BOOKING_STATUS_PAYMENT_DONE;
import static com.millionspaces.mobile.utils.Config.BOOKING_STATUS_PENDING_PAYMENT;
import static com.millionspaces.mobile.utils.Config.GUEST_SPACE_BOOKINGS_PAST;
import static com.millionspaces.mobile.utils.Config.GUEST_SPACE_BOOKINGS_UPCOMIG;
import static com.millionspaces.mobile.utils.Config.HOST_SPACE_BOOKINGS_PAST;
import static com.millionspaces.mobile.utils.Config.HOST_SPACE_BOOKINGS_UPCOMIG;
import static com.millionspaces.mobile.utils.Config.IMAGE_PLACEHOLDER_FACTOR;
import static com.millionspaces.mobile.utils.Config.getStatusColor;
import static com.millionspaces.mobile.utils.Config.isNumeric;

/**
 * Created by kasunka on 10/10/17.
 */

public class BookingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    private ArrayList<Booking> bookings;
    private int type;
    private OnItemClickListener onItemClickListener;

    private DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT);

    public BookingListAdapter(Context context, ArrayList<Booking> bookings, int type) {
        this.mContext = context;
        this.bookings = bookings;
        this.type = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case HOST_SPACE_BOOKINGS_PAST:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_bookings_host_item, parent, false);
                return new ViewHolderHostBookings(view);
            case GUEST_SPACE_BOOKINGS_PAST:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_booking_guest_item_upcoming, parent, false);
                return new ViewHolderGuestBookings(view);
            case GUEST_SPACE_BOOKINGS_UPCOMIG:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_booking_guest_item_past, parent, false);
                return new ViewHolderGuestBookings(view);
            default:
                return new ViewHolderGuestBookings(view);

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Booking booking = bookings.get(position);
        switch (type){
            case HOST_SPACE_BOOKINGS_UPCOMIG:
                ViewHolderHostBookings viewHolderHostBookings = (ViewHolderHostBookings)holder;
                setUpItemHostCard(viewHolderHostBookings,booking);
                break;
            case HOST_SPACE_BOOKINGS_PAST:
                ViewHolderHostBookings viewHolderHostBooking = (ViewHolderHostBookings)holder;
                setUpItemHostCard(viewHolderHostBooking,booking);
                break;
            default:
                ViewHolderGuestBookings viewHolderGuestBookings =((ViewHolderGuestBookings)holder);
                setUpItemGuestCard(viewHolderGuestBookings,booking,type);
                break;
        }
    }

    private void setUpItemGuestCard(ViewHolderGuestBookings viewHolderGuestBookings, Booking booking, int type) {
        viewHolderGuestBookings.spaceName.setText(booking.getSpace().getName());
        viewHolderGuestBookings.spacePlace.setText(booking.getSpace().getAddressLine2() != null? booking.getSpace().getAddressLine2():booking.getSpace().getAddressLine1());
        viewHolderGuestBookings.spaceDate.setText(formatter.parseDateTime(booking.getDates().get(0).getFromDate()).toString("dd MMM yyyy"));
        viewHolderGuestBookings.spaceTime.setText(formatter.parseDateTime(booking.getDates().get(0).getFromDate()).toString("hh a")+"-"+
                formatter.parseDateTime(booking.getDates().get(0).getToDate()).toString("hh a"));
        RequestBuilder<Drawable> thumbnailRequest = Glide.with(mContext).load(BuildConfig.IMAGE_SERVER_URL+IMAGE_PLACEHOLDER_FACTOR+BuildConfig.IMAGE_SERVER_DOMAIN+booking.getSpace().getImage());
        Glide.with(mContext).load(BuildConfig.IMAGE_SERVER_URL+Config.optimizedImage(mContext)+BuildConfig.IMAGE_SERVER_DOMAIN+booking.getSpace().getImage()).thumbnail(thumbnailRequest).into(viewHolderGuestBookings.bookingImage);
        viewHolderGuestBookings.bookingStatus.setText(booking.getReservationStatus().getLabel());
        viewHolderGuestBookings.bookingStatus.setBackgroundColor(getStatusColor(booking.getReservationStatus().getName(),mContext));
        viewHolderGuestBookings.bookingCost.setText(booking.getBookingCharge() != null &&  isNumeric(booking.getBookingCharge())?
                "LKR " + new DecimalFormat("####,###,###").format(Float.parseFloat(booking.getBookingCharge())) : "N/A");

        if (type == GUEST_SPACE_BOOKINGS_PAST){
            if ((booking.getReservationStatus().getName().contains(BOOKING_STATUS_CONFIRMED) || booking.getReservationStatus().getName().contains(BOOKING_STATUS_PAYMENT_DONE))
                    && !booking.isReviewed()) {
                viewHolderGuestBookings.bookingAction.setVisibility(View.VISIBLE);
                viewHolderGuestBookings.bookingAction.setText(mContext.getString(R.string.booking_list_action_review));
                viewHolderGuestBookings.bookingAction.setTextColor(ContextCompat.getColor(mContext,R.color.booking_review_button_color));
                viewHolderGuestBookings.bookingAction.setBackground(ContextCompat.getDrawable(mContext,R.drawable.background_booking_status_button_review));
            }else {
                viewHolderGuestBookings.bookingAction.setVisibility(View.GONE);
            }
        }else if (type == GUEST_SPACE_BOOKINGS_UPCOMIG){
            if (booking.getReservationStatus().getName().contains(BOOKING_STATUS_CONFIRMED) || booking.getReservationStatus().getName().contains(BOOKING_STATUS_PENDING_PAYMENT)
                    || booking.getReservationStatus().getName().contains(BOOKING_STATUS_PAYMENT_DONE)) {
                viewHolderGuestBookings.bookingAction.setVisibility(View.VISIBLE);
                viewHolderGuestBookings.bookingAction.setText(mContext.getString(R.string.booking_list_action_cancel));
                viewHolderGuestBookings.bookingAction.setTextColor(ContextCompat.getColor(mContext,R.color.booking_cancel_button_color));
                viewHolderGuestBookings.bookingAction.setBackground(ContextCompat.getDrawable(mContext,R.drawable.background_booking_status_button_cancel));
            }else {
                viewHolderGuestBookings.bookingAction.setVisibility(View.GONE);
            }
        }
    }



    private void setUpItemHostCard(ViewHolderHostBookings viewHolderHostBookings, Booking booking) {
        viewHolderHostBookings.hostName.setText(booking.getUser().getName());
        viewHolderHostBookings.hostRefId.setText(mContext.getString(R.string.host_booking_fragment_ref_id)+": "+booking.getOrderId());
        viewHolderHostBookings.hostBookingPrice.setText(booking.getBookingCharge() != null &&  isNumeric(booking.getBookingCharge())?
                "LKR " + new DecimalFormat("####,###,###").format(Float.parseFloat(booking.getBookingCharge())) : "N/A");
        viewHolderHostBookings.hostBookingDateFrom.setText(formatter.parseDateTime(booking.getDates().get(0).getFromDate()).toString("dd MMM yyyy, hh.mm a"));
        viewHolderHostBookings.hostBookingDateTo.setText(formatter.parseDateTime(booking.getDates().get(0).getToDate()).toString("dd MMM yyyy, hh.mm a"));

        if (booking.isManual()){
            viewHolderHostBookings.bookingIndicator.setVisibility(View.GONE);
        }else {
            viewHolderHostBookings.bookingIndicator.setVisibility(View.VISIBLE);
        }
        if (booking.getDates().size()>1){
            viewHolderHostBookings.indicator.setVisibility(View.VISIBLE);
        }else {
            viewHolderHostBookings.indicator.setVisibility(View.GONE);
        }
        if (booking.getReservationStatus() != null) {
            viewHolderHostBookings.hostBookingStatus.setText(booking.getReservationStatus().getLabel());
            viewHolderHostBookings.hostBookingStatus.setBackgroundColor(getStatusColor(booking.getReservationStatus().getName(),mContext));
            viewHolderHostBookings.hostBookingStatus.setVisibility(View.VISIBLE);
        }else {
            viewHolderHostBookings.hostBookingStatus.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        switch (type){
            case HOST_SPACE_BOOKINGS_PAST:
                viewType = HOST_SPACE_BOOKINGS_PAST;
                break;
            case HOST_SPACE_BOOKINGS_UPCOMIG:
                viewType = HOST_SPACE_BOOKINGS_PAST;
                break;
            case GUEST_SPACE_BOOKINGS_PAST:
                viewType = GUEST_SPACE_BOOKINGS_PAST;
                break;
            case GUEST_SPACE_BOOKINGS_UPCOMIG:
                viewType = GUEST_SPACE_BOOKINGS_UPCOMIG;
                break;
        }
        return viewType;
    }

    @Override
    public int getItemCount() {
        return bookings.size();
    }

    public class ViewHolderHostBookings extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.host_booking_refid)
        TextView hostRefId;
        @BindView(R.id.host_booking_name)
        TextView hostName;
        @BindView(R.id.host_booking_price)
        TextView hostBookingPrice;
        @BindView(R.id.space_card_view)
        CardView previewButton;
        @BindView(R.id.host_booking_date_from)
        TextView hostBookingDateFrom;
        @BindView(R.id.host_booking_date_to)
        TextView hostBookingDateTo;
        @BindView(R.id.host_booking_status)
        TextView hostBookingStatus;
        @BindView(R.id.host_booking_multiple_indicator)
        TextView indicator;
        @BindView(R.id.host_booking_made)
        ImageView bookingIndicator;

        public ViewHolderHostBookings(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            previewButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.OnClick(getAdapterPosition(),v);
        }
    }

    public class ViewHolderGuestBookings extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.guest_booking_name)
        TextView spaceName;
        @BindView(R.id.guest_booking_place)
        TextView spacePlace;
        @BindView(R.id.guest_booking_date)
        TextView spaceDate;
        @BindView(R.id.guest_booking_time)
        TextView spaceTime;
        @BindView(R.id.guest_booking_image)
        ImageView bookingImage;
        @BindView(R.id.guest_booking_status)
        TextView bookingStatus;
        @BindView(R.id.guest_booking_cost)
        TextView bookingCost;
        @BindView(R.id.guest_booking_action)
        TextView bookingAction;

        public ViewHolderGuestBookings(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            bookingAction.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.OnClick(getAdapterPosition(),v);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
