package com.millionspaces.mobile.network.interfaces;

import com.millionspaces.mobile.entities.request.ManualBookingRequest;

/**
 * Created by kasunka on 11/24/17.
 */

public interface OnSuccessGetManualBooking  extends OnErrorResponse{
    void onSuccessGetBooking(ManualBookingRequest booking);
}
