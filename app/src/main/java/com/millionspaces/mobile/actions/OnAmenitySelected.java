package com.millionspaces.mobile.actions;

/**
 * Created by kasunka on 9/14/17.
 */

public interface OnAmenitySelected {
    void onAmenitySelected(int amenityId, int count, boolean isAmenityAdded, float totalAmount);
    void onPerHourAmenitySelected(int amenityId, int count, boolean isAmenityAdded, float totalAmount);
    void onNullPerHourAmenityChecked();
}
