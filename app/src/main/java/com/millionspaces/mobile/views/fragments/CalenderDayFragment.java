package com.millionspaces.mobile.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.millionspaces.mobile.HostCalendarActivity;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnDateSelected;
import com.millionspaces.mobile.adapters.calendar.MonthPagerAdapter;
import com.millionspaces.mobile.utils.CalendarManager;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by kasunka on 8/30/17.
 */

public class CalenderDayFragment extends Fragment implements OnDateSelected, ViewPager.OnPageChangeListener, View.OnClickListener {

    @BindView(R.id.calender_day_viewpager)
    ViewPager viewPager;
    @BindView(R.id.calender_btn_back)
    ImageView calenderBack;
    @BindView(R.id.calender_btn_next)
    ImageView calenderNext;

    private MonthPagerAdapter monthPagerAdapter;
    private OnDateSelected onDateSelected;
    private String[] startDate;
    private int monthCount;
    private Calendar monthCalender;
    private int viewPagerCurrentItem = 0;
    private int numberOfMonths = 0;
    private CalendarManager mCalendarManager;
    private Unbinder unbinder;
    private boolean isHosterActivity = false;

    public static Fragment newInstance() {
        CalenderDayFragment fragment = new CalenderDayFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof OnDateSelected) {
                onDateSelected = (OnDateSelected) context;
            }
        }catch (ClassCastException e) {
            throw new ClassCastException();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calender_day, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        if (getParentFragment() instanceof CalenderFragment) {
            mCalendarManager = ((CalenderFragment) getParentFragment()).mCalendarManager;
            isHosterActivity = false;
        }else {
            mCalendarManager = ((HostCalendarActivity)getActivity()).calendarManager;
            isHosterActivity = true;
        }
        startDate = mCalendarManager.getStartDate().split("-");
        monthCount = mCalendarManager.getNumberOfMonths();

        int width = getContext().getResources().getDisplayMetrics().widthPixels;
        view.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, width-(width/10)));
        monthPagerAdapter = new MonthPagerAdapter(getContext(),width-(width/10),mCalendarManager);
        viewPager.setAdapter(monthPagerAdapter);
        init_views();
        init_actions();
    }

    private void init_views() {
        viewPager.addOnPageChangeListener(this);
        //TODO enable this for Past calendar
//        if (isHosterActivity) {
//            numberOfMonths = Months.monthsBetween(DateTime.parse(mCalendarManager.getStartDate()), new DateTime()).getMonths();
//            viewPager.setCurrentItem(numberOfMonths);
//        }
    }

    private void init_actions() {
        calenderBack.setOnClickListener(this);
        calenderNext.setOnClickListener(this);
        monthPagerAdapter.setOnItemClickListner(this);
    }

    @Override
    public void onDateSelected(String day, String month, int year, boolean isSelected) {
        getCalenderDate(day, viewPager.getCurrentItem(), isSelected);
    }

    private void getCalenderDate(String day, int currentItem, boolean isSelected) {
        monthCalender = new GregorianCalendar(Integer.parseInt(startDate[0]), Integer.parseInt(startDate[1])-1+currentItem, 1);
        onDateSelected.onDateSelected(day,monthCalender.getDisplayName(Calendar.MONTH,Calendar.LONG, Locale.getDefault()),monthCalender.get(Calendar.YEAR),isSelected);
        if (getParentFragment() instanceof CalenderFragment) {
            ((CalenderFragment) getParentFragment()).onDateChanged(day, monthCalender.get(Calendar.MONTH), monthCalender.get(Calendar.YEAR), isSelected);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onDateSelected = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (viewPagerCurrentItem != position) {
            getCalenderDate("1", position, false);
        }
        viewPagerCurrentItem = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.calender_btn_back:
                viewPager.setCurrentItem((viewPager.getCurrentItem() != 0)
                        ? viewPager.getCurrentItem() - 1 : 0);
                break;
            case R.id.calender_btn_next:
                viewPager.setCurrentItem((viewPager.getCurrentItem() < monthCount)
                        ? viewPager.getCurrentItem() + 1 : 0);
                break;
        }
    }

}
