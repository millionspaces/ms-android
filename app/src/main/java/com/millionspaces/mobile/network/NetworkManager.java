package com.millionspaces.mobile.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Kasunka on 12/19/16.
 */

public class NetworkManager {
    private static NetworkManager instance = null;

    private NetworkManager() {

    }

    public static NetworkManager getInstance() {
        if (instance == null) {
            instance = new NetworkManager();
        }
        return instance;
    }

    public boolean checkInternetConenction(Context contex) {
        boolean isConnexted = false;
        // get Connectivity Manager object to check connection
        ConnectivityManager connec = (ConnectivityManager) contex.getSystemService(contex.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connec.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                isConnexted = true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                isConnexted = true;
            }
        } else {
            // not connected to the internet
            isConnexted = false;
        }
        return isConnexted;
    }
}
