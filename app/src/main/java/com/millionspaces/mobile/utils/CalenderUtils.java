package com.millionspaces.mobile.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kasunka on 8/31/17.
 */

public class CalenderUtils {

    public static final Map<Integer, Integer > DATE_MAP = Collections.unmodifiableMap(
            new HashMap<Integer, Integer>() {{
                put(1,6);
                put(2,0);
                put(3,1);
                put(4,2);
                put(5,3);
                put(6,4);
                put(7,5);
            }});

    public static final String TOTALLY_BOOKED = "B";
    public static final String TOTALLY_BOOKED_MS = "BM";
    public static final String TOTALLY_BOOKED_MS_FIRST = "BMF";
    public static final String TOTALLY_BOOKED_GUEST = "BG";
    public static final String PARTIALLY_BOOKED = "P";
    public static final String TOTALLY_AVAILABLE = "N";
    public static final String SELECTED_DAY = "S";
    public static final String SELECTED_HOUR = "S";
    public static final String NOTICED_HOUR = "NP";

    public static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm";

}
