package com.millionspaces.mobile.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by kasunka on 13/10/17.
 */

public class Menu implements Parcelable, Comparable<Menu>{
    private int id;
    private String url;
    private String menuId;

    protected Menu(Parcel in) {
        id = in.readInt();
        url = in.readString();
        menuId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(url);
        dest.writeString(menuId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Menu> CREATOR = new Creator<Menu>() {
        @Override
        public Menu createFromParcel(Parcel in) {
            return new Menu(in);
        }

        @Override
        public Menu[] newArray(int size) {
            return new Menu[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getMenuId() {
        return menuId;
    }

    @Override
    public int compareTo(@NonNull Menu o) {
        if (getMenuId() == null || o.getMenuId() == null)
            return 0;
        return getMenuId().compareTo(o.getMenuId());
    }
}
