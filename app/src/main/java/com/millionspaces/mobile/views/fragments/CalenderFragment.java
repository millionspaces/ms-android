package com.millionspaces.mobile.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.millionspaces.mobile.BookingEngineActivity;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnBlockSelected;
import com.millionspaces.mobile.actions.OnDateSelected;
import com.millionspaces.mobile.actions.OnTimeSelected;
import com.millionspaces.mobile.utils.CalendarManager;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.millionspaces.mobile.utils.Config.RESPONSE_PRICE_HOUR;

/**
 * Created by kasunka on 8/29/17.
 */

public class CalenderFragment extends Fragment {

    private FragmentManager manager;
    private String backStateName;

    private OnDateSelected onDateSelected;
    private OnTimeSelected onTimeSelected;
    private OnBlockSelected onBlockSelected;

    private boolean isDateSelected = false;
    private Fragment calenderTimeFragment = null;

    public CalendarManager mCalendarManager;
    private Unbinder unbinder;

    public static Fragment newInstance(){
        CalenderFragment fragment = new CalenderFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof OnDateSelected) {
                onDateSelected = (OnDateSelected) context;
            }
            if (context instanceof  OnTimeSelected){
                onTimeSelected = (OnTimeSelected) context;
            }
            if (context instanceof OnBlockSelected){
                onBlockSelected = (OnBlockSelected)context;
            }
        } catch (ClassCastException e) {
            throw new ClassCastException();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calender, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mCalendarManager = ((BookingEngineActivity)getActivity()).calendarManager;
        switchCalenderDay();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void popFragment(Fragment fragment) {
        if (manager == null || !fragmentPoped(fragment)) {
            switchFragment(fragment);
        } else {
            manager.popBackStack(fragment.getClass().getName(), 0);
        }
    }

    private boolean fragmentPoped(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
        for (int i = 0; i < manager.getBackStackEntryCount(); i++) {
            if (fragClassName.equals(manager.getBackStackEntryAt(i).getName())) {
                return true;
            }
        }
        return false;
    }

    public void switchFragment(Fragment frag) {
        backStateName = frag.getClass().getName();
        manager = getChildFragmentManager();

        manager.beginTransaction()
                .replace(R.id.fragments_calender_container, frag, backStateName)
                .addToBackStack(backStateName)
                .commit();
    }

    public void onDateChanged(String day, int month, int year, boolean isSelected) {
        mCalendarManager.clearSelectedDate();
        isDateSelected = isSelected;
        if (isDateSelected) {
            mCalendarManager.setSelectedDate(year + String.format("%02d", month) + String.format("%02d", Integer.valueOf(day)));
            mCalendarManager.setSelectedDateObject(year + "-" + (month + 1) + "-" + day);
            if (mCalendarManager.getAvailabilityMethod().contains(RESPONSE_PRICE_HOUR)) {
                calenderTimeFragment = CalenderPerHourFragment.newInstance();
            }else {
                calenderTimeFragment = CalenderPerBlockFragment.newInstance();
            }
            popFragment(calenderTimeFragment);
        }
    }

    public void switchCalenderDay(){
        if (!isDateSelected) {
            popFragment(CalenderDayFragment.newInstance());
        } else {
            manager.popBackStack(calenderTimeFragment.getClass().getName(), 0);
        }
    }

    public void showBackFragment(){
        getActivity().onBackPressed();
    }

}
