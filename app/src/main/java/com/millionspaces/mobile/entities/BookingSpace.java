package com.millionspaces.mobile.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.millionspaces.mobile.entities.response.Amenity;
import com.millionspaces.mobile.entities.response.CancellationPolicy;

import java.util.ArrayList;

/**
 * Created by kasunka on 10/10/17.
 */

public class BookingSpace implements Parcelable{
    private String image;
    private String name;
    private String addressLine1;
    private String addressLine2;
    private ArrayList<Amenity> amenities;
    private int id;
    private CancellationPolicy cancellationPolicy;


    protected BookingSpace(Parcel in) {
        image = in.readString();
        name = in.readString();
        addressLine1 = in.readString();
        addressLine2 = in.readString();
        amenities = in.createTypedArrayList(Amenity.CREATOR);
        id = in.readInt();
        cancellationPolicy = in.readParcelable(CancellationPolicy.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeString(name);
        dest.writeString(addressLine1);
        dest.writeString(addressLine2);
        dest.writeTypedList(amenities);
        dest.writeInt(id);
        dest.writeParcelable(cancellationPolicy, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingSpace> CREATOR = new Creator<BookingSpace>() {
        @Override
        public BookingSpace createFromParcel(Parcel in) {
            return new BookingSpace(in);
        }

        @Override
        public BookingSpace[] newArray(int size) {
            return new BookingSpace[size];
        }
    };

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public ArrayList<Amenity> getAmenities() {
        return amenities;
    }

    public int getId() {
        return id;
    }

    public CancellationPolicy getCancellationPolicy() {
        return cancellationPolicy;
    }
}
