package com.millionspaces.mobile.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by kasunka on 12/6/17.
 */

public class MillionSpacesApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
