package com.millionspaces.mobile.views;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.SpacesListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by kasunka on 8/24/17.
 */

public class FilterSheet extends BottomSheetDialogFragment implements View.OnClickListener {

    @BindView(R.id.sort_distance)
    LinearLayout sortDistanceView;
    @BindView(R.id.sort_distance_radio)
    CompoundButton sortDistanceRadio;
    @BindView(R.id.sort_price_lowtohigh)
    LinearLayout sortPriceLowToHighView;
    @BindView(R.id.sort_price_low_radio)
    CompoundButton sortPriceLowRadio;
    @BindView(R.id.sort_price_hightolow)
    LinearLayout sortPriceHighToLowView;
    @BindView(R.id.sort_price_high_radio)
    CompoundButton sortPriceHighRadio;

    private Unbinder unbinder;
    private SpacesListActivity mActivity;
    private boolean isSortParamAdded = false;

    private static final String PRICE_HIGH_TO_LOW = "priceHighestFirst";
    private static final String PRICE_LOW_TO_HIGH = "priceLowestFirst";
    public static final String DISTANCE_LOW_TO_HIGH = "distance";
    private String param;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View contentView = View.inflate(getContext(), R.layout.bottomsheet_sort_layout, null);
        dialog.setContentView(contentView);
        unbinder = ButterKnife.bind(this,dialog);
        init_views();
    }

    private void init_views() {
        sortDistanceView.setOnClickListener(this);
        sortPriceLowToHighView.setOnClickListener(this);
        sortPriceHighToLowView.setOnClickListener(this);

        mActivity = (SpacesListActivity)getActivity();
        param = mActivity.getSortParam();
        if (param != null) {
            switch (param) {
                case PRICE_HIGH_TO_LOW:
                    checkAndUnCheck(sortPriceHighRadio, PRICE_HIGH_TO_LOW, false);
                    break;
                case PRICE_LOW_TO_HIGH:
                    checkAndUnCheck(sortPriceLowRadio, PRICE_LOW_TO_HIGH, false);
                    break;
                default:
                    checkAndUnCheck(sortDistanceRadio, DISTANCE_LOW_TO_HIGH, false);
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sort_distance:
                if (mActivity.loc != null) {
                    checkAndUnCheck(sortDistanceRadio, DISTANCE_LOW_TO_HIGH, true);
                }else {
                    mActivity.showErrorAlert();
                }
                break;
            case R.id.sort_price_lowtohigh:
                checkAndUnCheck(sortPriceLowRadio,PRICE_LOW_TO_HIGH, true);
                break;
            case R.id.sort_price_hightolow:
                checkAndUnCheck(sortPriceHighRadio,PRICE_HIGH_TO_LOW, true);
                break;
        }
    }


    private void checkAndUnCheck(CompoundButton radio, String sortParam, boolean isAdded) {
        sortDistanceRadio.setChecked(false);
        sortPriceLowRadio.setChecked(false);
        sortPriceHighRadio.setChecked(false);
        radio.setChecked(true);
        isSortParamAdded = isAdded;
        mActivity.onSortAdded(sortParam, isAdded);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (isSortParamAdded) {
//            mActivity.advanceSearch();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
