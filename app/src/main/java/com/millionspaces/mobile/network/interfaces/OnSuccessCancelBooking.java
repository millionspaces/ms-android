package com.millionspaces.mobile.network.interfaces;

import com.millionspaces.mobile.entities.response.CancelBookingResponse;

/**
 * Created by kasunka on 11/27/17.
 */

public interface OnSuccessCancelBooking extends OnErrorResponse{
    void onSuccessCancelBooking(CancelBookingResponse responce);
}
