package com.millionspaces.mobile.actions;

/**
 * Created by kasunka on 1/10/18.
 */

public interface OnPromoSelected {
    void onPromoSelected(String promo);
}
