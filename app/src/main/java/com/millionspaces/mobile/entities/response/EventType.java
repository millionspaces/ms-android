package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 7/19/17.
 */

public class EventType implements Parcelable{
    private int id;
    private String name;
    private String mobileIcon;
    private String timeStamp;

    protected EventType(Parcel in) {
        id = in.readInt();
        name = in.readString();
        mobileIcon = in.readString();
        timeStamp = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(mobileIcon);
        dest.writeString(timeStamp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EventType> CREATOR = new Creator<EventType>() {
        @Override
        public EventType createFromParcel(Parcel in) {
            return new EventType(in);
        }

        @Override
        public EventType[] newArray(int size) {
            return new EventType[size];
        }
    };

    public EventType(int id, String name, String icon, String timeStamp) {
        this.id = id;
        this.name = name;
        this.mobileIcon = icon;
        this.timeStamp = timeStamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return mobileIcon;
    }

    public void setIcon(String icon) {
        this.mobileIcon = icon;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
