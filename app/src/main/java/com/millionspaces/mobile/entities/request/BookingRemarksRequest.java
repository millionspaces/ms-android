package com.millionspaces.mobile.entities.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 11/17/17.
 */

public class BookingRemarksRequest implements Parcelable{
    private int bookingId;
    private boolean isManual;
    private String remark;

    public BookingRemarksRequest(int bookingId, boolean isManual, String remark) {
        this.bookingId = bookingId;
        this.isManual = isManual;
        this.remark = remark;
    }

    protected BookingRemarksRequest(Parcel in) {
        bookingId = in.readInt();
        isManual = in.readByte() != 0;
        remark = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(bookingId);
        dest.writeByte((byte) (isManual ? 1 : 0));
        dest.writeString(remark);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingRemarksRequest> CREATOR = new Creator<BookingRemarksRequest>() {
        @Override
        public BookingRemarksRequest createFromParcel(Parcel in) {
            return new BookingRemarksRequest(in);
        }

        @Override
        public BookingRemarksRequest[] newArray(int size) {
            return new BookingRemarksRequest[size];
        }
    };
}
