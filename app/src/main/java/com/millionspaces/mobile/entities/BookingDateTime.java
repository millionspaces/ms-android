package com.millionspaces.mobile.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 10/10/17.
 */

public class BookingDateTime implements Parcelable{
    private String fromDate;
    private String toDate;


    protected BookingDateTime(Parcel in) {
        fromDate = in.readString();
        toDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fromDate);
        dest.writeString(toDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingDateTime> CREATOR = new Creator<BookingDateTime>() {
        @Override
        public BookingDateTime createFromParcel(Parcel in) {
            return new BookingDateTime(in);
        }

        @Override
        public BookingDateTime[] newArray(int size) {
            return new BookingDateTime[size];
        }
    };

    public String getFromDate() {
        return fromDate;
    }

    public String getToDate() {
        return toDate;
    }
}
