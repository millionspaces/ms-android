package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 7/21/17.
 */

public class SeatingArrangement implements Parcelable{
    private int id;
    private String name;
    private String mobileIcon;
    private String timeStamp;
    private int participantCount;


    protected SeatingArrangement(Parcel in) {
        id = in.readInt();
        name = in.readString();
        mobileIcon = in.readString();
        timeStamp = in.readString();
        participantCount = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(mobileIcon);
        dest.writeString(timeStamp);
        dest.writeInt(participantCount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SeatingArrangement> CREATOR = new Creator<SeatingArrangement>() {
        @Override
        public SeatingArrangement createFromParcel(Parcel in) {
            return new SeatingArrangement(in);
        }

        @Override
        public SeatingArrangement[] newArray(int size) {
            return new SeatingArrangement[size];
        }
    };

    public SeatingArrangement(int id, String name, String icon, String timeStamp) {
        this.id = id;
        this.name = name;
        this.mobileIcon = icon;
        this.timeStamp = timeStamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return mobileIcon;
    }

    public void setIcon(String icon) {
        this.mobileIcon = icon;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getParticipantCount() {
        return participantCount;
    }

    public void setParticipantCount(int participantCount) {
        this.participantCount = participantCount;
    }
}
