package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 11/27/17.
 */

public class CancelBookingResponse implements Parcelable{
    private int action_taker;
    private String old_state;
    private String new_state;
    private String message;

    protected CancelBookingResponse(Parcel in) {
        action_taker = in.readInt();
        old_state = in.readString();
        new_state = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(action_taker);
        dest.writeString(old_state);
        dest.writeString(new_state);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CancelBookingResponse> CREATOR = new Creator<CancelBookingResponse>() {
        @Override
        public CancelBookingResponse createFromParcel(Parcel in) {
            return new CancelBookingResponse(in);
        }

        @Override
        public CancelBookingResponse[] newArray(int size) {
            return new CancelBookingResponse[size];
        }
    };

    public String getMessage() {
        return message;
    }
}
