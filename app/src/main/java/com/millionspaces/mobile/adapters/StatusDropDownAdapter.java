package com.millionspaces.mobile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.STATUS_LIST;

/**
 * Created by kasunka on 11/15/17.
 */

public class StatusDropDownAdapter extends RecyclerView.Adapter<StatusDropDownAdapter.ViewHolder> {

    private OnItemClickListener onItemClickListener;
    private Context mContext;

    public StatusDropDownAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_status_drop_down,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.status.setText(STATUS_LIST.get(position));
    }

    @Override
    public int getItemCount() {
        return STATUS_LIST.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.status_name)
        TextView status;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.OnClick(getAdapterPosition(),v);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
