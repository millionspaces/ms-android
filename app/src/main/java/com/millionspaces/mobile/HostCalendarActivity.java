package com.millionspaces.mobile;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.millionspaces.mobile.actions.OnDateSelected;
import com.millionspaces.mobile.actions.OnHostTimeSelected;
import com.millionspaces.mobile.entities.response.BookingCalendarResponse;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessCalendarBooking;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.CalendarManager;
import com.millionspaces.mobile.views.fragments.CalendarHostPerHourFragment;
import com.millionspaces.mobile.views.fragments.CalenderDayFragment;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.CalenderUtils.DATE_FORMAT;
import static com.millionspaces.mobile.utils.Config.BLOCK_CHARGE_HOST_NULL;
import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTENT_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.ERROR_NETWORK_FAILURE;
import static com.millionspaces.mobile.utils.Config.RESPONSE_PRICE_GUEST;

public class HostCalendarActivity extends BaseActivity implements OnSuccessCalendarBooking, OnDateSelected, OnHostTimeSelected, View.OnClickListener {

    @BindView(R.id.calender_date_txt)
    TextView currentDateDay;
    @BindView(R.id.calender_year_txt)
    TextView currentDateYear;
    @BindView(R.id.calender_month_txt)
    TextView currentDateMonth;
    @BindView(R.id.host_calender_parent)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.calender_action)
    ImageView actionButton;
    @BindView(R.id.host_calender_fab)
    FloatingActionButton floatingActionButton;

    private FragmentManager manager;
    private String backStateName;
    private Calendar calendar;

    public CalendarManager calendarManager;
    private Fragment calenderFragment;
    private String selectedDateString = null;
    private String selectedDate = null, startDate = null;
    private boolean isTimeSelected = false;
    private Bundle mBundle = null;
    private int spaceId;
    DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT);

    private final int HOST_BOOKING_REQUEST = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host_calendar);
        ButterKnife.bind(this);
        init_views();
        mBundle = getIntent().getExtras();
        spaceId = mBundle.getInt(BUNDLE_INTEGER_PARAM);
        getSpaceBookings(spaceId);
    }

    private void init_views() {
        init_toolbar();
        setToolbarTitle(getString(R.string.host_calender_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        calendar = Calendar.getInstance();
        setCalenderTabDate(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)), calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()), calendar.get(Calendar.YEAR));
        calendar.set(Calendar.DAY_OF_MONTH,01); //TODO : remove this after implementing calendar past days

        startDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        actionButton.setOnClickListener(this);
        floatingActionButton.setOnClickListener(this);
    }

    private void getSpaceBookings(int id) {
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).getSpaceBookings(String.valueOf(id), this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure), getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ERROR_NETWORK_FAILURE);
        }
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinatorLayout);
    }

    @Override
    public void onSuccessCalendarBooking(BookingCalendarResponse space) {
        hideProgress();
        DateTime calenderEndDate = formatter.parseDateTime(space.getCalendarStart() + "T00:00").plusYears(2);
        calendarManager = new CalendarManager(startDate, calenderEndDate.toString("yyyy-MM-dd"), space.getFutureBookingDates(), space.getAvailability(), RESPONSE_PRICE_GUEST, BLOCK_CHARGE_HOST_NULL,0,0,space.getBufferTime(),0);
        calenderFragment = CalenderDayFragment.newInstance();
        popFragment(calenderFragment);
    }

    @Override
    public void onDateSelected(String day, String month, int year, boolean isSelected) {
        setCalenderTabDate(day, month, year);
        calendarManager.clearSelectedDate();
        if (isSelected) {

            Calendar cal = Calendar.getInstance();
            try {
                cal.setTime(new SimpleDateFormat("MMMM").parse(month));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            selectedDate = year + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + day + "T";

            calendarManager.setSelectedDate(year + String.format("%02d", cal.get(Calendar.MONTH)) + String.format("%02d", Integer.valueOf(day)));
            calendarManager.setSelectedDateObject(year + "-" + (cal.get(Calendar.MONTH)+1) + "-" + day);
            popFragment(CalendarHostPerHourFragment.newInstance());
            actionButton.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onTimeSelected(HashMap<String, String> blockedHoursMap, float charge, int position, boolean isTimeAdded, boolean isNewPosition) {
        LocalDateTime tempStart = null, tempEnd = null;
        for (String timeString : blockedHoursMap.keySet()) {
            LocalDateTime time = LocalDateTime.parse(selectedDate+timeString);

            if (tempStart != null) {
                if (time.isBefore(tempStart)) {
                    tempStart = time;
                }
                if (time.plusHours(1).isAfter(tempEnd)) {
                    tempEnd = time.plusHours(1);
                }
            } else {
                tempStart = time;
                tempEnd = time.plusHours(1);
            }
        }

        if (!blockedHoursMap.isEmpty()) {
            isTimeSelected = true;
            floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(HostCalendarActivity.this,R.color.fabTimeActivatedColor)));
            floatingActionButton.setImageResource(R.drawable.ic_action_calendar_next);
            selectedDateString = tempStart.toString("yyyy-MM-dd'T'HH:mm") + ","+ tempEnd.toString("yyyy-MM-dd'T'HH:mm");
        }else {
            isTimeSelected = false;
            floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(HostCalendarActivity.this,R.color.fabDateActivatedColor)));
            floatingActionButton.setImageResource(R.drawable.ic_action_plus);
            selectedDateString = null;
        }
    }

    @Override
    public void onBookingSelected(boolean isManual, int bookingId) {
        if (isManual){
            showManualBookingScreen(true,spaceId,bookingId);
        }else {
            //TODO: NOT Working
//            Intent bookingPreview = new Intent(HostCalendarActivity.this,BookingPreviewActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putInt(BUNDLE_INTEGER_PARAM,bookingId);
//            bundle.putBoolean(BUNDLE_BOOLEAN_PARAM, isManual);
//            bundle.putInt(BUNDLE_INTEGER_PARAM2,HOST_SPACE_BOOKINGS_PAST);
//            bookingPreview.putExtras(bundle);
//            startActivityForResult(bookingPreview,HOST_SPACE_BOOKINGS_PAST);
        }
    }

    private void setCalenderTabDate(String day, String month, int year) {
        currentDateDay.setText((Integer.parseInt(day) < 10 ? "0" : "") + day);
        currentDateYear.setText(String.valueOf(year));
        currentDateMonth.setText(month);
    }

    private void popFragment(Fragment fragment) {
        if (manager == null || !fragmentPoped(fragment)) {
            switchFragment(fragment);
        } else {
            manager.popBackStack(fragment.getClass().getName(), 0);
        }
    }

    private boolean fragmentPoped(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
        for (int i = 0; i < manager.getBackStackEntryCount(); i++) {
            if (fragClassName.equals(manager.getBackStackEntryAt(i).getName())) {
                return true;
            }
        }
        return false;
    }

    public void switchFragment(Fragment frag) {
        backStateName = frag.getClass().getName();
        manager = getSupportFragmentManager();

        manager.beginTransaction()
                .replace(R.id.fragments_container, frag)
                .addToBackStack(backStateName)
                .commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.host_calender_fab:
                showManualBookingScreen(false,spaceId,-1);
                break;
            case R.id.calender_action:
                onBackPressed();
                break;
        }
    }

    private void showManualBookingScreen(boolean isCurrentBooking, int spaceId, int bookingId) {
        Intent bookingActivity = new Intent(HostCalendarActivity.this,HostBookingActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_INTEGER_PARAM,spaceId);
        bundle.putInt(BUNDLE_INTEGER_PARAM2,bookingId);
        bundle.putString(BUNDLE_STRING_PARAM,selectedDateString);
        bundle.putBoolean(BUNDLE_BOOLEAN_PARAM,isCurrentBooking);
        bookingActivity.putExtras(bundle);
        startActivityForResult(bookingActivity,HOST_BOOKING_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case HOST_BOOKING_REQUEST:
                    AlertMessageUtils.showSnackBarMessage(getString(R.string.success_manual_booking), mCoordinatorLayout);
                    getSpaceBookings(spaceId);
                    break;
                default:
                    String result = data.getStringExtra(BUNDLE_INTENT_PARAM);
                    AlertMessageUtils.showSnackBarMessage(result, mCoordinatorLayout);
                    break;
            }
            selectedDateString = null;
        }else {
            String result = data.getStringExtra(BUNDLE_INTENT_PARAM);
            AlertMessageUtils.showSnackBarMessage(result, mCoordinatorLayout);
        }
    }

    @Override
    public void onBackPressed() {
        manager = getSupportFragmentManager();
        int count = manager.getBackStackEntryCount();
        if (count > 1 ) {
            manager.popBackStack();
            actionButton.setVisibility(View.GONE);
            floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(HostCalendarActivity.this,R.color.fabDateActivatedColor)));
            floatingActionButton.setImageResource(R.drawable.ic_action_plus);
            isTimeSelected = false;
            selectedDateString = null;
            return;
        }else {
            finish();
        }
    }

    @Override
    public void onConnectionErrorCallBack(int requestCode) {
        getSpaceBookings(spaceId);
    }
}
