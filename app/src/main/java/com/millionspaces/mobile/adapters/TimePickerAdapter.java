package com.millionspaces.mobile.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnTimeSelected;

import org.joda.time.LocalTime;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kasunka on 11/20/17.
 */

public class TimePickerAdapter extends RecyclerView.Adapter<TimePickerAdapter.ViewHolderHours>{

    private LocalTime openHour;

    private OnTimeSelected onTimeSelected;


    public TimePickerAdapter() {
        this.openHour = LocalTime.parse("00:00:00");
    }

    @Override
    public ViewHolderHours onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_calendar_picker_hours, parent, false);
        return new ViewHolderHours(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderHours holder, int position) {
        holder.time.setText(openHour.plusHours(position).toString("hh a"));
    }

    @Override
    public int getItemCount() {
        return 24;
    }


    public class ViewHolderHours extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.calender_perhour_hour)
        TextView time;
        @BindView(R.id.calender_perhour_hourholder)
        RelativeLayout timeHolder;
        @BindView(R.id.calender_perHour_seperator)
        View seperator;
        @BindView(R.id.calender_perhour_indicator)
        RelativeLayout selectedIndicator;

        public ViewHolderHours(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public void setOnItemClick(OnTimeSelected onTimeSelected){
        this.onTimeSelected =  onTimeSelected;
    }
}
