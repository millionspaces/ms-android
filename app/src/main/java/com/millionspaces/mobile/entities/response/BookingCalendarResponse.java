package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by kasunka on 12/14/17.
 */

public class BookingCalendarResponse implements Parcelable{
    private String name;
    private String addressLine1;
    private String addressLine2;
    private String calendarStart;
    private String calendarEnd;
    private int bufferTime;
    private int noticePeriod;
    private String availabilityMethod;
    private int blockChargeType;
    private int minParticipantCount;
    private ArrayList<Availability> availability;
    private ArrayList<BookingDate> futureBookingDates;
    private ArrayList<ExtraAmenity> extraAmenity;

    protected BookingCalendarResponse(Parcel in) {
        name = in.readString();
        addressLine1 = in.readString();
        addressLine2 = in.readString();
        calendarStart = in.readString();
        calendarEnd = in.readString();
        bufferTime = in.readInt();
        noticePeriod = in.readInt();
        availabilityMethod = in.readString();
        blockChargeType = in.readInt();
        minParticipantCount = in.readInt();
        availability = in.createTypedArrayList(Availability.CREATOR);
        futureBookingDates = in.createTypedArrayList(BookingDate.CREATOR);
        extraAmenity = in.createTypedArrayList(ExtraAmenity.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(addressLine1);
        dest.writeString(addressLine2);
        dest.writeString(calendarStart);
        dest.writeString(calendarEnd);
        dest.writeInt(bufferTime);
        dest.writeInt(noticePeriod);
        dest.writeString(availabilityMethod);
        dest.writeInt(minParticipantCount);
        dest.writeInt(blockChargeType);
        dest.writeTypedList(availability);
        dest.writeTypedList(futureBookingDates);
        dest.writeTypedList(extraAmenity);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingCalendarResponse> CREATOR = new Creator<BookingCalendarResponse>() {
        @Override
        public BookingCalendarResponse createFromParcel(Parcel in) {
            return new BookingCalendarResponse(in);
        }

        @Override
        public BookingCalendarResponse[] newArray(int size) {
            return new BookingCalendarResponse[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getCalendarStart() {
        return calendarStart;
    }

    public String getCalendarEnd() {
        return calendarEnd;
    }

    public int getBufferTime() {
        return bufferTime;
    }

    public int getNoticePeriod() {
        return noticePeriod;
    }

    public String getAvailabilityMethod() {
        return availabilityMethod;
    }

    public int getMinParticipantCount() {
        return minParticipantCount;
    }

    public int getBlockChargeType() {
        return blockChargeType;
    }

    public ArrayList<Availability> getAvailability() {
        return availability;
    }

    public ArrayList<BookingDate> getFutureBookingDates() {
        return futureBookingDates;
    }

    public ArrayList<ExtraAmenity> getExtraAmenity() {
        return extraAmenity;
    }
}
