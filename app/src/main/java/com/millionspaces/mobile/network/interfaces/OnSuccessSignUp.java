package com.millionspaces.mobile.network.interfaces;

/**
 * Created by kasunka on 8/22/17.
 */

public interface OnSuccessSignUp extends OnErrorResponse{
    void onSuccessSignup();
    void onValidateSignUpMail(Boolean isExist);
    void onValidateSignUpMailError(String error);
}
