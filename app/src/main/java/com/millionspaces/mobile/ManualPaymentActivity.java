package com.millionspaces.mobile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.entities.request.CancelBookingRequest;
import com.millionspaces.mobile.entities.response.CancelBookingResponse;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessCancelBooking;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.PaymentCountDownTimer;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM2;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM3;
import static com.millionspaces.mobile.utils.Config.GUEST_SPACE_BOOKINGS_UPCOMIG;
import static com.millionspaces.mobile.utils.Config.RESERVE_GUEST_BOOKING;
import static com.millionspaces.mobile.utils.Config.RESERVE_GUEST_BOOKING_STRING;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_TENTATIVE_RESERVATION;

public class ManualPaymentActivity extends BaseActivity implements OnSuccessCancelBooking, View.OnClickListener, OnItemClickListener, PaymentCountDownTimer.CounterTimerListener {

    @BindView(R.id.manual_paymnt_txt)
    TextView msg;
    @BindView(R.id.payment_parent)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.manualpayment_btn_next)
    Button submitButton;
    @BindView(R.id.timer_view)
    TextView timerView;
    private Bundle mBundle = null;
    private Dialog dialog;
    private String orderId;
    private String[] time;
    private int orderIdInt;
    private PaymentCountDownTimer timer;
    private final Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_payment);
        ButterKnife.bind(this);
        init_view();
    }

    private void init_view() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.maual_payment_title);
        submitButton.setOnClickListener(this);

        mBundle = getIntent().getExtras();
        orderId = mBundle.getString(BUNDLE_STRING_PARAM3);
        orderIdInt = mBundle.getInt(BUNDLE_INTEGER_PARAM);
        time = mBundle.getString(BUNDLE_STRING_PARAM2).split(":");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            msg.setText(Html.fromHtml(getString(R.string.manual_payment_msg_txt,"<font color='red'>"+mBundle.getString(BUNDLE_STRING_PARAM)+"</font>"),Html.FROM_HTML_OPTION_USE_CSS_COLORS));
        }else {
            msg.setText(Html.fromHtml(getString(R.string.manual_payment_msg_txt,"<font color='red'>"+mBundle.getString(BUNDLE_STRING_PARAM)+"</font>")));
        }

        timer = new PaymentCountDownTimer(this);
        timer.setTimerDuration(Integer.valueOf(time[0]),Integer.valueOf(time[1]),PaymentCountDownTimer.TICK_IN_SECONDS);
        timer.start();
    }

    private void attemptToSubmit() {
        CancelBookingRequest bookingRequest = new CancelBookingRequest(orderIdInt,RESERVE_GUEST_BOOKING,RESERVE_GUEST_BOOKING_STRING,RESERVE_GUEST_BOOKING);
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).cancelGuestBooking(bookingRequest, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure), getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ENF_TENTATIVE_RESERVATION);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.manualpayment_btn_next:
                attemptToSubmit();
                break;
        }
    }

    @Override
    public void OnClick(int position, View view) {
        switch (view.getId()) {
            case R.id.payment_action_button_01:
                timer.cancel();
                finishAffinity();
                navigateActivity(HomeActivity.class);
                break;
            case R.id.payment_action_button_02:
                timer.cancel();
                finishAffinity();
                Bundle bundle = new Bundle();
                bundle.putInt(BUNDLE_INTEGER_PARAM,orderIdInt);
                bundle.putBoolean(BUNDLE_BOOLEAN_PARAM, false);
                bundle.putInt(BUNDLE_INTEGER_PARAM2,GUEST_SPACE_BOOKINGS_UPCOMIG);
                bundle.putBoolean(BUNDLE_BOOLEAN_PARAM2,true);
                navigateActivityWithExtra(BookingPreviewActivity.class,bundle);
                break;
            case R.id.dialog_button_positive:
                timer.cancel();
                navigateActivity(HomeActivity.class);
                finishAffinity();
                break;
            case R.id.dialog_button_negative:
                timer.cancel();
                navigateActivity(HomeActivity.class );
                finishAffinity();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        timer.cancel();
        Intent returnIntent = new Intent();
        returnIntent.putExtra("TIME", timerView.getText().toString());
        setResult(Activity.RESULT_CANCELED,returnIntent);
        finish();
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinatorLayout);
    }

    @Override
    public void onSuccessCancelBooking(CancelBookingResponse responce) {
        hideProgress();
        if (TextUtils.equals(responce.getMessage(),"false : you had another pending payment")){
            AlertMessageUtils.showSnackBarMessage(getString(R.string.error_already_have_pending_payment), mCoordinatorLayout);
        }else {
            dialog = AlertMessageUtils.ShowPaymentStatusDialog(ContextCompat.getDrawable(this, R.drawable.ic_payment_success), "4",
                    getString(R.string.ipg_dialog_payment_manual_description), this, this);
        }
    }

    @Override
    public void onTick(final String time, long remainingTimeInMilis) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                timerView.setText(time);
            }
        });
    }

    @Override
    public void onFinish() {
        timer.cancel();
        AlertMessageUtils.showConfirmDialog(this,getString(R.string.exceed_timer_alert),getString(R.string.exceed_timer_des),null,getString(R.string.exceed_timer_negative),View.GONE,this);
    }

    @Override
    public void onConnectionErrorCallBack(int error) {
        attemptToSubmit();
    }
}
