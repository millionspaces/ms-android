package com.millionspaces.mobile.actions;

import java.util.HashMap;

/**
 * Created by kasunka on 11/23/17.
 */

public interface OnHostTimeSelected {
    void onTimeSelected(HashMap<String, String> blockedHoursMap, float charge, int position, boolean isTimeAdded, boolean isNewPosition);
    void onBookingSelected(boolean isManual, int  bookingId);
}
