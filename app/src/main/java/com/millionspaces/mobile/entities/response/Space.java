package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.millionspaces.mobile.entities.Menu;
import com.millionspaces.mobile.entities.SpaceType;

import java.util.ArrayList;

/**
 * Created by kasunka on 7/21/17.
 */

@SuppressWarnings("unchecked")
public class Space implements Parcelable {
    private int id;
    private String name;
    private String addressLine1;
    private String addressLine2;
    private String description;
    private int size;  //
    private int participantCount;
    private Double ratePerHour;
    private float discount; // TODO : Add discount on Next Release,
//    private float preStaringFrom;
//    private int user; //
    private int cancellationPolicy;
    private double longitude;
    private double latitude;
    private String thumbnailImage;
//    private int securityDeposit;
    private float rating;
    private String calendarStart;
    private String calendarEnd;
//    private String calendarEventSize; //
    private int bufferTime;
    private int noticePeriod;
    private int blockChargeType;
    private int minParticipantCount;
    private MeasurementUnit measurementUnit;
//    private String contactPersonName;
//    private String mobileNumber;
//    private String companyName;
//    private String companyPhone;
//    private String contactPersonName2;
//    private String mobileNumber2;
//    private String email2;
//    private float commissionPercentage;
//    private String accountHolderName;
//    private String accountNumber;
//    private String bank;
//    private String bankBranch;
//    private String hostLogo;
//    private Space parent;
//    private Space linkSpace;
//    private Space childs;

//    private BookingDateTime createdAt; //
//    private BookingDateTime updatedAt; //

//    private boolean advanceOnlyEnable;

    private String availabilityMethod;
    private ArrayList<Availability> availability;
    private ArrayList<Integer> eventType;
    private ArrayList<Integer> amenity;
    private ArrayList<Integer> rules;
    private ArrayList<SeatingArrangement> seatingArrangements;
    private ArrayList<ExtraAmenity> extraAmenity;
    private ArrayList<Menu> menuFiles;
    private ArrayList<SpaceType> spaceType;
    private int reimbursableOption;
    private boolean isReimbursable = false;
    private ArrayList<String>images;
    private SpaceOwner spaceOwnerDto;
//    private SpaceOwner spaceOwnerDto; //
//    private ArrayList<Space> childSpaces;
//    private ArrayList<Space> linkedSpaces;
    private ArrayList<BookingDate> futureBookingDates;
    private ArrayList<Space> similarSpaces;
    private int approved;


    protected Space(Parcel in) {
        id = in.readInt();
        name = in.readString();
        addressLine1 = in.readString();
        addressLine2 = in.readString();
        description = in.readString();
        size = in.readInt();
        participantCount = in.readInt();
        ratePerHour = in.readDouble();
//        user = in.readInt();
        cancellationPolicy = in.readInt();
        longitude = in.readDouble();
        latitude = in.readDouble();
        thumbnailImage = in.readString();
        discount = in.readFloat(); //TODO : Add discount on Next Release,
//        securityDeposit = in.readInt();
        rating = in.readFloat();
        calendarStart = in.readString();
        calendarEnd = in.readString();
//        calendarEventSize = in.readString();
        availabilityMethod = in.readString();
        blockChargeType = in.readInt();
        minParticipantCount = in.readInt();
        bufferTime = in.readInt();
        noticePeriod = in.readInt();
        measurementUnit = in.readParcelable(MeasurementUnit.class.getClassLoader());
//        menuFileName = in.readString(); //TODO: Remove this
        availability = in.createTypedArrayList(Availability.CREATOR);
        eventType = (ArrayList<Integer>) in.readSerializable();
        seatingArrangements = in.createTypedArrayList(SeatingArrangement.CREATOR);
        extraAmenity = in.createTypedArrayList(ExtraAmenity.CREATOR);
        menuFiles = in.createTypedArrayList(Menu.CREATOR);
        spaceType = in.createTypedArrayList(SpaceType.CREATOR);
        reimbursableOption = in.readInt();
        images = in.createStringArrayList();
        spaceOwnerDto = in.readParcelable(SpaceOwner.class.getClassLoader());
//        spaceOwnerDto = in.readParcelable(SpaceOwner.class.getClassLoader());
        futureBookingDates = in.createTypedArrayList(BookingDate.CREATOR);
        similarSpaces = in.createTypedArrayList(Space.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(addressLine1);
        dest.writeString(addressLine2);
        dest.writeString(description);
        dest.writeInt(size);
        dest.writeInt(participantCount);
        dest.writeDouble(ratePerHour);
//        dest.writeInt(user);
        dest.writeInt(cancellationPolicy);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
        dest.writeString(thumbnailImage);
        dest.writeFloat(discount); //TODO : Add discount on Next Release,
//        dest.writeInt(securityDeposit);
        dest.writeFloat(rating);
        dest.writeString(calendarStart);
        dest.writeString(calendarEnd);
//        dest.writeString(calendarEventSize);
        dest.writeString(availabilityMethod);
        dest.writeInt(blockChargeType);
        dest.writeInt(minParticipantCount);
        dest.writeInt(bufferTime);
        dest.writeInt(noticePeriod);
        dest.writeParcelable(measurementUnit, flags);
//        dest.writeString(menuFileName); //TODO: remove this
        dest.writeTypedList(availability);
        dest.writeSerializable(eventType);
        dest.writeTypedList(seatingArrangements);
        dest.writeTypedList(extraAmenity);
        dest.writeTypedList(menuFiles);
        dest.writeTypedList(spaceType);
        dest.writeInt(reimbursableOption);
        dest.writeStringList(images);
        dest.writeParcelable(spaceOwnerDto, flags);
//        dest.writeParcelable(spaceOwnerDto, flags);
        dest.writeTypedList(futureBookingDates);
        dest.writeTypedList(similarSpaces);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Space> CREATOR = new Creator<Space>() {
        @Override
        public Space createFromParcel(Parcel in) {
            return new Space(in);
        }

        @Override
        public Space[] newArray(int size) {
            return new Space[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getParticipantCount() {
        return participantCount;
    }

    public void setParticipantCount(int participantCount) {
        this.participantCount = participantCount;
    }

    public double getRatePerHour() {
        return ratePerHour;
    }

    public void setRatePerHour(double ratePerHour) {
        this.ratePerHour = ratePerHour;
    }

//    public int getUser() {
//        return user;
//    }
//
//    public void setUser(int user) {
//        this.user = user;
//    }

    public int getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(int cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    // TODO : Add discount on Next Release,
//    public float getDiscount() {
//        return discount;
//    }
//
//    public void setDiscount(float discount) {
//        this.discount = discount;
//    }

    //    public int getSecurityDeposit() {
//        return securityDeposit;
//    }
//
//    public void setSecurityDeposit(int securityDeposit) {
//        this.securityDeposit = securityDeposit;
//    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getCalendarStart() {
        return calendarStart;
    }

    public void setCalendarStart(String calendarStart) {
        this.calendarStart = calendarStart;
    }

    public String getCalendarEnd() {
        return calendarEnd;
    }

    public void setCalendarEnd(String calendarEnd) {
        this.calendarEnd = calendarEnd;
    }

//    public String getCalendarEventSize() {
//        return calendarEventSize;
//    }
//
//    public void setCalendarEventSize(String calendarEventSize) {
//        this.calendarEventSize = calendarEventSize;
//    }

    public String getAvailabilityMethod() {
        return availabilityMethod;
    }

    public void setAvailabilityMethod(String availabilityMethod) {
        this.availabilityMethod = availabilityMethod;
    }

    public int getBlockChargeType() {
        return blockChargeType;
    }

    public void setBlockChargeType(int blockChargeType) {
        this.blockChargeType = blockChargeType;
    }

    public int getMinParticipantCount() {
        return minParticipantCount;
    }

    public void setMinParticipantCount(int minParticipantCount) {
        this.minParticipantCount = minParticipantCount;
    }

    public int getBufferTime() {
        return bufferTime;
    }

    public void setBufferTime(int bufferTime) {
        this.bufferTime = bufferTime;
    }

    public int getNoticePeriod() {
        return noticePeriod;
    }

    public void setNoticePeriod(int noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public MeasurementUnit getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(MeasurementUnit measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    ////    public BookingDateTime getCreatedAt() {
////        return createdAt;
////    }
////
////    public void setCreatedAt(BookingDateTime createdAt) {
////        this.createdAt = createdAt;
////    }
////
////    public BookingDateTime getUpdatedAt() {
////        return updatedAt;
////    }
////
////    public void setUpdatedAt(BookingDateTime updatedAt) {
////        this.updatedAt = updatedAt;
////    }

    public ArrayList<Availability> getAvailability() {
        return availability;
    }

    public void setAvailability(ArrayList<Availability> availability) {
        this.availability = availability;
    }

    public ArrayList<Integer> getEventType() {
        return eventType;
    }

    public void setEventType(ArrayList<Integer> eventType) {
        this.eventType = eventType;
    }

    public ArrayList<Integer> getAmenity() {
        return amenity;
    }

    public void setAmenity(ArrayList<Integer> amenity) {
        this.amenity = amenity;
    }

    public ArrayList<Integer> getRules() {
        return rules;
    }

    public void setRules(ArrayList<Integer> rules) {
        this.rules = rules;
    }

    public ArrayList<SeatingArrangement> getSeatingArrangements() {
        return seatingArrangements;
    }

    public void setSeatingArrangements(ArrayList<SeatingArrangement> seatingArrangements) {
        this.seatingArrangements = seatingArrangements;
    }

    public ArrayList<ExtraAmenity> getExtraAmenity() {
        return extraAmenity;
    }

    public void setExtraAmenity(ArrayList<ExtraAmenity> extraAmenity) {
        this.extraAmenity = extraAmenity;
    }

    public ArrayList<Menu> getMenuFiles() {
        return menuFiles;
    }

    public ArrayList<SpaceType> getSpaceType() {
        return spaceType;
    }

    public int isReimbursableOption() {
        return reimbursableOption;
    }

    public void setReimbursableOption(int reimbursableOption) {
        this.reimbursableOption = reimbursableOption;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public SpaceOwner getSpaceOwnerDto() {
        return spaceOwnerDto;
    }

    public void setSpaceOwnerDto(SpaceOwner spaceOwnerDto) {
        this.spaceOwnerDto = spaceOwnerDto;
    }

    //    public SpaceOwner getSpaceOwnerDto() {
//        return spaceOwnerDto;
//    }
//
//    public void setSpaceOwnerDto(SpaceOwner spaceOwnerDto) {
//        this.spaceOwnerDto = spaceOwnerDto;
//    }


    public ArrayList<BookingDate> getFutureBookingDates() {
        return futureBookingDates;
    }

    public void setFutureBookingDates(ArrayList<BookingDate> futureBookingDates) {
        this.futureBookingDates = futureBookingDates;
    }

    public ArrayList<Space> getSimilarSpaces() {
        return similarSpaces;
    }

    public void setSimilarSpaces(ArrayList<Space> similarSpaces) {
        this.similarSpaces = similarSpaces;
    }

    public boolean isReimbursable() {
        for (Availability availability : getAvailability()){
            if (availability.getIsReimbursable() ==  1){
                isReimbursable = true;
            }
            break;
        }
        return isReimbursable;
    }
}
