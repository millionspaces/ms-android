package com.millionspaces.mobile.network.interfaces;

/**
 * Created by kasunka on 11/26/17.
 */

public interface OnSuccessDeleteManualBooking extends OnErrorResponse{
    void onSuccessDeleteManualBooking(int isSuccess);
}
