package com.millionspaces.mobile.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 1/11/18.
 */

public class SpaceType implements Parcelable{
    private int id;
    private String name;
    private String mobileIcon;

    public SpaceType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    protected SpaceType(Parcel in) {
        id = in.readInt();
        name = in.readString();
        mobileIcon = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(mobileIcon);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SpaceType> CREATOR = new Creator<SpaceType>() {
        @Override
        public SpaceType createFromParcel(Parcel in) {
            return new SpaceType(in);
        }

        @Override
        public SpaceType[] newArray(int size) {
            return new SpaceType[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMobileIcon() {
        return mobileIcon;
    }
}
