package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 7/21/17.
 */

public class SpaceOwner implements Parcelable{
    private int id;
    private String name;
    private String imageUrl;

    protected SpaceOwner(Parcel in) {
        id = in.readInt();
        name = in.readString();
        imageUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(imageUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SpaceOwner> CREATOR = new Creator<SpaceOwner>() {
        @Override
        public SpaceOwner createFromParcel(Parcel in) {
            return new SpaceOwner(in);
        }

        @Override
        public SpaceOwner[] newArray(int size) {
            return new SpaceOwner[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
