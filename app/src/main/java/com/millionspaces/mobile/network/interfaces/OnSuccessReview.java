package com.millionspaces.mobile.network.interfaces;

/**
 * Created by kasunka on 11/16/17.
 */

public interface OnSuccessReview extends OnErrorResponse{
    void onSuccessReview(String value);
}
