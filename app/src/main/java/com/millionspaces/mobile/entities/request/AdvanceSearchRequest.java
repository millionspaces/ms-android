package com.millionspaces.mobile.entities.request;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.millionspaces.mobile.entities.Location;

import java.util.ArrayList;

import static com.millionspaces.mobile.utils.Config.HOME_KEY_EVENT_TYPE;
import static com.millionspaces.mobile.utils.Config.HOME_KEY_LOCATION;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_EVENT_HOME_SEARCH;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_EVENT_SPACE_LIST;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_HOME_EVENT_TYPE;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_HOME_EVENT_TYPE_EMPTY;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_AMENITIES;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_BUDGET;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_DATE;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_EVENT_TYPE;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_LOCATION;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_PARTICIPATION;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_RULES;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_SEATING;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_SORT;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_LIST_SPACE_TYPE;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_NO_DATA_SET;
import static com.millionspaces.mobile.utils.FireballConstant.FIREBASE_PARAM_SPACE_SEARCH_BY;

/**
 * Created by kasunka on 7/21/17.
 */

public class AdvanceSearchRequest implements Parcelable{
    private String participation;
    private String budget;
    private ArrayList<Integer> amenities;
    private ArrayList<Integer> events;
    private Location location;
    private String available;
    private ArrayList<Integer> rules;
    private ArrayList<Integer> seatingArrangements;
    private String sortBy;
    private String organizationName;
    private ArrayList<Integer> spaceType;


    protected AdvanceSearchRequest(Parcel in) {
        participation = in.readString();
        budget = in.readString();
        location = in.readParcelable(Location.class.getClassLoader());
        available = in.readString();
        sortBy = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(participation);
        dest.writeString(budget);
        dest.writeParcelable(location, flags);
        dest.writeString(available);
        dest.writeString(sortBy);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AdvanceSearchRequest> CREATOR = new Creator<AdvanceSearchRequest>() {
        @Override
        public AdvanceSearchRequest createFromParcel(Parcel in) {
            return new AdvanceSearchRequest(in);
        }

        @Override
        public AdvanceSearchRequest[] newArray(int size) {
            return new AdvanceSearchRequest[size];
        }
    };

    public AdvanceSearchRequest(String participation, String budget, ArrayList<Integer> amenities, ArrayList<Integer> events, ArrayList<Integer> spaces,
                                Location location, String available, ArrayList<Integer> rules, ArrayList<Integer> seatingArrangements,
                                String sort, String organizationName, FirebaseAnalytics firebaseAnalytics) {
        this.participation = participation;
        this.budget = budget;
        this.amenities = amenities;
        this.events = events;
        this.spaceType = spaces;
        this.location = location;
        this.available = available;
        this.rules = rules;
        this.seatingArrangements = seatingArrangements;
        this.sortBy = sort;
        this.organizationName = organizationName;

        Bundle mBundle = new Bundle();
        mBundle.putString(FIREBASE_PARAM_SPACE_LIST_PARTICIPATION,participation!=null?participation:FIREBASE_PARAM_SPACE_NO_DATA_SET);
        mBundle.putString(FIREBASE_PARAM_SPACE_LIST_BUDGET,budget!=null?budget:FIREBASE_PARAM_SPACE_NO_DATA_SET);
        mBundle.putString(FIREBASE_PARAM_SPACE_LIST_AMENITIES,amenities.size()>0?amenities.toString():FIREBASE_PARAM_SPACE_NO_DATA_SET);
        mBundle.putString(FIREBASE_PARAM_SPACE_LIST_EVENT_TYPE,events.size()>0?events.toString():FIREBASE_PARAM_SPACE_NO_DATA_SET);
        mBundle.putString(FIREBASE_PARAM_SPACE_LIST_SPACE_TYPE,spaces.size()>0?spaces.toString():FIREBASE_PARAM_SPACE_NO_DATA_SET);
        mBundle.putString(FIREBASE_PARAM_SPACE_LIST_LOCATION,location != null? location.getAddress():FIREBASE_PARAM_SPACE_NO_DATA_SET);
        mBundle.putString(FIREBASE_PARAM_SPACE_LIST_DATE,available != null? available:FIREBASE_PARAM_SPACE_NO_DATA_SET);
        mBundle.putString(FIREBASE_PARAM_SPACE_LIST_RULES,rules.size()>0? rules.toString():FIREBASE_PARAM_SPACE_NO_DATA_SET);
        mBundle.putString(FIREBASE_PARAM_SPACE_LIST_SEATING,seatingArrangements.size()>0? seatingArrangements.toString():FIREBASE_PARAM_SPACE_NO_DATA_SET);
        mBundle.putString(FIREBASE_PARAM_SPACE_LIST_SORT,sort != null? sort:FIREBASE_PARAM_SPACE_NO_DATA_SET);
        mBundle.putString(FIREBASE_PARAM_SPACE_SEARCH_BY,organizationName != null? organizationName:FIREBASE_PARAM_SPACE_NO_DATA_SET);

        firebaseAnalytics.logEvent(FIREBASE_EVENT_SPACE_LIST, mBundle);
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public ArrayList<Integer> getAmenities() {
        return amenities;
    }

    public void setAmenities(ArrayList<Integer> amenities) {
        this.amenities = amenities;
    }

    public ArrayList<Integer> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Integer> events) {
        this.events = events;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public ArrayList<Integer> getRules() {
        return rules;
    }

    public void setRules(ArrayList<Integer> rules) {
        this.rules = rules;
    }

    public ArrayList<Integer> getSeatingArrangements() {
        return seatingArrangements;
    }

    public void setSeatingArrangements(ArrayList<Integer> seatingArrangements) {
        this.seatingArrangements = seatingArrangements;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }
}
