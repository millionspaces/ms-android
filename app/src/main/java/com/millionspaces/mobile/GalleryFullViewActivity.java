package com.millionspaces.mobile;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.millionspaces.mobile.adapters.ImageAdapter;
import com.millionspaces.mobile.utils.Config;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.IMAGE_PLACEHOLDER_FACTOR;


public class GalleryFullViewActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private ImageAdapter adapter;
    @BindView(R.id.pager_gallery_full)
    ViewPager viewPager;
    @Nullable @BindView(R.id.btn_next)
    ImageButton btnNext;
    @Nullable @BindView(R.id.btn_back)
    ImageView btnBack;
    @Nullable @BindView(R.id.viewPagerCountDots)
    LinearLayout pager_indicator;
    @Nullable @BindView(R.id.indicatorScrollView)
    HorizontalScrollView scrollView;
    private int dotsCount, currentPosition;
    private ImageView[] dots;
    private ArrayList<String> images = new ArrayList<String>();
    private int orientation = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_gallery_fullview);
        ButterKnife.bind(this);
        setUpViewPager();
    }

    private void setUpViewPager() {
        images = getIntent().getStringArrayListExtra("IMAGES");
        currentPosition = getIntent().getIntExtra("POSITION",0);
        adapter = new ImageAdapter(GalleryFullViewActivity.this, images,this);
        viewPager.setAdapter(adapter);
        orientation = getResources().getConfiguration().orientation;

        btnNext.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        viewPager.addOnPageChangeListener(this);
        setUiPageViewController();

        if (orientation == 1){
            btnNext.setVisibility(View.GONE);
            btnBack.setVisibility(View.GONE);
        }else {
//            btnNext.setVisibility(View.VISIBLE);
//            btnBack.setVisibility(View.VISIBLE);
        }
    }

    private void setUiPageViewController() {
        dotsCount = adapter.getCount();
        dots = new ImageView[dotsCount];
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        params.setMargins(8, 0, 8, 0);
        if (orientation == 1) {
            params.width = 400;
            params.height = 250;
        }

        if (dotsCount == 1) {
            btnNext.setVisibility(View.GONE);
        }else{
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(this);
                setImageView(dots[i],i);
                final int finalI = i;
                dots[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPager.setCurrentItem(finalI);
                    }
                });
                pager_indicator.addView(dots[i], params);
            }
            setSelectedImageView(dots[currentPosition]);
            viewPager.setCurrentItem(currentPosition);
        }

    }

    private void setImageView(ImageView imageView, int position){
        if (orientation == 1){
            RequestBuilder<Drawable> thumbnailRequest = Glide.with(this)
                    .load(BuildConfig.IMAGE_SERVER_URL+IMAGE_PLACEHOLDER_FACTOR+ BuildConfig.IMAGE_SERVER_DOMAIN+images.get(position));
            Glide.with(this)
                    .load(BuildConfig.IMAGE_SERVER_URL+Config.optimizedImage(this)+BuildConfig.IMAGE_SERVER_DOMAIN+images.get(position)) // LIVE URL
                    .thumbnail(thumbnailRequest)
                    .into(imageView);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(10,10,10,10);
            imageView.setBackgroundColor(Color.TRANSPARENT);
        }else {
            imageView.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.gallery_indicator_nonselected));
        }
    }

    private void setSelectedImageView(ImageView imageView){
        if (orientation == 1){
            imageView.setBackgroundColor(ContextCompat.getColor(this,R.color.colorAccent));
            int x_pos = imageView.getLeft();
            scrollView.smoothScrollTo(x_pos,0);
        }else {
            imageView.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.gallery_indicator_selected));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_next:
                viewPager.setCurrentItem((viewPager.getCurrentItem() < dotsCount)
                        ? viewPager.getCurrentItem() + 1 : 0);
                break;
            case R.id.btn_back:
                viewPager.setCurrentItem((viewPager.getCurrentItem() != 0)
                        ? viewPager.getCurrentItem() - 1 : 0);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            setImageView(dots[i],i);
        }

        setSelectedImageView(dots[position]);

        if (position + 1 == dotsCount) {
            btnNext.setVisibility(View.GONE);
            btnBack.setVisibility(orientation != 1?View.VISIBLE:View.GONE);
        } else if (position == 0){
            btnNext.setVisibility(orientation != 1?View.VISIBLE:View.GONE);
            btnBack.setVisibility(View.GONE);
        }else {
            btnBack.setVisibility(orientation != 1?View.VISIBLE:View.GONE);
            btnNext.setVisibility(orientation != 1?View.VISIBLE:View.GONE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
