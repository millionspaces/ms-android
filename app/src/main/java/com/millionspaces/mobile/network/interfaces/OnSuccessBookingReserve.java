package com.millionspaces.mobile.network.interfaces;


import com.millionspaces.mobile.entities.response.BookingReserveResponse;

/**
 * Created by kasunka on 9/19/17.
 */

public interface OnSuccessBookingReserve extends OnErrorResponse{
    void onSuccessReserve(BookingReserveResponse response);
}
