package com.millionspaces.mobile.utils;

import android.os.CountDownTimer;

import java.text.DecimalFormat;

/**
 * Created by kasunka on 11/30/17.
 */

public class PaymentCountDownTimer {

    private CountDownTimer mTimer;
    private CounterTimerListener mListener;

    public static final int TICK_IN_MILISECONDS = 1;
    public static final int TICK_IN_SECONDS = 1000;
    public static final int TICK_IN_MINUTES = 60000;

    private final DecimalFormat df = new DecimalFormat("#.##");

    private long duration;

    public PaymentCountDownTimer(CounterTimerListener listener){
        this.mListener = listener;
    }

    public void setTimerDuration(int minutes, int seconds, int tickInterval){
        //convert to milisecs
        this.duration = minutes*60*1000 + seconds* 1000;
        //init timer
        mTimer = new CountDownTimer(this.duration, tickInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
                long minutes = millisUntilFinished/(60*1000);
                long seconds = (millisUntilFinished/1000)%60;

                String rt = minutes+":"+(seconds < 10 ? "0"+seconds:seconds);

                mListener.onTick(rt, millisUntilFinished);

            }

            @Override
            public void onFinish() {
                mListener.onFinish();
            }
        };
    }

    public void start(){
        if(null != mTimer)
            mTimer.start();
    }

    public void cancel(){
        if(null != mTimer)
            mTimer.cancel();
    }

    public interface CounterTimerListener{
        public void onTick(String time, long remainingTimeInMilis);
        public void onFinish();
    }
}
