package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 8/16/17.
 */

public class UserLoginResponse implements Parcelable {
    private int id;
    private String email;
    private String password;
    private String name;
    private String contactPersonName;
    private String address;
    private String mobileNumber;
    private String mobile; // TODO : Remove Field
    private String jobTitle;
    private String commissionPercentage;
    private String imageUrl;
    private String companyName;
    private String companyPhone;
    private String about;
    private String accountHolderName;
    private String accountNumber;
    private String bank;
    private String bankBranch;
    private int active;
    private String role;
    private boolean isTrustedUser;
    private String dateOfBirth;
    private String anniversary;
    private String createdAt;
    private String updatedAt;

    public UserLoginResponse() {

    }

    public UserLoginResponse(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public UserLoginResponse(int id, String email, String role) {
        this.id = id;
        this.email = email;
        this.role = role;
    }

    protected UserLoginResponse(Parcel in) {
        id = in.readInt();
        email = in.readString();
        password = in.readString();
        name = in.readString();
        contactPersonName = in.readString();
        address = in.readString();
        mobileNumber = in.readString();
        mobile = in.readString();
        jobTitle = in.readString();
        commissionPercentage = in.readString();
        imageUrl = in.readString();
        companyName = in.readString();
        companyPhone = in.readString();
        about = in.readString();
        accountHolderName = in.readString();
        accountNumber = in.readString();
        bank = in.readString();
        bankBranch = in.readString();
        active = in.readInt();
        role = in.readString();
        isTrustedUser = in.readByte() != 0;
        dateOfBirth = in.readString();
        anniversary = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(name);
        dest.writeString(contactPersonName);
        dest.writeString(address);
        dest.writeString(mobileNumber);
        dest.writeString(mobile);
        dest.writeString(jobTitle);
        dest.writeString(commissionPercentage);
        dest.writeString(imageUrl);
        dest.writeString(companyName);
        dest.writeString(companyPhone);
        dest.writeString(about);
        dest.writeString(accountHolderName);
        dest.writeString(accountNumber);
        dest.writeString(bank);
        dest.writeString(bankBranch);
        dest.writeInt(active);
        dest.writeString(role);
        dest.writeByte((byte) (isTrustedUser ? 1 : 0));
        dest.writeString(dateOfBirth);
        dest.writeString(anniversary);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserLoginResponse> CREATOR = new Creator<UserLoginResponse>() {
        @Override
        public UserLoginResponse createFromParcel(Parcel in) {
            return new UserLoginResponse(in);
        }

        @Override
        public UserLoginResponse[] newArray(int size) {
            return new UserLoginResponse[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getMobile() {
        return mobile;
    }

    public int getActive() {
        return active;
    }

    public String getRole() {
        return role;
    }

    public boolean isTrustedUser() {
        return isTrustedUser;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
