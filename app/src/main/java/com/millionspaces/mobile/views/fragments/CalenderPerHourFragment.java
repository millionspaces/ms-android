package com.millionspaces.mobile.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.millionspaces.mobile.BookingEngineActivity;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.IMethodUpdater;
import com.millionspaces.mobile.actions.OnTimeSelected;
import com.millionspaces.mobile.adapters.calendar.DayTimeHourAdapter;
import com.millionspaces.mobile.utils.CalendarManager;

import org.joda.time.Hours;
import org.joda.time.LocalTime;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.millionspaces.mobile.utils.CalenderUtils.SELECTED_HOUR;

/**
 * Created by kasunka on 8/31/17.
 */

public class CalenderPerHourFragment extends Fragment implements OnTimeSelected, IMethodUpdater {

    @BindView(R.id.calender_hours_hours)
    RecyclerView calenderHoursRecyclerView;
    @BindView(R.id.calender_hours_backto_day)
    ImageView backToDay;

    private OnTimeSelected onTimeSelected;

    private HashMap<String ,String> blockedHoursMap;
    private int noOfWorkingHours = 0;
    private float chargePerHour = 0;
    private DayTimeHourAdapter timeHourAdapter;

    private CalendarManager mCalendarManager;
    private Unbinder unbinder;

    public static Fragment newInstance() {
        CalenderPerHourFragment fragment = new CalenderPerHourFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof OnTimeSelected) {
                onTimeSelected = (OnTimeSelected) context;
            }
        }catch (ClassCastException e) {
            throw new ClassCastException();
        }
        mCalendarManager = ((BookingEngineActivity)getActivity()).calendarManager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calender_per_hour, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String limits[] = mCalendarManager.hourLimitsForTheDay();
        LocalTime openTime = LocalTime.parse(limits[0]);
        LocalTime closeTime = LocalTime.parse(limits[1]);
        noOfWorkingHours = TextUtils.equals(limits[1],"23:59:59")?Hours.hoursBetween(openTime,closeTime).getHours()+1:Hours.hoursBetween(openTime,closeTime).getHours();
        chargePerHour = mCalendarManager.getDayAvailability(mCalendarManager.getSelectedDateObject().getDayOfWeek()).getCharge();
        blockedHoursMap = mCalendarManager.getUnavailableHoursPerHour(mCalendarManager.getSelectedDate());
        setUpHourList(openTime,noOfWorkingHours);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCalendarManager.getBlockedHoursMap().clear();
    }

    private void setUpHourList(LocalTime openTime, int noOfWorkingHours) {
        calenderHoursRecyclerView.setHasFixedSize(true);
        calenderHoursRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        timeHourAdapter = new DayTimeHourAdapter(getContext(),openTime,noOfWorkingHours,blockedHoursMap,this);
        calenderHoursRecyclerView.setAdapter(timeHourAdapter);
        setUpActions();
    }

    private void setUpActions() {
        timeHourAdapter.setOnItemClick(this);
    }


    @Override
    public void updateList(boolean isadded,String key, String value) {
        if (isadded){
            mCalendarManager.getBlockedHoursMap().put(key,value);
        }else {
            mCalendarManager.getBlockedHoursMap().remove(key);
        }
    }

    @OnClick(R.id.calender_hours_backto_day)
    void backToDay(){
        ((CalenderFragment)getParentFragment()).showBackFragment();
    }

    @Override
    public void onTimeSelected(HashMap<String, String> blockedHoursMap, float charge, int position, boolean isAdded, boolean isNewPosition) {
        HashMap<String, String> selectedHoursMap = new LinkedHashMap<>();
        if (isNewPosition){
            selectedHoursMap.clear();
        }
        for (Map.Entry<String, String> entry : blockedHoursMap.entrySet()){
            if (entry.getValue().contains(SELECTED_HOUR)){
                selectedHoursMap.put(entry.getKey(),entry.getValue());
            }
        }
        onTimeSelected.onTimeSelected(selectedHoursMap,chargePerHour,position,isAdded,isNewPosition);
    }

    @Override
    public void onNoticePeriodSelected() {
        onTimeSelected.onNoticePeriodSelected();
    }
}
