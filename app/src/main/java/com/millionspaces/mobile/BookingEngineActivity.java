package com.millionspaces.mobile;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.millionspaces.mobile.actions.OnAmenitySelected;
import com.millionspaces.mobile.actions.OnBlockSelected;
import com.millionspaces.mobile.actions.OnDateSelected;
import com.millionspaces.mobile.actions.OnNumberSelected;
import com.millionspaces.mobile.actions.OnPromoSelected;
import com.millionspaces.mobile.actions.OnTimeSelected;
import com.millionspaces.mobile.entities.request.PromoCodeRequest;
import com.millionspaces.mobile.entities.request.SapceBookingExtraAmenityRequest;
import com.millionspaces.mobile.entities.request.SpaceBookingDateRequest;
import com.millionspaces.mobile.entities.request.SpaceBookingRequest;
import com.millionspaces.mobile.entities.response.Availability;
import com.millionspaces.mobile.entities.response.BookingCalendarResponse;
import com.millionspaces.mobile.entities.response.BookingReserveResponse;
import com.millionspaces.mobile.entities.response.PromoCodeResponse;
import com.millionspaces.mobile.entities.response.UserLoginResponse;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessBookingReserve;
import com.millionspaces.mobile.network.interfaces.OnSuccessCalendarBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessPromoCode;
import com.millionspaces.mobile.network.interfaces.OnUpdateUser;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.AnimationsUtils;
import com.millionspaces.mobile.utils.CalendarManager;
import com.millionspaces.mobile.utils.NestedScrollingView;
import com.millionspaces.mobile.utils.PreferenceUtils;
import com.millionspaces.mobile.views.fragments.CalenderFragment;
import com.millionspaces.mobile.views.fragments.ExtraAmenityFragment;

import org.joda.time.LocalTime;
import org.joda.time.Period;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.APP_CURRENCY_NODECIMAL_FORMAT;
import static com.millionspaces.mobile.utils.Config.APP_CURRENCY_TYPE_PRIFIX;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.KEY_CURRENT_CHECKING_EVENT_TYPE;
import static com.millionspaces.mobile.utils.Config.KEY_CURRENT_CHECKING_GUEST_COUNT;
import static com.millionspaces.mobile.utils.Config.KEY_CURRENT_CHECKING_SEATING_ID;
import static com.millionspaces.mobile.utils.Config.KEY_CURRENT_CHECKING_SPACE;
import static com.millionspaces.mobile.utils.Config.KEY_IS_LOGGED_IN;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_EMAIL;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_ID;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_ROLE;
import static com.millionspaces.mobile.utils.Config.KEY_ONACTIVITY_RESULT_GUEST_BOOKING;
import static com.millionspaces.mobile.utils.Config.KEY_ONACTIVITY_RESULT_LOGIN;
import static com.millionspaces.mobile.utils.Config.MILLIONSPACE_MOBILE_NUMBER;
import static com.millionspaces.mobile.utils.Config.RESPONSE_PRICE_HOUR;
import static com.millionspaces.mobile.utils.Config.SUCCESS_PROMO_CODE;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_GET_GUEST_CALENDAR_DETAILS;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_GET_GUEST_PROMO_CODE;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_GET_GUEST_RESERVE_BOOKING;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_PUT_GUEST_MOBILE_NUMBER;

public class BookingEngineActivity extends BaseActivity implements OnSuccessCalendarBooking, View.OnClickListener, OnDateSelected, OnTimeSelected, OnBlockSelected, OnAmenitySelected, OnSuccessBookingReserve, OnSuccessPromoCode, OnPromoSelected, OnUpdateUser, OnNumberSelected {
    @BindView(R.id.parent_booking)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.booking_proceed_btn)
    Button prceed;
    @BindView(R.id.fragments_container)
    FrameLayout frameLayout;
    @BindView(R.id.booking_scroll_parent)
    NestedScrollingView parent;
    @BindView(R.id.booking_action_button_container)
    LinearLayout actionButtonContainer;
    @BindView(R.id.booking_bottom_action)
    LinearLayout actionBottom;
    @BindView(R.id.tab_calender_container)
    LinearLayout tabCalender;
    @BindView(R.id.tab_extra_container)
    LinearLayout tabExtra;

    @BindView(R.id.calender_date_txt)
    TextView currentDateDay;
    @BindView(R.id.calender_year_txt)
    TextView currentDateYear;
    @BindView(R.id.calender_month_txt)
    TextView currentDateMonth;
    @BindView(R.id.calender_extra_txt_total)
    TextView extraTotalView;
    @BindView(R.id.calender_space_txt_total)
    TextView spaceTotalView;
    @BindView(R.id.calender_total_txt_total)
    TextView totalView;
    @BindView(R.id.calender_promo_container)
    RelativeLayout promoContainer;
    @BindView(R.id.calender_promo_txt_title)
    TextView promoTitleView;
    @BindView(R.id.calender_promo_txt_total)
    TextView promoDiscountView;

    private FragmentManager manager;
    private String backStateName;
    private Calendar calendar;
    private BookingCalendarResponse bookingCalendarObject;
    private SpaceBookingRequest request;
    private String startDate , selectedDateString = null, spaceId,eventTypeId, eventGuestCount, eventSeatingId;
    private float totalPrice, extraTotal, spaceTotal, totdalDiscountedPrice, discountPrice = 0;
    public int spaceTotalHours = 0;
    private LocalTime startTime = null, endTime = null;
    private ArrayList<SpaceBookingDateRequest> dateRequest = new ArrayList<>();
    private ArrayList<Availability> selectedAvailabiliyList = new ArrayList<>();
    private boolean isDateSelected = false;
    private Dialog confirmDialog;
    private String promoCode = null;
    private int discount = 0;
    private boolean isFlatDiscountRate = false, isPromoCodeAdded = false;
    private String mobileNumber = null;

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String MONTH_FORMAT = "MMMM";

    private HashMap<Integer, Integer> selectedAmenityMap = new LinkedHashMap<>();
    private HashMap<Integer, Float> selectePeHourdAmenityMap = new LinkedHashMap<>();

    public CalendarManager calendarManager;
    private Fragment calenderFragment, extraAmenityFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_engine);
        ButterKnife.bind(this);
        init_views();
        getSpaceBookings();
    }

    private void init_views() {
        prceed.setOnClickListener(this);

        init_toolbar();
        setToolbarTitle(getString(R.string.calender_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        spaceId = PreferenceUtils.showPreferenceString(KEY_CURRENT_CHECKING_SPACE,this);
        eventTypeId = PreferenceUtils.showPreferenceString(KEY_CURRENT_CHECKING_EVENT_TYPE,this);
        eventGuestCount = PreferenceUtils.showPreferenceString(KEY_CURRENT_CHECKING_GUEST_COUNT,this);
        eventSeatingId = PreferenceUtils.showPreferenceString(KEY_CURRENT_CHECKING_SEATING_ID,this);

        request = new SpaceBookingRequest(Integer.valueOf(spaceId),Integer.valueOf(eventTypeId),Integer.valueOf(eventGuestCount),Integer.valueOf(eventSeatingId));

        tabCalender.setOnClickListener(this);
        tabExtra.setOnClickListener(this);
        promoContainer.setOnClickListener(this);

        calendar = Calendar.getInstance();
        setCalenderTabDate(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)),
                calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()), calendar.get(Calendar.YEAR));
        setSelectedTab(tabCalender, tabExtra);
    }

    private void getSpaceBookings() {
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).getSpaceBookings(spaceId, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout, this, ENF_GET_GUEST_CALENDAR_DETAILS);
        }
    }

    @Override
    public void onSuccessCalendarBooking(BookingCalendarResponse value) {
        hideProgress();
        mCoordinatorLayout.setVisibility(View.VISIBLE);
        this.bookingCalendarObject = value;

        startDate = new SimpleDateFormat(DATE_FORMAT).format(calendar.getTime());
        calendarManager = new CalendarManager(startDate, value.getCalendarEnd(), value.getFutureBookingDates(), value.getAvailability(),
                value.getAvailabilityMethod(), value.getBlockChargeType(), value.getMinParticipantCount(),
                value.getMinParticipantCount()>request.getGuestCount()?value.getMinParticipantCount():request.getGuestCount(),value.getBufferTime(),value.getNoticePeriod());
        calenderFragment = CalenderFragment.newInstance();
        extraAmenityFragment = ExtraAmenityFragment.newInstance(value.getExtraAmenity());
        popFragment(calenderFragment);
    }

    private void getPromoCode() {
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).getPromoCode(new PromoCodeRequest(spaceId,promoCode),this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ENF_GET_GUEST_PROMO_CODE);
        }
    }

    @Override
    public void onSuccessPromoCode(PromoCodeResponse promoCodeResponse) {
        hideProgress();
        if (promoCodeResponse.getStatus() == SUCCESS_PROMO_CODE){
            this.isPromoCodeAdded = true;
            this.isFlatDiscountRate = promoCodeResponse.getPromoDetails().getIsFlatDiscount()==1?true:false;
            this.discount = promoCodeResponse.getPromoDetails().getDiscount();
            promoDiscountView.setVisibility(View.VISIBLE);
            promoTitleView.setText(isFlatDiscountRate?getString(R.string.calendar_promocode_flat_rate_title): getString(R.string.calendar_promocode_precentage_title,discount));
            updateActionContainerView();
        }else {
            AlertMessageUtils.showSnackBarMessage(promoCodeResponse.getMessage(),mCoordinatorLayout);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.booking_proceed_btn:
                proceedTocheckOut();
                break;
            case R.id.tab_calender_container:
                setSelectedTab(v, tabExtra);
                popFragment(calenderFragment);
                if (!isDateSelected) {
                    AnimationsUtils.sendDown(actionButtonContainer);
                }
                break;
            case R.id.tab_extra_container:
                setSelectedTab(v, tabCalender);
                popFragment(extraAmenityFragment);
                actionButtonContainer.setVisibility(View.VISIBLE);
                AnimationsUtils.moveUp(actionButtonContainer);
                break;
//            case R.id.dialog_button_positive:
//                confirmDialog.dismiss();
//                proceedToBooking();
//                break;
//            case R.id.dialog_button_negative:
//                confirmDialog.dismiss();
//                break;
            case R.id.edit_action_button:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(MILLIONSPACE_MOBILE_NUMBER));
                startActivity(intent);
                break;
            case R.id.calender_promo_container:
                AlertMessageUtils.showPromoInputDialog(this,this);
                break;
        }
    }

    @Override
    public void onDateSelected(String day, String month, int year, boolean isSelected) {
        setCalenderTabDate(day, month, year);
        isDateSelected = isSelected;
        if (isSelected) {
            actionButtonContainer.setVisibility(View.VISIBLE);
            AnimationsUtils.moveUp(actionButtonContainer);

            Calendar cal = Calendar.getInstance();
            try {
                cal.setTime(new SimpleDateFormat(MONTH_FORMAT).parse(month));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            selectedDateString = year + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + day + "T";
        }
    }

    @Override
    public void onTimeSelected(HashMap<String, String> bookedHoursMap, float charge, int position, boolean isTimeAdded, boolean isNewPosition) {
        LocalTime tempStart = null, tempEnd = null;
        for (String timeString : bookedHoursMap.keySet()) {
            LocalTime time = LocalTime.parse(timeString);

            if (tempStart != null) {
                if (time.isBefore(tempStart)) {
                    tempStart = time;
                }

                if (time.plusHours(1).isAfter(tempEnd)) {
                    tempEnd = time.plusHours(1);
                }
            } else {
                tempStart = time;
                tempEnd = time.plusHours(1);
            }
        }

        if (isNewPosition){
            for (Map.Entry<Integer, Float> entry : selectePeHourdAmenityMap.entrySet()) {
                extraTotal -= entry.getValue() * spaceTotalHours;
            }
        }

        if (!bookedHoursMap.isEmpty()) {
            spaceTotalHours = Period.fieldDifference(tempStart, tempEnd).getHours();
            startTime = tempStart;
            endTime = tempEnd;
        } else {
            startTime = null;
            spaceTotalHours = 0;
        }

        if (isTimeAdded) {
            for (Map.Entry<Integer, Float> entry : selectePeHourdAmenityMap.entrySet()) {
                extraTotal += entry.getValue() * 1;
            }
        } else {
            for (Map.Entry<Integer, Float> entry : selectePeHourdAmenityMap.entrySet()) {
                extraTotal -= entry.getValue() * 1;
            }
        }

        spaceTotal = spaceTotalHours * charge;
        totalPrice = spaceTotal + extraTotal;
        updateActionContainerView();
    }

    @Override
    public void onBlockSelected(Availability availability, float charge, boolean isBlockAdded) {
        if (isBlockAdded){
            selectedAvailabiliyList.add(availability);
            spaceTotalHours += Math.abs(Period.fieldDifference(LocalTime.parse(availability.getFrom()), LocalTime.parse(availability.getTo())).getHours());
            spaceTotal += charge;
            for (Map.Entry<Integer, Float> entry : selectePeHourdAmenityMap.entrySet()) {
                extraTotal += entry.getValue() * Period.fieldDifference(LocalTime.parse(availability.getFrom()), LocalTime.parse(availability.getTo())).getHours();
            }
        }else {
            selectedAvailabiliyList.remove(availability);
            spaceTotalHours -= Math.abs(Period.fieldDifference(LocalTime.parse(availability.getFrom()), LocalTime.parse(availability.getTo())).getHours());
            spaceTotal -= charge;
            for (Map.Entry<Integer, Float> entry : selectePeHourdAmenityMap.entrySet()) {
                extraTotal -= entry.getValue() * Period.fieldDifference(LocalTime.parse(availability.getFrom()), LocalTime.parse(availability.getTo())).getHours();
            }
        }
        totalPrice = spaceTotal + extraTotal;
        updateActionContainerView();
    }

    @Override
    public void onAmenitySelected(int amenityId, int count, boolean isAmenityAdded, float totalExtraAmount) {
        if (isAmenityAdded) {
            selectedAmenityMap.put(amenityId, count);
        } else {
            selectedAmenityMap.remove(amenityId);
        }
        extraTotal = totalExtraAmount;
        totalPrice = spaceTotal + extraTotal;
        updateActionContainerView();
    }

    @Override
    public void onPerHourAmenitySelected(int amenityId, int count, boolean isAmenityAdded, float amenityAmount) {
        if (isAmenityAdded) {
            if (!selectePeHourdAmenityMap.containsKey(amenityId)) {
                selectedAmenityMap.put(amenityId, count);
                selectePeHourdAmenityMap.put(amenityId, amenityAmount);
                extraTotal += amenityAmount * spaceTotalHours;
            }
        } else {
            selectedAmenityMap.remove(amenityId);
            selectePeHourdAmenityMap.remove(amenityId);
            extraTotal -= amenityAmount * spaceTotalHours;
        }
        totalPrice = spaceTotal + extraTotal;
        updateActionContainerView();
    }

    private void updateActionContainerView(){
        if (isPromoCodeAdded){
            if (isFlatDiscountRate){
                discountPrice = discount;
            }else {
                discountPrice = (int)Math.ceil(totalPrice * discount/100);
            }
            totdalDiscountedPrice = (totalPrice - discountPrice)>0?totalPrice - discountPrice:0;
        }else {
            totdalDiscountedPrice = totalPrice;
        }
        spaceTotalView.setText(APP_CURRENCY_TYPE_PRIFIX + new DecimalFormat(APP_CURRENCY_NODECIMAL_FORMAT).format(spaceTotal));
        extraTotalView.setText(APP_CURRENCY_TYPE_PRIFIX + new DecimalFormat(APP_CURRENCY_NODECIMAL_FORMAT).format(extraTotal));
        promoDiscountView.setText(APP_CURRENCY_TYPE_PRIFIX+ new DecimalFormat(APP_CURRENCY_NODECIMAL_FORMAT).format(discountPrice));
        totalView.setText(APP_CURRENCY_TYPE_PRIFIX + new DecimalFormat(APP_CURRENCY_NODECIMAL_FORMAT).format(totdalDiscountedPrice));
    }

    @Override
    public void onNullPerHourAmenityChecked() {
        AlertMessageUtils.showSnackBarMessage(getString(R.string.error_non_selced_hour), mCoordinatorLayout);
    }

    @Override
    public void onPromoSelected(String promo) {
        this.promoCode = promo;
        getPromoCode();
    }

    private void proceedTocheckOut() {
        boolean isValid = true;
        String error = null;
        if (bookingCalendarObject.getAvailabilityMethod().contains(RESPONSE_PRICE_HOUR)){
            if (startTime == null || startTime.toString().isEmpty()) {
                isValid = false;
                error = getString(R.string.error_non_selced_date);
            }
        }else {
            if (selectedAvailabiliyList.isEmpty()){
                isValid = false;
                error = getString(R.string.error_non_selced_date);
            }
        }

        if (isValid) {
            confirmDialog = AlertMessageUtils.showNumberPickerDialog(this, this);
        } else {
            AlertMessageUtils.showSnackBarMessage(error, mCoordinatorLayout);
        }
    }

    @Override
    public void onNoticePeriodSelected() {
        AlertMessageUtils.showEditSpaceDialog(this,getString(R.string.booking_dialog_description),this);
    }

    @Override
    public void onNumberAdded(String number) {
        this.mobileNumber = number;
        updateMobileNumber();
    }

    private void updateMobileNumber(){
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).updateUserMobile(mobileNumber,this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ENF_PUT_GUEST_MOBILE_NUMBER);
        }
//        onUpdateUser("kkk");
    }

    @Override
    public void onUpdateUser(String responce) {
        proceedToBooking();
    }

    //    Proceed to booking with network call
    private void proceedToBooking() {
        request.setBookingCharge(totdalDiscountedPrice);
        request.setBookingWithExtraAmenityDtoSet(getAmenityList());
        request.setDates(getDateTimeRangeList());
        request.setPromoCode(promoCode);

//        if (PreferenceUtils.showPreferenceBoolean(KEY_IS_LOGGED_IN, false, this)) {
            reserveBooking(request);
//        } else {
//            navigateActivityForResult(LoginActivity.class, KEY_ONACTIVITY_RESULT_LOGIN);
//        }
    }

    private void reserveBooking(SpaceBookingRequest request) {
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
//            showProgress();
            MillionSpaceService.getInstance(this).reserveBooking(request, this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout,this,ENF_GET_GUEST_RESERVE_BOOKING);
        }
    }

    @Override
    public void onSuccessReserve(BookingReserveResponse response) {
        hideProgress();
        if (response.getError()== null) {
//            if (response.getBookingCharge() != totdalDiscountedPrice){
//                AlertMessageUtils.showSnackBarMessage(getString(R.string.error_mismatch_client_server_cost), mCoordinatorLayout);
//            }else {
                response.setAmount(new DecimalFormat("#.00").format(totdalDiscountedPrice));
                response.setSpaceName(bookingCalendarObject.getName());
                response.setAddress(bookingCalendarObject.getAddressLine2() != null ? bookingCalendarObject.getAddressLine2() + ", " + bookingCalendarObject.getAddressLine1() : bookingCalendarObject.getAddressLine1());
                response.setReservationTime(dateRequest);
                if (PreferenceUtils.showPreferenceBoolean(KEY_IS_LOGGED_IN, false, this)) {
                    Bundle mBundle = new Bundle();
                    mBundle.putParcelable(BUNDLE_STRING_PARAM, response);
//                    navigateActivityForResultWithExtra(PaymentActivity.class, mBundle, KEY_ONACTIVITY_RESULT_GUEST_BOOKING);
                    navigateActivityForResultWithExtra(PaymentWebActivity.class, mBundle, KEY_ONACTIVITY_RESULT_GUEST_BOOKING);
                    finish();
                } else {
                    navigateActivityForResult(LoginActivity.class, KEY_ONACTIVITY_RESULT_LOGIN);
                }
//            }
        }else {
            onErrorResponse(response.getError());
        }
    }

    //    Get Selected Time range as a Lists
    private ArrayList<SpaceBookingDateRequest> getDateTimeRangeList() {
        dateRequest.clear();
        if (bookingCalendarObject.getAvailabilityMethod().contains(RESPONSE_PRICE_HOUR)) {
            SpaceBookingDateRequest dates = new SpaceBookingDateRequest(selectedDateString + startTime.toString("HH:mm") + "Z",
                    selectedDateString + endTime.toString("HH:mmZ") + "Z");
            dateRequest.add(dates);
        }else {
            for (Availability a: selectedAvailabiliyList){
                startTime = LocalTime.parse(a.getFrom());
                endTime =LocalTime.parse(a.getTo());
                SpaceBookingDateRequest date = new SpaceBookingDateRequest(selectedDateString + startTime.toString("HH:mm") + "Z",
                        selectedDateString + endTime.toString("HH:mmZ") + "Z");
                dateRequest.add(date);
            }
        }
        return dateRequest;
    }

    //    Get Selected Amenities as a Lists
    private ArrayList<SapceBookingExtraAmenityRequest> getAmenityList() {
        ArrayList<SapceBookingExtraAmenityRequest> Amenitylist = new ArrayList<>();
        for (Integer i : selectedAmenityMap.keySet()) {
            Amenitylist.add(new SapceBookingExtraAmenityRequest(i, selectedAmenityMap.get(i)));
        }
        return Amenitylist;
    }

    private void popFragment(Fragment fragment) {
        if (manager == null || !fragmentPoped(fragment)) {
            switchFragment(fragment);
        } else {
            manager.popBackStack(fragment.getClass().getName(), 0);
        }
    }

    private boolean fragmentPoped(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
        for (int i = 0; i < manager.getBackStackEntryCount(); i++) {
            if (fragClassName.equals(manager.getBackStackEntryAt(i).getName())) {
                return true;
            }
        }
        return false;
    }

    public void switchFragment(Fragment frag) {
        backStateName = frag.getClass().getName();
        manager = getSupportFragmentManager();

        manager.beginTransaction()
                .replace(R.id.fragments_container, frag)
                .addToBackStack(backStateName)
                .commit();
    }

    private void clearFragmentContainer() {
        manager = getSupportFragmentManager();
        Fragment frg = manager.findFragmentById(R.id.fragments_container);
        if (frg != null) {
            if (frg instanceof CalenderFragment){
                FragmentManager fragmentChildManager = frg.getChildFragmentManager();
                Fragment childFrag = fragmentChildManager.findFragmentById(R.id.fragments_calender_container);
                if (childFrag != null){
                    fragmentChildManager.beginTransaction().detach(childFrag).attach(childFrag).commit();
                    setSelectedTab(tabCalender,tabExtra);
                }
            }else {
                manager.beginTransaction().detach(frg).attach(frg).commit();
                setSelectedTab(tabExtra,tabCalender);
            }

        }
    }

    private void setCalenderTabDate(String day, String month, int year) {
        currentDateDay.setText((Integer.parseInt(day) < 10 ? "0" : "") + day);
        currentDateYear.setText(String.valueOf(year));
        currentDateMonth.setText(month);
    }


    @Override
    public void onBackPressed() {
        int count = manager.getBackStackEntryCount();
        if (count > 0) {
            Fragment fragment = manager.findFragmentById(R.id.fragments_container);
            FragmentManager fragmentChildManager = fragment.getChildFragmentManager();

            if (fragment instanceof CalenderFragment) {
                int childCount = fragmentChildManager.getBackStackEntryCount();
                if (childCount > 1) {
                    fragmentChildManager.popBackStack();
                    AnimationsUtils.sendDown(actionButtonContainer);
                    for (Map.Entry<Integer, Float> entry : selectePeHourdAmenityMap.entrySet()) {
                        extraTotal -= entry.getValue() * spaceTotalHours;
                    }
                    spaceTotal = 0;
                    spaceTotalHours = 0;
                    totalPrice = spaceTotal + extraTotal;
                    updateActionContainerView();
                    isDateSelected = false;
                    startTime = null;
                    selectedAvailabiliyList.clear();
                } else {
                    finish();
                }
            } else {
                manager.popBackStack();
                setSelectedTab(tabCalender, tabExtra);
                AnimationsUtils.moveUp(actionButtonContainer);
            }
        } else {
            super.onBackPressed();
        }
    }

    private void setSelectedTab(View viewSelected, View deselect) {
        viewSelected.setBackground(ContextCompat.getDrawable(this, R.drawable.background_calender_tab_button));
        deselect.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        setselectedTabTextColor(deselect, ContextCompat.getColor(this, R.color.colorPrimaryTextLight));
        setselectedTabTextColor(viewSelected, ContextCompat.getColor(this, R.color.white));
    }

    private void setselectedTabTextColor(View view, int color) {
        if (view instanceof TextView) {
            ((TextView) view).setTextColor(color);
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setselectedTabTextColor(innerView, color);
            }
        }
    }

    public void setUpUI(View view, final Fragment frag) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(frag);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setUpUI(innerView, frag);
            }
        }
    }

    private void hideSoftKeyboard(Fragment frag) {
        InputMethodManager inputMethodManager = (InputMethodManager) frag.getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(frag.getView().getWindowToken(), 0);
    }

    @Override
    public void onConnectionErrorCallBack(int requestCode) {
        switch (requestCode){
            case ENF_GET_GUEST_CALENDAR_DETAILS:
                getSpaceBookings();
                break;
            case ENF_GET_GUEST_RESERVE_BOOKING:
                reserveBooking(request);
                break;
            case ENF_GET_GUEST_PROMO_CODE:
                getPromoCode();
                break;
            case ENF_PUT_GUEST_MOBILE_NUMBER:
                updateMobileNumber();
                break;
        }
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinatorLayout);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish(); //TODO: Handle with timer
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            switch (requestCode){
                case KEY_ONACTIVITY_RESULT_LOGIN:
                    reserveBooking(request);
                    break;
                case KEY_ONACTIVITY_RESULT_GUEST_BOOKING:
                    getSpaceBookings();
                    break;
            }
        }
    }
}
