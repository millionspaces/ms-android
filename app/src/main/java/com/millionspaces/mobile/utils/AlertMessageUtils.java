package com.millionspaces.mobile.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatRatingBar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.millionspaces.mobile.BuildConfig;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnAddedReview;
import com.millionspaces.mobile.actions.OnCancelBooking;
import com.millionspaces.mobile.actions.OnConnectionErrorCallBack;
import com.millionspaces.mobile.actions.OnDatePicked;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.actions.OnNumberSelected;
import com.millionspaces.mobile.actions.OnPromoSelected;
import com.millionspaces.mobile.actions.OnSuccessGuestMailEntered;
import com.millionspaces.mobile.entities.request.CancelBookingRequest;
import com.millionspaces.mobile.entities.request.SpaceReviewRequest;
import com.millionspaces.mobile.entities.response.Booking;
import com.millionspaces.mobile.views.ThumbTextSeekBar;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.Hours;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.CANCEL_GUEST_BOOKING;
import static com.millionspaces.mobile.utils.Config.IMAGE_PLACEHOLDER_FACTOR;
import static com.millionspaces.mobile.utils.Config.REFUND_FLEXIBLE;
import static com.millionspaces.mobile.utils.Config.REFUND_MODERATE;
import static com.millionspaces.mobile.utils.Config.REFUND_STRICT;
import static com.millionspaces.mobile.utils.Config.TIME_LIST;
import static com.millionspaces.mobile.utils.Config.getStatusColor;
import static com.millionspaces.mobile.utils.Config.isValidMail;

/**
 * Created by kasunka on 8/8/17.
 */

public class AlertMessageUtils {

    public static final AlertMessageUtils instance = new AlertMessageUtils();

    public static AlertMessageUtils getInstance(){
        return instance;
    }

    private AlertMessageUtils() {

    }

    public static Dialog showProgressDialog(Context context){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_progress_bar);
        dialog.getWindow().setBackgroundDrawableResource(R.color.colorProgressBackground);
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }

    public static void showSnackBarMessage(String message, CoordinatorLayout view) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        // Changing message text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    public static void showSnackBarCallBack(String message, String action, CoordinatorLayout view, final OnConnectionErrorCallBack callBack, final int error){
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
                .setAction(action.toUpperCase(), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callBack.onConnectionErrorCallBack(error);
                    }
                });
        // Changing message text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    // These matrices will be used to scale points of the image
    static Matrix matrix = new Matrix();
    static Matrix savedMatrix = new Matrix();

    // The 3 states (events) which the user is trying to perform
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    static int mode = NONE;

    // these PointF objects are used to record the point(s) the user is touching
    static PointF start = new PointF();
    static PointF mid = new PointF();
    static float oldDist = 1f;

    public static void showImageEnlargeDialog(String url, Context context){
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_image_enlarge_view);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);
        ImageView image = ButterKnife.findById(d, R.id.dialog_image);
        Picasso.with(context).load(BuildConfig.IMAGE_SERVER_URL+BuildConfig.IMAGE_SERVER_DOMAIN+url).into(image);

        View.OnTouchListener touchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ImageView view = (ImageView) v;
                view.setScaleType(ImageView.ScaleType.MATRIX);
                float scale;

                dumpEvent(event);
                // Handle touch events here...

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:   // first finger down only
                        savedMatrix.set(matrix);
                        start.set(event.getX(), event.getY());
                        Log.d("ZOOM", "mode=DRAG"); // write to LogCat
                        mode = DRAG;
                        break;
                    case MotionEvent.ACTION_UP: // first finger lifted

                    case MotionEvent.ACTION_POINTER_UP: // second finger lifted
                        mode = NONE;
                        Log.d("ZOOM", "mode=NONE");
                        break;

                    case MotionEvent.ACTION_POINTER_DOWN: // first and second finger down

                        oldDist = spacing(event);
                        Log.d("ZOOM", "oldDist=" + oldDist);
                        if (oldDist > 5f) {
                            savedMatrix.set(matrix);
                            midPoint(mid, event);
                            mode = ZOOM;
                            Log.d("ZOOM", "mode=ZOOM");
                        }
                        break;

                    case MotionEvent.ACTION_MOVE:

                        if (mode == DRAG) {
                            matrix.set(savedMatrix);
                            matrix.postTranslate(event.getX() - start.x, event.getY() - start.y); // create the transformation in the matrix  of points
                        } else if (mode == ZOOM) {
                            // pinch zooming
                            float newDist = spacing(event);
                            Log.d("ZOOM", "newDist=" + newDist);
                            if (newDist > 5f) {
                                matrix.set(savedMatrix);
                                scale = newDist / oldDist; // setting the scaling of the
                                // matrix...if scale > 1 means
                                // zoom in...if scale < 1 means
                                // zoom out
                                matrix.postScale(scale, scale, mid.x, mid.y);
                            }
                        }
                        break;
                }

                view.setImageMatrix(matrix); // display the transformation on screen
                return true;
            }
        };

        image.setOnTouchListener(touchListener);
        d.show();
        d.getWindow().setAttributes(getLayoutParams(d));
    }

    private static float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    private static void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    /**
     * Show an event in the LogCat view, for debugging
     */
    private static void dumpEvent(MotionEvent event) {
        String names[] = {"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"};
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);

        if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }

        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }

        sb.append("]");
        Log.d("Touch Events ---------", sb.toString());
    }

    public void ShowSignUpSuccessDialog(String mail, Context context, final OnItemClickListener click){
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_succes_signup);
        d.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.background_dialog_view));

        TextView msg = ButterKnife.findById(d,R.id.signup_succses_text);
        Button signin = ButterKnife.findById(d,R.id.succes_signup_signin);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.OnClick(0,null);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            msg.setText(Html.fromHtml(context.getString(R.string.signup_signin_msg_text,"<strong>"+ mail+"</strong>"),Html.FROM_HTML_OPTION_USE_CSS_COLORS));
        }else {
            msg.setText(Html.fromHtml(context.getString(R.string.signup_signin_msg_text,"<strong>"+ mail+"</strong>")));
        }
        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);
        d.show();
        d.getWindow().setAttributes(getLayoutParams(d));
    }

    public static Dialog ShowPaymentStatusDialog(Drawable drawable, final String code, String status, Context context, final OnItemClickListener callBack){
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_payment_status);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView statusImage = ButterKnife.findById(d,R.id.payment_status_image);
        TextView statusMsg = ButterKnife.findById(d,R.id.payment_status_msg);
        TextView statusTitle = ButterKnife.findById(d,R.id.payment_status_title);
        Button button01 = ButterKnife.findById(d,R.id.payment_action_button_01);
        Button button02 = ButterKnife.findById(d,R.id.payment_action_button_02);

        //TODO: ADD values to config
        button01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (code){
                    case "1":
                        callBack.OnClick(0,v);
                        break;
                    case "3":
                        callBack.OnClick(1,v);
                        break;
                    default:
                        callBack.OnClick(2,v);
                        break;
                }
            }
        });

        button02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (code){
                    case "1":
                        callBack.OnClick(0,v);
                        break;
                    case "3":
                        callBack.OnClick(1,v);
                        break;
                    default:
                        callBack.OnClick(2,v);
                        break;
                }
            }
        });

        statusImage.setImageDrawable(drawable);
        statusTitle.setText(getSubTitle(code,context));
        statusTitle.setTextColor((code.contains("1") || code.contains("4"))? Color.GREEN:Color.RED);

//        statusMsg.setText(code.contains("1")?status:code.contains("3")?context.getString(R.string.ipg_dialog_payment_canceled_description):context.getString(R.string.payment_dialog_description));
//        statusMsg.setText(!code.contains("3")?status:status+" "+context.getString(R.string.payment_dialog_description));
        statusMsg.setText(status);

//        button01.setText(code.contains("1")?context.getString(R.string.ipg_dialog_payment_action_home):code.contains("3")?context.getString(R.string.ipg_dialog_payment_action_yes):context.getString(R.string.ipg_dialog_payment_action_retry));
//        button02.setText(code.contains("1")?context.getString(R.string.ipg_dialog_payment_action_booking):code.contains("3")?context.getString(R.string.ipg_dialog_payment_action_no):context.getString(R.string.ipg_dialog_payment_action_back));

        button01.setText((code.contains("1") || code.contains("4"))?context.getString(R.string.ipg_dialog_payment_action_home):context.getString(R.string.ipg_dialog_payment_action_retry));
        button02.setText((code.contains("1") || code.contains("4"))?context.getString(R.string.ipg_dialog_payment_action_booking):context.getString(R.string.ipg_dialog_payment_action_back));

        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);
        d.show();
        d.getWindow().setAttributes(getLayoutParams(d));
        return d;
    }

    private static String getSubTitle(String code, Context context) {
        switch (code){
            case "1":
                return context.getString(R.string.ipg_dialog_payment_success);
            case "3":
                return context.getString(R.string.ipg_dialog_payment_canceled);
            case "4":
                return context.getString(R.string.ipg_dialog_payment_tentative);
            default:
                return context.getString(R.string.ipg_dialog_payment_fail);
        }
    }

    public static Dialog showExitWaringMsg(Context context, View.OnClickListener listener){
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_cancel_warning);
        d.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.background_dialog_view));

        Button buttonPositive = ButterKnife.findById(d,R.id.dialog_button_positive);
        Button buttonNegative = ButterKnife.findById(d,R.id.dialog_button_negative);

        buttonNegative.setOnClickListener(listener);
        buttonPositive.setOnClickListener(listener);

        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);
        d.show();
        d.getWindow().setAttributes(getLayoutParams(d));
        return d;
    }

    public static Dialog showReviewScreen(final Context context, final int bookingId, final OnAddedReview callBack){
        final Dialog d = new Dialog(context);
        final String[] review = new String[1];
        final String[] description = new String[1];
        final int[] serviceReview = new int[1];
        final int[] cleanReview = new int[1];
        final int[] valueReview = new int[1];
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_review_screen);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        AppCompatRatingBar serviceRating = ButterKnife.findById(d,R.id.review_dialog_service);
        AppCompatRatingBar cleannessRating = ButterKnife.findById(d,R.id.review_dialog_clean);
        AppCompatRatingBar valueRating = ButterKnife.findById(d,R.id.review_dialog_value);
        final EditText reviewTitleView = ButterKnife.findById(d,R.id.edt_review_title);
        final EditText reviewDesView = ButterKnife.findById(d,R.id.edt_review_description);
        Button submit = ButterKnife.findById(d,R.id.dialog_button_positive);

        RatingBar.OnRatingBarChangeListener ratingListener = new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                switch (ratingBar.getId()){
                    case R.id.review_dialog_service:
                        Log.d("Rating Service",rating+"");
                        serviceReview[0] = (int) Math.round(rating);
                        break;
                    case R.id.review_dialog_clean:
                        Log.d("Rating Clean",rating+"");
                        cleanReview[0] = (int) Math.round(rating);
                        break;
                    case R.id.review_dialog_value:
                        Log.d("Rating Value",rating+"");
                        valueReview[0] = (int) Math.round(rating);
                        break;
                }
            }
        };

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptReview();
            }

            private void attemptReview() {
                review[0] = reviewTitleView.getText().toString();
                description[0] = reviewDesView.getText().toString();
                View focusedView = null;
                boolean isValid = true;
                String error = null;
                if (TextUtils.isEmpty(review[0])){
                    isValid = false;
                    focusedView = reviewTitleView;
                    error = context.getString(R.string.error_empty_review_title);
                }else if (TextUtils.isEmpty(description[0])){
                    isValid = false;
                    focusedView = reviewDesView;
                    error = context.getString(R.string.error_empty_review_description);
                }

                if (isValid){
                    leaveReview(review[0],description[0],serviceReview[0],cleanReview[0],valueReview[0],bookingId,callBack);
                }else {
                    ((TextView)focusedView).setError(error);
                    focusedView.requestFocus();
                }
            }
        });
        serviceRating.setOnRatingBarChangeListener(ratingListener);
        cleannessRating.setOnRatingBarChangeListener(ratingListener);
        valueRating.setOnRatingBarChangeListener(ratingListener);

        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);
        d.show();
        d.getWindow().setAttributes(getLayoutParams(d));
        return d;
    }

    private static void leaveReview(String title, String des, int v1, int v2, int v3, int bookingId, OnAddedReview callBack) {
        SpaceReviewRequest  reviewRequest = new SpaceReviewRequest(
                (int) Math.round((v1+v2+v3)/3)+"|"+v1+"|"+v2+"|"+v3, title, des,
                new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()), bookingId);
        callBack.onAddedReview(reviewRequest);
    }

    public static Dialog showCancelScreen(final Context context, final Booking booking, final View.OnClickListener listener, final OnCancelBooking callBack) {
        final Dialog d = new Dialog(context);
        final float[] cancelRefundAmount = new float[1];

        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_cancel_screen);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView coverImage = ButterKnife.findById(d,R.id.cancel_booking_card_img);
        TextView spaceName = ButterKnife.findById(d,R.id.cancel_booking_card_name);
        TextView bookingStatus = ButterKnife.findById(d,R.id.cancel_booking_status);
        TextView cancelStatus = ButterKnife.findById(d,R.id.cancel_booking_cancel_status);
        TextView cancelDescription = ButterKnife.findById(d,R.id.cancel_booking_cancel_des);
        TextView paidAmount = ButterKnife.findById(d,R.id.cancel_booking_cancel_paid_amount);
        RelativeLayout priceContainer = ButterKnife.findById(d,R.id.cancel_booking_price_container);
        final TextView refundAmount = ButterKnife.findById(d,R.id.cancel_booking_cancel_refund_amount);
        Button button = ButterKnife.findById(d,R.id.cancel_booking_button_positive);
        final ThumbTextSeekBar thumbSeekBar = ButterKnife.findById(d,R.id.cancel_booking_progress);

        RequestBuilder<Drawable> thumbnailRequest = Glide.with(context)
                .load(BuildConfig.IMAGE_SERVER_URL+IMAGE_PLACEHOLDER_FACTOR+BuildConfig.IMAGE_SERVER_DOMAIN+booking.getSpace().getImage());
        Glide.with(context)
                .load(BuildConfig.IMAGE_SERVER_URL+Config.optimizedImage(context)+BuildConfig.IMAGE_SERVER_DOMAIN+booking.getSpace().getImage())
                .thumbnail(thumbnailRequest)
                .into(coverImage);

        spaceName.setText(booking.getSpace().getName());
        bookingStatus.setText(booking.getReservationStatus().getLabel());
        bookingStatus.setBackgroundColor(getStatusColor(booking.getReservationStatus().getName(),context));
        cancelStatus.setText(booking.getSpace().getCancellationPolicy().getName());
//        cancelStatus.setBackgroundColor(getCancellationColors(booking.getSpace().getCancellationPolicy().getId(),context));
        cancelDescription.setText(booking.getSpace().getCancellationPolicy().getDescription());

        cancelRefundAmount[0] = Math.round(Float.parseFloat(booking.getBookingCharge()) * booking.getSpace().getCancellationPolicy().getRefundRate());
        paidAmount.setText("LKR " +new DecimalFormat("####,###,###").format(Float.parseFloat(booking.getBookingCharge())));
        refundAmount.setText("LKR " +new DecimalFormat("####,###,###").format(cancelRefundAmount[0]));

        DateTime bookingMadeDate = DateTime.parse(booking.getBookedDate());
        DateTime bookingStartDate = DateTime.parse(booking.getDates().get(0).getFromDate());
        final DateTime today = new DateTime();

        final int maxHours = Hours.hoursBetween(bookingMadeDate, bookingStartDate).getHours();
        int refundHours = 0;
        int progress = Math.abs(Hours.hoursBetween(today,bookingMadeDate).getHours());

        switch (booking.getSpace().getCancellationPolicy().getId()){
            case REFUND_FLEXIBLE:
                refundHours = maxHours-(24*2);
                break;
            case REFUND_MODERATE:
                refundHours = maxHours-(24*7);
                break;
            case REFUND_STRICT:
                refundHours = 0;
                refundAmount.setTextColor(ContextCompat.getColor(context,R.color.booking_cancel_button_color));
                break;
        }

//        if (booking.getReservationStatus().getId() != BOOKING_STATUS_PENDING_PAYMENT_IDD) {
            final int finalRefundHours = refundHours;
            thumbSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (progress <= finalRefundHours) {
                        switch (booking.getSpace().getCancellationPolicy().getId()) {
                            case REFUND_FLEXIBLE:
                                thumbSeekBar.setThumbText("Today " + today.toString("dd") + "\nFull refund");
                                break;
                            case REFUND_MODERATE:
                                thumbSeekBar.setThumbText("Today " + today.toString("dd") + "\n50% refund");
                                break;
                            case REFUND_STRICT:
                                thumbSeekBar.setThumbText("Today " + today.toString("dd") + "\nNo refund");
                                refundAmount.setText("LKR 0");
                                cancelRefundAmount[0] = 0;
                                refundAmount.setTextColor(ContextCompat.getColor(context, R.color.booking_cancel_button_color));
                                break;
                        }
                    } else {
                        thumbSeekBar.setThumbText("Today " + today.toString("dd") + "\nNo refund");
                        refundAmount.setText("LKR 0");
                        cancelRefundAmount[0] = 0;
                        refundAmount.setTextColor(ContextCompat.getColor(context, R.color.booking_cancel_button_color));
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
//        }else {
//            priceContainer.setVisibility(View.GONE);
//        }

        thumbSeekBar.setBackgroundProgress(maxHours,refundHours,progress, bookingMadeDate.toString("dd\nMMM"),bookingStartDate.toString("dd\nMMM"));

        coverImage.setOnClickListener(listener);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelBooking(booking, cancelRefundAmount[0],callBack);
            }
        });

        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);
        d.show();
        d.getWindow().setAttributes(getLayoutParams(d));
        return d;
    }

    private static void cancelBooking(Booking booking, float cancelRefundAmount, OnCancelBooking callBack) {
        CancelBookingRequest cancelBookingrequest = new CancelBookingRequest(booking.getId(),CANCEL_GUEST_BOOKING,cancelRefundAmount);
        callBack.onCancelBooking(cancelBookingrequest);
    }

    public static Dialog showConfirmDialog(Context context, String title, String des, String bNeg, String bPos, int visibility, View.OnClickListener listener){
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_payment_confirm);
        d.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.background_dialog_view));

        Button buttonPositive = ButterKnife.findById(d,R.id.dialog_button_positive);
        Button buttonNegative = ButterKnife.findById(d,R.id.dialog_button_negative);

        TextView titleView = ButterKnife.findById(d,R.id.alert_dialog_title);
        titleView.setText(title);
        TextView descriptionView = ButterKnife.findById(d,R.id.alert_dialog_description);
        descriptionView.setText(des);
        buttonPositive.setText(bPos);
        buttonNegative.setText(bNeg);

        buttonPositive.setOnClickListener(listener);
        buttonNegative.setOnClickListener(listener);
        buttonNegative.setVisibility(visibility);

        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);
        d.show();
        return d;
    }

    public static Dialog showNumberPickerDialog(Context context, final OnNumberSelected callback){
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_payment_mobile_picker);
        d.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.background_dialog_view));

        Button buttonPositive = d.findViewById(R.id.dialog_button_positive);

        final EditText editText = d.findViewById(R.id.dialog_dialog_number_picker);

        buttonPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contactNumber = editText.getText().toString();
                if (Config.isValidMobile(contactNumber)){
                    callback.onNumberAdded(contactNumber);
                    d.dismiss();
                }else {
                    editText.setError("Invalid Mobile Number");
                }

            }
        });

        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);
        d.show();
        return d;
    }

    public static Dialog showAlertDialog(Context context, View.OnClickListener listener){
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_view_alert);
        d.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.background_dialog_view));

        TextView action = ButterKnife.findById(d,R.id.dialog_action);
        action.setOnClickListener(listener);

        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);
        d.show();
        return d;
    }

    public static Dialog showEditSpaceDialog(Context context, String description ,View.OnClickListener listener){
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_edit_space);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView descriptionView = ButterKnife.findById(d,R.id.payment_status_title);
        Button button = ButterKnife.findById(d,R.id.edit_action_button);
        button.setOnClickListener(listener);

        descriptionView.setText(description);

        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);
        d.show();
        d.getWindow().setAttributes(getLayoutParams(d));
        return d;
    }

    public static Dialog showDateTimePickerDialog(Context context, String title, final int year, final int monthOfYear, final int dayOfMonth, boolean isBlocked, final OnDatePicked callBack){

        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_datetime_picker);

        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH, monthOfYear-1);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        final int selectedColor = ContextCompat.getColor(context,R.color.status_expired);
        final int normalColor = ContextCompat.getColor(context,R.color.colorPrimaryTextWhite);

        final DatePicker datePicker = ButterKnife.findById(d,R.id.datePicker_view);
        final NumberPicker timePicker = ButterKnife.findById(d,R.id.timepicker_view);
        final TextView datePickerTitle = ButterKnife.findById(d,R.id.picker_title);
        final TextView datePickerYear = ButterKnife.findById(d,R.id.picker_year);
        final TextView datePickerMonthDate = ButterKnife.findById(d,R.id.picker_month_and_date);
        final TextView datePickerTime = ButterKnife.findById(d,R.id.picker_time);
        final LinearLayout datePickerTimeContainer = ButterKnife.findById(d,R.id.timepicker_view_container);
        final TextView positiveButton = ButterKnife.findById(d,R.id.timepicker_positive_action);

        final int[] selectedYear = new int[1];
        final int[] selectedMonth = new int[1];
        final int[] selectedDate = new int[1];
        final float[] selectedTime = new float[]{Float.parseFloat(TIME_LIST[0].split(" ")[0])};

        if (isBlocked) {
            datePicker.setMinDate(calendar.getTimeInMillis());
        }

        timePicker.setMinValue(0);
        timePicker.setMaxValue(TIME_LIST.length-1);
        timePicker.setDisplayedValues(TIME_LIST);

        DatePicker.OnDateChangedListener dateChangedListener = new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.MONTH, monthOfYear);
                datePickerYear.setText(String.valueOf(year));
                datePickerMonthDate.setText(dayOfMonth+" "+new SimpleDateFormat("MMM").format(calendar.getTime()));
                datePickerYear.setTextColor(normalColor);
                datePickerMonthDate.setTextColor(normalColor);
                datePickerTime.setTextColor(selectedColor);
                datePicker.setVisibility(View.GONE);
                datePickerTimeContainer.setVisibility(View.VISIBLE);
                selectedYear[0] = year;
                selectedMonth[0] = monthOfYear+1;
                selectedDate[0] = dayOfMonth;
            }
        };

        NumberPicker.OnValueChangeListener timeChangeListener = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                datePickerTime.setText(TIME_LIST[newVal]);
                selectedTime[0] = newVal <= 12 ? Float.parseFloat(TIME_LIST[newVal].split(" ")[0]) : Float.parseFloat(TIME_LIST[newVal].split(" ")[0])+12;
            }
        };

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.picker_year:
                        datePickerYear.setTextColor(selectedColor);
                        datePickerMonthDate.setTextColor(selectedColor);
                        datePickerTime.setTextColor(normalColor);
//                        datePickerYear.startAnimation(scale);
//                        datePickerMonthDate.startAnimation(scale);
                        datePicker.setVisibility(View.VISIBLE);
                        datePickerTimeContainer.setVisibility(View.GONE);
                        break;
                    case R.id.picker_month_and_date:
                        datePickerYear.setTextColor(selectedColor);
                        datePickerMonthDate.setTextColor(selectedColor);
                        datePickerTime.setTextColor(normalColor);
//                        datePickerYear.startAnimation(scale);
//                        datePickerMonthDate.startAnimation(scale);
                        datePicker.setVisibility(View.VISIBLE);
                        datePickerTimeContainer.setVisibility(View.GONE);
                        break;
                    case R.id.picker_time:
                        datePickerYear.setTextColor(normalColor);
                        datePickerMonthDate.setTextColor(normalColor);
                        datePickerTime.setTextColor(selectedColor);
                        datePicker.setVisibility(View.GONE);
                        datePickerTimeContainer.setVisibility(View.VISIBLE);
                        break;
                    case R.id.timepicker_positive_action:
                        callBack.onDatePicked(new DateTime((selectedYear[0]!=0?selectedYear[0]:year) + "-" + (selectedMonth[0]!=0?selectedMonth[0]:monthOfYear) + "-" + (selectedDate[0]!=0?selectedDate[0]:dayOfMonth) + "T"+selectedTime[0]));
                        d.dismiss();
                        break;
                }
            }
        };

        datePickerTitle.setText(title);
        datePickerYear.setText(String.valueOf(year));
        datePickerYear.setTextColor(selectedColor);
        datePickerMonthDate.setText(dayOfMonth+" "+new SimpleDateFormat("MMM").format(calendar.getTime()));
        datePickerMonthDate.setTextColor(selectedColor);
//        datePickerYear.startAnimation(scale);
//        datePickerMonthDate.startAnimation(scale);


        datePickerYear.setOnClickListener(listener);
        datePickerMonthDate.setOnClickListener(listener);
        datePickerTime.setOnClickListener(listener);
        timePicker.setOnValueChangedListener(timeChangeListener);
        positiveButton.setOnClickListener(listener);

        datePicker.init(year,monthOfYear-1,dayOfMonth,dateChangedListener);

        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);
        d.show();
        d.getWindow().setAttributes(getLayoutParams(d));
        return d;
    }

    public static Dialog showPromoInputDialog(Context context, final OnPromoSelected promoCallBack){
        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_payment_promocode);
        d.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.background_dialog_view));

        Button buttonPositive = ButterKnife.findById(d,R.id.dialog_promo_button_positive);
        Button buttonNegative = ButterKnife.findById(d,R.id.dialog_promo_button_negative);
        final EditText promoView = ButterKnife.findById(d,R.id.alert_dialog_promo);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.dialog_promo_button_positive:
                        promoCallBack.onPromoSelected(promoView.getText().toString());
                        d.dismiss();
                        break;
                    case R.id.dialog_promo_button_negative:
                        d.dismiss();
                        break;
                }
            }
        };

        buttonPositive.setOnClickListener(onClickListener);
        buttonNegative.setOnClickListener(onClickListener);

        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);
        d.show();
        return d;
    }


    public static Dialog showTextPicker(final Context mContext, String title, String action, final OnSuccessGuestMailEntered callBack){
        final Dialog d = new Dialog(mContext);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_text_input);
        d.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(mContext,R.drawable.background_dialog_view));

        final String[] email = {null};
        TextView titleView = d.findViewById(R.id.alert_dialog_title);
        final EditText textInput = d.findViewById(R.id.alert_dialog_promo);
        Button actionButton = d.findViewById(R.id.dialog_button_positive);
        LinearLayout actionSignUp = d.findViewById(R.id.dialog_button_signup);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.dialog_button_positive:
                        email[0] = textInput.getText().toString();
                        validateString();
                        break;
                    case R.id.dialog_button_signup:
                        d.dismiss();
                        break;
                }
            }

            private void validateString() {
                if (!isValidMail(email[0])){
                    textInput.setError(mContext.getString(R.string.error_wrong_email));
                }else {
                    d.dismiss();
                    callBack.onSuccessMail(email[0]);
                }
            }
        };

        actionSignUp.setOnClickListener(onClickListener);
        actionButton.setOnClickListener(onClickListener);

        titleView.setText(title);
        actionButton.setText(action);

        d.setCancelable(true);
        d.setCanceledOnTouchOutside(true);
        d.show();
        return d;
    }

    private static WindowManager.LayoutParams getLayoutParams(Dialog d) {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(d.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        return lp;
    }

}
