package com.millionspaces.mobile.entities.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 11/16/17.
 */

public class SpaceReviewRequest implements Parcelable{
    private String rate; //" : "4|3|4|5",
    private String title;
    private String description;
    private String createdAt;
    private int bookingId;

    public SpaceReviewRequest(String rate, String title, String description, String createdAt, int bookingId) {
        this.rate = rate;
        this.title = title;
        this.description = description;
        this.createdAt = createdAt;
        this.bookingId = bookingId;
    }

    protected SpaceReviewRequest(Parcel in) {
        rate = in.readString();
        title = in.readString();
        description = in.readString();
        createdAt = in.readString();
        bookingId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(rate);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(createdAt);
        dest.writeInt(bookingId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SpaceReviewRequest> CREATOR = new Creator<SpaceReviewRequest>() {
        @Override
        public SpaceReviewRequest createFromParcel(Parcel in) {
            return new SpaceReviewRequest(in);
        }

        @Override
        public SpaceReviewRequest[] newArray(int size) {
            return new SpaceReviewRequest[size];
        }
    };

    public void setRate(String rate) {
        this.rate = rate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public String getRate() {
        return rate;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public int getBookingId() {
        return bookingId;
    }
}
