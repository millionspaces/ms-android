package com.millionspaces.mobile.network.interfaces;

import com.millionspaces.mobile.entities.response.BookingCalendarResponse;

/**
 * Created by kasunka on 12/14/17.
 */

public interface OnSuccessCalendarBooking extends OnErrorResponse{
    void onSuccessCalendarBooking(BookingCalendarResponse value);
}
