package com.millionspaces.mobile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.adapters.ImageAdapter;
import com.millionspaces.mobile.adapters.SimilarSpaceAdapter;
import com.millionspaces.mobile.adapters.SpaceHorizontalItemAdapter;
import com.millionspaces.mobile.entities.Menu;
import com.millionspaces.mobile.entities.SpaceType;
import com.millionspaces.mobile.entities.response.Space;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessSpace;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.AnimationsUtils;
import com.millionspaces.mobile.utils.DataBaseUtils;
import com.millionspaces.mobile.utils.PreferenceUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.BLOCK_CHARGE_GUEST_BASE;
import static com.millionspaces.mobile.utils.Config.BLOCK_CHARGE_SPACE_ONLY;
import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_PARCELABLE_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.KEY_CURRENT_CHECKING_SPACE;
import static com.millionspaces.mobile.utils.Config.KEY_IS_LOGGED_IN;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_ID;
import static com.millionspaces.mobile.utils.Config.RESPONSE_PRICE_BLOCK;
import static com.millionspaces.mobile.utils.Config.SPACE_AMINITY_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_AMINITY_TYPE_LIST_CHARGBLE;
import static com.millionspaces.mobile.utils.Config.SPACE_EVENT_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_MENU_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_PRICE_TYPE_LIST_BLOCKBASED;
import static com.millionspaces.mobile.utils.Config.SPACE_PRICE_TYPE_LIST_BLOCKGUESTBASED;
import static com.millionspaces.mobile.utils.Config.SPACE_PRICE_TYPE_LIST_HOURLYBASED;
import static com.millionspaces.mobile.utils.Config.SPACE_RULE_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_SEATING_TYPE_LIST;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_GET_SPACE_DETAILS;

public class SpaceProfileActivity extends BaseHomeActivity implements View.OnClickListener, ViewPager.OnPageChangeListener, OnSuccessSpace, OnMapReadyCallback, View.OnTouchListener {

    @BindView(R.id.space_view_pager_gallery)
    ViewPager viewPager;
    @BindView(R.id.gallery_btn_next)
    ImageButton btnNext;
    @BindView(R.id.gallery_btn_back)
    ImageButton btnBack;
    //    @BindView(R.id.space_switch_vr_btn) Button btnSwitch;    // 360 widget
    //    @BindView(R.id.space_pano_view) VrPanoramaView vrPanoramaView;
    @BindView(R.id.viewPagerCountDots)
    LinearLayout pagerIndicator;
    @BindView(R.id.viewPagerIndicator)
    RelativeLayout viewPagerContainer;
    @BindView(R.id.space_profile_parent)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.space_detail_container)
    LinearLayout spaceDetailContainer;

    @BindView(R.id.spacepro_space_title)
    TextView spaceTitle;
    @BindView(R.id.spacepro_space_venue)
    TextView spaceVenue;
    @BindView(R.id.spacepro_space_description)
    TextView spaceDescription;
    @BindView(R.id.eventtype_name_sqft)
    TextView spaceSize;
    @BindView(R.id.eventtype_name_guscnt)
    TextView guestCount;
    @BindView(R.id.spacepro_space_everntypes)
    RecyclerView spaceEventRecyclerViwe;
    @BindView(R.id.spacepro_space_price_container)
    LinearLayout priceContainer;
    @BindView(R.id.spacepro_space_price_sub_title)
    TextView priceSubDescription;
    @BindView(R.id.spacepro_space_price_description)
    TextView priceSubEventBasedDescription;
    @BindView(R.id.spacepro_space_prices)
    RecyclerView spacePriceRecyclerView;
    @BindView(R.id.spacepro_space_type_container)
    RelativeLayout spaceTypeContainer;
    @BindView(R.id.spacetype_icon)
    ImageView spaceTypeIco;
    @BindView(R.id.spacetype_name)
    TextView spaceTypeNameView;

    @BindView(R.id.spacepro_space_aminity_container)
    LinearLayout amenityContainer;
    @BindView(R.id.spacepro_space_comple_aminity_container)
    LinearLayout compleAmenityContainer;
    @BindView(R.id.spacepro_space_chargble_aminity_container)
    LinearLayout chargableAmenityContainer;
    @BindView(R.id.spacepro_space_complementary)
    RecyclerView spaceComplementaryRecyclerView;
    @BindView(R.id.spacepro_space_chargable)
    RecyclerView spaceChargebleRecyclerView;
    @BindView(R.id.spacepro_space_menu_container)
    LinearLayout menuContainer;
    @BindView(R.id.spacepro_space_menus)
    RecyclerView spaceMenuRecylerView;
    @BindView(R.id.spacepro_space_seatingstyle)
    RecyclerView spaceSeatingRecyclerView;
    @BindView(R.id.spacepro_space_seating_container)
    LinearLayout seatingContainer;
    @BindView(R.id.spacepro_space_rule_container)
    LinearLayout ruleContainer;
    @BindView(R.id.spacepro_space_rules)
    RecyclerView spaceRuleRecyclerView;
    @BindView(R.id.spacepro_space_similarspaces)
    RecyclerView spaceSimilaSpacesRecyclerView;
    @BindView(R.id.spacepro_space_similar_container)
    LinearLayout similarSpaceContainer;
    @BindView(R.id.spacepro_overview_container)
    LinearLayout overViewContainer;
    @BindView(R.id.spacepro_space_cancel_policy)
    TextView cancellationPolicy;
    @BindView(R.id.space_scroll)
    ScrollView parent;
    @BindView(R.id.spacepro_space_book_btn)
    Button bookSpace;
    @BindView(R.id.sapcepro_action_button_container)
    RelativeLayout actionButtonContainer;


    private int dotsCount;
    private ImageView[] dots;
    private ArrayList<String> images;
    private ImageAdapter adapter;

    private GoogleMap map;
    private Space mSpace;
    private MapFragment mapFragment;

    private boolean isVrVisible = false;
    private boolean isScrolling = false;
    private DataBaseUtils dataBaseUtils;
    private Bundle mBundle = null;
    private Rect scrollBounds;

    private final String SPACE_ONCREATE_CALLBACK_KEY = "SPACEKEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space_profile);
        MapsInitializer.initialize(getApplicationContext());
        supportPostponeEnterTransition();
        ButterKnife.bind(this);
        init_views();
    }

    private void init_views() {
        mBundle = getIntent().getExtras();
        getSpace();
        init_toolbar();
        init_drawer(false);
        init_navigationview();
        dataBaseUtils = new DataBaseUtils(this);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        parent.setOnTouchListener(this);

        scrollBounds = new Rect();
        parent.getHitRect(scrollBounds);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SPACE_ONCREATE_CALLBACK_KEY, mSpace);
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideProgress();
    }

    @Override
    public Activity currentActivity() {
        return this;
    }

    private void getSpace() {
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
//            MillionSpaceService.getInstance(this).getSpace(mBundle.getInt(BUNDLE_INTEGER_PARAM),
//                    mBundle.getString(BUNDLE_STRING_PARAM).replace("[", "").replace("]", ""), this); // TODO Changed this to New API
            MillionSpaceService.getInstance(this).getSpace(mBundle.getInt(BUNDLE_INTEGER_PARAM), this);
        } else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorLayout, this, ENF_GET_SPACE_DETAILS);
        }
    }

    /**
     * Instantiate view pager UI widgets
     * Created 23/06/2017 By Kasunka
     **/
    private void setUpViewPager() {
        viewPager.setAdapter(adapter);

        btnNext.setOnClickListener(this);
        btnBack.setOnClickListener(this);
//        btnSwitch.setOnClickListener(this);  // VR Switch button
        viewPager.addOnPageChangeListener(this);
        bookSpace.setOnClickListener(this);
        setUiPageViewController();

    }

    /**
     * Handle ViewPager Actions
     * Created 23/06/2017 By Kasunka
     **/
    private void setUiPageViewController() {
        dotsCount = adapter.getCount();
        dots = new ImageView[dotsCount];

        if (dotsCount == 1) {
            btnNext.setVisibility(View.GONE);
        } else {

            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.gallery_indicator_nonselected));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.setMargins(8, 0, 8, 0);

                pagerIndicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.gallery_indicator_selected));
        }
    }

    /**
     * Handle Click events
     * Created 23/06/2017 By Kasunka
     **/
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gallery_btn_next:
                viewPager.setCurrentItem((viewPager.getCurrentItem() < dotsCount)
                        ? viewPager.getCurrentItem() + 1 : 0);
                break;
            case R.id.gallery_btn_back:
                viewPager.setCurrentItem((viewPager.getCurrentItem() != 0)
                        ? viewPager.getCurrentItem() - 1 : 0);
                break;
            // TODO : Enable 360 Widgets
//            case R.id.space_switch_vr_btn:
//                if (isVrVisible) {
//                    viewPagerContainer.setVisibility(View.VISIBLE);
//                    vrPanoramaView.setVisibility(View.INVISIBLE);
//                }else {
//                    viewPagerContainer.setVisibility(View.INVISIBLE);
//                    vrPanoramaView.setVisibility(View.VISIBLE);
//                    loadVRImage();
//                }
//                isVrVisible = !isVrVisible;
//                break;
            case R.id.spacepro_space_book_btn:
                if (PreferenceUtils.showPreferenceBoolean(KEY_IS_LOGGED_IN, false, this) &&
                        PreferenceUtils.showPreferenceString(KEY_LOGGED_USER_ID, this).contains(String.valueOf(mSpace.getSpaceOwnerDto().getId()))) {
                    AlertMessageUtils.showSnackBarMessage(getString(R.string.error_reserve_own_space), mCoordinatorLayout);
                    break;
                }
                navigateActivityWithExtraObj(BookingPreActivity.class, mSpace);
                break;

        }
    }

    //TODO: enable below code for 360 gallery
//    private void loadVRImage() {
//        final VrPanoramaView.Options panoOptions =  new VrPanoramaView.Options();
//        panoOptions.inputType = VrPanoramaView.Options.TYPE_MONO;

//        Picasso.with(this)
//                .load("https://answers.unrealengine.com/storage/temp/81934-2d-vr.jpg")
//                .load("http://www.airpano.com/files/video_monblanc_02.jpg")
//                .load("http://www.360virtualtour.info/wp-content/uploads/2014/02/paris-virtual-tour-by-christian-kleiman-www.christiankleiman.com-12.png")
//                .into(new Target() {
//                    @Override
//                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                        vrPanoramaView.loadImageFromBitmap(bitmap, panoOptions);
//                    }
//
//                    @Override
//                    public void onBitmapFailed(Drawable errorDrawable) {
//
//                    }
//
//                    @Override
//                    public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                    }
//                });
//    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    /**
     * Handle View Pager Swipe events
     * Created 23/06/2017 By Kasunka
     **/
    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.gallery_indicator_nonselected));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.gallery_indicator_selected));

        if (position + 1 == dotsCount) {
            btnNext.setVisibility(View.GONE);
            btnBack.setVisibility(View.VISIBLE);
        } else if (position == 0) {
            btnNext.setVisibility(View.VISIBLE);
            btnBack.setVisibility(View.GONE);
        } else {
            btnBack.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void showGalleryFullView() {
        Intent intent = new Intent(this, GalleryFullViewActivity.class);
        intent.putStringArrayListExtra("IMAGES", images);
        intent.putExtra("POSITION", viewPager.getCurrentItem());
        startActivity(intent);
    }

    @Override
    public void onConnectionErrorCallBack(int requestCode) {
        switch (requestCode) {
            case ENF_GET_SPACE_DETAILS:
                getSpace();
                break;
        }
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinatorLayout);
    }


    @Override
    public void onSuccessSpace(final Space space) {
        this.mSpace = space;

        spaceDetailContainer.setVisibility(View.VISIBLE);
        spaceDetailContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up));

        hideProgress();
        images = space.getImages();
        adapter = new ImageAdapter(this, images,this);
        setUpViewPager();

        overViewContainer.setVisibility(View.VISIBLE);
        spaceTitle.setText(space.getName());
        spaceVenue.setText(space.getAddressLine2() != null ? space.getAddressLine2() + ", " + space.getAddressLine1() : "" + space.getAddressLine1());
        spaceDescription.setText(space.getDescription());
        spaceSize.setText(space.getSize() + " " + space.getMeasurementUnit().getName());
        guestCount.setText(getResources().getQuantityString(R.plurals.space_profile_guest_count, space.getParticipantCount(), space.getParticipantCount()));

        spaceEventRecyclerViwe.setHasFixedSize(true);
        spaceEventRecyclerViwe.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        spaceEventRecyclerViwe.setAdapter(new SpaceHorizontalItemAdapter(SPACE_EVENT_TYPE_LIST, null, space.getEventType(),null , null, null, this));

        if (!space.getSpaceType().isEmpty()){
            spaceTypeContainer.setVisibility(View.VISIBLE);
            SpaceType spaceType = space.getSpaceType().get(0);
            Glide.with(getApplicationContext()).load(spaceType.getMobileIcon()).into(spaceTypeIco);
            spaceTypeNameView.setText(spaceType.getName());
        }else {
            spaceTypeContainer.setVisibility(View.GONE);
        }

        if (!space.getAvailability().isEmpty()) {
            priceContainer.setVisibility(View.VISIBLE);
            priceSubDescription.setText(space.getAvailabilityMethod().contains(RESPONSE_PRICE_BLOCK) ?
                    getString(R.string.space_profile_space_price_block) : getString(R.string.space_profile_space_price_hourly));
            if (space.getAvailabilityMethod().contains(RESPONSE_PRICE_BLOCK)) {
                priceSubEventBasedDescription.setVisibility(View.VISIBLE);
                priceSubEventBasedDescription.setText(space.getBlockChargeType() == BLOCK_CHARGE_SPACE_ONLY ?
                        getString(R.string.space_profile_space_price_block_space_only) : getString(R.string.space_profile_space_price_block_guest_based, space.getMinParticipantCount()));
            }
            if (space.getAvailability().get(0).getIsReimbursable() == 1) {
                priceSubEventBasedDescription.setText(getString(R.string.space_profile_space_price_block_reimbursable));
            }
            spacePriceRecyclerView.setHasFixedSize(true);
            spacePriceRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            spacePriceRecyclerView.setAdapter(new SpaceHorizontalItemAdapter(space.getAvailabilityMethod().contains(RESPONSE_PRICE_BLOCK) ?
                    space.getBlockChargeType() == BLOCK_CHARGE_GUEST_BASE ?SPACE_PRICE_TYPE_LIST_BLOCKGUESTBASED:SPACE_PRICE_TYPE_LIST_BLOCKBASED
                    : SPACE_PRICE_TYPE_LIST_HOURLYBASED, null, null, space.getAvailability(),null, null, this));
        } else {
            priceContainer.setVisibility(View.GONE);
        }

        if (!space.getAmenity().isEmpty()) {
            amenityContainer.setVisibility(View.VISIBLE);
            compleAmenityContainer.setVisibility(View.VISIBLE);
            spaceComplementaryRecyclerView.setHasFixedSize(true);
            spaceComplementaryRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            spaceComplementaryRecyclerView.setAdapter(new SpaceHorizontalItemAdapter(SPACE_AMINITY_TYPE_LIST, null, space.getAmenity(), null, null, null, this));
        } else {
            compleAmenityContainer.setVisibility(View.GONE);
        }

        if (!space.getExtraAmenity().isEmpty()) {
            amenityContainer.setVisibility(View.VISIBLE);
            chargableAmenityContainer.setVisibility(View.VISIBLE);
            spaceChargebleRecyclerView.setHasFixedSize(true);
            spaceChargebleRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            spaceChargebleRecyclerView.setAdapter(new SpaceHorizontalItemAdapter(SPACE_AMINITY_TYPE_LIST_CHARGBLE, null, null, null, null, space.getExtraAmenity(), this));
        } else {
            chargableAmenityContainer.setVisibility(View.GONE);
        }

        if (space.getMenuFiles() != null && !space.getMenuFiles().isEmpty()) {
            menuContainer.setVisibility(View.VISIBLE);
            final ArrayList<String> images = new ArrayList<>();
            for (Menu menu : space.getMenuFiles()) {
                images.add(menu.getMenuId() + ":" + menu.getUrl());
            }
            spaceMenuRecylerView.setHasFixedSize(true);
            spaceMenuRecylerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            SpaceHorizontalItemAdapter menuAdapter = new SpaceHorizontalItemAdapter(SPACE_MENU_TYPE_LIST, images, null, null ,null, null, this);
            spaceMenuRecylerView.setAdapter(menuAdapter);

            menuAdapter.setOnItemClickListner(new OnItemClickListener() {
                @Override
                public void OnClick(int position, View view) {
                    AlertMessageUtils.showImageEnlargeDialog(images.get(position).split(":")[1], SpaceProfileActivity.this);
                }
            });
        }

        if (!space.getSeatingArrangements().isEmpty()) {
            seatingContainer.setVisibility(View.VISIBLE);
            spaceSeatingRecyclerView.setHasFixedSize(true);
            spaceSeatingRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            spaceSeatingRecyclerView.setAdapter(new SpaceHorizontalItemAdapter(SPACE_SEATING_TYPE_LIST, null, null, null,space.getSeatingArrangements(), null, this));
        } else {
            seatingContainer.setVisibility(View.GONE);
        }

        if (!space.getRules().isEmpty()) {
            ruleContainer.setVisibility(View.VISIBLE);
            spaceRuleRecyclerView.setHasFixedSize(true);
            spaceRuleRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            spaceRuleRecyclerView.setAdapter(new SpaceHorizontalItemAdapter(SPACE_RULE_TYPE_LIST, null, space.getRules(), null, null, null, this));

        } else {
            ruleContainer.setVisibility(View.GONE);
        }

        cancellationPolicy.setText(getString(R.string.space_profile_cancellation_policy) + " "
                + dataBaseUtils.getCancellationPolicy(space.getCancellationPolicy()).getName() + ", "
                + dataBaseUtils.getCancellationPolicy(space.getCancellationPolicy()).getDescription());

        MarkerOptions marker = new MarkerOptions()
                .position(new LatLng(space.getLatitude(), space.getLongitude()))
                .title(space.getName())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_loaction_place));
        CameraUpdate center = CameraUpdateFactory.newLatLng(marker.getPosition());
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);

        if (map != null) {
            map.addMarker(marker);
            map.moveCamera(center);
            map.animateCamera(zoom);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.getUiSettings().setZoomGesturesEnabled(true);
            map.getUiSettings().setMyLocationButtonEnabled(true);
        }

        if (!space.getSimilarSpaces().isEmpty() && !mBundle.getBoolean(BUNDLE_BOOLEAN_PARAM)) {
            similarSpaceContainer.setVisibility(View.VISIBLE);
            spaceSimilaSpacesRecyclerView.setHasFixedSize(true);
            spaceSimilaSpacesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            SimilarSpaceAdapter similarSpaceAdapter = new SimilarSpaceAdapter(space.getSimilarSpaces(), this);
            spaceSimilaSpacesRecyclerView.setAdapter(similarSpaceAdapter);
            similarSpaceAdapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void OnClick(int position, View view) {
                    Intent intent = new Intent(SpaceProfileActivity.this,SpaceProfileActivity.class);
                    intent.putExtra(BUNDLE_INTEGER_PARAM, mSpace.getSimilarSpaces().get(position).getId());
                    startActivity(intent);
                    finish();
                }
            });
        } else {
            similarSpaceContainer.setVisibility(View.GONE);
        }

        if (mBundle.getBoolean(BUNDLE_BOOLEAN_PARAM)){
            bookSpace.setOnClickListener(null);
            bookSpace.setVisibility(View.GONE);
        }

        PreferenceUtils.savePreferenceString(KEY_CURRENT_CHECKING_SPACE,String.valueOf(mBundle.getInt(BUNDLE_INTEGER_PARAM)),this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
    }

    @Override
    public void onLowMemory() {
        mapFragment.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_SCROLL:
            case MotionEvent.ACTION_MOVE:
                if (!isScrolling) {
                    isScrolling = !isScrolling;
                    AnimationsUtils.sendDown(bookSpace);
                }
                break;
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (isScrolling) {
                    isScrolling = !isScrolling;
                    AnimationsUtils.moveUp(bookSpace);
                }
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        PreferenceUtils.clearPreferenceString(KEY_CURRENT_CHECKING_SPACE,this);
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_share){ //TODO: Add proper link to share
            String message = "Check out "+mSpace.getName()+
                    (mSpace.getAddressLine2()!= null?" at "+mSpace.getAddressLine2()+"!":"! ")+ "\n"+
                    "https://www.millionspaces.com/#/spaces/"+mSpace.getId()+"/"+mSpace.getName().replaceAll(  "\\s+?","-");
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, message);
            startActivity(Intent.createChooser(share, getString(R.string.space_profile_space_menu_share_title_share)));
        }else {
            super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu, menu);
        return true;
    }
}
