package com.millionspaces.mobile.network.interfaces;


import com.millionspaces.mobile.entities.ComposeMataData;

/**
 * Created by kasunka on 7/31/17.
 */

public interface OnSuccessMetaData extends OnErrorResponse {
    void onSuccessMeta(ComposeMataData value, String key);
}
