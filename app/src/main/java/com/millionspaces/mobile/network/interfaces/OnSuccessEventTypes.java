package com.millionspaces.mobile.network.interfaces;

import com.millionspaces.mobile.entities.response.EventType;

import java.util.ArrayList;


/**
 * Created by kasunka on 7/19/17.
 */

public interface OnSuccessEventTypes extends OnErrorResponse{
    void onSuccessEventTypes(ArrayList<EventType> eventTypes);
}
