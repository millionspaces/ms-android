package com.millionspaces.mobile.network;

import com.millionspaces.mobile.BuildConfig;
import com.millionspaces.mobile.entities.AmenityUnits;
import com.millionspaces.mobile.entities.request.AdvanceSearchRequest;
import com.millionspaces.mobile.entities.request.BatchSyncRequest;
import com.millionspaces.mobile.entities.request.BookingDetailsRequest;
import com.millionspaces.mobile.entities.request.BookingRemarksRequest;
import com.millionspaces.mobile.entities.request.CancelBookingRequest;
import com.millionspaces.mobile.entities.request.EmailValidateRequest;
import com.millionspaces.mobile.entities.request.ManualBookingRequest;
import com.millionspaces.mobile.entities.request.PromoCodeRequest;
import com.millionspaces.mobile.entities.request.SpaceBookingRequest;
import com.millionspaces.mobile.entities.request.SpaceReviewRequest;
import com.millionspaces.mobile.entities.request.UserSignUpRequest;
import com.millionspaces.mobile.entities.response.Amenity;
import com.millionspaces.mobile.entities.response.BatchSyncResponse;
import com.millionspaces.mobile.entities.response.Booking;
import com.millionspaces.mobile.entities.response.BookingCalendarResponse;
import com.millionspaces.mobile.entities.response.BookingRemarksResponse;
import com.millionspaces.mobile.entities.response.BookingReserveResponse;
import com.millionspaces.mobile.entities.response.CancelBookingResponse;
import com.millionspaces.mobile.entities.response.CancellationPolicy;
import com.millionspaces.mobile.entities.response.EventType;
import com.millionspaces.mobile.entities.response.FilterdSpace;
import com.millionspaces.mobile.entities.response.PromoCodeResponse;
import com.millionspaces.mobile.entities.response.Rule;
import com.millionspaces.mobile.entities.response.SeatingArrangement;
import com.millionspaces.mobile.entities.response.ServerResponse;
import com.millionspaces.mobile.entities.response.Space;
import com.millionspaces.mobile.entities.response.UserLoginResponse;

import java.util.ArrayList;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by kasunka on 7/19/17.
 */

public interface MillionSpaceAPI {

    String VERSION = BuildConfig.VERSION;

    @GET("common/amenities")
    Single<ArrayList<Amenity>> getAminities();

    @GET("common/eventTypes")
    Single<ArrayList<EventType>> getEventType();

    @GET("common/cancellationPolicies")
    Single<ArrayList<CancellationPolicy>> getCancellationPolicies();

    @GET("common/rules")
    Single<ArrayList<Rule>> getRules();

    @GET("common/seatingArrangement")
    Single<ArrayList<SeatingArrangement>> getSeatingArrangement();

    @GET("common/amenityUnits")
    Single<ArrayList<AmenityUnits>> getAmenityUnits();

    @GET("space")
    Single<ArrayList<Space>> getSpaces();

    @Headers("Content-Type:application/json")
    @POST("sync/batch")
    Single<BatchSyncResponse> getBatchSync(@Body ArrayList<BatchSyncRequest> batchSyncRequest);

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("grant_eventspace_security")
    Single<UserLoginResponse> login(@Field("username") String username, @Field("password") String password);

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("grant_eventspace_security")
    Single<UserLoginResponse> loginGoogle(@Field("googleToken") String googleToken); // Verified

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("grant_eventspace_security")
    Single<UserLoginResponse> loginFacebook(@Field("facebookToken") String fbToken);

    @POST("user")
    Single<String> signup(@Body UserSignUpRequest userSignUpRequest);

    @Headers("Content-Type:application/json")
    @POST("user/verify")
    Single<Boolean> validate(@Body EmailValidateRequest param);

    @Headers("Content-Type:application/x-www-form-urlencoded ")
    @FormUrlEncoded
    @POST("user/emailcheck")
    Single<Boolean> validateEmail(@Field("email") String s);

    @GET("user")
    Single<UserLoginResponse> getUser();

    @GET("user/spaces")
    Single<ArrayList<Space>> getHostedSpaces();

    @Headers("Content-Type:application/json")
    @PUT("user")
    Single<String> updateCurrentUser(@Body UserLoginResponse user);

    @GET("device_logout")
    Single<Integer> logout();

    @GET("old/space/{id}")
    Single<Space> getSpace(@Path("id") int id, @Query("eventId") String eventId);

    @GET("space/{id}")
    Single<Space> getSpace(@Path("id") int id); // Verified

    @Headers("Content-Type:application/json")
    @POST("space/asearch/page/{page}")
    Single<FilterdSpace> getSpaceByFilter(@Body AdvanceSearchRequest request, @Path("page") int page); // Verified

    @Headers("Content-Type:application/json")
    @POST("book/space")
    Single<BookingReserveResponse> reserveBooking(@Body SpaceBookingRequest request);

    @GET("book/spaces/space/{spaceId}")
    Single<ArrayList<Booking>> getBookings(@Path("spaceId") int spaceId);

    @GET("book/spaces/guest")
    Single<ArrayList<Booking>> getUserBookings();

    @Headers("Content-Type:application/json")
    @POST("book/spaces")
    Single<Booking> getUserBooking(@Body BookingDetailsRequest booking);

    @Headers("Content-Type:application/json")
    @POST("book/manual")
    Single<Response<String>> postManualBooking(@Body ManualBookingRequest request);

    @Headers("Content-Type:application/json")
    @POST("book/addReview")
    Single<String> postSpaceReview(@Body SpaceReviewRequest reviewRequest);

    @Headers("Content-Type:application/json")
    @PUT("book/addRemark")
    Single<BookingRemarksResponse> postBookingRemarks(@Body BookingRemarksRequest remarksRequest);

    @GET("book/manual/{bookingID}")
    Single<ManualBookingRequest> getManualBooking(@Path("bookingID") int bookingID);

    @Headers("Content-Type:application/json")
    @PUT("book/manual")
    Single<Integer> updateManualBooking(@Body ManualBookingRequest bookingID);

    @Headers("Content-Type:application/json")
    @DELETE("book/manual/{bookingId}")
    Single<Integer> deleteManualBooking(@Path("bookingId") int bookingID);

    @Headers("Content-Type:application/json")
    @PUT("book/space")
    Single<CancelBookingResponse> cancelGuestBooking(@Body CancelBookingRequest request);

    @GET("calendarDetails/space/{spaceId}")
    Single<BookingCalendarResponse> getBookingCalendar(@Path("spaceId") String spaceId);

    @Headers("Content-Type:application/json")
    @POST("space/promo")
    Single<PromoCodeResponse> getPromoDetails(@Body PromoCodeRequest promo);

    @Headers("Content-Type:application/json")
    @PUT("user/mobile/"+VERSION)
    Single<ServerResponse> updateUserMobile(@Body UserLoginResponse user);
}
