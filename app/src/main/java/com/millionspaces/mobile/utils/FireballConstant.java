package com.millionspaces.mobile.utils;

/**
 * Created by kasunka on 2/13/18.
 */

public class FireballConstant {

//    Home page
    public static final String FIREBASE_EVENT_HOME_SEARCH = "EVENT_HOME_SEARCH";
    public static final String FIREBASE_PARAM_HOME_EVENT_TYPE = "Event_Type";
    public static final String FIREBASE_PARAM_HOME_EVENT_TYPE_EMPTY = "- NON -";

//    Space List page Filter And Sort
    public static final String FIREBASE_EVENT_SPACE_LIST = "EVENT_SPACE_LIST_FILTER_AND_SORT";
    public static final String FIREBASE_PARAM_SPACE_NO_DATA_SET = "- NON -";
    public static final String FIREBASE_PARAM_SPACE_LIST_PARTICIPATION = "Participation_Count";
    public static final String FIREBASE_PARAM_SPACE_LIST_BUDGET = "Space_Budget";
    public static final String FIREBASE_PARAM_SPACE_LIST_AMENITIES = "Space_Amenities";
    public static final String FIREBASE_PARAM_SPACE_LIST_EVENT_TYPE = "Event_Type";
    public static final String FIREBASE_PARAM_SPACE_LIST_SPACE_TYPE = "Space_Type";
    public static final String FIREBASE_PARAM_SPACE_LIST_LOCATION = "Space_Location";
    public static final String FIREBASE_PARAM_SPACE_LIST_DATE = "Available_Date";
    public static final String FIREBASE_PARAM_SPACE_LIST_RULES = "Space_Rules";
    public static final String FIREBASE_PARAM_SPACE_LIST_SEATING = "Space_Seating";
    public static final String FIREBASE_PARAM_SPACE_LIST_SORT = "Sorting";
    public static final String FIREBASE_PARAM_SPACE_SEARCH_BY = "Search_Name";

    public static final String FIREBASE_EVENT_SPACE_LIST_SELECT = "EVENT_SELECT_SPACE_FROM_LIST";
    public static final String FIREBASE_PARAM_SPACE_LIST_NAME = "Space_Name";
}
