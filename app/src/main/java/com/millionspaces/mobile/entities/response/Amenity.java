package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.millionspaces.mobile.entities.AmenityUnits;

/**
 * Created by kasunka on 7/19/17.
 */

public class Amenity implements Parcelable{


    private int id;
    private String name;
    private String mobileIcon;
    private String timeStamp;
    @SerializedName("amenityUnit")
    private int amenityUnitId;
    private AmenityUnits amenityUnitsDto;

    private String amenityUnitName;

    public Amenity(int id, String name, String icon, String timeStamp) {
        this.id = id;
        this.name = name;
        this.mobileIcon = icon;
        this.timeStamp = timeStamp;
    }


    protected Amenity(Parcel in) {
        id = in.readInt();
        name = in.readString();
        mobileIcon = in.readString();
        timeStamp = in.readString();
        amenityUnitId = in.readInt();
        amenityUnitsDto = in.readParcelable(AmenityUnits.class.getClassLoader());
        amenityUnitName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(mobileIcon);
        dest.writeString(timeStamp);
        dest.writeInt(amenityUnitId);
        dest.writeParcelable(amenityUnitsDto, flags);
        dest.writeString(amenityUnitName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Amenity> CREATOR = new Creator<Amenity>() {
        @Override
        public Amenity createFromParcel(Parcel in) {
            return new Amenity(in);
        }

        @Override
        public Amenity[] newArray(int size) {
            return new Amenity[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return mobileIcon;
    }

    public void setIcon(String icon) {
        this.mobileIcon = icon;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getAmenityUnitId() {
        return amenityUnitId;
    }

    public void setAmenityUnitId(int amenityUnitId) {
        this.amenityUnitId = amenityUnitId;
    }

    public AmenityUnits getAmenityUnitsDto() {
        return amenityUnitsDto;
    }

    public void setAmenityUnitsDto(AmenityUnits amenityUnitsDto) {
        this.amenityUnitsDto = amenityUnitsDto;
    }

    public String getAmenityUnitName() {
        return amenityUnitName;
    }

    public void setAmenityUnitName(String amenityUnitName) {
        this.amenityUnitName = amenityUnitName;
    }
}
