package com.millionspaces.mobile.actions;

/**
 * Created by kasunka on 9/27/17.
 */

public interface IMethodUpdater {
    void updateList(boolean isAdded, String key, String value);
}
