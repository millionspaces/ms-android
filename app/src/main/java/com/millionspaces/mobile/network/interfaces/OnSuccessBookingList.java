package com.millionspaces.mobile.network.interfaces;

import com.millionspaces.mobile.entities.response.Booking;

import java.util.ArrayList;


/**
 * Created by kasunka on 10/10/17.
 */

public interface OnSuccessBookingList extends OnErrorResponse {
    void onSuccessBookingList(ArrayList<Booking> value);
}
