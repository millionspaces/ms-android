package com.millionspaces.mobile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.millionspaces.mobile.actions.OnSuccessGuestMailEntered;
import com.millionspaces.mobile.entities.request.EmailValidateRequest;
import com.millionspaces.mobile.entities.request.UserLoginRequest;
import com.millionspaces.mobile.entities.response.UserLoginResponse;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessLogin;
import com.millionspaces.mobile.network.interfaces.OnSuccessValidateMail;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.KEY_IS_LOGGED_IN;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_EMAIL;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_ID;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_IMAGE;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_NAME;
import static com.millionspaces.mobile.utils.Config.KEY_LOGGED_USER_ROLE;
import static com.millionspaces.mobile.utils.Config.isValidMail;
import static com.millionspaces.mobile.utils.Config.sha256;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_LOGIN_REQUEST;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_VALIDATE_SIGNUP_MAIL;

public class LoginActivity extends BaseActivity implements OnSuccessLogin, OnSuccessValidateMail,View.OnClickListener, FacebookCallback<LoginResult> , OnSuccessGuestMailEntered {

    @BindView(R.id.login_parent)
    CoordinatorLayout mCoordinatorlayout;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.txt_username)
    EditText usernameText;
    @BindView(R.id.txt_password)
    EditText passwordText;
    @BindView(R.id.btn_login)
    Button login;
    @BindView(R.id.login_facebook)
    LoginButton facebook;
    @BindView(R.id.facebook_login_content)
    RelativeLayout facebookLogin;
    @BindView(R.id.login_google)
    SignInButton google;
    @BindView(R.id.google_login_content)
    RelativeLayout googleLogin;
    @BindView(R.id.guest_login_content)
    RelativeLayout guestLogin;
    @BindView(R.id.login_terms01)
    LinearLayout termsO1;
    @BindView(R.id.login_terms02)
    TextView termsO2;

    private GoogleSignInResult googleSignInResult;

    private static final int REQUEST_AUTHORIZATION = 1001;
    private static final int EMAIL_SIGNIN = 1;
    private static final int FACEBOOK_SIGNIN = 2;
    private static final int GOOGLE_SIGNIN = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookSDKInitialize();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        init_views();
        init_actions();
    }

    private void init_views() {
        Glide.with(this).load(R.drawable.splash_logo).into(logo);
    }

    private void init_actions() {
        getAuthKey();
        login.setOnClickListener(this);
        facebookLogin.setOnClickListener(this);
        googleLogin.setOnClickListener(this);
        guestLogin.setOnClickListener(this);
        termsO1.setOnClickListener(this);
        termsO2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_login:
                attemptLogin();
                break;
            case R.id.facebook_login_content:
                keyHash(); // TODO: Remove this method,
                performFaceBookLogin();
                break;
            case R.id.google_login_content:
                GoogleSDKInitialize();
                performGoogleLogin();
                break;
            case R.id.login_terms01:
                navigateActivity(WebViewActivity.class);
                break;
            case R.id.login_terms02:
                navigateActivity(WebViewActivity.class);
                break;
            case R.id.guest_login_content:
                AlertMessageUtils.showTextPicker(this,getResources().getString(R.string.login_guest_title),getResources().getString(R.string.login_guest_action),this);
                break;
        }
    }

    @OnClick(R.id.signup_container)
    void navigateSignUp(){
        navigateFinishActivity(SignUpActivity.class);
    }

    private void getAuthKey() {
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            Uri data = intent.getData();
            if (data == null) return;
            String url = data.toString();
            Log.d("URL",url);
            String[] params = url.split("(?<==)");
            Log.d("url",params[params.length - 1]);
            if (params.length > 1) { //TODO: Make Deeplink in proper way
                validateSignUpMail(params[params.length - 1]);
            }else {
                String[] pathParams = url.split("/");
                for (String a: pathParams){
                    Log.d("url", a);
                }
                Intent intent1  = new Intent(this,SpaceProfileActivity.class);
                intent1.putExtra(BUNDLE_INTEGER_PARAM,Integer.parseInt(pathParams[pathParams.length-2]));
                intent1.putExtra(BUNDLE_STRING_PARAM,"[1]");
                startActivity(intent1);
            }
        }
    }

    private void validateSignUpMail(String param) {
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            showProgress();
            MillionSpaceService.getInstance(this).validateMail(new EmailValidateRequest(param), this);
        }else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure), getResources().getString(R.string.error_network_failure_action),mCoordinatorlayout,
                    this,ENF_VALIDATE_SIGNUP_MAIL);
        }
    }

    private void performGoogleLogin() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_SIGNIN);
    }

    private void performFaceBookLogin() {
        facebook.performClick();
        facebook.setPressed(true);
        facebook.invalidate();
        facebook.setReadPermissions("public_profile", "email", "user_friends");
        LoginManager.getInstance().registerCallback(callbackManager,this);
    }

    private void attemptLogin() {
        String usrName = usernameText.getText().toString();
        String password = passwordText.getText().toString();
        View facusedView = null;
        String error = null;
        boolean isValid = true;

        if (usrName.isEmpty()){
            error = getString(R.string.error_empty_email);
            facusedView = usernameText;
            isValid = false;
        }else if (!isValidMail(usrName)) {// TODO : Check Valid email
            error = getString(R.string.error_wrong_email);
            facusedView = usernameText;
            isValid = false;
        }else if (password.isEmpty()){
            error =getString(R.string.error_empty_password);
            facusedView = passwordText;
            isValid = false;
        }

        if (isValid){
            try {
                password = sha256(password);
                loginRequest(new UserLoginRequest(usrName, password,EMAIL_SIGNIN));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            facusedView.requestFocus();
            AlertMessageUtils.showSnackBarMessage(error, mCoordinatorlayout);
        }
    }

    private void loginRequest(UserLoginRequest userLoginRequest) {
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            showProgress();
            MillionSpaceService.getInstance(this).login(userLoginRequest, this);
        }else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure), getResources().getString(R.string.error_network_failure_action),mCoordinatorlayout,
                    this,ENF_LOGIN_REQUEST);
        }
    }

    @Override
    public void onSuccessMail(String mail) {
        UserLoginRequest userLoginRequest = new UserLoginRequest(mail,null,EMAIL_SIGNIN);
        loginRequest(userLoginRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case GOOGLE_SIGNIN:
                if (resultCode == RESULT_OK) {
                    GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                    handleGoogleSignInResult(result);
                }else {
                    mGoogleApiClient.stopAutoManage(this);
                    mGoogleApiClient.disconnect();
                }
                break;
            case REQUEST_AUTHORIZATION:
                //TODO: Handle logging for new users
                new Thread(runnable).start();
                break;
            default:
                callbackManager.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //TODO: Handle google connection errors
    }


    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error, mCoordinatorlayout);
    }

    @Override
    public void onSuccessLogin(UserLoginResponse user) {
        hideProgress();
        PreferenceUtils.savePreferenceBoolen(KEY_IS_LOGGED_IN,true,this);
        PreferenceUtils.savePreferenceString(KEY_LOGGED_USER_NAME,user.getName(),this);
        PreferenceUtils.savePreferenceString(KEY_LOGGED_USER_ID,String.valueOf(user.getId()),this);
        PreferenceUtils.savePreferenceString(KEY_LOGGED_USER_IMAGE,user.getImageUrl(),this);
        PreferenceUtils.savePreferenceString(KEY_LOGGED_USER_EMAIL,user.getEmail(),this);
        PreferenceUtils.savePreferenceString(KEY_LOGGED_USER_ROLE,user.getRole(),this);

        if (getCallingActivity() == null){
            navigateFinishActivity(HomeActivity.class);
        }else {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }
    }

    @Override
    public void onSuccesValidate(boolean isValid) {
        hideProgress();
        if (isValid){
            navigateActivity(HomeActivity.class);
        }else {
            AlertMessageUtils.showSnackBarMessage(getString(R.string.error_fail_signupemail_validation), mCoordinatorlayout);
        }
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        loginRequest(new UserLoginRequest(loginResult.getAccessToken().getToken(),FACEBOOK_SIGNIN));
        Log.d("FACEBOOK",loginResult.getAccessToken().getToken());

        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    Log.d("Facebook",object.getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //TODO: Get Facebook parameters if needed
            }
        });
        Bundle parameters = new Bundle();
        //TODO: Remove irrelevant parameters
        parameters.putString("fields", "id,name,link,birthday,first_name,gender,last_name,location,email,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();

        LoginManager.getInstance().logOut();
    }

    @Override
    public void onCancel() {
        //TODO: Handle Facebook cancel error message
    }

    @Override
    public void onError(FacebookException error) {
        AlertMessageUtils.showSnackBarMessage(error.getMessage(), mCoordinatorlayout);
    }

    private void handleGoogleSignInResult(final GoogleSignInResult result) {
        if (result.isSuccess()){
            Log.d("GOOGLE", result.getSignInAccount().getDisplayName());
            Log.d("GOOGLE", result.getSignInAccount().getServerAuthCode());

            this.googleSignInResult =  result;
            new Thread(runnable).start();

        }else {
            Log.d("GOOGLE","Fail Logon" + " "+ result.getStatus().toString());
        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                String token = GoogleAuthUtil.getToken(getApplicationContext(),googleSignInResult.getSignInAccount().getAccount(),"oauth2:https://www.googleapis.com/auth/plus.login");
                Log.d("ACCESS", token);
                Message message = new Message();
                Bundle bundle = new Bundle();
                bundle.putString("AUTH",token);
                message.setData(bundle);
                handler.sendMessage(message);
            } catch (UserRecoverableAuthException e) {
                startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (GoogleAuthException e) {
                e.printStackTrace();
            }
        }
    };

    Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if(!msg.getData().isEmpty()) {
                loginRequest(new UserLoginRequest(msg.getData().getString("AUTH"),GOOGLE_SIGNIN));
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            }
            return false;
        }
    });


}
