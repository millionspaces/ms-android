package com.millionspaces.mobile.entities.request;

/**
 * Created by kasunka on 1/10/18.
 */

public class PromoCodeRequest {
    private String space;
    private String promoCode;

    public PromoCodeRequest(String spaceId, String promoCode) {
        this.space = spaceId;
        this.promoCode = promoCode;
    }
}
