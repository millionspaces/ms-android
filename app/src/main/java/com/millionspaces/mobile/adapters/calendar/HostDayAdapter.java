package com.millionspaces.mobile.adapters.calendar;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnDateSelected;
import com.millionspaces.mobile.utils.CalendarManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.CalenderUtils.DATE_MAP;
import static com.millionspaces.mobile.utils.CalenderUtils.PARTIALLY_BOOKED;
import static com.millionspaces.mobile.utils.CalenderUtils.SELECTED_DAY;
import static com.millionspaces.mobile.utils.CalenderUtils.TOTALLY_BOOKED;


/**
 * Created by kasunka on 11/17/17.
 */

public class HostDayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private int noDayOfMonth;
    private int startDayOfWeek;
    private int currentYear;
    private int currentMonth;
    private int width;
    private int totalDateFill;
    private HashMap<String, String> mBookingDates;
    private int selectedPosition = -1;

    private static final int LAST_DATES_OF_PREVIOUS_MONTH = 6;
    private static final int CURRENT_DATES_OF_CURRENT_MONTH = 4;
    private static final int FIRTS_DATES_OF_NEXT_MONTH = 7;

    private OnDateSelected onDateSelected;

    public HostDayAdapter(Context context, int width, int year, int month, int noOfDays, int startDays, CalendarManager calendarManager) {
        this.mContext = context;
        this.noDayOfMonth = noOfDays;
        this.startDayOfWeek = DATE_MAP.get(startDays);
        this.currentYear = year;
        this.currentMonth = month;
        this.totalDateFill = noDayOfMonth + startDayOfWeek;
        this.width = width / 7;
        this.mBookingDates = calendarManager.getBlockDaysMap();

        for (String key : mBookingDates.keySet()) {
            Log.d("days", key + " " + mBookingDates.get(key));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case LAST_DATES_OF_PREVIOUS_MONTH:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_calender_day, parent, false);
                view.setMinimumHeight(width);
                return new ViewHolderDays(view);
            case CURRENT_DATES_OF_CURRENT_MONTH:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_calender_day, parent, false);
                view.setMinimumHeight(width);
                return new ViewHolderDays(view);
            case FIRTS_DATES_OF_NEXT_MONTH:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_calender_day, parent, false);
                view.setMinimumHeight(width);
                return new ViewHolderDays(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (position < startDayOfWeek) {
            ((HostDayAdapter.ViewHolderDays) holder).dateTxt.setText(""); // TODO: Make days unavailable if needed in the first month
        } else if (position <= totalDateFill - 1) {
            int day = position - startDayOfWeek + 1;
            ((HostDayAdapter.ViewHolderDays) holder).dateTxt.setText(String.valueOf(day));
            ((HostDayAdapter.ViewHolderDays) holder).totallyBookingIndicator.setSelected(selectedPosition == position ? true : false);
            setDayIndicator(day, ((HostDayAdapter.ViewHolderDays) holder), position);
        } else {
            ((HostDayAdapter.ViewHolderDays) holder).dateTxt.setText("");
        }
    }

    private void setDayIndicator(int day, final HostDayAdapter.ViewHolderDays holder, final int position) {
        String value = mBookingDates.get(currentYear + String.format("%02d", currentMonth) + String.format("%02d", day));

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateSelected.onDateSelected(holder.dateTxt.getText().toString(), null, -1, true);
                notifyItemChanged(selectedPosition);
                selectedPosition = position;
                notifyItemChanged(selectedPosition);
                v.setSelected(true);
            }
        };


        if (value == TOTALLY_BOOKED) {
            holder.dateTxt.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryTextLightest));
            holder.totallyBookingIndicator.setOnClickListener(clickListener);
        } else if (value == PARTIALLY_BOOKED) {
            holder.dateTxt.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryTextLight));
            holder.partiallyBookingIndicator.setVisibility(View.VISIBLE);
            holder.totallyBookingIndicator.setOnClickListener(clickListener);
        } else if (value == SELECTED_DAY) {
            holder.dateTxt.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryTextWhite));
            holder.totallyBookingIndicator.setBackgroundColor(ContextCompat.getColor(mContext, R.color.calenderPrimaryBlue));
            holder.partiallyBookingIndicator.setVisibility(View.GONE);
            holder.totallyBookingIndicator.setOnClickListener(clickListener);
        } else {
            holder.dateTxt.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryTextLight));
            holder.totallyBookingIndicator.setOnClickListener(clickListener);
        }
    }

    @Override
    public int getItemCount() {
        return totalDateFill == 28 ? totalDateFill : totalDateFill <= 35 ? 35 : 42;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < startDayOfWeek) {
            return LAST_DATES_OF_PREVIOUS_MONTH;
        } else if (position <= totalDateFill - 1) {
            return CURRENT_DATES_OF_CURRENT_MONTH;
        } else {
            return FIRTS_DATES_OF_NEXT_MONTH;
        }
    }

    public class ViewHolderDays extends RecyclerView.ViewHolder {

        @BindView(R.id.calender_date_calender)
        TextView dateTxt;
        @BindView(R.id.calender_date_parcially_bokking_indicator)
        ImageView partiallyBookingIndicator;
        @BindView(R.id.calender_date_blocked_indicator)
        RelativeLayout totallyBookingIndicator;

        public ViewHolderDays(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnItemClickListner(OnDateSelected onDateSelected) {
        this.onDateSelected = onDateSelected;
    }
}
