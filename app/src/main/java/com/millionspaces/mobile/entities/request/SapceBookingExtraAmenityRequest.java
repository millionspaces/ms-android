package com.millionspaces.mobile.entities.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 9/14/17.
 */

public class SapceBookingExtraAmenityRequest implements Parcelable{
    private int amenityId;
    private int number;

    public SapceBookingExtraAmenityRequest(int amenityId, int number) {
        this.amenityId = amenityId;
        this.number = number;
    }

    protected SapceBookingExtraAmenityRequest(Parcel in) {
        amenityId = in.readInt();
        number = in.readInt();
    }

    public static final Creator<SapceBookingExtraAmenityRequest> CREATOR = new Creator<SapceBookingExtraAmenityRequest>() {
        @Override
        public SapceBookingExtraAmenityRequest createFromParcel(Parcel in) {
            return new SapceBookingExtraAmenityRequest(in);
        }

        @Override
        public SapceBookingExtraAmenityRequest[] newArray(int size) {
            return new SapceBookingExtraAmenityRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(amenityId);
        dest.writeInt(number);
    }

}
