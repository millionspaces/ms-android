package com.millionspaces.mobile.actions;

import android.view.View;

/**
 * Created by Kasunka on 6/7/2017.
 */

public interface OnItemClickListener {
    void OnClick(int position, View view);
}
