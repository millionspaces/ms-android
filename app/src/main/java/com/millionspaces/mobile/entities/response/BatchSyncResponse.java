package com.millionspaces.mobile.entities.response;

import java.util.ArrayList;

/**
 * Created by kasunka on 2/12/18.
 */

public class BatchSyncResponse {
    private ArrayList<Amenity> amenity;
    private ArrayList<EventType> eventType;
    private ArrayList<CancellationPolicy> cancellationPolicy;
    private ArrayList<Rule> rules;
    private ArrayList<SeatingArrangement> seatingArrangements;

    public ArrayList<Amenity> getAmenity() {
        return amenity;
    }

    public ArrayList<EventType> getEventType() {
        return eventType;
    }

    public ArrayList<CancellationPolicy> getCancellationPolicy() {
        return cancellationPolicy;
    }

    public ArrayList<Rule> getRules() {
        return rules;
    }

    public ArrayList<SeatingArrangement> getSeatingArrangements() {
        return seatingArrangements;
    }
}
