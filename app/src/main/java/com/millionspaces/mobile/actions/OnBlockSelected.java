package com.millionspaces.mobile.actions;

import com.millionspaces.mobile.entities.response.Availability;

/**
 * Created by kasunka on 9/28/17.
 */

public interface OnBlockSelected extends OnNoticePeriodSelected{
    void onBlockSelected(Availability availability, float charge, boolean isBlockAdded);
}
