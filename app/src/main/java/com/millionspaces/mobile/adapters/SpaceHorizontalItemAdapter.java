package com.millionspaces.mobile.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.millionspaces.mobile.BuildConfig;
import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.entities.response.Amenity;
import com.millionspaces.mobile.entities.response.Availability;
import com.millionspaces.mobile.entities.response.EventType;
import com.millionspaces.mobile.entities.response.ExtraAmenity;
import com.millionspaces.mobile.entities.response.Rule;
import com.millionspaces.mobile.entities.response.SeatingArrangement;
import com.millionspaces.mobile.utils.Config;
import com.millionspaces.mobile.utils.DataBaseUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.DAY_MAP;
import static com.millionspaces.mobile.utils.Config.SPACE_AMINITY_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_AMINITY_TYPE_LIST_CHARGBLE;
import static com.millionspaces.mobile.utils.Config.SPACE_EVENT_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_MENU_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_PRICE_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_PRICE_TYPE_LIST_BLOCKBASED;
import static com.millionspaces.mobile.utils.Config.SPACE_PRICE_TYPE_LIST_BLOCKGUESTBASED;
import static com.millionspaces.mobile.utils.Config.SPACE_PRICE_TYPE_LIST_HOURLYBASED;
import static com.millionspaces.mobile.utils.Config.SPACE_RULE_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_SEATING_TYPE_LIST;

/**
 * Created by kasunka on 7/24/17.
 */

public class SpaceHorizontalItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private ArrayList<Integer> intList = new ArrayList<>();
    private ArrayList<String> strList = new ArrayList<>();
    private ArrayList<Availability> availabilities = new ArrayList<>();
    private ArrayList<SeatingArrangement> seatings = new ArrayList<>();
    private ArrayList<ExtraAmenity> extraAmenities = new ArrayList<>();
    private Context context;
    private int cardViewType;
    private DataBaseUtils db;

    private static OnItemClickListener onItemClickListener;

    public SpaceHorizontalItemAdapter(int type, ArrayList<String> strings, ArrayList<Integer> elementList, ArrayList<Availability> availability,
                                      ArrayList<SeatingArrangement> seatingArrangements, ArrayList<ExtraAmenity> extraAmenities, Context context) {
        this.cardViewType = type;
        this.intList = elementList;
        this.strList = strings;
        this.availabilities = availability != null? cardViewType == SPACE_PRICE_TYPE_LIST_HOURLYBASED ?availability: getBlockArray(availability):null;
        if (cardViewType == SPACE_PRICE_TYPE_LIST_HOURLYBASED || cardViewType == SPACE_PRICE_TYPE_LIST_BLOCKBASED){
            Collections.sort(availabilities, new Comparator<Availability>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public int compare(Availability o1, Availability o2) {
                    return Integer.compare(o1.getDay(), o2.getDay());
                }
            });
        }
        this.seatings = seatingArrangements;
        this.extraAmenities = extraAmenities;
        this.context = context;
        db = new DataBaseUtils(context);
    }

    private ArrayList<Availability> getBlockArray(ArrayList<Availability> availabilities) {
        ArrayList<Availability> list = new ArrayList<>();
        ArrayList<Availability> tempList = new ArrayList<>();
        ArrayList<Integer> intList;
        Availability availabilityI = null, availabilityJ = null;
        list.addAll(availabilities);
        int size = list.size();

        if (cardViewType != SPACE_PRICE_TYPE_LIST_BLOCKGUESTBASED) {
            for (int i = 0; i < size; i++) {
                availabilityI = list.get(i);
                intList = new ArrayList<>();
                intList.add(availabilityI.getDay());
                for (int j = i + 1; j < size; j++) {
                    availabilityJ = list.get(j);

                    if (availabilityI.getFrom().equals(availabilityJ.getFrom()) && availabilityI.getTo().equals(availabilityJ.getTo())
                            && availabilityI.getCharge() == availabilityJ.getCharge()) {
                        intList.add(availabilityJ.getDay());
                        list.remove(availabilityJ);
                        size = list.size();
                    }
                }
                availabilityI.setDayList(intList);
                tempList.add(availabilityI);
            }
        }else {
            for (int i = 0; i < size; i++) {
                availabilityI = list.get(i);
                intList = new ArrayList<>();
                intList.add(availabilityI.getDay());
                for (int j = i + 1; j < size; j++) {
                    availabilityJ = list.get(j);

                    if (availabilityI.getFrom().equals(availabilityJ.getFrom()) && availabilityI.getTo().equals(availabilityJ.getTo())
                            && availabilityI.getCharge() == availabilityJ.getCharge()) {
                        intList.add(availabilityJ.getDay());
                        list.remove(availabilityJ);
                        size = list.size();
                    }
                }
                availabilityI.setDayList(intList);
                tempList.add(availabilityI);
            }
        }
        return list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        switch (viewType){
            case SPACE_EVENT_TYPE_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_space_eventtype_card, parent, false);
                return new ViewHolderEventTypes(v);
            case SPACE_PRICE_TYPE_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_space_price_card, parent, false);
                return new ViewHolderAvalability(v);
            case SPACE_AMINITY_TYPE_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_space_aminity_card, parent, false);
                return new ViewHolderAminityTypes(v);
            case SPACE_SEATING_TYPE_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_space_seating_card, parent, false);
                return new ViewHolderSeatingTypes(v);
            case  SPACE_MENU_TYPE_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_space_menu_card,parent,false);
                return new ViewHolderMenuTypes(v);
            case SPACE_RULE_TYPE_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_sapce_rules_card,parent,false);
                return new ViewHolderRuleTypes(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (cardViewType){
            case SPACE_EVENT_TYPE_LIST:
                EventType eventType = db.getEventTypeById(intList.get(position));
                Glide.with(context).load(eventType.getIcon()).into(((ViewHolderEventTypes)holder).icon);
                ((ViewHolderEventTypes)holder).name.setText(eventType.getName());
                break;
            case SPACE_PRICE_TYPE_LIST_HOURLYBASED:
                Availability availabilityHour = availabilities.get(position);
                ((ViewHolderAvalability) holder).title.setText(DAY_MAP.get(availabilityHour.getDay()));
                ((ViewHolderAvalability) holder).charge.setText("LKR " +new DecimalFormat("####,###,###").format(availabilityHour.getCharge()));
                ((ViewHolderAvalability) holder).unit.setVisibility(View.VISIBLE);
                ((ViewHolderAvalability) holder).time.setVisibility(View.GONE);
                break;
            case SPACE_PRICE_TYPE_LIST_BLOCKBASED:
                Availability availabilityblock = availabilities.get(position);
                ((ViewHolderAvalability) holder).title.setText(get12HoursValue(availabilityblock.getFrom().split(":"))
                        +" - "+get12HoursValue(availabilityblock.getTo().split(":")));
                ((ViewHolderAvalability) holder).charge.setText("LKR " +new DecimalFormat("####,###,###").format(availabilityblock.getCharge()));
                Collections.sort(availabilityblock.getDayList(),new Config.DateSorter());
                ((ViewHolderAvalability) holder).time.setText(getDateList(availabilityblock.getDayList()));
                ((ViewHolderAvalability) holder).unit.setVisibility(View.GONE);
                ((ViewHolderAvalability) holder).time.setVisibility(View.VISIBLE);
                break;
            case SPACE_PRICE_TYPE_LIST_BLOCKGUESTBASED:
                Availability availabilityGuestblock = availabilities.get(position);
                ((ViewHolderAvalability) holder).title.setText(get12HoursValue(availabilityGuestblock.getFrom().split(":"))
                        +" - "+get12HoursValue(availabilityGuestblock.getTo().split(":")));
                ((ViewHolderAvalability) holder).charge.setText("LKR " +new DecimalFormat("####,###,###").format(availabilityGuestblock.getCharge()));
                Collections.sort(availabilityGuestblock.getDayList(),new Config.DateSorter());
                ((ViewHolderAvalability) holder).time.setText(getDateList(availabilityGuestblock.getDayList()));
                ((ViewHolderAvalability) holder).unit.setVisibility(View.GONE);
                ((ViewHolderAvalability) holder).time.setVisibility(View.VISIBLE);
                ((ViewHolderAvalability) holder).menu.setVisibility(View.VISIBLE);
                ((ViewHolderAvalability) holder).menu.setText(context.getString(R.string.space_profile_space_menu_name_prefix)+" "+availabilityGuestblock.getMenuFiles().get(0).getMenuId());
                break;
            case SPACE_AMINITY_TYPE_LIST:
                Amenity amenity = db.getAmenity(intList.get(position));
                Glide.with(context).load(amenity.getIcon()).into(((ViewHolderAminityTypes)holder).icon);
                ((ViewHolderAminityTypes)holder).name.setText(amenity.getName());
                break;
            case SPACE_AMINITY_TYPE_LIST_CHARGBLE:
                Amenity amenityExtra = db.getAmenity(extraAmenities.get(position).getAmenityId());
                Glide.with(context).load(amenityExtra.getIcon()).into(((ViewHolderAminityTypes)holder).icon);
                ((ViewHolderAminityTypes)holder).name.setText(extraAmenities.get(position).getName());
                ((ViewHolderAminityTypes)holder).charge.setText("LKR " +new DecimalFormat("####,###,###").format(extraAmenities.get(position).getExtraRate()));
                ((ViewHolderAminityTypes)holder).container.setVisibility(View.VISIBLE);
                ((ViewHolderAminityTypes)holder).unitType.setText(db.getAmenityUnit(extraAmenities.get(position).getAmenityUnit()).getName());
                break;
            case SPACE_SEATING_TYPE_LIST:
                SeatingArrangement seating = seatings.get(position);
                ((ViewHolderSeatingTypes)holder).title.setText(seating.getName());
                ((ViewHolderSeatingTypes)holder).count.setText("Maximum " +seating.getParticipantCount());
                Glide.with(context).load(db.getSeatingArranmnet(seating.getId()).getIcon()).into(((ViewHolderSeatingTypes)holder).icon);
                break;
            case SPACE_MENU_TYPE_LIST:
                String menu[] = strList.get(position).split(":");
                ((ViewHolderMenuTypes)holder).name.setText(context.getString(R.string.space_profile_space_menu_name_prefix)+" "+menu[0]);
                Glide.with(context).load(BuildConfig.IMAGE_SERVER_URL+BuildConfig.IMAGE_SERVER_DOMAIN+menu[1]).into(((ViewHolderMenuTypes)holder).icon);
                break;
            case SPACE_RULE_TYPE_LIST:
                Rule rule = db.getRule(intList.get(position));
                ((ViewHolderRuleTypes)holder).rule.setText(rule.getName());
                break;
        }
    }

    private String getDateList(ArrayList<Integer> dayList) {
        StringBuilder string = new StringBuilder();
        for (Integer i: dayList){
            string.append(i == 8? "WEEKDAY ":DAY_MAP.get(i).substring(0,3).toUpperCase()+" ");
        }

        return string.toString().substring(0,string.length()-1);
    }


    private String get12HoursValue(String [] time) {
        String meridianNess = (Integer.valueOf(time[0])/12==0?"AM":"PM");
        int hour = Integer.valueOf(time[0])%12;
        if (hour == 0) hour = 12;
        return hour+ meridianNess;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        switch (cardViewType){
            case SPACE_EVENT_TYPE_LIST:
                type = SPACE_EVENT_TYPE_LIST;
                break;
            case SPACE_PRICE_TYPE_LIST_BLOCKBASED:
                type = SPACE_PRICE_TYPE_LIST;
                break;
            case SPACE_PRICE_TYPE_LIST_HOURLYBASED:
                type = SPACE_PRICE_TYPE_LIST;
                break;
            case SPACE_PRICE_TYPE_LIST_BLOCKGUESTBASED:
                type = SPACE_PRICE_TYPE_LIST;
                break;
            case SPACE_AMINITY_TYPE_LIST:
                type = SPACE_AMINITY_TYPE_LIST;
                break;
            case SPACE_AMINITY_TYPE_LIST_CHARGBLE:
                type = SPACE_AMINITY_TYPE_LIST;
                break;
            case SPACE_SEATING_TYPE_LIST:
                type = SPACE_SEATING_TYPE_LIST;
                break;
            case SPACE_MENU_TYPE_LIST:
                type = SPACE_MENU_TYPE_LIST;
                break;
            case SPACE_RULE_TYPE_LIST:
                type = SPACE_RULE_TYPE_LIST;
                break;
        }
        return type;
    }

    @Override
    public int getItemCount() {
        int count = 0;
        switch (cardViewType){
            case SPACE_EVENT_TYPE_LIST:
                count = intList.size();
                break;
            case SPACE_PRICE_TYPE_LIST_BLOCKBASED:
                count = availabilities.size();
                break;
            case SPACE_PRICE_TYPE_LIST_HOURLYBASED:
                count = availabilities.size();
                break;
            case SPACE_PRICE_TYPE_LIST_BLOCKGUESTBASED:
                count = availabilities.size();
                break;
            case SPACE_AMINITY_TYPE_LIST:
                count = intList.size();
                break;
            case SPACE_AMINITY_TYPE_LIST_CHARGBLE:
                count = extraAmenities.size();
                break;
            case SPACE_SEATING_TYPE_LIST:
                count = seatings.size();
                break;
            case SPACE_MENU_TYPE_LIST:
                count = strList.size();
                break;
            case SPACE_RULE_TYPE_LIST:
                count = intList.size();
                break;
        }
        return count;
    }

    public class ViewHolderEventTypes extends RecyclerView.ViewHolder{
        @BindView(R.id.eventtype_icon)
        ImageView icon;
        @BindView(R.id.eventtype_name)
        TextView name;

        public ViewHolderEventTypes(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public class ViewHolderAvalability extends RecyclerView.ViewHolder{
        @BindView(R.id.avalability_card_title)
        TextView title;
        @BindView(R.id.avalability_card_time)
        TextView time;
        @BindView(R.id.avalability_card_charge)
        TextView charge;
        @BindView(R.id.avalability_card_unit)
        TextView unit;
        @BindView(R.id.avalability_card_menu)
        TextView menu;

        public ViewHolderAvalability(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public class ViewHolderAminityTypes extends RecyclerView.ViewHolder{
        @BindView(R.id.aminity_icon)
        ImageView icon;
        @BindView(R.id.aminity_name)
        TextView name;
        @BindView(R.id.aminity_card_charge)
        TextView charge;
        @BindView(R.id.aminity_card_pricecontainer)
        LinearLayout container;
        @BindView(R.id.avalability_card_unit)
        TextView unitType;

        public ViewHolderAminityTypes(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public class ViewHolderMenuTypes extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.menu_name)
        TextView name;
        @BindView(R.id.menu_icon)
        ImageView icon;

        public ViewHolderMenuTypes(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.OnClick(getAdapterPosition(),v);
        }
    }

    public class ViewHolderSeatingTypes extends RecyclerView.ViewHolder{
        @BindView(R.id.seating_card_name) TextView title;
        @BindView(R.id.seating_card_count) TextView count;
        @BindView(R.id.seeating_icon) ImageView icon;

        public ViewHolderSeatingTypes(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public class ViewHolderRuleTypes extends RecyclerView.ViewHolder{
        @BindView(R.id.rule_card_title) TextView rule;
        public ViewHolderRuleTypes(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void setOnItemClickListner(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
