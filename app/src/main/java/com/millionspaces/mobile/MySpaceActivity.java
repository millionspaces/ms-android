package com.millionspaces.mobile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.millionspaces.mobile.actions.OnItemClickListener;
import com.millionspaces.mobile.adapters.SpaceViewAdapter;
import com.millionspaces.mobile.entities.response.Space;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessSpacesList;
import com.millionspaces.mobile.utils.AlertMessageUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.BUNDLE_BOOLEAN_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_INTEGER_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_PARCELABLE_PARAM;
import static com.millionspaces.mobile.utils.Config.BUNDLE_STRING_PARAM;
import static com.millionspaces.mobile.utils.Config.MILLIONSPACE_MOBILE_NUMBER;
import static com.millionspaces.mobile.utils.Config.intersection;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_HOSTED_SPACES;

public class MySpaceActivity extends BaseActivity implements OnSuccessSpacesList, View.OnClickListener {
    @BindView(R.id.myspace_parent)
    CoordinatorLayout mCoordinatorlayout;
    @BindView(R.id.spaces_host_list)
    RecyclerView mRecyclerView;

    private SpaceViewAdapter mAdapter ;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<Space> spaces = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_space);
        ButterKnife.bind(this);
        init_views();
    }

    private void init_views() {
        init_toolbar();
        setToolbarTitle(getString(R.string.my_space_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        init_listview();
        init_actions();
    }

    private void init_listview() {
        getSpaceList();
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager );
        mAdapter = new SpaceViewAdapter(spaces, this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void getSpaceList() {
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            showProgress();
            MillionSpaceService.getInstance(this).getHostedSpaces(this);
        }else {
            mAdapter.notifyDataSetChanged();
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure),
                    getResources().getString(R.string.error_network_failure_action), mCoordinatorlayout,this,ENF_HOSTED_SPACES);
        }
    }

    private void init_actions() {

    }

    @Override
    public void onConnectionErrorCallBack(int error) {
        getSpaceList();
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
    }

    @Override
    public void onSuccessSpacesList(ArrayList<Space> spaceList) {
        hideProgress();
        spaces.addAll(spaceList);

        mAdapter.notifyDataSetChanged();
        mAdapter.setOnItemClickListner(new OnItemClickListener() {
            @Override
            public void OnClick(int position, View view) {
                switch (view.getId()){
                    case R.id.button_availability:
                        navigateActivityWithExtraInt(HostCalendarActivity.class,spaces.get(position).getId());
                        break;
                    case R.id.button_booking:
                        navigateActivityWithExtraInt(BookingListActivity.class, spaces.get(position).getId());
                        break;
                    case R.id.button_edit:
                        AlertMessageUtils.showEditSpaceDialog(MySpaceActivity.this,getString(R.string.my_space_edit_description),MySpaceActivity.this);
                        break;
                    default:
//                        Bundle bundle = new Bundle();
//                        bundle.putInt(BUNDLE_INTEGER_PARAM,spaces.get(position).getId());
//                        bundle.putString(BUNDLE_STRING_PARAM,spaces.get(position).getEventType().toString());
//                        bundle.putBoolean(BUNDLE_BOOLEAN_PARAM,true);
//                        navigateActivityWithExtra(SpaceProfileActivity.class,bundle);

                        //TODO : format code with removing event Types
                        Intent intent = new Intent(MySpaceActivity.this,SpaceProfileActivity.class);
                        intent.putExtra(BUNDLE_INTEGER_PARAM, spaces.get(position).getId());
                        intent.putExtra(BUNDLE_PARCELABLE_PARAM,spaces.get(position).getImages());
                        intent.putExtra(BUNDLE_STRING_PARAM, spaces.get(position).getEventType().toString());
                        intent.putExtra(BUNDLE_BOOLEAN_PARAM,true);
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                MySpaceActivity.this,
                                view,
                                ViewCompat.getTransitionName(view));
                        startActivity(intent,options.toBundle());
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(MILLIONSPACE_MOBILE_NUMBER));
        startActivity(intent);
    }

    @Override
    public void onSuccessSpacesListCount(int count) {
        if (count == 0){
            AlertMessageUtils.showSnackBarMessage(getResources().getString(R.string.error_empty_space), mCoordinatorlayout);
        }
    }
}
