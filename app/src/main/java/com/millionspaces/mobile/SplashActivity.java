package com.millionspaces.mobile;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import com.millionspaces.mobile.entities.ComposeMataData;
import com.millionspaces.mobile.network.MillionSpaceService;
import com.millionspaces.mobile.network.NetworkManager;
import com.millionspaces.mobile.network.interfaces.OnSuccessMetaData;
import com.millionspaces.mobile.utils.AlertMessageUtils;
import com.millionspaces.mobile.utils.DataBaseUtils;
import com.millionspaces.mobile.utils.PreferenceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.KEY_IS_INITIAL_LOUNCH;
import static com.millionspaces.mobile.utils.Config.KEY_IS_UPDATED_META;
import static com.millionspaces.mobile.utils.Config.KEY_UPDATED_TIMESTAMP;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_INITIAL_SYNC;
import static com.millionspaces.mobile.utils.ConnectionCallBackConfig.ENF_NON_INITIAL_SYNC;

public class SplashActivity extends BaseActivity implements OnSuccessMetaData {

    private DataBaseUtils db;

    @BindView(R.id.parent_splash)
    CoordinatorLayout parent;
    @BindView(R.id.prmo_image)
    ImageView promoImage;
    private ScaleAnimation fade_in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        init_views();
        db = new DataBaseUtils(this);
        if (!PreferenceUtils.showPreferenceBoolean(KEY_IS_INITIAL_LOUNCH, true, this)) {
//            syncDataBase();  // TODO : Sync database with updating existing feilds
            reSetupDataBase();
        } else {
            initialSync();
        }
    }

    private void init_views() {
        fade_in =  new ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        fade_in.setDuration(500);
        fade_in.setFillAfter(true);
    }
    
    /**
     * Run Splash for 4 seconds
     */
    private void runSplash() {

        Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            promoImage.startAnimation(fade_in);
                        }
                    });
                    Thread.sleep(getResources().getInteger(R.integer.splash_time));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            finish();
                            StartActivity();
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

    }

    private void StartActivity() {
        navigateActivity(HomeActivity.class);
        this.overridePendingTransition(0, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.overridePendingTransition(0, 0);
        finish();
    }

    private void initialSync() {
        if (NetworkManager.getInstance().checkInternetConenction(this)) {
            showProgress();
            MillionSpaceService.getInstance(this).getMetaData(this,KEY_IS_INITIAL_LOUNCH);
        }else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure), getResources().getString(R.string.error_network_failure_action),parent,
                    this,ENF_INITIAL_SYNC);
        }
    }

    private void syncDataBase(){
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            runSplash();
            MillionSpaceService.getInstance(this).getBatchSync(this,PreferenceUtils.showPreferenceString(KEY_UPDATED_TIMESTAMP,this),KEY_IS_UPDATED_META);
        }else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure), getResources().getString(R.string.error_network_failure_action),parent,
                    this,ENF_NON_INITIAL_SYNC);
        }
    }

    private void reSetupDataBase(){
        if (NetworkManager.getInstance().checkInternetConenction(this)){
            db.dropScheme();
            MillionSpaceService.getInstance(this).getMetaData(this,KEY_IS_INITIAL_LOUNCH);
        }else {
            AlertMessageUtils.showSnackBarCallBack(getResources().getString(R.string.error_network_failure), getResources().getString(R.string.error_network_failure_action),parent,
                    this,ENF_NON_INITIAL_SYNC);
        }
    }


    @Override
    public void onSuccessMeta(ComposeMataData value, String key) {
        String timeStamp = PreferenceUtils.showPreferenceString(KEY_UPDATED_TIMESTAMP,this);
        if (key.contains(KEY_IS_INITIAL_LOUNCH)) {
            db.addAmenities(value.getAmenities());
            db.addEventTypes(value.getEventTypes());
            db.addRules(value.getRules());
            db.addSeatingList(value.getSeatingArrangements());
            db.addCancellationPolicyList(value.getCancellationPolicies());
            db.addAmenityUnites(value.getExtraUnits());
            PreferenceUtils.savePreferenceBoolen(KEY_IS_INITIAL_LOUNCH, false, this);
            StartActivity();
            timeStamp = value.getAmenities().get(0).getTimeStamp();
        }else {
            timeStamp = value.getAmenities() != null ? db.syncAmenities(value.getAmenities()) : timeStamp;
            timeStamp = value.getEventTypes() != null ? db.syncEventTypes(value.getEventTypes()) : timeStamp;
            timeStamp = value.getRules() != null ? db.syncSpaceRules(value.getRules()) : timeStamp;
            timeStamp = value.getSeatingArrangements() != null ? db.syncSeatingArrangement(value.getSeatingArrangements()) : timeStamp;
            timeStamp = value.getCancellationPolicies() != null ? db.syncCancellationPolicy(value.getCancellationPolicies()) : timeStamp;
        }
        PreferenceUtils.savePreferenceBoolen(KEY_IS_UPDATED_META, true, this);
        PreferenceUtils.savePreferenceString(KEY_UPDATED_TIMESTAMP, timeStamp, this);
        hideProgress();
    }

    @Override
    public void onErrorResponse(String error) {
        hideProgress();
        AlertMessageUtils.showSnackBarMessage(error,parent);
    }

    @Override
    public void onConnectionErrorCallBack(int requestCode) {
        switch (requestCode){
            case ENF_INITIAL_SYNC:
                initialSync();
                break;
            case ENF_NON_INITIAL_SYNC:
                syncDataBase();
                break;
        }
    }
}
