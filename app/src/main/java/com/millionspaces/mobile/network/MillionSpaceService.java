package com.millionspaces.mobile.network;

import android.content.Context;
import android.util.Log;
import android.webkit.CookieManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.millionspaces.mobile.BuildConfig;
import com.millionspaces.mobile.entities.AmenityUnits;
import com.millionspaces.mobile.entities.ComposeMataData;
import com.millionspaces.mobile.entities.request.AdvanceSearchRequest;
import com.millionspaces.mobile.entities.request.BatchSyncRequest;
import com.millionspaces.mobile.entities.request.BookingDetailsRequest;
import com.millionspaces.mobile.entities.request.BookingRemarksRequest;
import com.millionspaces.mobile.entities.request.CancelBookingRequest;
import com.millionspaces.mobile.entities.request.EmailValidateRequest;
import com.millionspaces.mobile.entities.request.ManualBookingRequest;
import com.millionspaces.mobile.entities.request.PromoCodeRequest;
import com.millionspaces.mobile.entities.request.SpaceBookingRequest;
import com.millionspaces.mobile.entities.request.SpaceReviewRequest;
import com.millionspaces.mobile.entities.request.UserLoginRequest;
import com.millionspaces.mobile.entities.request.UserSignUpRequest;
import com.millionspaces.mobile.entities.response.Amenity;
import com.millionspaces.mobile.entities.response.BatchSyncResponse;
import com.millionspaces.mobile.entities.response.Booking;
import com.millionspaces.mobile.entities.response.BookingCalendarResponse;
import com.millionspaces.mobile.entities.response.BookingRemarksResponse;
import com.millionspaces.mobile.entities.response.BookingReserveResponse;
import com.millionspaces.mobile.entities.response.CancelBookingResponse;
import com.millionspaces.mobile.entities.response.CancellationPolicy;
import com.millionspaces.mobile.entities.response.EventType;
import com.millionspaces.mobile.entities.response.FilterdSpace;
import com.millionspaces.mobile.entities.response.PromoCodeResponse;
import com.millionspaces.mobile.entities.response.Rule;
import com.millionspaces.mobile.entities.response.SeatingArrangement;
import com.millionspaces.mobile.entities.response.ServerResponse;
import com.millionspaces.mobile.entities.response.Space;
import com.millionspaces.mobile.entities.response.UserLoginResponse;
import com.millionspaces.mobile.network.interfaces.OnSuccessAminity;
import com.millionspaces.mobile.network.interfaces.OnSuccessBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessBookingList;
import com.millionspaces.mobile.network.interfaces.OnSuccessBookingReserve;
import com.millionspaces.mobile.network.interfaces.OnSuccessCalendarBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessCancelBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessDeleteManualBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessEventTypes;
import com.millionspaces.mobile.network.interfaces.OnSuccessGetManualBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessLogin;
import com.millionspaces.mobile.network.interfaces.OnSuccessManualBooking;
import com.millionspaces.mobile.network.interfaces.OnSuccessMetaData;
import com.millionspaces.mobile.network.interfaces.OnSuccessPromoCode;
import com.millionspaces.mobile.network.interfaces.OnSuccessRemarks;
import com.millionspaces.mobile.network.interfaces.OnSuccessReview;
import com.millionspaces.mobile.network.interfaces.OnSuccessSignUp;
import com.millionspaces.mobile.network.interfaces.OnSuccessSpace;
import com.millionspaces.mobile.network.interfaces.OnSuccessSpacesList;
import com.millionspaces.mobile.network.interfaces.OnSuccessValidateMail;
import com.millionspaces.mobile.network.interfaces.OnSucessLogOut;
import com.millionspaces.mobile.network.interfaces.OnUpdateUser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function6;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kasunka on 6/10/17.
 * NETWORK call common layer
 */

public class MillionSpaceService  {

    private MillionSpaceAPI millionSpaceAPI;
    private static MillionSpaceService millionSpaceService;

    private Retrofit retrofit;
    private static Context mContext;
    private static int errorCode;


    public static MillionSpaceService getInstance(Context context){
        mContext = context;
        if (millionSpaceService == null) {
            millionSpaceService = new MillionSpaceService();
        }
        return millionSpaceService;
    }

    private MillionSpaceService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        CookieJar cookieJar = new CookieJar() {
            @Override
            public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {

                CookieManager cookieManager = CookieManager.getInstance();
                for (Cookie cookie : cookies) {
                    cookieManager.setCookie(url.toString(), cookie.toString());
                }
            }

            @Override
            public List<Cookie> loadForRequest(HttpUrl url) {
                CookieManager cookieManager = CookieManager.getInstance();
                List<Cookie> cookies = new ArrayList<>();
                if (cookieManager.getCookie(url.toString()) != null) {
                    String[] splitCookies = cookieManager.getCookie(url.toString()).split("[,;]");
                    for (int i=0; i<splitCookies.length; i++) {
                        cookies.add(Cookie.parse(url, splitCookies[i].trim()));
                    }
                }
                return cookies;
            }
        };

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .cookieJar(cookieJar)
                .connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();

        RxJava2CallAdapterFactory rxAdapter = RxJava2CallAdapterFactory.create();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(rxAdapter)
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
        try {
            this.millionSpaceAPI = retrofit.create(MillionSpaceAPI.class);
        } catch (Exception ex) {
        }
    }


    // Get Amenities META DATA
    public void getAmenities(final OnSuccessAminity callback) {
        Single<ArrayList<Amenity>> requestAminity = millionSpaceAPI.getAminities();
        requestAminity.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<ArrayList<Amenity>>() {
                    @Override
                    public void onSuccess(ArrayList<Amenity> value) {
                        callback.successAmenity(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onErrorResponse(e.getMessage());
                    }
                });
    }

    // Get Event Types META DATA
    public void getEventTypes(final OnSuccessEventTypes callback) {
        Single<ArrayList<EventType>> requestEventType = millionSpaceAPI.getEventType();
        requestEventType.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<ArrayList<EventType>>() {
                    @Override
                    public void onSuccess(ArrayList<EventType> value) {
                        callback.onSuccessEventTypes(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onErrorResponse(e.getMessage());
                    }
                });
    }

    // get Cancellation Policy META DATA
    public void getCancellationPolicy(){
        Single<ArrayList<CancellationPolicy>> requestCancellationPolicy = millionSpaceAPI.getCancellationPolicies();
        requestCancellationPolicy.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<ArrayList<CancellationPolicy>>() {
                    @Override
                    public void onSuccess(ArrayList<CancellationPolicy> value) {
                        Log.d("Succ Can polcy",value.get(0).getName());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail can polcy",e.toString());
                        //TODO: Handle network error in Evenet Type
                    }
                });
    }

//    Get All Metadata
    public void getMetaData(final OnSuccessMetaData callBack, final String key){
        Single<ArrayList<Amenity>> requestAminity = millionSpaceAPI.getAminities();
        Single<ArrayList<EventType>> requestEventType = millionSpaceAPI.getEventType();
        Single<ArrayList<CancellationPolicy>> requestCancellationPolicies = millionSpaceAPI.getCancellationPolicies();
        Single<ArrayList<Rule>> requestRules = millionSpaceAPI.getRules();
        Single<ArrayList<SeatingArrangement>> requestSeatingArrangement = millionSpaceAPI.getSeatingArrangement();
        Single<ArrayList<AmenityUnits>> requestAmenityUnits = millionSpaceAPI.getAmenityUnits();
        Single.zip(requestAminity, requestEventType, requestCancellationPolicies, requestRules, requestSeatingArrangement, requestAmenityUnits,mergeArrays())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<ComposeMataData>() {
                    @Override
                    public void onSuccess(ComposeMataData value) {
                        callBack.onSuccessMeta(value,key);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.onErrorResponse(e.getLocalizedMessage());
                        //TODO: Handle network error in Meta
                    }
                });
    }

    //    Metadata Transformation Function
    private Function6<? super ArrayList<Amenity>, ? super ArrayList<EventType>, ? super ArrayList<CancellationPolicy>, ? super ArrayList<Rule>, ? super ArrayList<SeatingArrangement>, ? super ArrayList<AmenityUnits>, ComposeMataData> mergeArrays() {
        return new Function6<ArrayList<Amenity>, ArrayList<EventType>, ArrayList<CancellationPolicy>, ArrayList<Rule>, ArrayList<SeatingArrangement>, ArrayList<AmenityUnits>, ComposeMataData>() {
            @Override
            public ComposeMataData apply(ArrayList<Amenity> list, ArrayList<EventType> eventTypes, ArrayList<CancellationPolicy> cancellationPolicies, ArrayList<Rule> rules, ArrayList<SeatingArrangement> seatingArrangements, ArrayList<AmenityUnits> amenityUnitses) throws Exception {
                return new ComposeMataData(list,eventTypes,cancellationPolicies,rules,seatingArrangements, amenityUnitses);
            }
        };
    }

//    get All Spaces
    public void getSpaces(final OnSuccessSpacesList callback) {
        Single<ArrayList<Space>> requestSpaces = millionSpaceAPI.getSpaces();
        requestSpaces.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<ArrayList<Space>>() {
                    @Override
                    public void onSuccess(ArrayList<Space> value) {
                        Log.d("Succ Spaces",value.size()+"");
                        callback.onSuccessSpacesList(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        //TODO: Handle network error in Space List
                        Log.d("Fail Space", e.getCause().getLocalizedMessage());
                    }
                });
    }

//    get Space by Id and Event Types
    public void getSpace(int id,String stringExtra, final OnSuccessSpace callback) {
        Log.d("Extra",stringExtra);
        Single<Space> requestSpace = millionSpaceAPI.getSpace(id, stringExtra);
        requestSpace.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Space>() {
                    @Override
                    public void onSuccess(Space value) {
                        Log.d("Succ Space",value.getName());
                        callback.onSuccessSpace(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        //TODO: Handle network error in Space List
                        Log.d("Fail Space", e.getLocalizedMessage());
                        callback.onErrorResponse(e.getLocalizedMessage());
                    }
                });
    }

    //    get Space by Id
    public void getSpace(int id, final OnSuccessSpace callback) {
        Single<Space> requestSpace = millionSpaceAPI.getSpace(id);
        requestSpace.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Space>() {
                    @Override
                    public void onSuccess(Space value) {
                        Log.d("Succ Space",value.getName());
                        callback.onSuccessSpace(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        //TODO: Handle network error in Space List
                        Log.d("Fail Space", e.getLocalizedMessage());
                        callback.onErrorResponse(e.getLocalizedMessage());
                    }
                });
    }

//    get All Spaces With filter
    public void getSpaceByFilter(AdvanceSearchRequest request, Context context, int page, final OnSuccessSpacesList callback){
        Single<FilterdSpace> requestSpaceByFiler = millionSpaceAPI.getSpaceByFilter(request,page);
        requestSpaceByFiler.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<FilterdSpace>() {
                    @Override
                    public void onSuccess(FilterdSpace value) {
                        Log.d("Succ Space Filter",value.getSpaces().size()+"");
                        callback.onSuccessSpacesList(value.getSpaces());
                        callback.onSuccessSpacesListCount(value.getCount());
                    }

                    @Override
                    public void onError(Throwable e) {
                        //TODO: Handle network error in Space List
                        callback.onErrorResponse(e.getLocalizedMessage());
                        Log.d("Fail Space filter", e.getLocalizedMessage());
                    }
                });
    }

//    Sync Database Sync upto date
    public void getBatchSync(final OnSuccessMetaData callBack, String timeStamp, final String key) {
        String [] names = {"amenity","eventType","cancellationPolicy","rules","seatingArrangement"};
        Single<BatchSyncResponse> getBatchSync = millionSpaceAPI.getBatchSync(getBatchSyncParams(names,timeStamp));
        getBatchSync.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<BatchSyncResponse>() {
                    @Override
                    public void onSuccess(BatchSyncResponse value) {
                        Log.d("Succ Batch Sync",value.toString()+"");
                        callBack.onSuccessMeta(new ComposeMataData(value.getAmenity(),value.getEventType(),value.getCancellationPolicy(),
                                value.getRules(),value.getSeatingArrangements(),null),key);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Succ Batch Sync",e.toString());
                        callBack.onErrorResponse(e.getLocalizedMessage());
                        //TODO: Handle network error in Meta
                    }
                });
    }

    private ArrayList<BatchSyncRequest> getBatchSyncParams(String[] names, String stamp) {
        ArrayList<BatchSyncRequest> obj = new ArrayList<>();
        for (String s :names){
            obj.add(new BatchSyncRequest(s,stamp));
        }
        return obj;
    }

//    Login Request
    public void login(UserLoginRequest user, final OnSuccessLogin callBack) {
        Single<UserLoginResponse> loginReq = user.getType()==1?millionSpaceAPI.login(user.getUsername(),user.getPassword()):
                user.getType()==2? millionSpaceAPI.loginFacebook(user.getToken()):millionSpaceAPI.loginGoogle(user.getToken());
        loginReq.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<UserLoginResponse>() {
                    @Override
                    public void onSuccess(UserLoginResponse value) {
                        Log.d("Succ Login",value.getEmail());
                        callBack.onSuccessLogin(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail Login",e.toString());
                        callBack.onErrorResponse("Wrong username or Password");
                    }
                });
    }

//    Log out
    public void logout(final OnSucessLogOut callBack) {
        Single<Integer> logout = millionSpaceAPI.logout();
        logout.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Integer>() {
                    @Override
                    public void onSuccess(Integer value) {
                        Log.d("Succ Logout",value+"");
                        callBack.onSuccessLogout();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail Logout",e.toString());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

//    Sing Up
    public void signUp(UserSignUpRequest userSignUpRequest, final OnSuccessSignUp callBack) {
        Single<String> signupRequest = millionSpaceAPI.signup(userSignUpRequest);
        signupRequest.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<String>() {
                    @Override
                    public void onSuccess(String value) {
                        Log.d("Succ signup",value+"");
                        callBack.onSuccessSignup();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail signup",e.toString());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

//    Validate Sign Up mail
    public void validateMail(EmailValidateRequest param, final OnSuccessValidateMail callback) {
        Single<Boolean> validateRequest = millionSpaceAPI.validate(param);
        validateRequest.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Boolean>() {
                    @Override
                    public void onSuccess(Boolean value) {
                        Log.d("Succ validate",value+"");
                        callback.onSuccesValidate(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail validate",e.toString());
                        callback.onErrorResponse(e.getMessage());
                    }
                });
    }

//    Validate Mail
    public void validateSignUpMail(String s, final OnSuccessSignUp callBack) {
        Single<Boolean> validateRequest = millionSpaceAPI.validateEmail(s);
        validateRequest.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Boolean>() {
                    @Override
                    public void onSuccess(Boolean value) {
                        Log.d("Succ validate",value+"");
                        callBack.onValidateSignUpMail(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail validate",e.toString());
                        callBack.onValidateSignUpMailError(e.getMessage());
                    }
                });
    }

    public void reserveBooking(SpaceBookingRequest request, final OnSuccessBookingReserve callBack) {
        Single<BookingReserveResponse> reserveResponse = millionSpaceAPI.reserveBooking(request);
        reserveResponse.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<BookingReserveResponse>() {
                    @Override
                    public void onSuccess(BookingReserveResponse value) {
                        Log.d("Succ reserve",value.getId()+"");
                        callBack.onSuccessReserve(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail reserve",e.toString());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

//    get Current User Details
    public void getLoggedUser(final OnSuccessLogin callBack){
        Single<UserLoginResponse> getLoggedUser = millionSpaceAPI.getUser();
        getLoggedUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<UserLoginResponse>() {
                    @Override
                    public void onSuccess(UserLoginResponse value) {
                        callBack.onSuccessLogin(value);
                        Log.d("Succes Login", value.getName());
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.onErrorResponse(e.getMessage());
                        Log.d("Fail Login",e.toString());
                    }
                });
    }

//    get Hosted Spaces
    public void getHostedSpaces(final OnSuccessSpacesList callBack){
        Single<ArrayList<Space>> getHostedSpacesList = millionSpaceAPI.getHostedSpaces();
        getHostedSpacesList.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<ArrayList<Space>>() {
                    @Override
                    public void onSuccess(ArrayList<Space> value) {
                        Log.d("Succes Host Space", value.size()+"");
                        callBack.onSuccessSpacesList(value);
                        callBack.onSuccessSpacesListCount(value.size());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail Host Space",e.toString());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

//    Update Current User
    public void updateUser(UserLoginResponse user, final OnUpdateUser callBack) {
        Single<String> updateCurrentUser = millionSpaceAPI.updateCurrentUser(user);
        updateCurrentUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<String>() {
                    @Override
                    public void onSuccess(String value) {
                        callBack.onUpdateUser(value);
                        Log.d("Succes Update", value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callBack.onErrorResponse(e.getMessage());
                        Log.d("Fail Update",e.toString());
                    }
                });
    }

    public void updateUserMobile(String number, final OnUpdateUser callback){
        Single<ServerResponse> updateUserMobile = millionSpaceAPI.updateUserMobile(new UserLoginResponse(number));
        updateUserMobile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<ServerResponse>() {
                    @Override
                    public void onSuccess(ServerResponse s) {
                        callback.onUpdateUser(s.getMessage());
                        Log.d("Succes Update", s.getMessage());
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onErrorResponse(e.getMessage());
                        Log.d("Fail Update",e.toString());
                    }
                });
    }

//    get Bookings List By Space Id
    public void getBookingList(int spaceId, final OnSuccessBookingList callBack) {
        Single<ArrayList<Booking>> getBookings = millionSpaceAPI.getBookings(spaceId);
        getBookings.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<ArrayList<Booking>>() {
                    @Override
                    public void onSuccess(ArrayList<Booking> value) {
                        Log.d("Succes Bookings", value.size()+"");
                        callBack.onSuccessBookingList(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail Bookings",e.toString());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

//    Get Bookings List by Guest
    public void getMyBookingList(final OnSuccessBookingList callBack) {
        Single<ArrayList<Booking>> getBookings = millionSpaceAPI.getUserBookings();
        getBookings.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<ArrayList<Booking>>() {
                    @Override
                    public void onSuccess(ArrayList<Booking> value) {
                        Log.d("Succes Bookings", value.size()+"");
                        callBack.onSuccessBookingList(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail Bookings",e.toString());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

    public void getUserBooking(BookingDetailsRequest req, final OnSuccessBooking callBack) {
        Single<Booking> getBookings = millionSpaceAPI.getUserBooking(req);
        getBookings.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Booking>() {
                    @Override
                    public void onSuccess(Booking value) {
                        Log.d("Succes Bookings", value.getOrderId()+"");
                        callBack.onSuccessBooking(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail Bookings",e.toString());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

//    Set Review
    public void setReview(SpaceReviewRequest reviewRequest, final OnSuccessReview callBack) {
        Single<String> submitSpaceReview = millionSpaceAPI.postSpaceReview(reviewRequest);
        submitSpaceReview.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<String>() {
                    @Override
                    public void onSuccess(String value) {
                        Log.d("Success Review", value);
                        callBack.onSuccessReview(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail Review", e.getMessage());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

    public void setRemarks(BookingRemarksRequest remarksRequest, final OnSuccessRemarks callBack) {
        Single<BookingRemarksResponse> submitBookingRemarks = millionSpaceAPI.postBookingRemarks(remarksRequest);
        submitBookingRemarks.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<BookingRemarksResponse>() {
                    @Override
                    public void onSuccess(BookingRemarksResponse value) {
                        Log.d("Success Remarks", value+"");
                        callBack.onSuccessRemarks(value.isResponse());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail Remarks", e.getMessage());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

//    Get Manual booking
    public void getSpaceManualBooking(int bookingID, final OnSuccessGetManualBooking callBack) {
        Single<ManualBookingRequest> getManualBooking = millionSpaceAPI.getManualBooking(bookingID);
        getManualBooking.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<ManualBookingRequest>() {
                    @Override
                    public void onSuccess(ManualBookingRequest value) {
                        Log.d("Success ManBooking", value.getFromDate());
                        callBack.onSuccessGetBooking(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail ManBooking", e.getMessage());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

    //    Set Manual Booking
    public void putMaualBooking(ManualBookingRequest request, final OnSuccessManualBooking callBack) {
        Single<Response<String>> submitManualBooking = millionSpaceAPI.postManualBooking(request);
        submitManualBooking.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Response<String>>() {
                    @Override
                    public void onSuccess(Response<String> value) {
                        if (value.code() == 201 && value.body().length() != 1) {
                            Log.d("Succes Manual Bookings", value.body().toString());
                            callBack.onSuccessManualBooking(value.body());
                        }else {
                            Log.d("Fail Manual Bookings", "201 overlap bookings");
                            callBack.onErrorResponse("This booking cannot be made as it overlaps with an existing booking.");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail Manual Bookings", e.toString());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

    public void updateManualBooking(ManualBookingRequest request, final OnSuccessManualBooking callBack) {
        Single<Integer> updateManualBooking = millionSpaceAPI.updateManualBooking(request);
        updateManualBooking.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Integer>() {
                    @Override
                    public void onSuccess(Integer value) {
                        Log.d("Succes Update MBookings", value+"");
                        callBack.onSuccessManualBooking(value+"");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail Update MBookings", e.getMessage());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

    public void deleteManualBooking(int bookingID, final OnSuccessDeleteManualBooking callBack) {
        Single<Integer> deleteBooking = millionSpaceAPI.deleteManualBooking(bookingID);
        deleteBooking.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Integer>() {
                    @Override
                    public void onSuccess(Integer value) {
                        Log.d("Succes delete MBookings", value+"");
                        callBack.onSuccessDeleteManualBooking(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail delete MBookings", e.getMessage());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

    public void cancelGuestBooking(CancelBookingRequest request, final OnSuccessCancelBooking callBack) {
        Single<CancelBookingResponse> cancelBooking = millionSpaceAPI.cancelGuestBooking(request);
        cancelBooking.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<CancelBookingResponse>() {
                    @Override
                    public void onSuccess(CancelBookingResponse value) {
                        Log.d("Succes cancel MBookings", value.getMessage());
                        callBack.onSuccessCancelBooking(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Succes cancel MBookings", e.getMessage());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }

    public void getSpaceBookings(String spaceId, final OnSuccessCalendarBooking callback) {
        Single<BookingCalendarResponse> getBookingCalendar = millionSpaceAPI.getBookingCalendar(spaceId);
        getBookingCalendar.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<BookingCalendarResponse>() {
                    @Override
                    public void onSuccess(BookingCalendarResponse value) {
                        Log.d("Success Calendar", value.toString());
                        callback.onSuccessCalendarBooking(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail Calendar", e.getMessage());
                        callback.onErrorResponse(e.getMessage());
                    }
                });
    }

    public void getPromoCode(PromoCodeRequest promo, final OnSuccessPromoCode callBack) {
        Single<PromoCodeResponse> getPromo = millionSpaceAPI.getPromoDetails(promo);
        getPromo.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<PromoCodeResponse>() {
                    @Override
                    public void onSuccess(PromoCodeResponse value) {
                        Log.d("Success Pormo", value.getMessage());
                        callBack.onSuccessPromoCode(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Fail Promo", e.getMessage());
                        callBack.onErrorResponse(e.getMessage());
                    }
                });
    }
}
