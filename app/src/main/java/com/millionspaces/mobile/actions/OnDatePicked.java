package com.millionspaces.mobile.actions;

import org.joda.time.DateTime;

/**
 * Created by kasunka on 11/20/17.
 */

public interface OnDatePicked {
    void onDatePicked(DateTime dateTime);
}
