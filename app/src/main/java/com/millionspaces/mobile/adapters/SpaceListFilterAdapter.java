package com.millionspaces.mobile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.millionspaces.mobile.R;
import com.millionspaces.mobile.actions.OnItemCheckedListener;
import com.millionspaces.mobile.entities.SpaceType;
import com.millionspaces.mobile.entities.response.Amenity;
import com.millionspaces.mobile.entities.response.EventType;
import com.millionspaces.mobile.entities.response.Rule;
import com.millionspaces.mobile.entities.response.SeatingArrangement;
import com.millionspaces.mobile.utils.DataBaseUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.millionspaces.mobile.utils.Config.BUDGET_COUNT;
import static com.millionspaces.mobile.utils.Config.PARTICIPATION_COUNT;
import static com.millionspaces.mobile.utils.Config.SPACE_AMINITY_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_BUDGET_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_EVENT_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_PARTICIPATION_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_RULE_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_SEATING_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_SPACE_TYPE_LIST;
import static com.millionspaces.mobile.utils.Config.SPACE_TYPES_LISTS;

/**
 * Created by kasunka on 7/31/17.
 */

public class SpaceListFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private DataBaseUtils dataBaseUtils;
    private Context mContext;
    private int adapterType;
    private ArrayList<Integer> existingValues;

    private OnItemCheckedListener onItemCheckedListener;

    private ArrayList<EventType> eventTypes;
    private ArrayList<Amenity> aminities;
    private ArrayList<Rule> rules;
    private ArrayList<SeatingArrangement> seatingArrangements;

    public SpaceListFilterAdapter(Context context, int type, ArrayList<Integer> existing) {
        this.mContext = context;
        this.adapterType = type;
        this.existingValues = existing;
        dataBaseUtils = new DataBaseUtils(mContext);

        this.eventTypes = dataBaseUtils.getEventTypes();
        this.aminities = dataBaseUtils.getAmenities();
        this.rules = dataBaseUtils.getRules();
        this.seatingArrangements = dataBaseUtils.getSeating();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        switch (viewType){
            case SPACE_SEATING_TYPE_LIST:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_filter_checkbox_card, parent, false);
                return new ViewHolderSelectionCheck(v);
            default:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_filter_checkbox_card, parent, false);
                return new ViewHolderSelectionCheck(v);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        switch (adapterType) {
            case SPACE_EVENT_TYPE_LIST:
                EventType eventType = eventTypes.get(position);
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setText(eventType.getName());
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setChecked(existingValues != null?existingValues.contains(eventType.getId()):false);
                break;
            case SPACE_SPACE_TYPE_LIST:
                SpaceType spaceType = SPACE_TYPES_LISTS.get(position);
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setText(spaceType.getName());
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setChecked(existingValues != null? existingValues.contains(spaceType.getId()):false);
                break;
            case SPACE_PARTICIPATION_TYPE_LIST:
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setText(PARTICIPATION_COUNT.get(position));
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setChecked(existingValues != null?existingValues.contains(position):false);
                break;
            case SPACE_BUDGET_TYPE_LIST:
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setText(BUDGET_COUNT.get(position));
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setChecked(existingValues != null?existingValues.contains(position):false);
                break;
            case SPACE_AMINITY_TYPE_LIST:
                Amenity amenity = aminities.get(position);
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setText(amenity.getName());
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setChecked(existingValues != null?existingValues.contains(amenity.getId()):false);
                break;
            case SPACE_RULE_TYPE_LIST:
                Rule rule =  rules.get(position);
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setText(rule.getDisplayName());
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setChecked(existingValues != null?existingValues.contains(rule.getId()):false);
                break;
            case SPACE_SEATING_TYPE_LIST:
                SeatingArrangement seatingArrangement = seatingArrangements.get(position);
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setText(seatingArrangement.getName());
                ((ViewHolderSelectionCheck)holder).chekBoxSelection.setChecked(existingValues != null?existingValues.contains(seatingArrangement.getId()):false);
                ((ViewHolderSelectionCheck)holder).icon.setVisibility(View.VISIBLE);
                Picasso.with(mContext).load(seatingArrangement.getIcon()).into(((ViewHolderSelectionCheck)holder).icon);
                break;
        }
    }

    @Override
    public int getItemCount() {
        int count = 0;
        switch (adapterType){
            case SPACE_EVENT_TYPE_LIST:
                count = eventTypes.size();
                break;
            case SPACE_SPACE_TYPE_LIST:
                count = SPACE_TYPES_LISTS.size();
                break;
            case SPACE_PARTICIPATION_TYPE_LIST:
                count = PARTICIPATION_COUNT.size();
                break;
            case SPACE_BUDGET_TYPE_LIST:
                count = BUDGET_COUNT.size();
                break;
            case SPACE_AMINITY_TYPE_LIST:
                count = aminities.size();
                break;
            case SPACE_RULE_TYPE_LIST:
                count = rules.size();
                break;
            case SPACE_SEATING_TYPE_LIST:
                count = seatingArrangements.size();
                break;
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        switch (adapterType){
            case SPACE_EVENT_TYPE_LIST:
                type = SPACE_EVENT_TYPE_LIST;
                break;
            case SPACE_SPACE_TYPE_LIST:
                type = SPACE_SPACE_TYPE_LIST;
                break;
            case SPACE_PARTICIPATION_TYPE_LIST:
                type = SPACE_PARTICIPATION_TYPE_LIST;
                break;
            case SPACE_BUDGET_TYPE_LIST:
                type = SPACE_BUDGET_TYPE_LIST;
                break;
            case SPACE_AMINITY_TYPE_LIST:
                type = SPACE_AMINITY_TYPE_LIST;
                break;
            case SPACE_RULE_TYPE_LIST:
                type = SPACE_RULE_TYPE_LIST;
                break;
            case SPACE_SEATING_TYPE_LIST:
                type = SPACE_SEATING_TYPE_LIST;
                break;
        }
        return type;
    }

    public class ViewHolderSelectionCheck extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.filtercheckBox)
        CheckBox chekBoxSelection;
        @BindView(R.id.seating_icon)
        ImageView icon;

        public ViewHolderSelectionCheck(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            chekBoxSelection.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (getItemViewType()) {
                case SPACE_EVENT_TYPE_LIST:
                    if (chekBoxSelection.isChecked()) {
                        onItemCheckedListener.onItemChecked(true, eventTypes.get(getAdapterPosition()).getId(),SPACE_EVENT_TYPE_LIST);
                    } else {
                        onItemCheckedListener.onItemChecked(false, eventTypes.get(getAdapterPosition()).getId(),SPACE_EVENT_TYPE_LIST);
                    }
                    break;
                case SPACE_SPACE_TYPE_LIST:
                    if (chekBoxSelection.isChecked()){
                        onItemCheckedListener.onItemChecked(true,SPACE_TYPES_LISTS.get(getAdapterPosition()).getId(),SPACE_SPACE_TYPE_LIST);
                    }else {
                        onItemCheckedListener.onItemChecked(false,SPACE_TYPES_LISTS.get(getAdapterPosition()).getId(),SPACE_SPACE_TYPE_LIST);
                    }
                    break;
                case SPACE_PARTICIPATION_TYPE_LIST:
                    if (chekBoxSelection.isChecked()) {
                        onItemCheckedListener.onItemChecked(true, getAdapterPosition(),SPACE_PARTICIPATION_TYPE_LIST);
                    } else {
                        onItemCheckedListener.onItemChecked(false, getAdapterPosition(),SPACE_PARTICIPATION_TYPE_LIST);
                    }
                    break;
                case SPACE_BUDGET_TYPE_LIST:
                    if (chekBoxSelection.isChecked()) {
                        onItemCheckedListener.onItemChecked(true, getAdapterPosition(),SPACE_BUDGET_TYPE_LIST);
                    } else {
                        onItemCheckedListener.onItemChecked(false, getAdapterPosition(),SPACE_BUDGET_TYPE_LIST);
                    }
                    break;
                case SPACE_AMINITY_TYPE_LIST:
                    if (chekBoxSelection.isChecked()) {
                        onItemCheckedListener.onItemChecked(true, aminities.get(getAdapterPosition()).getId(),SPACE_AMINITY_TYPE_LIST);
                    } else {
                        onItemCheckedListener.onItemChecked(false, aminities.get(getAdapterPosition()).getId(),SPACE_AMINITY_TYPE_LIST);
                    }
                    break;
                case SPACE_RULE_TYPE_LIST:
                    if (chekBoxSelection.isChecked()) {
                        onItemCheckedListener.onItemChecked(true, rules.get(getAdapterPosition()).getId(),SPACE_RULE_TYPE_LIST);
                    } else {
                        onItemCheckedListener.onItemChecked(false, rules.get(getAdapterPosition()).getId(),SPACE_RULE_TYPE_LIST);
                    }
                    break;
                case SPACE_SEATING_TYPE_LIST:
                    if (chekBoxSelection.isChecked()) {
                        onItemCheckedListener.onItemChecked(true, seatingArrangements.get(getAdapterPosition()).getId(),SPACE_SEATING_TYPE_LIST);
                    } else {
                        onItemCheckedListener.onItemChecked(false, seatingArrangements.get(getAdapterPosition()).getId(),SPACE_SEATING_TYPE_LIST);
                    }
                    break;
            }
        }
    }

    public void setOnItemCheck(OnItemCheckedListener onItemClickListner){
        this.onItemCheckedListener = onItemClickListner;
    }
}
