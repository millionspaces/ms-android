package com.millionspaces.mobile.network.interfaces;

/**
 * Created by kasunka on 8/21/17.
 */

public interface OnSucessLogOut extends OnErrorResponse{
    void onSuccessLogout();
}
