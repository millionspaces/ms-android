package com.millionspaces.mobile.entities.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kasunka on 7/21/17.
 */

public class BookingDate implements Parcelable{
    private String guestContactNumber;
    private String note;
    private String cost;
    private int isManual;
    private String noOfGuests;
    private String from;
    private int id;
    private String to;
    private String title;
    private String guestEmail;
    private String extrasRequested;
    private String dateBookingMade;
    private String guestName;


    protected BookingDate(Parcel in) {
        guestContactNumber = in.readString();
        note = in.readString();
        cost = in.readString();
        isManual = in.readInt();
        noOfGuests = in.readString();
        from = in.readString();
        id = in.readInt();
        to = in.readString();
        title = in.readString();
        guestEmail = in.readString();
        extrasRequested = in.readString();
        dateBookingMade = in.readString();
        guestName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(guestContactNumber);
        dest.writeString(note);
        dest.writeString(cost);
        dest.writeInt(isManual);
        dest.writeString(noOfGuests);
        dest.writeString(from);
        dest.writeInt(id);
        dest.writeString(to);
        dest.writeString(title);
        dest.writeString(guestEmail);
        dest.writeString(extrasRequested);
        dest.writeString(dateBookingMade);
        dest.writeString(guestName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingDate> CREATOR = new Creator<BookingDate>() {
        @Override
        public BookingDate createFromParcel(Parcel in) {
            return new BookingDate(in);
        }

        @Override
        public BookingDate[] newArray(int size) {
            return new BookingDate[size];
        }
    };

    public String getGuestContactNumber() {
        return guestContactNumber;
    }

    public void setGuestContactNumber(String guestContactNumber) {
        this.guestContactNumber = guestContactNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCost() {
        return cost;
    }

    public int isManual() {
        return isManual;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getNoOfGuests() {
        return noOfGuests;
    }

    public void setNoOfGuests(String noOfGuests) {
        this.noOfGuests = noOfGuests;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(String guestEmail) {
        this.guestEmail = guestEmail;
    }

    public String getExtrasRequested() {
        return extrasRequested;
    }

    public void setExtrasRequested(String extrasRequested) {
        this.extrasRequested = extrasRequested;
    }

    public String getDateBookingMade() {
        return dateBookingMade;
    }

    public void setDateBookingMade(String dateBookingMade) {
        this.dateBookingMade = dateBookingMade;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }
}
