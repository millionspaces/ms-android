package com.millionspaces.mobile.actions;

/**
 * Created by kasunka on 7/20/17.
 */

public interface OnItemSelected {
    void onItemSelected(int position, int id, String value, int functionCode);
}
